package fr.com.panda.test.test.manager

import android.os.Build
import fr.com.panda.event.DisposableBag
import fr.com.panda.test.robot.manager.gpsManagerRobot
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.junit.MockitoJUnitRunner

/**
 * @author Edouard Roussillon
 */

@RunWith(MockitoJUnitRunner::class)
class GPSManagerUnitTest {

    val bag: DisposableBag = DisposableBag()

    @After
    fun resetTest() {
        bag.dispose()
    }

    @Test
    fun testUserAlreadyDeniedGSP() {
        gpsManagerRobot(bag) {
            eventDefault()
            eventSimulatorUtilMock()
            eventBuildVersion(Build.VERSION_CODES.M)
            eventCheckSelfPermission(fail = true)
            eventShouldShowRequestPermissionRational()
            getLastLocalisation()
            doWaitDenied()
        } checkResult {
            isLocationNull()
            isDeniedEqual(true)
            isNotDeterminedEqual(false)
            isLocalisationFailEqual(false)
        }
    }

    @Test
    fun testUserWillDeniedGPS() {
        gpsManagerRobot(bag) {
            eventDefault()
            eventSimulatorUtilMock()
            eventBuildVersion(Build.VERSION_CODES.M)
            eventCheckSelfPermission(fail = true)
            eventShouldShowRequestPermissionRational(success = false)
            eventRequestPermissions(success = false)
            getLastLocalisation()
            doWaitDenied()
        } checkResult {
            isLocationNull()
            isDeniedEqual(true)
            isNotDeterminedEqual(false)
            isLocalisationFailEqual(false)
        }
    }

    @Test
    fun testUserWillAcceptGPS() {
        gpsManagerRobot(bag) {
            eventDefault()
            eventSimulatorUtilMock()
            eventBuildVersion(Build.VERSION_CODES.M)
            eventCheckSelfPermission(fail = true)
            eventShouldShowRequestPermissionRational(success = false)
            eventGetLatitude(-23.515599)
            eventGetLongitude(-46.189608)
            eventRequestPermissions()
            evenGetProviders()
            eventGetLastKnownLocation()
            getLastLocalisation()
            doWaitLocation()
        } checkResult {
            isLocationEqual(latitude = "-23.515599", longitude = "-46.189608")
            isDeniedEqual(false)
            isNotDeterminedEqual(false)
            isLocalisationFailEqual(false)
        }
    }

    @Test
    fun testUserWillAcceptGPSButLocalisationIsNull() {
        gpsManagerRobot(bag) {
            eventDefault()
            eventSimulatorUtilMock()
            eventBuildVersion(Build.VERSION_CODES.M)
            eventCheckSelfPermission(fail = true)
            eventShouldShowRequestPermissionRational(success = false)
            eventGetLatitude(-23.515599)
            eventGetLongitude(-46.189608)
            eventRequestPermissions()
            evenGetProviders(emptyList = true)
            getLastLocalisation()
            doWaitIsLocalisationFail()
        } checkResult {
            isLocationNull()
            isDeniedEqual(false)
            isNotDeterminedEqual(false)
            isLocalisationFailEqual(true)
        }
    }

    @Test
    fun testUserWillAcceptGPSButGetSecurityException() {
        gpsManagerRobot(bag) {
            eventDefault()
            eventSimulatorUtilMock()
            eventBuildVersion(Build.VERSION_CODES.M)
            eventCheckSelfPermission(fail = true)
            eventShouldShowRequestPermissionRational(success = false)
            eventGetLatitude(-23.515599)
            eventGetLongitude(-46.189608)
            eventRequestPermissions()
            evenGetProviders(fail = true)
            getLastLocalisation()
            doWaitIsLocalisationFail()
        } checkResult {
            isLocationNull()
            isDeniedEqual(false)
            isNotDeterminedEqual(false)
            isLocalisationFailEqual(true)
        }
    }

}