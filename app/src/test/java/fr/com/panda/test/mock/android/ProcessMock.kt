package com.rockspoon.test.mock.android

import android.os.Process
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class ProcessMock : MockUp<Process>() {
	companion object {
		@Mock
		@JvmStatic
		fun setThreadPriority(priority: Int) {}
	}

	@Mock
	fun `$init`() {}
}