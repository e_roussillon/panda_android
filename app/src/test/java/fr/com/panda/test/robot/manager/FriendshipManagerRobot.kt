package fr.com.panda.test.robot.manager

import br.com.concretesolutions.requestmatcher.RequestMatcherRule
import br.com.concretesolutions.requestmatcher.model.HttpMethod
import fr.com.panda.enums.FriendshipType
import fr.com.panda.enums.GenderType
import fr.com.panda.event.DisposableBag
import fr.com.panda.manager.FriendshipManager
import fr.com.panda.model.ErrorResponse
import fr.com.panda.model.UserModel
import fr.com.panda.test.robot.db.ReturnUserModelsRobot
import fr.com.panda.test.robot.db.UserData
import fr.com.panda.test.utils.ResultAPIError
import fr.com.panda.test.utils.doWait
import fr.com.panda.test.utils.isEqual
import fr.com.panda.utils.DateUtil
import java.util.*

fun friendshipManager(server: RequestMatcherRule, func: FriendshipManagerRobot.() -> Unit) = FriendshipManagerRobot(server).apply { func() }

class FriendshipManagerRobot(val server: RequestMatcherRule) {

    private val bag: DisposableBag = DisposableBag()
    private var user: UserModel? = null
    private var error: ErrorResponse? = null
    private var isUpdateFriendshipFailedUserIdNotFound: Boolean = false

    fun addUserWithFriendship(@FriendshipType.FriendshipMode friendshipStatus: Int) {
        val user = UserModel(1,
                     "test@gmail.com",
                     "test",
                     "",
                     DateUtil.getStringToSmallDate("10-10-1990"),
                     GenderType.NONE,
                     null,
                     null,
                     null,
                     false,
                     null,
                     friendshipStatus,
                     1,
                     false,
                     Date())
        user.update()
    }

    infix fun getUsersByFriendshipTypeFriend(func: ResultFriendshipManagerRobot.() -> Unit) : ResultFriendshipManagerRobot {
        val users: List<UserModel> = FriendshipManager.sharedInstance.usersByFriendshipTypeFriend

        return ResultFriendshipManagerRobot(users, this.user, this.error, this.isUpdateFriendshipFailedUserIdNotFound).apply { func() }
    }

    infix fun getUsersByFriendshipTypeBlocked(func: ResultFriendshipManagerRobot.() -> Unit) : ResultFriendshipManagerRobot {
        val users: List<UserModel> = FriendshipManager.sharedInstance.usersByFriendshipTypeBlocked

        return ResultFriendshipManagerRobot(users, this.user, this.error, this.isUpdateFriendshipFailedUserIdNotFound).apply { func() }
    }

    infix fun getUsersByFriendshipTypePending(func: ResultFriendshipManagerRobot.() -> Unit) : ResultFriendshipManagerRobot {
        val users: List<UserModel> = FriendshipManager.sharedInstance.usersByFriendshipTypePending

        return ResultFriendshipManagerRobot(users, this.user, this.error, this.isUpdateFriendshipFailedUserIdNotFound).apply { func() }
    }

    fun registeToEventFriendshipSuccess() {
        bag.add(FriendshipManager.sharedInstance.onUpdateFriendshipSucceed.add({ user ->
            this.user = user
        }))
    }

    fun registeToEventFriendshipFailed() {
        bag.add(FriendshipManager.sharedInstance.onUpdateFriendshipFailed.add({ error ->
            this.error = error
        }))
    }

    fun registeToEventFriendshipFailedUserIdNotFound() {
        bag.add(FriendshipManager.sharedInstance.onUpdateFriendshipFailedUserIdNotFound.add({ v ->
            this.isUpdateFriendshipFailedUserIdNotFound = true
        }))
    }

    fun initRequestUpdateFriendshipWithCode200(json: String) {
        this.server.addFixture(200, json)
                .ifRequestMatches()
                .pathIs("/api/v1/update_friendship")
                .methodIs(HttpMethod.POST)
    }

    fun initRequestUpdateFriendshipWithCode401(json: String) {
        this.server.addFixture(401, json)
                .ifRequestMatches()
                .pathIs("/api/v1/update_friendship")
                .methodIs(HttpMethod.POST)
    }

    fun updateFriendship(userId: Int, @FriendshipType.FriendshipMode status: Int, func: ResultFriendshipManagerRobot.() -> Unit) : ResultFriendshipManagerRobot {
        FriendshipManager.sharedInstance.updateFriendship(userId, status)
        doWait(2000)
        return ResultFriendshipManagerRobot(null, this.user, this.error, this.isUpdateFriendshipFailedUserIdNotFound).apply { func() }
    }
}

class ResultFriendshipManagerRobot(val users: List<UserModel>?,
                                   val userModel: UserModel?,
                                   error: ErrorResponse?,
                                   var isUpdateFriendshipFailedUserIdNotFound: Boolean) : ResultAPIError(error) {

    fun isUpdateFriendshipFailedUserIdNotFound() {
        isEqual(this.isUpdateFriendshipFailedUserIdNotFound, true)
    }

    fun isEqualTo(listData: List<UserData>) {
        ReturnUserModelsRobot(users!!).isEqualTo(listData)
    }

    fun isEqualTo(userId: Int, @FriendshipType.FriendshipMode status: Int) {
        isEqual(userModel!!.userId, userId)
        isEqual(userModel!!.friendshipStatus, status)
    }
}