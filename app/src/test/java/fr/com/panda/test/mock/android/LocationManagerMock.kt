package fr.com.panda.test.mock.android

import android.location.Location
import android.location.LocationManager
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class LocationManagerMock : MockUp<LocationManager>() {

    companion object {
        var locationManagerIsProviderEnabled: ((String) -> Boolean)? = null
        var locationManagerGetProviders: ((Boolean) -> List<String>)? = null
        var locationManagerGetLastKnownLocation: ((String) -> Location)? = null
    }



    @Mock
    fun isProviderEnabled(provider: String): Boolean {
        return locationManagerIsProviderEnabled?.invoke(provider) ?: false
    }

    @Mock
    fun getProviders(enabledOnly: Boolean): List<String> {
        return locationManagerGetProviders?.invoke(enabledOnly) ?: emptyList()
    }

    @Mock
    fun getLastKnownLocation(provider: String): Location {
        return locationManagerGetLastKnownLocation?.invoke(provider) ?: Location(provider)
    }

}