package fr.com.panda.test.robot.manager

import android.content.pm.PackageManager
import com.rockspoon.test.mock.android.AndroidMock
import fr.com.panda.event.DisposableBag
import fr.com.panda.manager.GPSManager
import fr.com.panda.model.Location
import fr.com.panda.test.mock.BuildVersionMock
import fr.com.panda.test.mock.SimulatorUtilMock
import fr.com.panda.test.mock.android.ActivityCompatMock
import fr.com.panda.test.mock.android.ContextCompatMock
import fr.com.panda.test.mock.android.LocationManagerMock
import fr.com.panda.test.mock.android.LocationMock
import fr.com.panda.test.utils.doWaitFor
import fr.com.panda.test.utils.isEqual
import fr.com.panda.test.utils.isNotNull
import fr.com.panda.test.utils.isNull

/**
 * @author Edouard Roussillon
 */

fun gpsManagerRobot(bag: DisposableBag, func: GPSManagerRobot.() -> Unit) = GPSManagerRobot(bag).apply { func() }

class GPSManagerRobot(val bag: DisposableBag) {
    private val androidMock = AndroidMock()
    private val simulatorUtilMock = SimulatorUtilMock()
    private val gpsManager = GPSManager()
    private var location: Location? = null
    private var isDenied: Boolean = false
    private var isNotDetermined: Boolean = false
    private var isLocalisationFail: Boolean = false

    fun eventDefault() {
        eventOnGetLocalisation()
        eventOnGetLocalisationDenied()
        eventOnGetLocalisationNotDetermined()
        eventOnGetLocalisationFail()
    }

    fun eventOnGetLocalisation() {
        bag.add(this.gpsManager.onGetLocalisation.add { location ->
            this.location = location
        })
    }

    fun eventOnGetLocalisationDenied() {
        bag.add(this.gpsManager.onGetLocalisationDenied.add { void ->
            isDenied = true
        })
    }

    fun eventOnGetLocalisationNotDetermined() {
        bag.add(this.gpsManager.onGetLocalisationNotDetermined.add { void ->
            isNotDetermined = true
        })
    }

    fun eventOnGetLocalisationFail() {
        bag.add(this.gpsManager.onGetLocalisationFail.add { void ->
            isLocalisationFail = true
        })
    }

    fun eventSimulatorUtilMock(isSimulator: Boolean = false) {
        SimulatorUtilMock.simulatorUtiIsRunningOnEmulator = {
            isSimulator
        }
    }

    fun eventBuildVersion(number: Int) {
        BuildVersionMock.buildVersionGetSDK = {
            number
        }
    }

    fun eventShouldShowRequestPermissionRational(success: Boolean = true) {
        ActivityCompatMock.activityCompatShouldShowRequestPermissionRationale = { activity, permission ->
            success
        }
    }

    fun eventRequestPermissions(success: Boolean = true) {
        ActivityCompatMock.activityCompatRequestPermissions = { activity, permissions, requestCode ->
            if (success) {
                gpsManager.onRequestPermissionsResult(requestCode, emptyArray(), intArrayOf(PackageManager.PERMISSION_GRANTED))
            } else {
                gpsManager.onRequestPermissionsResult(requestCode, emptyArray(), intArrayOf(PackageManager.PERMISSION_DENIED))
            }

            true
        }
    }

    fun eventCheckSelfPermission(fail: Boolean = false) {
        ContextCompatMock.contextCompatCheckSelfPermission = { context, string ->
            if (fail) {
                PackageManager.PERMISSION_DENIED
            } else {
                PackageManager.PERMISSION_GRANTED
            }
        }
    }

    fun evenGetProviders(emptyList: Boolean = false, fail: Boolean = false) {
        LocationManagerMock.locationManagerGetProviders = { boolean ->
            if (fail) {
                throw SecurityException()
            } else if (!emptyList) {
                listOf("hello")
            } else {
                emptyList()
            }
        }
    }

    fun eventGetLastKnownLocation() {
        LocationManagerMock.locationManagerGetLastKnownLocation = { provider ->
            android.location.Location(provider)
        }
    }

    fun eventGetLatitude(latitude: Double) {
        LocationMock.locationGetLatitude = {
            latitude
        }
    }

    fun eventGetLongitude(longitude: Double) {
        LocationMock.locationGetLongitude = {
            longitude
        }
    }

    fun resetLocalisation() {
        gpsManager.resetLocalisation()
    }

    fun getCurrentLocalisation() {
        this.location = gpsManager.getCurrentLocalisation()
    }

    fun getLastLocalisation() {
        this.gpsManager.getLastLocalisation(androidMock.activity())
    }

    fun doWaitDenied() {
        doWaitFor {
            this.isDenied == true
        }
    }

    fun doWaitIsLocalisationFail() {
        doWaitFor {
            this.isLocalisationFail == true
        }
    }

    fun doWaitLocation() {
        doWaitFor {
            this.location != null
        }
    }

    infix fun checkResult(func: GPSManagerResult.() -> Unit) : GPSManagerResult {
        return GPSManagerResult(location, isDenied, isNotDetermined, isLocalisationFail).apply { func() }
    }

}

class GPSManagerResult(val location: Location? = null, val isDenied: Boolean = false, val isNotDetermined: Boolean = false, val isLocalisationFail: Boolean = false) {

    fun isLocationEqual(latitude: String, longitude: String) {
        isNotNull(this.location)
        isEqual(latitude, this.location?.latitude)
        isEqual(longitude, this.location?.longitude)
    }

    fun isLocationNull() {
        isNull(this.location)
    }

    fun isDeniedEqual(isDenied: Boolean) {
        isEqual(isDenied, this.isDenied)
    }

    fun isNotDeterminedEqual(isNotDetermined: Boolean) {
        isEqual(isNotDetermined, this.isNotDetermined)
    }

    fun isLocalisationFailEqual(isLocalisationFail: Boolean) {
        isEqual(isLocalisationFail, this.isLocalisationFail)
    }

}