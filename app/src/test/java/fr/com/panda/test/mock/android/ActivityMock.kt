package com.rockspoon.test.mock.android

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Intent
import android.content.IntentFilter
import fr.com.panda.test.mock.android.LocationManagerMock
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class ActivityMock : MockUp<Activity>() {

    val audioManagerWithMock = AudioManagerMock()
    val locationManagerMock = LocationManagerMock()

    companion object {
        var activityOnRequestPermissionsResult: ((Int, Array<String>, IntArray) -> Unit)? = null
    }


    @Mock
    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        activityOnRequestPermissionsResult?.invoke(requestCode, permissions, grantResults)
    }


    @Mock
    fun getSystemService(name: String): Any {
        if (name == "audio") {
            return audioManagerWithMock.mockInstance
        } else if (name == "location") {
            return locationManagerMock.mockInstance
        }
        return ""
    }

    @Mock
    fun startService(intent: Intent) : ComponentName? {
        return ContextMock.contextStartService?.invoke(intent)
    }

    @Mock
    fun stopService(intent: Intent) : Boolean {
        return ContextMock.contextStopService?.invoke(intent) ?: false
    }

    @Mock
    fun registerReceiver(receiver: BroadcastReceiver, filter: IntentFilter): Intent? {
        return ContextMock.contextRegisterReceiver?.invoke(receiver, filter)
    }

    @Mock
    fun sendBroadcast(intent: Intent) {
        ContextMock.contextSendBroadcast?.invoke(intent)
    }

}