package fr.com.panda.test.robot.db

import fr.com.panda.enums.DataType
import fr.com.panda.enums.FriendshipType
import fr.com.panda.model.PrivateChatModel
import fr.com.panda.test.utils.isEqual
import java.net.URL
import java.util.*

/**
 * Created by edouardroussillon on 1/14/17.
 */

fun privateChatModel(func: PrivateChatModelRobot.() -> Unit) = PrivateChatModelRobot().apply { func() }

data class PrivateChatData(var roomId: Int,
                           var isGroup: Boolean,
                           var nameRoom: String,
                           var urlThumb: URL,
                           var urlOriginal: URL,
                           var urlBlur: URL,
                           @DataType.DataMode var type: Int,
                           @FriendshipType.FriendshipMode var userStatus: Int,
                           var userStatusLastAction: Int,
                           var msg: String,
                           var msgSent: Date,
                           var numOfMsgNotRead: Int,
                           var userId: Int,
                           var userIsOnline: Boolean,
                           var userLastSeen: Date)

class PrivateChatModelRobot {


    infix fun getRooms(func: ReturnPrivateChatModelsRobot.() -> Unit): ReturnPrivateChatModelsRobot {
        return ReturnPrivateChatModelsRobot(PrivateChatModel.getRooms()).apply { func() }
    }

    fun getRoom(userId: Int, func: ReturnIntIdPrivateChatModelRobot.() -> Unit): ReturnIntIdPrivateChatModelRobot {
        return ReturnIntIdPrivateChatModelRobot(PrivateChatModel.getRoom(userId)).apply { func() }
    }

    infix fun getMessagesCount(func: ReturnIntIdPrivateChatModelRobot.() -> Unit): ReturnIntIdPrivateChatModelRobot {
        return ReturnIntIdPrivateChatModelRobot(PrivateChatModel.getMessagesCount()).apply { func() }
    }

    fun insertRoom(roomId: Int, name: String) {
        PrivateChatModel.insertRoom(roomId, name)
    }

    fun insertUsersRoom(roomId: Int, toUserId: Int) {
        PrivateChatModel.insertUsersRoom(roomId, toUserId)
    }


}

class ReturnPrivateChatModelRobot(val privateChatModel: PrivateChatModel) {

    fun isEqualTo(item: PrivateChatData) {
        isEqual(item!!.roomId, privateChatModel.roomId)
        isEqual(item!!.isGroup, privateChatModel.isGroup)
        isEqual(item!!.nameRoom, privateChatModel.nameRoom)
        isEqual(item!!.urlThumb, privateChatModel.urlThumb)
        isEqual(item!!.urlOriginal, privateChatModel.urlOriginal)
        isEqual(item!!.urlBlur, privateChatModel.urlBlur)
        isEqual(item!!.type, privateChatModel.type)
        isEqual(item!!.userStatus, privateChatModel.userStatus)
        isEqual(item!!.userStatusLastAction, privateChatModel.userStatusLastAction)
        isEqual(item!!.msg, privateChatModel.msg)
        isEqual(item!!.msgSent.toString(), privateChatModel.msgSent.toString())
        isEqual(item!!.numOfMsgNotRead, privateChatModel.numOfMsgNotRead)
        isEqual(item!!.userId, privateChatModel.userId)
        isEqual(item!!.userIsOnline, privateChatModel.userIsOnline)
        isEqual(item!!.userLastSeen.toString(), privateChatModel.userLastSeen.toString())
    }

}

class ReturnPrivateChatModelsRobot(val list: List<PrivateChatModel>) {

    fun isEqualTo(listData: List<PrivateChatData>) {
        isEqual(listData.size, list.size)
        var isChecked = false

        for (itemData: PrivateChatData in listData) {
            isChecked = false
            for ((index, item) in list.withIndex()) {
                if (item!!.userLastSeen.toString() == itemData.userLastSeen.toString()) {
                    isChecked = true
                    ReturnPrivateChatModelRobot(item).isEqualTo(itemData)
                    break
                } else if (!isChecked && (list.size - 1) == index) {
                    //List do not have any item like other item
                    isEqual(true, false)
                }
            }
        }
    }
}

class ReturnIntIdPrivateChatModelRobot(val valInt: Int) {

    fun isEqualTo(valInt: Int) {
        isEqual(this.valInt, valInt)
    }

}