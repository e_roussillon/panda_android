package fr.com.panda.test.mock.android

import com.orhanobut.hawk.Hawk
import mockit.Mock
import mockit.MockUp
import java.util.*

/**
 * @author Edouard Roussillon
 */

class HawkMock : MockUp<Hawk>() {

    companion object {
        val list: HashMap<String, Any> = hashMapOf()

        @Mock
        @JvmStatic
        fun <T> get(key: String): T {
            return list[key] as T
        }

        @Mock
        @JvmStatic
        fun <T> put(key: String, value: T): Boolean {
            list.put(key, value as Any)
            return true
        }
    }

}