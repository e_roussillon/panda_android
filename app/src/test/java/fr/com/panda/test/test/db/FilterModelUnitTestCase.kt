package fr.com.panda.test.test.db

import fr.com.panda.test.base.PandaUnitTestCase
import fr.com.panda.test.robot.db.FilterData
import fr.com.panda.test.robot.db.filterModel
import org.junit.Test
import java.util.*


/**
 * Created by edouardroussillon on 12/31/16.
 */

class FilterModelUnitTestCase : PandaUnitTestCase() {

    @Test
    fun testInsert_getResult() {
        val filterId = 1
        val name = "hello"
        val isHighlight = true
        val timestamp = Date()

        val filterData = FilterData(filterId, name, isHighlight, timestamp)

        filterModel {
            insert(filterData)
        }.getFilters(isHighlight) {
            isSingleResult(filterId, name, isHighlight, timestamp)
        }
    }

    @Test
    fun testInsert_getResultById() {
        val filterId = 1
        val name = "hello"
        val isHighlight = true
        val timestamp = Date()

        val filterData = FilterData(filterId, name, isHighlight, timestamp)

        filterModel {
            insert(filterData)
        }.getFilter(filterId) {
            isEqualTo(filterId, name, isHighlight, timestamp)
        }
    }

    @Test
    fun testInsert_getNoResult() {
        val filterId = 1
        val name = "hello"
        val isHighlight = true
        val timestamp = Date()

        val filterData = FilterData(filterId, name, isHighlight, timestamp)

        filterModel {
            insert(filterData)
        }.getFilters(!isHighlight, {
            isEmptyList()
        })
    }

    @Test
    fun testInsert_deleteInsert() {
        val filterId = 1
        val name = "hello"
        val isHighlight = true
        val timestamp = Date()

        val filterData = FilterData(filterId, name, isHighlight, timestamp)

        filterModel {
            insert(filterData)
            remove(filterId)
        }.getFilter(filterId) {
            isEmpty()
        }
    }

    @Test
    fun testInsertMultiple_singleResultHighlight() {
        val filterId = 1
        val filterId2 = 2
        val name = "hello"
        val isHighlight = true
        val timestamp = Date()

        val filterData = FilterData(filterId, name, isHighlight, timestamp)
        val filterData2 = FilterData(filterId2, name, !isHighlight, timestamp)

        filterModel {
            insert(filterData)
            insert(filterData2)
        }.getFilters(isHighlight) {
            isSingleResult(filterId, name, isHighlight, timestamp)
        }
    }

    @Test
    fun testInsertMultiple_singleResultHighlightNegative() {
        val filterId = 1
        val filterId2 = 2
        val name = "hello"
        val isHighlight = true
        val timestamp = Date()

        val filterData = FilterData(filterId, name, isHighlight, timestamp)
        val filterData2 = FilterData(filterId2, name, !isHighlight, timestamp)

        filterModel {
            insert(filterData)
            insert(filterData2)
        }.getFilters(!isHighlight) {
            isSingleResult(filterId2, name, !isHighlight, timestamp)
        }
    }

}