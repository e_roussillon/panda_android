package fr.com.panda.test.mock

import fr.com.panda.model.ResponsePhoenixModel
import fr.com.panda.socket.phoenix.PhoenixPayload
import fr.com.panda.test.execption.PandaTestWeSocketException
import fr.com.panda.test.utils.doWait
import fr.com.panda.utils.JsonUtil
import mockit.Mock
import mockit.MockUp
import okhttp3.*
import okhttp3.internal.ws.RealWebSocket
import okio.ByteString
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.util.*

/**
 * @author Edouard Roussillon
 */

data class Item(val reserved: PhoenixPayload, val toSend: PhoenixPayload)

class RealWebSocketWithMock : MockUp<RealWebSocket>(), WebSocket {

    private lateinit var originalRequest: Request
    private lateinit var listener: WebSocketListener
    private lateinit var random: Random
    private val fixturesRootFolder: String = "ws"

    private val list: MutableSet<Item> = mutableSetOf()
    private val listPublicMessage: MutableSet<Item> = mutableSetOf()

    @Mock
    fun `$init`(request: Request, listener: WebSocketListener, random: Random) {
        if ("GET" != request.method()) {
            throw IllegalArgumentException("Request must be GET: " + request.method())
        }
        this.originalRequest = request
        this.listener = listener
        this.random = random
    }

    @Mock
    fun connect(client: OkHttpClient) {
        val response = Response.Builder()
                .request(originalRequest)
                .protocol(Protocol.HTTP_2)
                .code(200)
                .build()

        if (list.size > 0 || listPublicMessage.size > 0) {
            listener.onOpen(this, response)
        } else {
            disconnect()
        }
    }

    fun disconnect() {
        listener.onClosed(this, 1000, "Close request")
    }


    fun addFiles(reserved: String, toSend: String) {
        list.add(Item(readFixture(reserved), readFixture(toSend)))
    }

    fun addPublicMessageFiles(reserved: String, toSend: String) {
        listPublicMessage.add(Item(readFixture(reserved), readFixture(toSend)))
    }

    fun isAllConsumed() {
        if (!list.isEmpty()) {
            throw PandaTestWeSocketException("All items not consumed : \n             - reserved: " + list.first().reserved.toJsonString(0) +"\n             - toSend: " + list.first().toSend.toJsonString(0))
        }
    }

    fun sizeList() : Int {
        return list.size
    }

    fun sendFile(fileName: String) {
        listener.onMessage(this, readFixture(fileName).toJsonString(0))
    }

    override fun send(text: String?): Boolean {
        val message = JsonUtil.fromJson<PhoenixPayload>(text, PhoenixPayload::class.java)

        var consumed: Boolean = false

        for (item in list) {
            if (item.reserved.event == message.event && item.reserved.topic == message.topic && JsonUtil.toJson(item.reserved.payload) == JsonUtil.toJson(message.payload)) {
                doWait()
                consumed = true
                listener.onMessage(this, item.toSend.toJsonString(0))
                list.remove(item)
                return consumed
            }
        }

        for (item in listPublicMessage) {
            if (item.reserved.event == message.event
                    && item.reserved.topic == message.topic
                    && item.reserved.payload["message"] == message.payload["message"]
                    && item.reserved.payload["user"] == message.payload["user"]) {
                doWait()
                consumed = true

                item.toSend.payload["index"] = message.payload["index"]
                item.toSend.payload["inserted_at"] = message.payload["index"].toString().replace(item.reserved.payload["user"].toString() + "_", "", true)

                listener.onMessage(this, item.toSend.toJsonString(0))
                list.remove(item)
                return consumed
            }
        }

        if (!consumed) {
            throw PandaTestWeSocketException("message below is not into the list : \n" + text)
        }

        return consumed
    }

    override fun send(bytes: ByteString?): Boolean {
        return true
    }

    override fun queueSize(): Long {
        return 0L
    }

    override fun close(code: Int, reason: String?): Boolean {
        listener.onClosing(null, code, reason)
        return true
    }

    override fun cancel() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun request(): Request {
        return originalRequest
    }


    private fun readFixture(fixturePath: String): PhoenixPayload {
        try {
            return JsonUtil.fromJson<PhoenixPayload>(read(open(fixturesRootFolder + "/" + fixturePath)), PhoenixPayload::class.java)
        } catch (e: IOException) {
            throw RuntimeException("Failed to read asset with path " + fixturePath, e)
        }
    }

    @Throws(IOException::class)
    private fun open(path: String): InputStream {
        return RealWebSocketWithMock::class.java.classLoader.getResourceAsStream(path)
    }

    private fun read(inputStream: InputStream): String {

        if (inputStream == null) {
            throw IllegalArgumentException("Could not open resource stream.")
        }

        val bis = BufferedReader(InputStreamReader(inputStream))
        val builder = StringBuilder()

        var line: String
        try {

            line = inputStream.bufferedReader().use { it.readText() }

        } catch (e: IOException) {
            throw RuntimeException("Could not read resource fully", e)
        } finally {

            try {
                inputStream.close()
            } catch (e: IOException) {
//                Log.e(IOReader::class.java.simpleName, "Error while trying to close stream", e)
                // do nothing here
            }

        }

        return line.replace("\\n", "", true)
    }
}
