package fr.com.panda.test.base

import android.os.Build
import fr.com.panda.BuildConfig
import fr.com.panda.manager.SocketManager
import fr.com.panda.socket.phoenix.PhoenixSocket
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import fr.com.panda.test.mock.RealWebSocketWithMock
import fr.com.panda.test.utils.doWait
import fr.com.panda.utils.Constants
import org.robolectric.RobolectricTestRunner


/**
 * Created by edouardroussillon on 12/31/16.
 */
abstract class PandaWSUnitTestCase : PandaUnitTestCase() {

    var realWebSocketWithMock: RealWebSocketWithMock = RealWebSocketWithMock()

    override fun setUp() {
        super.setUp()

        realWebSocketWithMock.isAllConsumed()
        SocketManager.sharedInstance.leaveRooms()
        SocketManager.sharedInstance.forceStopSocket {}
        PhoenixSocket.MESSAGE_REFERENCE = 0
        PhoenixSocket.HEARTBEAT_IN_SEC = Constants.SOCKET.HEARTBEAT_IN_SEC
        doWait()
    }

    override fun reset() {
        super.reset()
        realWebSocketWithMock.isAllConsumed()
        SocketManager.sharedInstance.leaveRooms()
        SocketManager.sharedInstance.forceStopSocket {}
        PhoenixSocket.MESSAGE_REFERENCE = 0
        PhoenixSocket.HEARTBEAT_IN_SEC = Constants.SOCKET.HEARTBEAT_IN_SEC
        doWait()
    }
}