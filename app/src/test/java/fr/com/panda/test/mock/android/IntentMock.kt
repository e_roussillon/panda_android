package com.rockspoon.test.mock.android

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class IntentMock : MockUp<Intent>() {
    var mAction: String? = null
    var mExtras: Bundle? = null

    companion object {
        var packageContext: Context? = null
        var cls: Class<*>? = null
    }


    @Mock
    fun `$init`() {}

    @Mock
    fun `$init`(action: String) {
        mAction = action
    }

    @Mock
    fun `$init`(packageContext: Context, cls: Class<*>) {
        IntentMock.packageContext = packageContext
        IntentMock.cls = cls
    }

    @Mock
    fun setAction(action: String?): Intent {
        mAction = action?.intern()
        return this.mockInstance
    }

    @Mock
    fun getAction(): String? {
        return mAction
    }

    @Mock
    fun putExtra(name: String, value: Int): Intent {
        if (mExtras == null) {
            mExtras = Bundle()
        }
        mExtras?.putInt(name, value)
        return this.mockInstance
    }

    @Mock
    fun getIntExtra(name: String, defaultValue: Int): Int {
        return mExtras?.getInt(name, defaultValue) ?: 0
    }

    @Mock
    fun putExtra(name: String, value: Parcelable): Intent {
        if (mExtras == null) {
            mExtras = Bundle()
        }
        mExtras?.putParcelable(name, value)
        return this.mockInstance
    }

    @Mock
    fun putExtra(name: String, value: String): Intent {
        if (mExtras == null) {
            mExtras = Bundle()
        }
        mExtras?.putString(name, value)
        return this.mockInstance
    }

    @Mock
    fun <T : Parcelable> getParcelableExtra(name: String): T? {
        val extras = mExtras ?: return null

        return extras.getParcelable<T>(name)
    }

    @Mock
    fun getExtras(): Bundle? {
        return if (mExtras != null)
            Bundle(mExtras)
        else
            null
    }

}