package fr.com.panda.test.test.socket

import fr.com.panda.R
import fr.com.panda.enums.StatusType
import fr.com.panda.test.base.PandaWSUnitTestCase
import fr.com.panda.test.robot.socket.socketPublicRobot
import fr.com.panda.test.utils.doWait
import org.junit.Test
import org.robolectric.annotation.Config

/**
 * @author Edouard Roussillon
 */

class SocketPublicUnitTestCase : PandaWSUnitTestCase() {


    @Test
    fun connectToPublicRooms() {

        socketPublicRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initGSPLocalistation()
            registerGetSessionSucceed()
            registerBackupSucceed()
            registerFindPublicRoomSucceed()
            registerJoinedChannelBroadcast()
            registerJoinedChannelGeneral()
            registerJoinedChannelSale()
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initRequestFindRoomWithCode200()
            initJoinPrivateRoom()
            initJoinBroadcastRoom()
            initJoinGeneralRoom()
            initJoinSaleRoom()
            loginUser("dovi@gmail.com", "123456")
            waitForResult(isJoinedChannelGeneral = true, countJoinedChannelBroadcast = 3, isJoinedChannelSale = true)
        } checkResult {
            isJoinedChannelGeneral()
            isJoinedChannelBroadcast()
            isJoinedChannelSale()
        }
    }

    @Test
    fun connectToPublicRoomsAndSendMessageGeneral() {
        socketPublicRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initGSPLocalistation()
            registerGetSessionSucceed()
            registerBackupSucceed()
            registerFindPublicRoomSucceed()
            registerJoinedChannelBroadcast()
            registerJoinedChannelGeneral()
            registerJoinedChannelSale()
            registerNewGeneralMessageNotification()
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initRequestFindRoomWithCode200()
            initSendMessageGeneral()
            initJoinPrivateRoom()
            initJoinBroadcastRoom()
            initJoinGeneralRoom()
            initJoinSaleRoom()
            loginUser("dovi@gmail.com", "123456")
            waitForResult(isJoinedChannelGeneral = true, countJoinedChannelBroadcast = 3, isJoinedChannelSale = true)
            sendMessage(true, "hello")
            waitForResultNewGeneralMessage()
        } checkResult {
            isJoinedChannelGeneral()
            isJoinedChannelBroadcast()
            isJoinedChannelSale()
            isMessageSave(true, "hello", StatusType.SENT)
        }
    }

    @Test
    fun connectToPublicRoomsAndSendMessageSale() {
        socketPublicRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initGSPLocalistation()
            registerGetSessionSucceed()
            registerBackupSucceed()
            registerFindPublicRoomSucceed()
            registerJoinedChannelBroadcast()
            registerJoinedChannelGeneral()
            registerJoinedChannelSale()
            registerNewSaleMessageNotification()
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initRequestFindRoomWithCode200()
            initSendMessageSale()
            initJoinPrivateRoom()
            initJoinBroadcastRoom()
            initJoinGeneralRoom()
            initJoinSaleRoom()
            loginUser("dovi@gmail.com", "123456")
            waitForResult(isJoinedChannelGeneral = true, countJoinedChannelBroadcast = 3, isJoinedChannelSale = true)
            sendMessage(false, "hello")
            waitForResultNewSaleMessage()
        } checkResult {
            isJoinedChannelGeneral()
            isJoinedChannelBroadcast()
            isJoinedChannelSale()
            isMessageSave(false, "hello", StatusType.SENT)
        }
    }

    @Test
    fun connectToPublicRoomsAndSendMessageBroadcastGeneral() {
        socketPublicRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initGSPLocalistation()
            registerGetSessionSucceed()
            registerBackupSucceed()
            registerFindPublicRoomSucceed()
            registerJoinedChannelBroadcast()
            registerJoinedChannelGeneral()
            registerJoinedChannelSale()
            registerNewBroadcastMessageNotification()
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initRequestFindRoomWithCode200()
            initJoinPrivateRoom()
            initJoinBroadcastRoom()
            initJoinGeneralRoom()
            initJoinSaleRoom()
            loginUser("dovi@gmail.com", "123456")
            waitForResult(isJoinedChannelGeneral = true, countJoinedChannelBroadcast = 3, isJoinedChannelSale = true)
            sendMessageBroadcast(true)
            waitForResultNewBroadcastMessage()
        } checkResult {
            isJoinedChannelGeneral()
            isJoinedChannelBroadcast()
            isJoinedChannelSale()
            isMessageBroadcast(true)
        }
    }

    @Test
    fun connectToPublicRoomsAndSendMessageBroadcastSale() {
        socketPublicRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initGSPLocalistation()
            registerGetSessionSucceed()
            registerBackupSucceed()
            registerFindPublicRoomSucceed()
            registerJoinedChannelBroadcast()
            registerJoinedChannelGeneral()
            registerJoinedChannelSale()
            registerNewBroadcastMessageNotification()
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initRequestFindRoomWithCode200()
            initJoinPrivateRoom()
            initJoinBroadcastRoom()
            initJoinGeneralRoom()
            initJoinSaleRoom()
            loginUser("dovi@gmail.com", "123456")
            waitForResult(isJoinedChannelGeneral = true, countJoinedChannelBroadcast = 3, isJoinedChannelSale = true)
            sendMessageBroadcast(false)
            waitForResultNewBroadcastMessage()
        } checkResult {
            isJoinedChannelGeneral()
            isJoinedChannelBroadcast()
            isJoinedChannelSale()
            isMessageBroadcast(false)
        }
    }

    @Test
    fun connectToPublicRoomsAndSendWelcomeMessageGeneral() {

        socketPublicRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initGSPLocalistation()
            registerGetSessionSucceed()
            registerBackupSucceed()
            registerFindPublicRoomSucceed()
            registerJoinedChannelBroadcast()
            registerJoinedChannelGeneral()
            registerJoinedChannelSale()
            registerNewGeneralMessageNotification()
            registerWelcomeMessage(R.string.welcome_message_general, "Welcome to %1 general channel.")
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initRequestFindRoomWithCode200()
            initJoinPrivateRoom()
            initJoinBroadcastRoom()
            initJoinGeneralRoom()
            initJoinSaleRoom()
            loginUser("dovi@gmail.com", "123456")
            waitForResult(isJoinedChannelGeneral = true, countJoinedChannelBroadcast = 3, isJoinedChannelSale = true)
            sendWelcomeMessage(true)
            waitForResultNewGeneralMessage()
        } checkResult {
            isJoinedChannelGeneral()
            isJoinedChannelBroadcast()
            isJoinedChannelSale()
            isMessageWelcome(true)
        }
    }

    @Test
    fun connectToPublicRoomsAndSendWelcomeMessageSale() {

        socketPublicRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initGSPLocalistation()
            registerGetSessionSucceed()
            registerBackupSucceed()
            registerFindPublicRoomSucceed()
            registerJoinedChannelBroadcast()
            registerJoinedChannelGeneral()
            registerJoinedChannelSale()
            registerNewSaleMessageNotification()
            registerWelcomeMessage(R.string.welcome_message_sale, "Welcome to %1 trade channel.")
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initRequestFindRoomWithCode200()
            initJoinPrivateRoom()
            initJoinBroadcastRoom()
            initJoinGeneralRoom()
            initJoinSaleRoom()
            loginUser("dovi@gmail.com", "123456")
            waitForResult(isJoinedChannelGeneral = true, countJoinedChannelBroadcast = 3, isJoinedChannelSale = true)
            sendWelcomeMessage(false)
            waitForResultNewSaleMessage()
        } checkResult {
            isJoinedChannelGeneral()
            isJoinedChannelBroadcast()
            isJoinedChannelSale()
            isMessageWelcome(isGeneral = false)
        }
    }


//    @Test
//    fun connectToPublicRoomsAndSendWelcomeMessageSale() {
//        socketPublicRobot(this.bag, this.realWebSocketWithMock, this.server) {
//            registerGetSessionSucceed()
//            registerBackupSucceed()
//            registerFindPublicRoomSucceed()
//            registerJoinedChannelBroadcast()
//            registerJoinedChannelGeneral()
//            registerJoinedChannelSale()
//            registerNewSaleMessageNotification()
//            initRequestLoginWithCode200()
//            initRequestBackupWithCode200()
//            initRequestFindRoomWithCode200()
//            initSendMessageSale()
//            initJoinPrivateRoom()
//            initJoinBroadcastRoom()
//            initJoinGeneralRoom()
//            initJoinSaleRoom()
//            loginUser("dovi@gmail.com", "123456")
//            waitForResult(isJoinedChannelGeneral = true, countJoinedChannelBroadcast = 3, isJoinedChannelSale = true)
//            sendMessage(false, "hello")
//            waitForResultNewSaleMessage()
//        } checkResult {
//            isJoinedChannelGeneral()
//            isJoinedChannelBroadcast()
//            isJoinedChannelSale()
//            isMessageSave(false, "hello", StatusType.SENT)
//        }
//    }
}