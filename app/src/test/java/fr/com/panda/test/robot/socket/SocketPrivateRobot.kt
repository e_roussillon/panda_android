package fr.com.panda.test.robot.socket

import br.com.concretesolutions.requestmatcher.RequestMatcherRule
import br.com.concretesolutions.requestmatcher.model.HttpMethod
import fr.com.panda.enums.ChannelType
import fr.com.panda.event.DisposableBag
import fr.com.panda.manager.BackupManager
import fr.com.panda.manager.ChatManager
import fr.com.panda.manager.SessionManager
import fr.com.panda.manager.SocketManager
import fr.com.panda.model.FilterModel
import fr.com.panda.model.FriendshipModel
import fr.com.panda.model.UserModel
import fr.com.panda.socket.notification.SocketNotificaterManager
import fr.com.panda.socket.phoenix.PhoenixSocket
import fr.com.panda.test.mock.RealWebSocketWithMock
import fr.com.panda.test.utils.*
import fr.com.panda.utils.Constants

/**
 * @author Edouard Roussillon
 */

fun socketPrivateRobot(bag: DisposableBag, realWebSocketWithMock: RealWebSocketWithMock, server: RequestMatcherRule, func: SocketPrivateRobot.() -> Unit) = SocketPrivateRobot(bag, realWebSocketWithMock, server).apply { func() }

class SocketPrivateRobot(val bag: DisposableBag, val realWebSocketWithMock: RealWebSocketWithMock, val server: RequestMatcherRule) {
    private val sessionManager: SessionManager = SessionManager.sharedInstance
    private val backupManager: BackupManager = BackupManager.sharedInstance
    private val socketNotificaterManager: SocketNotificaterManager = SocketNotificaterManager.sharedInstance
    private val socketManager: SocketManager = SocketManager.sharedInstance

    private var isShowSplashScreen: Boolean = false
    private var userReported: Int = 0
    private var isNewConnection: List<UserModel>? = null
    private var isNewDisconnection: UserModel? = null
    private var isUpdateFriendship: UserModel? = null


    fun registerGetSessionSucceed() {
        bag.add(sessionManager.onGetSessionSucceed.add { result ->
            backupManager.backup()
        })
    }

    fun registerJoinedChannelPrivate() {
        bag.add(socketNotificaterManager.onJoinedChannelPrivate.add {
            realWebSocketWithMock.sendFile("pending_private_channel.json")
        })
    }

    fun registerShowSplashScreen() {
        bag.add(socketNotificaterManager.onShowSplashScreen.add {
            isShowSplashScreen = true
        })
    }

    fun registerUserReported() {
        bag.add(socketNotificaterManager.onUserReported.add { time ->
            userReported = time
        })
    }

    fun registerUsersConnected() {
        bag.add(socketNotificaterManager.onNewConnection.add { list ->
            isNewConnection = list
        })
    }

    fun registerNewDisconnection() {
        bag.add(socketNotificaterManager.onNewDisconnection.add { user ->
            isNewDisconnection = user
        })
    }

    fun registerUpdateFriendship() {
        bag.add(socketNotificaterManager.onUpdateFriendship.add { user ->
            isUpdateFriendship = user
        })
    }


    fun loginUser(email: String, password: String) {
        sessionManager.login(email, password)
    }

    fun sendDuplicate() {
        realWebSocketWithMock.sendFile("duplicate_user.json")
    }

    fun sendUserConnected() {
        realWebSocketWithMock.sendFile("user_connected.json")
    }

    fun sendUsersConnect() {
        realWebSocketWithMock.sendFile("users_connected.json")
    }

    fun sendReported() {
        realWebSocketWithMock.sendFile("user_reported.json")
    }

    fun sendUserDisconnected() {
        realWebSocketWithMock.sendFile("user_disconnected.json")
    }

    fun askedFriendship() {
        realWebSocketWithMock.sendFile("ask_friendship.json")
    }

    fun waitForResultJoinPrivateRoom() {
        doWaitFor {
            socketManager.isJoined(ChannelType.PRIVATE, 1)
        }
    }

    fun waitForResultFundFilter() {
        doWaitFor {
            FilterModel.getFilter(2) != null
        }
    }

    fun waitForResultUserReported() {
        doWaitFor {
            userReported != 0
        }
    }

    fun waitForResultIsShowSplashScreen() {
        doWaitFor {
            isShowSplashScreen == true
        }
    }

    fun waitForResultIsUpdateFriendship() {
        doWaitFor {
            isUpdateFriendship != null
        }
    }

    fun waitForResultUserConnected() {
        doWaitFor {
            UserModel.getUser(2) != null && UserModel.getUser(2).isOnline
        }
    }

    fun waitForResultUserDisconnected() {
        doWaitFor {
            UserModel.getUser(2) != null && !UserModel.getUser(2).isOnline
        }
    }

    fun waitForResultHeartbeatUsed() {
        doWaitFor {
            this.realWebSocketWithMock.sizeList() == 0
        }
    }

    fun checkResult(func: SocketPrivateResult.() -> Unit) : SocketPrivateResult {
        return SocketPrivateResult(isShowSplashScreen, userReported, isNewConnection, isNewDisconnection, isUpdateFriendship).apply { func() }
    }

    //
    // Request
    //

    fun initRequestLoginWithCode200() {
        this.server.addFixture(200, "login.json")
                .ifRequestMatches()
                .pathIs("/api/v1/auth")
                .methodIs(HttpMethod.POST)
    }

    fun initRequestBackupWithCode200() {
        this.server.addFixture(200, "backup.json")
                .ifRequestMatches()
                .pathIs("/api/v1/auth/backup")
                .methodIs(HttpMethod.GET)
    }

    fun initRequestLogoutWithCode200() {
        this.server.addFixture(200, "empty.json")
                .ifRequestMatches()
                .pathIs("/api/v1/logout")
                .methodIs(HttpMethod.POST)
    }

    fun initJoinPrivateRoom() {
        this.realWebSocketWithMock.addFiles("join_private_channel.json", "join_private_channel_response.json")
    }

    fun initHeartbeat() {
        PhoenixSocket.HEARTBEAT_IN_SEC = 3
        this.realWebSocketWithMock.addFiles("heartbeat.json", "heartbeat_response.json")
    }
}

class SocketPrivateResult(val isShowSplashScreen: Boolean,
                          val userReported: Int,
                          val isNewConnection: List<UserModel>?,
                          val isNewDisconnection: UserModel?,
                          val isUpdateFriendship: UserModel?) {
    private val socketManager: SocketManager = SocketManager.sharedInstance

    fun isJoinPrivateRoom() {
        isTrue(socketManager.isJoined(ChannelType.PRIVATE, 1))
    }

    fun isFilterPendingResultSet() {
        val filterModel = FilterModel.getFilter(2)
        isNotNull(filterModel)
        isEqual(filterModel.name, "pending")
    }

    fun isUserLogout() {
        isFalse(SessionManager.sharedInstance.user().isLogin)
    }

    fun isShowSplashScreen() {
        isTrue(isShowSplashScreen)
    }

    fun isUserReported(time: Int) {
        isEqual(time, userReported)
    }

    fun isSocketDisconnected() {
        isFalse(SocketManager.sharedInstance.isSocketConnected)
    }

    fun isFriendshipFound() {
        val users = UserModel.getUsersByFriendshipTypePending()
        isNotNull(users)
        isEqual(users.size, 1)
        isEqual(users[0].pseudo, "dovi3")
    }

    fun isFriendsConnected(isConnected: Boolean) {
        val user = UserModel.getUser(2)
        isNotNull(user)
        isEqual(user.isOnline, isConnected)
    }

    fun isNewConnection() {
        isNotNull(isNewConnection)
    }

    fun isNewDisconnection() {
        isNotNull(isNewDisconnection)
    }

    fun isUpdateFriendship() {
        isNotNull(isUpdateFriendship)
    }
}