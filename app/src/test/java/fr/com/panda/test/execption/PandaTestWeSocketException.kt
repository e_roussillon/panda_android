package fr.com.panda.test.execption

/**
 * @author Edouard Roussillon
 */

class PandaTestWeSocketException(message: String) : Exception(message) {}