package com.rockspoon.test.mock.android

import android.media.AudioManager
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class AudioManagerMock : MockUp<AudioManager>() {

    var audioIsWiredHeadsetOn: AudioIsWiredHeadsetOn? = null

    @Mock
    fun isWiredHeadsetOn(): Boolean {
        return audioIsWiredHeadsetOn?.isWiredHeadsetOn() ?: false
    }

    interface AudioIsWiredHeadsetOn {
        fun isWiredHeadsetOn() : Boolean
    }

}