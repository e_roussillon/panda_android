package fr.com.panda.test.test.db

import fr.com.panda.enums.DataType
import fr.com.panda.enums.StatusType
import fr.com.panda.test.base.PandaUnitTestCase
import fr.com.panda.test.robot.db.FilterData
import fr.com.panda.test.robot.db.PublicMessageData
import fr.com.panda.test.robot.db.publicMessageModel
import org.junit.Test
import java.net.URL
import java.util.*

/**
 * Created by edouardroussillon on 1/10/17.
 */

class PublicMessageModelUnitTest : PandaUnitTestCase() {

    @Test
    fun testInsertPublicMessage_getPublicMessageGeneral() {

        val messageId = 1
        val msg = "hello"
        val fromUserId = -1
        val fromUserName = "pseudo"
        val urlThumb = URL("http://google.com")
        val urlOriginal = URL("http://google.com")
        val urlBlur = URL("http://google.com")
        val type = DataType.NONE
        val status = StatusType.SENT
        val isGeneral = true
        val isHighlight = false
        val isWelcomeMessage = false
        val insertedAt = Date()

        val message = PublicMessageData(messageId,
                msg,
                fromUserId,
                fromUserName,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                "index_1",
                status,
                isGeneral,
                isHighlight,
                isWelcomeMessage,
                insertedAt)

        val message2 = PublicMessageData(messageId,
                msg,
                fromUserId,
                fromUserName,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                "index_2",
                status,
                !isGeneral,
                isHighlight,
                isWelcomeMessage,
                insertedAt)

        publicMessageModel {
            insertPublicMessage(message)
            insertPublicMessage(message2)
        } getAllPublicMessageGeneral {
            isEqualTo(listOf(message))
        }
    }

    @Test
    fun testInsertPublicMessage_getPublicMessageSale() {

        val messageId = 1
        val msg = "hello"
        val fromUserId = -1
        val fromUserName = "pseudo"
        val urlThumb = URL("http://google.com")
        val urlOriginal = URL("http://google.com")
        val urlBlur = URL("http://google.com")
        val type = DataType.NONE
        val status = StatusType.SENT
        val isGeneral = true
        val isHighlight = false
        val isWelcomeMessage = false
        val insertedAt = Date()

        val message = PublicMessageData(messageId,
                msg,
                fromUserId,
                fromUserName,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                "index_1",
                status,
                isGeneral,
                isHighlight,
                isWelcomeMessage,
                insertedAt)

        val message2 = PublicMessageData(messageId,
                msg,
                fromUserId,
                fromUserName,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                "index_2",
                status,
                !isGeneral,
                isHighlight,
                isWelcomeMessage,
                insertedAt)

        publicMessageModel {
            insertPublicMessage(message)
            insertPublicMessage(message2)
        } getAllPublicMessageSale {
            isEqualTo(listOf(message2))
        }
    }

    @Test
    fun testInsertLightPublicMessage_getPublicMessageSale() {

        val messageId = 1
        val msg = "hello"
        val fromUserId = -1
        val fromUserName = "pseudo"
        val urlThumb = URL("http://google.com")
        val urlOriginal = URL("http://google.com")
        val urlBlur = URL("http://google.com")
        val type = DataType.NONE
        val status = StatusType.SENT
        val isGeneral = true
        val isHighlight = false
        val isWelcomeMessage = false
        val insertedAt = Date()

        val message = PublicMessageData(messageId,
                msg,
                fromUserId,
                fromUserName,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                "index_1",
                status,
                isGeneral,
                isHighlight,
                isWelcomeMessage,
                insertedAt)

        val message2 = PublicMessageData(messageId,
                msg,
                fromUserId,
                fromUserName,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                "index_2",
                status,
                !isGeneral,
                isHighlight,
                isWelcomeMessage,
                insertedAt)

        publicMessageModel {
            insertPublicMessageLight(message)
            insertPublicMessageLight(message2)
        } getAllPublicMessageSale {
            isEqualTo(listOf(message2))
        }
    }

    @Test
    fun testInsertPublicMessage_isWelcomeMessage() {

        val messageId = 1
        val msg = "hello"
        val fromUserId = -1
        val fromUserName = "pseudo"
        val urlThumb = URL("http://google.com")
        val urlOriginal = URL("http://google.com")
        val urlBlur = URL("http://google.com")
        val type = DataType.NONE
        val status = StatusType.SENT
        val isGeneral = true
        val isHighlight = false
        val isWelcomeMessage = true
        val insertedAt = Date()

        val message = PublicMessageData(messageId,
                msg,
                fromUserId,
                fromUserName,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                "index_1",
                status,
                isGeneral,
                isHighlight,
                isWelcomeMessage,
                insertedAt)

        publicMessageModel {
            insertPublicMessage(message)
        }.isContainWelcomeMessage(message) {
            isWelcomMessage(true)
        }
    }

    @Test
    fun testInsertPublicMessage_checkLikeFilter() {

        val messageId = 1
        val msg = "test to see if 'HeLLo everyone'"
        val fromUserId = -1
        val fromUserName = "pseudo"
        val urlThumb = URL("http://google.com")
        val urlOriginal = URL("http://google.com")
        val urlBlur = URL("http://google.com")
        val type = DataType.NONE
        val status = StatusType.SENT
        val isGeneral = true
        val isHighlight = false
        val isWelcomeMessage = true
        val insertedAt = Date()

        val message = PublicMessageData(messageId,
                msg,
                fromUserId,
                fromUserName,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                "index_1",
                status,
                isGeneral,
                isHighlight,
                isWelcomeMessage,
                insertedAt)

        val message2 = PublicMessageData(messageId,
                msg,
                fromUserId,
                fromUserName,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                "index_1",
                status,
                isGeneral,
                !isHighlight,
                isWelcomeMessage,
                insertedAt)

        val filterId = 1
        val name = "hello"
        val timestamp = Date()

        val filterData = FilterData(filterId, name, !isHighlight, timestamp)

        publicMessageModel {
            insertPublicMessage(message)
            updateMessageWithFilter(filterData)
        } getAllPublicMessageGeneral {
            isEqualTo(listOf(message2))
        }
    }

    @Test
    fun testInsertPublicMessage_checkDislikeFilter() {

        val messageId = 1
        val msg = "test to see if 'HeLLo everyone'"
        val fromUserId = -1
        val fromUserName = "pseudo"
        val urlThumb = URL("http://google.com")
        val urlOriginal = URL("http://google.com")
        val urlBlur = URL("http://google.com")
        val type = DataType.NONE
        val status = StatusType.SENT
        val isGeneral = true
        val isHighlight = true
        val isWelcomeMessage = true
        val insertedAt = Date()

        val message = PublicMessageData(messageId,
                msg,
                fromUserId,
                fromUserName,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                "index_1",
                status,
                isGeneral,
                isHighlight,
                isWelcomeMessage,
                insertedAt)

        val message2 = PublicMessageData(messageId,
                msg,
                fromUserId,
                fromUserName,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                "index_1",
                status,
                isGeneral,
                !isHighlight,
                isWelcomeMessage,
                insertedAt)

        val filterId = 1
        val name = "hello"
        val timestamp = Date()

        val filterData = FilterData(filterId, name, !isHighlight, timestamp)

        publicMessageModel {
            insertPublicMessage(message)
            removeMessageWithFilter(filterData)
        } getAllPublicMessageGeneral {
            isEqualTo(listOf(message2))
        }
    }

    @Test
    fun testInsertPublicMessage_checkNotApplyLikeFilter() {

        val messageId = 1
        val msg = "test to see if 'bonjour everyone'"
        val fromUserId = -1
        val fromUserName = "pseudo"
        val urlThumb = URL("http://google.com")
        val urlOriginal = URL("http://google.com")
        val urlBlur = URL("http://google.com")
        val type = DataType.NONE
        val status = StatusType.SENT
        val isGeneral = true
        val isHighlight = false
        val isWelcomeMessage = true
        val insertedAt = Date()

        val message = PublicMessageData(messageId,
                msg,
                fromUserId,
                fromUserName,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                "index_1",
                status,
                isGeneral,
                isHighlight,
                isWelcomeMessage,
                insertedAt)

        val filterId = 1
        val name = "hello"
        val timestamp = Date()

        val filterData = FilterData(filterId, name, !isHighlight, timestamp)

        publicMessageModel {
            insertPublicMessage(message)
            updateMessageWithFilter(filterData)
        } getAllPublicMessageGeneral {
            isEqualTo(listOf(message))
        }
    }

    @Test
    fun testInsertPublicMessage_checkNotApplyDislikeFilter() {

        val messageId = 1
        val msg = "test to see if 'bonjour everyone'"
        val fromUserId = -1
        val fromUserName = "pseudo"
        val urlThumb = URL("http://google.com")
        val urlOriginal = URL("http://google.com")
        val urlBlur = URL("http://google.com")
        val type = DataType.NONE
        val status = StatusType.SENT
        val isGeneral = true
        val isHighlight = true
        val isWelcomeMessage = true
        val insertedAt = Date()

        val message = PublicMessageData(messageId,
                msg,
                fromUserId,
                fromUserName,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                "index_1",
                status,
                isGeneral,
                isHighlight,
                isWelcomeMessage,
                insertedAt)

        val filterId = 1
        val name = "hello"
        val timestamp = Date()

        val filterData = FilterData(filterId, name, !isHighlight, timestamp)

        publicMessageModel {
            insertPublicMessage(message)
            removeMessageWithFilter(filterData)
        } getAllPublicMessageGeneral {
            isEqualTo(listOf(message))
        }
    }

}

