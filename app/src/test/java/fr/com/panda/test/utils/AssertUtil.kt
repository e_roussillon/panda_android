package fr.com.panda.test.utils

import fr.com.panda.enums.ErrorType
import fr.com.panda.model.ErrorResponse
import org.junit.Assert

/**
 * Created by edouardroussillon on 12/31/16.
 */

fun doWait(milliseconds: Long = 1000) = Thread.sleep(milliseconds)

fun doWaitFor(timer: Int = 15, func: () -> Boolean) {
    var time = timer
    while (time >= 0) {
        if (func()) {
            time = 0
        }
        doWait()
        time--
    }

}

fun isNull(expected: Any?) = Assert.assertNull(expected)
fun isNotNull(expected: Any?) = Assert.assertNotNull(expected)
fun isEqual(expected: Any?, actual: Any?) = Assert.assertEquals(expected, actual)
fun isNotEqual(expected: Any?, actual: Any?) = Assert.assertNotEquals(expected, actual)
fun isTrue(expected: Boolean) = Assert.assertTrue(expected)
fun isFalse(expected: Boolean) = Assert.assertFalse(expected)

open class ResultAPIError(val error: ErrorResponse?) {
    fun isErrorEqual(@ErrorType.ErrorMode type: Int, code: Int, msg: String) {

        isEqual(true, (error != null))
        isEqual(error!!.type, type)
        isEqual(error!!.msg, msg)
        isEqual(error!!.code, code)
//        isEqual((error!!.fields != null), true)
    }
}