package fr.com.panda.test.test.manager

import android.util.Log
import fr.com.panda.enums.ErrorType
import fr.com.panda.test.base.PandaUnitTestCase
import fr.com.panda.test.robot.db.filterModel
import fr.com.panda.test.robot.manager.sessionManager
import fr.com.panda.test.utils.doWait
import org.junit.Test
import java.util.*

class SessionManagerUnitTestCase : PandaUnitTestCase() {

    /**
     * Login
     */

    @Test
    fun session_loginUserSuccess() {
        val email = "test@gmail.com"
        val password = "123456"

        sessionManager(this.server) {
            registeToEventLoginSuccess()
            initRequestLoginWithCode200("login.json")
        }.login(email, password) {
            isLogin("token login", "refresh token login")
        }
    }

    @Test
    fun session_loginUserWithErrorParam() {
        val email = "test@gmail.com"
        val password = "123456"

        sessionManager(this.server) {
            registeToEventGetLoginFailedNotFoundEmailOrPassword()
            initRequestLoginWithCode422("login_param_error.json")
        }.login(email, password) {
            isLoginNotFound()
        }
    }

    @Test
    fun session_loginUserRateLimit() {
        val email = "test@gmail.com"
        val password = "123456"

        sessionManager(this.server) {
            registeToEventGetLoginFailedRateLimit()
            initRequestLoginWithCode401("login_rate_limit.json")
        }.login(email, password) {
            isLoginRateLimit()
        }
    }

    @Test
    fun session_loginUserWithErrorGeneric() {
        val email = "test@gmail.com"
        val password = "123456"

        sessionManager(this.server) {
            registeToEventGetLoginFailed()
            initRequestLoginWithCode401("token_not_renew.json")
        }.login(email, password) {
            isErrorEqual(ErrorType.AUTH, 3, "token not renew")
        }
    }

    /**
     * Register
     */

    @Test
    fun session_registerSuccess() {
        val pseudo = "test"
        val email = "test@gmail.com"
        val birthday = Date()
        val password = "123456"

        sessionManager(this.server) {
            registeToEventLoginSuccess()
            initRequestRegisterWithCode200("login.json")
        }.register(pseudo, email, birthday, password) {
            isLogin("token login", "refresh token login")
        }
    }

    @Test
    fun session_registerFail() {
        val pseudo = "test"
        val email = "test@gmail.com"
        val birthday = Date()
        val password = "123456"

        sessionManager(this.server) {
            registeToEventGetLoginFailed()
            initRequestRegisterWithCode401("register_param_error.json")
        }.register(pseudo, email, birthday, password) {
            isErrorEqual(ErrorType.CHANGESET, 0, "Parameter Error")
        }
    }

    /**
     * Logout
     */

    @Test
    fun session_logoutUserSuccess() {
        val email = "test@gmail.com"
        val password = "123456"

        sessionManager(this.server) {
            initRequestLoginWithCode200("login.json")
            initRequestLogoutWithCode200("empty.json")
            login(email, password)
        } logout {
            isLogout()
        }
    }

    @Test
    fun session_logoutUserFail() {
        val email = "test@gmail.com"
        val password = "123456"

        sessionManager(this.server) {
            initRequestLoginWithCode200("login.json")
            initRequestLogoutWithCode401("token_not_renew.json")
            login(email, password)
        } logout {
            isLogout()
        }
    }

    /**
     * Upload PushToken
     */

    @Test
    fun session_pushTokenSuccess() {
        val email = "test@gmail.com"
        val password = "123456"
        val pushToken = "push token"

        sessionManager(this.server) {
            initRequestLoginWithCode200("login.json")
            initRequestPushTokenWithCode200("empty.json")
            login(email, password)
        }.pushToken(pushToken) {
            isPushToken(pushToken)
        }
    }

    @Test
    fun session_pushTokenFail() {
        val email = "test@gmail.com"
        val password = "123456"
        val pushToken = "push token"

        sessionManager(this.server) {
            initRequestLoginWithCode200("login.json")
            initRequestPushTokenWithCode401("empty.json")
            login(email, password)
        }.pushToken(pushToken) {
            isPushToken("")
        }
    }

    /**
     * Update UserId
     */

    @Test
    fun session_updateUserId() {
        val userId = 42

        sessionManager(this.server) {
        }.updateUserId(userId) {
            isUserId(userId)
        }
    }

    /**
     * RefreshToken
     */

    @Test
    fun session_refreshTokenSuccess() {
        val name = "name filter"
        val isHighlight = true
        val id = 42

        sessionManager(this.server) {
            registeToEventRefreshSessionSuccess()
            initRequestTokenInvalid()
            initRequestTokenRenew()
            addNewFilterWithSuccess(name, isHighlight)
            waitTokenUpdated("token is renew")
        } checkResult {
            isRenewToken("token is renew", "refresh token is renew")
            filterModel {
            }.getFilter(id) {
                isEqualTo(id, name, isHighlight)
            }
        }
    }

    @Test
    fun session_refreshTokenFailed() {
        val name = "name filter"
        val isHighlight = true

        sessionManager(this.server) {
            registeToEventRefreshSessionFailed()
            registeToEventGenericEventForceLogout()
            initRequestTokenInvalid()
            initRequestTokenNotRenew()
        }.addNewFilterWithError(name, isHighlight) {
            isForceLogout()
            isErrorEqual(ErrorType.AUTH, 3, "token not renew")
        }
    }
}