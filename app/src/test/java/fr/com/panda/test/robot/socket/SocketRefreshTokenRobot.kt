package fr.com.panda.test.robot.socket

import br.com.concretesolutions.requestmatcher.RequestMatcherRule
import br.com.concretesolutions.requestmatcher.model.HttpMethod
import fr.com.panda.enums.ChannelType
import fr.com.panda.event.DisposableBag
import fr.com.panda.manager.ChatManager
import fr.com.panda.manager.SessionManager
import fr.com.panda.manager.SocketManager
import fr.com.panda.test.mock.RealWebSocketWithMock
import fr.com.panda.test.mock.SimulatorUtilMock
import fr.com.panda.test.utils.doWaitFor
import fr.com.panda.test.utils.isEqual
import fr.com.panda.test.utils.isTrue

/**
 * @author Edouard Roussillon
 */

fun socketRefreshTokenRobot(bag: DisposableBag, realWebSocketWithMock: RealWebSocketWithMock, server: RequestMatcherRule, func: SocketRefreshTokenRobot.() -> Unit) = SocketRefreshTokenRobot(bag, realWebSocketWithMock, server).apply { func() }

class SocketRefreshTokenRobot(val bag: DisposableBag, val realWebSocketWithMock: RealWebSocketWithMock, val server: RequestMatcherRule) {
    val simulatorUtilMock = SimulatorUtilMock()
    val sessionManager = SessionManager.sharedInstance
    var isOnRefreshSessionSucceed: Boolean = false
    var isOnFindPublicRoomSucceed: Boolean = false
    var chatManager = ChatManager.sharedInstance
    var socketManager = SocketManager.sharedInstance

    fun eventIsRunningOnEmulator() {
        SimulatorUtilMock.simulatorUtiIsRunningOnEmulator = {
            true
        }
    }

    fun eventRefreshSessionSucceed() {
        bag.add(sessionManager.onRefreshSessionSucceed.add {
            isOnRefreshSessionSucceed = true
            startFindRoom()
        })
    }

    private fun startFindRoom() {
        registerFindPublicRoomSucceed()
        initRequestFindRoomWithCode200()
        chatManager.refreshPublicRoom(null)
    }

    private fun registerFindPublicRoomSucceed() {
        bag.add(chatManager.onFindPublicRoomSucceed.add { result ->
            isOnFindPublicRoomSucceed = true
        })
    }

    private fun initRequestFindRoomWithCode200() {
        this.server.addFixture(200, "room_find_socket.json")
                .ifRequestMatches()
                .pathIs("/api/v1/find_room")
                .methodIs(HttpMethod.POST)
    }

    fun initJoinPrivateRoom() {
        this.realWebSocketWithMock.addFiles("join_private_channel_new_token.json", "join_private_channel_response.json")
        this.realWebSocketWithMock.addFiles("join_private_channel.json", "join_private_channel_response.json")
    }

    fun initJoinGeneralRoom() {
        this.realWebSocketWithMock.addFiles("join_public_general_channel_new_token.json", "join_public_general_channel_response.json")
        this.realWebSocketWithMock.addFiles("join_public_general_channel.json", "join_public_general_channel_response.json")
    }

    fun initJoinSaleRoom() {
        this.realWebSocketWithMock.addFiles("join_public_sale_channel_new_token.json", "join_public_sale_channel_response.json")
        this.realWebSocketWithMock.addFiles("join_public_sale_channel.json", "join_public_sale_channel_response.json")
    }

    fun initJoinBroadcastRoom() {
        this.realWebSocketWithMock.addFiles("join_public_broadcast_1_channel_new_token.json", "join_public_broadcast_1_channel_response.json")
        this.realWebSocketWithMock.addFiles("join_public_broadcast_1_channel.json", "join_public_broadcast_1_channel_response.json")
        this.realWebSocketWithMock.addFiles("join_public_broadcast_2_channel_new_token.json", "join_public_broadcast_2_channel_response.json")
        this.realWebSocketWithMock.addFiles("join_public_broadcast_2_channel.json", "join_public_broadcast_2_channel_response.json")
        this.realWebSocketWithMock.addFiles("join_public_broadcast_3_channel_new_token.json", "join_public_broadcast_3_channel_response.json")
        this.realWebSocketWithMock.addFiles("join_public_broadcast_3_channel.json", "join_public_broadcast_3_channel_response.json")
    }

    fun waitFinishRefresh() {
        doWaitFor {
            isOnFindPublicRoomSucceed == true
        }
    }

    fun waitReconnect() {
        doWaitFor {
            socketManager.isSocketConnected == true
        }
    }

    infix fun checkResult(func: SocketRefreshTokenResult.() -> Unit) : SocketRefreshTokenResult {
        return SocketRefreshTokenResult(isOnRefreshSessionSucceed, isOnFindPublicRoomSucceed).apply { func() }
    }
}

class SocketRefreshTokenResult(var isOnRefreshSessionSucceed: Boolean, var isOnFindPublicRoomSucceed: Boolean) {

    private val socketManager: SocketManager = SocketManager.sharedInstance

    fun isJoinPrivateRoom() {
        isTrue(socketManager.isJoined(ChannelType.PRIVATE, 1))
    }

    fun isRefreshSessionSucceed(isTrue: Boolean) {
        isEqual(isTrue, isOnRefreshSessionSucceed)
    }

    fun isFindPublicRoomSucceed(isTrue: Boolean) {
        isEqual(isTrue, isOnFindPublicRoomSucceed)
    }

    fun isConnected() {
        isTrue(socketManager.isSocketConnected)
    }

}