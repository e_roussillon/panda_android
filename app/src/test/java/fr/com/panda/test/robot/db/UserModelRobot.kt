package fr.com.panda.test.robot.db

import fr.com.panda.enums.FriendshipType
import fr.com.panda.enums.GenderType
import fr.com.panda.model.UserModel
import fr.com.panda.test.utils.isEqual
import java.net.URL
import java.util.*

/**
 * Created by edouardroussillon on 1/8/17.
 */

fun userModelRobot(func: UserModelRobot.() -> Unit) = UserModelRobot().apply { func() }

data class UserData(var userId: Int,
                    var email: String,
                    var pseudo: String,
                    var details: String,
                    var birthday: Date?,
                    @GenderType.GenderMode var gender: Int,
                    var urlBlur: URL?,
                    var urlThumb: URL?,
                    var urlOriginal: URL?,
                    var isOnline: Boolean,
                    var lastSeen: Date,
                    @FriendshipType.FriendshipMode var friendshipStatus: Int,
                    var friendshipLastAction: Int,
                    var reported: Boolean,
                    var updatedAt: Date)

class UserModelRobot {

    private fun initUserModel(userData: UserData) : UserModel {
        return UserModel(
                userData.userId,
                userData.email,
                userData.pseudo,
                userData.details,
                userData.birthday,
                userData.gender,
                userData.urlBlur,
                userData.urlThumb,
                userData.urlOriginal,
                userData.isOnline,
                userData.lastSeen,
                userData.friendshipStatus,
                userData.friendshipLastAction,
                userData.reported,
                userData.updatedAt)
    }

    fun insertUser(user: UserData) {
        initUserModel(user).update()
    }

    fun updateUserFriendship(userId: Int, pseudo: String, @FriendshipType.FriendshipMode friendshipStatus: Int, friendshipLastAction: Int) {
        UserModel.insert(userId, pseudo, friendshipStatus, friendshipLastAction)
    }

    infix fun getCurrentUser(func: ReturnUserModelRobot.() -> Unit) : ReturnUserModelRobot {
        return ReturnUserModelRobot(UserModel.getCurrentUser()).apply { func() }
    }

    fun getUserById(userId: Int, func: ReturnUserModelRobot.() -> Unit) : ReturnUserModelRobot {
        return ReturnUserModelRobot(UserModel.getUser(userId)).apply { func() }
    }

    infix fun getCountPendingUsers(func: ReturnUserModelCountRobot.() -> Unit) : ReturnUserModelCountRobot {
        return ReturnUserModelCountRobot(UserModel.getCountUsersByFriendshipTypePending()).apply { func() }
    }

    infix fun getPendingUsers(func: ReturnUserModelsRobot.() -> Unit) : ReturnUserModelsRobot {
        return ReturnUserModelsRobot(UserModel.getUsersByFriendshipTypePending()).apply { func() }
    }

    infix fun getBlockedUsers(func: ReturnUserModelsRobot.() -> Unit) : ReturnUserModelsRobot {
        return ReturnUserModelsRobot(UserModel.getUsersByFriendshipTypeBlocked()).apply { func() }
    }

    infix fun getFriendUsers(func: ReturnUserModelsRobot.() -> Unit) : ReturnUserModelsRobot {
        return ReturnUserModelsRobot(UserModel.getUsersByFriendshipTypeFriend()).apply { func() }
    }

    fun setUserPseudo(userId: Int, pseudo: String, func: ReturnUserModelRobot.() -> Unit) : ReturnUserModelRobot {
        return ReturnUserModelRobot(UserModel.initModel(userId, pseudo)).apply { func() }
    }

    fun updateUsers(users: List<UserData>) {
        var list: MutableList<UserModel> = mutableListOf()

        for (item: UserData in users) {
            list.add(initUserModel(item))
        }

        UserModel.updateUsers(list)
    }

}

class ReturnUserModelCountRobot(val count: Long) {

    fun isEqualTo(count: Long) {
        isEqual(this.count, count)
    }

}

class ReturnUserModelRobot(val userModel: UserModel) {

    fun isEqualTo(item: UserData) {
        isEqual(item!!.userId, userModel.userId)
        isEqual(item!!.email, userModel.email)
        isEqual(item!!.pseudo, userModel.pseudo)
        isEqual(item!!.details, userModel.details)
        isEqual(item!!.birthday.toString(), userModel.birthday.toString())
        isEqual(item!!.gender, userModel.gender)
//        isEqual(item!!.urlBlur, userModel.urlBlur)
//        isEqual(item!!.urlThumb, userModel.urlThumb)
//        isEqual(item!!.urlOriginal, userModel.urlOriginal)
        isEqual(item!!.isOnline, userModel.isOnline)
        isEqual(item!!.lastSeen.toString(), userModel.lastSeen.toString())
        isEqual(item!!.friendshipStatus, userModel.friendshipStatus)
        isEqual(item!!.friendshipLastAction, userModel.friendshipLastAction)
        isEqual(item!!.reported, userModel.reported)
        isEqual(item!!.updatedAt.toString(), userModel.updatedAt.toString())
    }

    fun isEqualTo(userId: Int, pseudo: String) {
        isEqual(userId, userModel.userId)
        isEqual(pseudo, userModel.pseudo)
    }

}

class ReturnUserModelsRobot(val list: List<UserModel>) {

    fun isEqualTo(listData: List<UserData>) {
        isEqual(listData.size, list.size)
        var isChecked = false

        for (itemData: UserData in listData) {
            isChecked = false
            for ((index, item) in list.withIndex()) {
                if (item!!.userId == itemData.userId) {
                    isChecked = true
                    ReturnUserModelRobot(item).isEqualTo(itemData)
                    break
                } else if (!isChecked && (list.size-1) == index) {
                    //List do not have any item like other item
                    isEqual(true, false)
                }
            }
        }
    }
}