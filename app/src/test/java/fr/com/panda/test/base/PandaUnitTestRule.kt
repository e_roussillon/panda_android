package fr.com.panda.test.base

import com.orhanobut.hawk.Hawk
import com.raizlabs.android.dbflow.config.FlowConfig
import com.raizlabs.android.dbflow.config.FlowManager
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import org.robolectric.RuntimeEnvironment

/**
 * Created by edouardroussillon on 12/31/16.
 */

class PandaUnitTestRule private constructor() : TestRule {

    override fun apply(base: Statement, description: Description): Statement {
        return object : Statement() {

            @Throws(Throwable::class)
            override fun evaluate() {
                Hawk.init(RuntimeEnvironment.application).build()
                FlowManager.init(FlowConfig.Builder(RuntimeEnvironment.application).build())
//                FlowManager.destroy()
                try {
                    base.evaluate()
                } finally {
                    FlowManager.reset()
                }
            }
        }
    }

    companion object {

        fun create(): PandaUnitTestRule {
            return PandaUnitTestRule()
        }
    }
}