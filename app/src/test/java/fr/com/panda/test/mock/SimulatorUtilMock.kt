package fr.com.panda.test.mock

import fr.com.panda.utils.SimulatorUtil
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class SimulatorUtilMock : MockUp<SimulatorUtil>() {

    companion object {
        var simulatorUtiIsRunningOnEmulator: (() -> Boolean)? = null


        @Mock
        @JvmStatic
        fun isRunningOnEmulator(): Boolean {
            return this.simulatorUtiIsRunningOnEmulator?.invoke() ?: false
        }
    }

}