package fr.com.panda.test.robot.socket

import fr.com.panda.test.utils.isEqual
import fr.com.panda.utils.PathUtil
import okhttp3.Request
import java.util.*

fun pathUnit(func: PathUtilRobot.() -> Unit) = PathUtilRobot().apply { func() }

class PathUtilRobot {
    fun setupURL(protocol: String?, domainAndPort: String?, path: String, transport: String, params: HashMap<String, String>, func: ResultPathUtilRobot.() -> Unit) : ResultPathUtilRobot {
        return ResultPathUtilRobot(PathUtil.endpointWithProtocol(protocol, domainAndPort, path, transport, params)).apply { func }
    }
}

class ResultPathUtilRobot(val request: Request) {
    fun isEqualTo(url: String) {
        isEqual(request.url().toString(), url)
    }
}