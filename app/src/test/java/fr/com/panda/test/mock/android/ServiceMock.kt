package com.rockspoon.test.mock.android

import android.app.Service
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */



class ServiceMock : MockUp<Service>() {

    var serviceSendBroadcast: ServiceSendBroadcast? = null

    @Mock
    fun `$init`() {}

    @Mock
    fun onCreate() {}

    @Mock
    fun sendBroadcast(intent: Intent) {
        if (serviceSendBroadcast != null) {
            serviceSendBroadcast?.sendBroadcast(intent)
        }
    }

    interface ServiceSendBroadcast {
        fun sendBroadcast(intent: Intent)
    }
}