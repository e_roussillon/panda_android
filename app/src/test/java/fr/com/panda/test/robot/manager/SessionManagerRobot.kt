package fr.com.panda.test.robot.manager

import br.com.concretesolutions.requestmatcher.RequestMatcherRule
import br.com.concretesolutions.requestmatcher.model.HttpMethod
import com.orhanobut.hawk.Hawk
import fr.com.panda.event.DisposableBag
import fr.com.panda.http.ApiEventGlobal
import fr.com.panda.manager.FilterManager
import fr.com.panda.manager.SessionManager
import fr.com.panda.model.ErrorResponse
import fr.com.panda.test.utils.ResultAPIError
import fr.com.panda.test.utils.doWait
import fr.com.panda.test.utils.doWaitFor
import fr.com.panda.test.utils.isEqual
import fr.com.panda.utils.Constants
import fr.com.panda.utils.KeychainUtil
import java.util.*

fun sessionManager(server: RequestMatcherRule, func: SessionManagerRobot.() -> Unit) = SessionManagerRobot(server).apply { func() }

class SessionManagerRobot(val server: RequestMatcherRule) {

    private val bag: DisposableBag = DisposableBag()
    private var isGetSessionSuccess: Boolean = false
    private var error: ErrorResponse? = null
    private var isGetSessionFailedNotFoundEmailOrPassword = false
    private var isGetSessionFailedRateLimit = false
    private var isRefreshSessionSucceed = false
    private var isGenericEventForceLogout = false

    /**
     * GENERIC
     */

    fun registeToEventLoginSuccess() {
        bag.add(SessionManager.sharedInstance.onGetSessionSucceed.add({ v ->
            this.isGetSessionSuccess = true
        }))
    }

    fun registeToEventGetLoginFailed() {
        bag.add(SessionManager.sharedInstance.onGetSessionFailed.add({ error ->
            this.error = error
        }))
    }


    /**
     * Login
     */

    fun initRequestLoginWithCode200(json: String) {
        this.server.addFixture(200, json)
                .ifRequestMatches()
                .pathIs("/api/v1/auth")
                .methodIs(HttpMethod.POST)
    }

    fun initRequestLoginWithCode422(json: String) {
        this.server.addFixture(422, json)
                .ifRequestMatches()
                .pathIs("/api/v1/auth")
                .methodIs(HttpMethod.POST)
    }

    fun initRequestLoginWithCode401(json: String) {
        this.server.addFixture(401, json)
                .ifRequestMatches()
                .pathIs("/api/v1/auth")
                .methodIs(HttpMethod.POST)
    }

    fun registeToEventGetLoginFailedNotFoundEmailOrPassword() {
        bag.add(SessionManager.sharedInstance.onGetSessionFailedNotFoundEmailOrPassword.add({ v ->
            this.isGetSessionFailedNotFoundEmailOrPassword = true
        }))
    }

    fun registeToEventGetLoginFailedRateLimit() {
        bag.add(SessionManager.sharedInstance.onGetSessionFailedRateLimit.add({ v ->
            this.isGetSessionFailedRateLimit = true
        }))
    }

    fun login(email: String, password: String) {
        SessionManager.sharedInstance.login(email, password)
        doWait(2000)
    }

    fun login(email: String, password: String, func: ResultSessionManagerRobot.() -> Unit) : ResultSessionManagerRobot {
        this.login(email, password)
        return ResultSessionManagerRobot(this.isGetSessionSuccess,
                this.isGetSessionFailedNotFoundEmailOrPassword,
                this.isGetSessionFailedRateLimit,
                this.isRefreshSessionSucceed,
                this.isGenericEventForceLogout,
                this.error).apply { func() }
    }

    /**
     * Register
     */

    fun initRequestRegisterWithCode200(json: String) {
        this.server.addFixture(200, json)
                .ifRequestMatches()
                .pathIs("/api/v1/register")
                .methodIs(HttpMethod.POST)
    }

    fun initRequestRegisterWithCode401(json: String) {
        this.server.addFixture(401, json)
                .ifRequestMatches()
                .pathIs("/api/v1/register")
                .methodIs(HttpMethod.POST)
    }

    fun register(pseudo: String, email: String, birthday: Date, password: String) {
        SessionManager.sharedInstance.register(pseudo, email, birthday, password)
        doWait(2000)
    }

    fun register(pseudo: String, email: String, birthday: Date, password: String, func: ResultSessionManagerRobot.() -> Unit) : ResultSessionManagerRobot {
        this.register(pseudo, email, birthday, password)
        return ResultSessionManagerRobot(this.isGetSessionSuccess,
                this.isGetSessionFailedNotFoundEmailOrPassword,
                this.isGetSessionFailedRateLimit,
                this.isRefreshSessionSucceed,
                this.isGenericEventForceLogout,
                this.error).apply { func() }
    }

    /**
     * Logout
     */

    fun initRequestLogoutWithCode200(json: String) {
        this.server.addFixture(200, json)
                .ifRequestMatches()
                .pathIs("/api/v1/logout")
                .methodIs(HttpMethod.POST)
    }

    fun initRequestLogoutWithCode401(json: String) {
        this.server.addFixture(401, json)
                .ifRequestMatches()
                .pathIs("/api/v1/logout")
                .methodIs(HttpMethod.POST)
    }

    infix fun logout(func: ResultSessionManagerRobot.() -> Unit) : ResultSessionManagerRobot {
        SessionManager.sharedInstance.logout()
        doWait(2000)
        return ResultSessionManagerRobot(this.isGetSessionSuccess,
                this.isGetSessionFailedNotFoundEmailOrPassword,
                this.isGetSessionFailedRateLimit,
                this.isRefreshSessionSucceed,
                this.isGenericEventForceLogout,
                this.error).apply { func() }
    }


    /**
     * Upload PushToken
     */

    fun initRequestPushTokenWithCode200(json: String) {
        this.server.addFixture(200, json)
                .ifRequestMatches()
                .pathIs("/api/v1/push_token")
                .methodIs(HttpMethod.POST)
    }

    fun initRequestPushTokenWithCode401(json: String) {
        this.server.addFixture(401, json)
                .ifRequestMatches()
                .pathIs("/api/v1/push_token")
                .methodIs(HttpMethod.POST)
    }

    fun pushToken(token: String, func: ResultSessionManagerRobot.() -> Unit) : ResultSessionManagerRobot {
        SessionManager.sharedInstance.uploadPushToken(token)
        doWait(2000)
        return ResultSessionManagerRobot(this.isGetSessionSuccess,
                this.isGetSessionFailedNotFoundEmailOrPassword,
                this.isGetSessionFailedRateLimit,
                this.isRefreshSessionSucceed,
                this.isGenericEventForceLogout,
                this.error).apply { func() }
    }



    /**
     * Update UserId
     */

    fun updateUserId(id: Int, func: ResultSessionManagerRobot.() -> Unit) : ResultSessionManagerRobot {
        SessionManager.sharedInstance.updateUserId(id)
        return ResultSessionManagerRobot(this.isGetSessionSuccess,
                this.isGetSessionFailedNotFoundEmailOrPassword,
                this.isGetSessionFailedRateLimit,
                this.isRefreshSessionSucceed,
                this.isGenericEventForceLogout,
                this.error).apply { func() }
    }

    /**
     * RefreshToken
     */

    fun addNewFilterWithError(name: String, isHighlight: Boolean, func: ResultSessionManagerRobot.() -> Unit) : ResultSessionManagerRobot {
        FilterManager.sharedInstance.addFilter(name, isHighlight)
        doWait(2000)
        return ResultSessionManagerRobot(this.isGetSessionSuccess,
                this.isGetSessionFailedNotFoundEmailOrPassword,
                this.isGetSessionFailedRateLimit,
                this.isRefreshSessionSucceed,
                this.isGenericEventForceLogout,
                this.error).apply { func() }
    }

    fun addNewFilterWithSuccess(name: String, isHighlight: Boolean) {
        FilterManager.sharedInstance.addFilter(name, isHighlight)
    }

    fun initRequestAddWithCode200(json: String) {
        this.server.addFixture(200, json)
                .ifRequestMatches()
                .pathIs("/api/v1/filters")
                .methodIs(HttpMethod.POST)
    }

    fun initRequestTokenInvalid() {
        this.server.addFixture(401, "token_invalid.json")
                .ifRequestMatches()
                .pathIs("/api/v1/filters")
                .methodIs(HttpMethod.POST)
    }

    fun initRequestTokenNotRenew() {
        this.server.addFixture(422, "token_not_renew.json")
                .ifRequestMatches()
                .pathIs("/api/v1/auth/renew")
                .methodIs(HttpMethod.POST)
    }

    fun initRequestTokenRenew() {
        this.server.addFixture(200, "token_renew.json")
                .ifRequestMatches()
                .pathIs("/api/v1/auth/renew")
                .methodIs(HttpMethod.POST)
    }

    fun registeToEventRefreshSessionSuccess() {
        bag.add(SessionManager.sharedInstance.onRefreshSessionSucceed.add({ v ->
            initRequestAddWithCode200("filter.json")
            this.isRefreshSessionSucceed = true
        }))
    }

    fun registeToEventRefreshSessionFailed() {
        bag.add(SessionManager.sharedInstance.onRefreshSessionFailed.add({ error ->
            this.error = error
        }))
    }

    fun registeToEventGenericEventForceLogout() {
        bag.add(ApiEventGlobal.sharedInstance.onGenericEventForceLogout.add({ v ->
            this.isGenericEventForceLogout = true
        }))
    }

    fun waitTokenUpdated(newToken: String) {
        doWaitFor {
            SessionManager.sharedInstance.user().token == newToken
        }
    }

    infix fun checkResult(func: ResultSessionManagerRobot.() -> Unit) : ResultSessionManagerRobot {
        return ResultSessionManagerRobot(this.isGetSessionSuccess,
                this.isGetSessionFailedNotFoundEmailOrPassword,
                this.isGetSessionFailedRateLimit,
                this.isRefreshSessionSucceed,
                this.isGenericEventForceLogout,
                this.error).apply { func() }
    }
}

class ResultSessionManagerRobot(val isGetSessionSuccess : Boolean,
                                val isGetSessionFailedNotFoundEmailOrPassword : Boolean,
                                val isGetSessionFailedRateLimit : Boolean,
                                val isRefreshSessionSucceed : Boolean,
                                val isGenericEventForceLogout: Boolean,
                                error: ErrorResponse?) : ResultAPIError(error) {

    fun isLoginRateLimit() {
        isEqual(isGetSessionFailedRateLimit, true)
        isEqual(isGetSessionSuccess, false)
        isEqual(isGetSessionFailedNotFoundEmailOrPassword, false)
        isEqual(isRefreshSessionSucceed, false)
        isEqual(isGenericEventForceLogout, false)
    }

    fun isLoginNotFound() {
        isEqual(isGetSessionFailedRateLimit, false)
        isEqual(isGetSessionSuccess, false)
        isEqual(isGetSessionFailedNotFoundEmailOrPassword, true)
        isEqual(isRefreshSessionSucceed, false)
        isEqual(isGenericEventForceLogout, false)
    }

    fun isForceLogout() {
        isEqual(isGetSessionFailedRateLimit, false)
        isEqual(isGetSessionSuccess, false)
        isEqual(isGetSessionFailedNotFoundEmailOrPassword, false)
        isEqual(isRefreshSessionSucceed, false)
        isEqual(isGenericEventForceLogout, true)
    }

    fun isLogin(token: String, refreshToken: String) {
        isEqual(isGetSessionFailedRateLimit, false)
        isEqual(isGetSessionSuccess, true)
        isEqual(isGetSessionFailedNotFoundEmailOrPassword, false)
        isEqual(isRefreshSessionSucceed, false)
        isEqual(isGenericEventForceLogout, false)

        isEqual(SessionManager.sharedInstance.user().token, token)
        isEqual(SessionManager.sharedInstance.user().tokenRefresh, refreshToken)
    }

    fun isRenewToken(token: String, refreshToken: String) {
        isEqual(SessionManager.sharedInstance.user().token, token)
        isEqual(SessionManager.sharedInstance.user().tokenRefresh, refreshToken)

        isEqual(isGetSessionFailedRateLimit, false)
        isEqual(isGetSessionSuccess, false)
        isEqual(isGetSessionFailedNotFoundEmailOrPassword, false)
        isEqual(isRefreshSessionSucceed, true)
        isEqual(isGenericEventForceLogout, false)
    }

    fun isLogout() {
        isEqual("", Hawk.get(Constants.KEYS.kTOKEN, ""))
        isEqual("", Hawk.get(Constants.KEYS.kTOKEN_REFRESH, ""))
        isEqual("", Hawk.get(Constants.KEYS.kUSER_ID, ""))
        isEqual("", Hawk.get(Constants.KEYS.kJUST_LOGIN, ""))
        isEqual("", Hawk.get(Constants.KEYS.kIMAGE_DOWNLOADED, ""))
        isEqual("", Hawk.get(Constants.KEYS.kCURRENT_LOCALISATION, ""))
        isEqual("", Hawk.get(Constants.KEYS.kFIND_ROOM, ""))
        isEqual("", Hawk.get(Constants.KEYS.kCURRENT_ROOM, ""))
        isEqual("", Hawk.get(Constants.KEYS.kCURRENT_ROOM_ID, ""))
    }

    fun isPushToken(token: String) {
        isEqual(token, Hawk.get(Constants.KEYS.kPUSH_TOKEN, ""))
    }

    fun isUserId(id: Int) {
        isEqual(id, KeychainUtil.sharedInstance.userId)
    }
}