package fr.com.panda.test.robot.manager

import br.com.concretesolutions.requestmatcher.RequestMatcherRule
import br.com.concretesolutions.requestmatcher.model.HttpMethod
import fr.com.panda.enums.ErrorType
import fr.com.panda.event.DisposableBag
import fr.com.panda.manager.FilterManager
import fr.com.panda.model.ErrorResponse
import fr.com.panda.model.FilterModel
import fr.com.panda.test.utils.ResultAPIError
import fr.com.panda.test.utils.doWait
import fr.com.panda.test.utils.isEqual

fun filterManager(server: RequestMatcherRule, func: FilterManagerRobot.() -> Unit) = FilterManagerRobot(server).apply { func() }

class FilterManagerRobot(val server: RequestMatcherRule) {

    private val bag: DisposableBag = DisposableBag()
    private var filter: FilterModel? = null
    private var isDelete: Boolean = false
    private var error: ErrorResponse? = null

    /**
     * Add Filter
     */

    fun initRequestAddWithCode200(json: String) {
        this.server.addFixture(200, json)
                .ifRequestMatches()
                .pathIs("/api/v1/filters")
                .methodIs(HttpMethod.POST)
    }

    fun initRequestAddWithParamError(json: String) {
        this.server.addFixture(422, json)
                .ifRequestMatches()
                .pathIs("/api/v1/filters")
                .methodIs(HttpMethod.POST)
    }

    fun registeToEventAddSuccess() {
        bag.add(FilterManager.sharedInstance.onAddFilterSucceed.add({ filter ->
            this.filter = filter
        }))
    }

    fun registeToEventAddFail() {
        bag.add(FilterManager.sharedInstance.onAddFilterFailed.add({ error ->
            this.error = error
        }))
    }

    fun addNewFilter(name: String, isHighlight: Boolean) {
        FilterManager.sharedInstance.addFilter(name, isHighlight)
        doWait()
    }

    fun addNewFilter(name: String, isHighlight: Boolean, func: ResultFilterManagerRobotSuccess.() -> Unit) : ResultFilterManagerRobotSuccess {
        this.addNewFilter(name, isHighlight)
        doWait(2000)
        return ResultFilterManagerRobotSuccess(this.filter).apply { func() }
    }

    fun addNewFilterWithError(name: String, isHighlight: Boolean, func: ResultAPIError.() -> Unit) : ResultAPIError {
        this.addNewFilter(name, isHighlight)
        return ResultAPIError(this.error).apply { func() }
    }

    /**
     * Update Filter
     */

    fun initRequestUpdateWithCode200(filterId: Int, json: String) {
        this.server.addFixture(200, json)
                .ifRequestMatches()
                .pathIs("/api/v1/filters/$filterId")
                .methodIs(HttpMethod.PUT)
    }

    fun initRequestUpdateWithParamError(filterId: Int, json: String) {
        this.server.addFixture(422, json)
                .ifRequestMatches()
                .pathIs("/api/v1/filters/$filterId")
                .methodIs(HttpMethod.PUT)
    }

    fun registeToEventUpdateSuccess() {
        bag.add(FilterManager.sharedInstance.onUpdateFilterSucceed.add({ filter ->
            this.filter = filter
        }))
    }

    fun registeToEventUpdateFailMissingParam() {
        bag.add(FilterManager.sharedInstance.onUpdateFilterFailedMissingParam.add({ error ->
            this.error = error
        }))
    }

//    fun registeToEventUpdateFail() {
//        bag.add(FilterManager.sharedInstance.onUpdateFilterFailed.add({ error ->
//            this.error = error
//        }))
//    }

    fun updateFilter(filterModel: FilterModel, name: String, func: ResultFilterManagerRobotSuccess.() -> Unit) : ResultFilterManagerRobotSuccess {
        FilterManager.sharedInstance.updateFilter(filterModel, name)
        doWait()
        return ResultFilterManagerRobotSuccess(this.filter).apply { func() }
    }

    fun updateFilterError(filterModel: FilterModel, name: String, func: ResultAPIError.() -> Unit) : ResultAPIError {
        FilterManager.sharedInstance.updateFilter(filterModel, name)
        doWait()
        return ResultAPIError(this.error).apply { func() }
    }

    /**
     * Remove Filter
     */

    fun initRequestRemoveWithCode200(filterId: Int, json: String) {
        this.server.addFixture(200, json)
                .ifRequestMatches()
                .pathIs("/api/v1/filters/$filterId")
                .methodIs(HttpMethod.DELETE)
    }

    fun initRequestRemoveWithParamError(filterId: Int, json: String) {
        this.server.addFixture(422, json)
                .ifRequestMatches()
                .pathIs("/api/v1/filters/$filterId")
                .methodIs(HttpMethod.DELETE)
    }

    fun registeToEventRemoveSuccess() {
        bag.add(FilterManager.sharedInstance.onRemoveFilterSucceed.add({ empty ->
            this.isDelete = true
        }))
    }

    fun registerToEventRemoveFail() {
        bag.add(FilterManager.sharedInstance.onRemoveFilterFailed.add({ error ->
            this.error = error
        }))
    }

    fun removeFilter(filterModel: FilterModel, func: ResultFilterEmptyManagerRobotSuccess.() -> Unit) : ResultFilterEmptyManagerRobotSuccess {
        FilterManager.sharedInstance.removeFilter(filterModel)
        doWait()
        return ResultFilterEmptyManagerRobotSuccess(this.isDelete).apply { func() }
    }

    fun removeFilterFail(filterModel: FilterModel, func: ResultAPIError.() -> Unit) : ResultAPIError {
        FilterManager.sharedInstance.removeFilter(filterModel)
        doWait()
        return ResultAPIError(this.error).apply { func() }
    }
}

class ResultFilterManagerRobotSuccess(val filter: FilterModel?) {
    fun isFilterEqual(name: String, isHighlight: Boolean) {
        isEqual(true, (filter != null))
        isEqual(filter!!.name, name)
        isEqual(filter!!.isHighlight, isHighlight)
    }

    fun isFilterEqual(dbId: Int) {
        val filterModel = FilterModel.getFilter(dbId)

        isEqual(true, (filterModel != null))
        isEqual(filter!!.name, filterModel.name)
        isEqual(filter!!.isHighlight, filterModel.isHighlight)
        isEqual(filter!!.filterId, filterModel.filterId)
        isEqual(filter!!.timestamp.toString(), filterModel.timestamp.toString())
    }
}

class ResultFilterEmptyManagerRobotSuccess(val isDelete: Boolean) {

    fun isFilterDeleted() {
        val listHighlight = FilterModel.getFilters(true)
        val listNotHighlight = FilterModel.getFilters(false)

        isEqual(true, isDelete)
        isEqual(0, listHighlight.count())
        isEqual(0, listNotHighlight.count())
    }

}
