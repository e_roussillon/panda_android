package fr.com.panda.test.mock.android

import android.app.Activity
import android.support.v4.app.ActivityCompat
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class ActivityCompatMock : MockUp<ActivityCompat>() {
    companion object {
        var activityCompatRequestPermissions: ((Activity, Array<String>, Int) -> Unit)? = null
        var activityCompatShouldShowRequestPermissionRationale: ((Activity, String) -> Boolean)? = null

        @Mock
        @JvmStatic
        fun requestPermissions(activity: Activity, permissions: Array<String>, requestCode: Int) {
            activityCompatRequestPermissions?.invoke(activity, permissions, requestCode)
        }

        @Mock
        @JvmStatic
        fun shouldShowRequestPermissionRationale(activity: Activity, permission: String): Boolean {
            return activityCompatShouldShowRequestPermissionRationale?.invoke(activity, permission) ?: false
        }
    }
}