package com.rockspoon.test.mock.android

import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.IntentFilter
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class ContextWrapperMock : MockUp<ContextWrapper>() {
    @Mock
    fun `$init`(base: Context?) {}

    @Mock
    fun startService(intent: Intent) : ComponentName? {
        return ContextMock.contextStartService?.invoke(intent)
    }

    @Mock
    fun stopService(intent: Intent) : Boolean {
        return ContextMock.contextStopService?.invoke(intent) ?: false
    }

    @Mock
    fun registerReceiver(receiver: BroadcastReceiver, filter: IntentFilter): Intent? {
        return ContextMock.contextRegisterReceiver?.invoke(receiver, filter)
    }

    @Mock
    fun sendBroadcast(intent: Intent) {
        ContextMock.contextSendBroadcast?.invoke(intent)
    }
}