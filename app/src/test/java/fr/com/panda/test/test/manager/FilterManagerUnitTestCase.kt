package fr.com.panda.test.test.manager

import fr.com.panda.enums.ErrorType
import fr.com.panda.model.FilterModel
import fr.com.panda.test.base.PandaUnitTestCase
import fr.com.panda.test.robot.db.filterModel
import fr.com.panda.test.robot.manager.filterManager
import org.junit.Test
import java.util.*

class FilterManagerUnitTestCase : PandaUnitTestCase() {

    /**
     * Add Filter
     */

    @Test
    fun filter_addNewFilterWithSuccess() {
        val name = "name filter"
        val isHighlight = true
        val id = 42

        filterManager(this.server) {
            registeToEventAddSuccess()
            initRequestAddWithCode200("filter.json")
        }.addNewFilter(name, isHighlight) {
            isFilterEqual(name, isHighlight)
            isFilterEqual(id)
        }
    }

    @Test
    fun filter_addNewFilterWithFail422() {
        val name = ""
        val isHighlight = true

        filterManager(this.server) {
            registeToEventAddFail()
            initRequestAddWithParamError("filter_param_error.json")
        }.addNewFilterWithError(name, isHighlight) {
            isErrorEqual(ErrorType.CHANGESET, 0, "Parameter Error")
            filterModel {
            }.getFilters(isHighlight) {
                isEmptyList()
            }
            filterModel {
            }.getFilters(!isHighlight) {
                isEmptyList()
            }
        }
    }

    /**
     * Update Filter
     */

    @Test
    fun filter_addFilterThenUpdateFilterWithSuccess() {
        val name = "name filter"
        val isHighlight = true
        val nameUpdated = "updated filter"
        val isHighlightUpdated = !isHighlight
        val id = 42

        val filter = FilterModel(id, nameUpdated, isHighlightUpdated, Date())

        filterManager(this.server) {
            registeToEventAddSuccess()
            initRequestAddWithCode200("filter.json")
            addNewFilter(name, isHighlight)
            registeToEventUpdateSuccess()
            initRequestUpdateWithCode200(id, "update_filter.json")
        }.updateFilter(filter, nameUpdated) {
            isFilterEqual(nameUpdated, isHighlightUpdated)
            isFilterEqual(id)
        }
    }

    fun filter_addFilterThenUpdateFilterWithFail() {
        val name = "name filter"
        val isHighlight = true
        val nameUpdated = "updated filter"
        val isHighlightUpdated = !isHighlight
        val id = 42

        val filter = FilterModel(id, nameUpdated, isHighlightUpdated, Date())

        filterManager(this.server) {
            registeToEventAddSuccess()
            initRequestAddWithCode200("filter.json")
            addNewFilter(name, isHighlight)
            registeToEventUpdateFailMissingParam()
            initRequestUpdateWithParamError(id, "filter_param_error.json")
        }.updateFilterError(filter, nameUpdated) {
            isErrorEqual(ErrorType.CHANGESET, 0, "Parameter Error")
            filterModel {
            }.getFilter(id) {
                isEqualTo(id, name, isHighlight, filter.timestamp)
            }
        }
    }

    /**
     * Remove Filter
     */

    @Test
    fun filter_addFilterThenRemoveFilterWithSuccess() {
        val name = "name filter"
        val isHighlight = true
        val id = 42

        val filter = FilterModel(id, "", true, Date())

        filterManager(this.server) {
            registeToEventAddSuccess()
            initRequestAddWithCode200("filter.json")
            addNewFilter(name, isHighlight)
            registeToEventRemoveSuccess()
            initRequestRemoveWithCode200(id, "empty.json")
        }.removeFilter(filter) {
            isFilterDeleted()
        }
    }

    @Test
    fun filter_addFilterThenRemoveFilterWithFail() {
        val name = "name filter"
        val isHighlight = true
        val id = 42

        val filter = FilterModel(id, "", true, Date())

        filterManager(this.server) {
            registeToEventAddSuccess()
            initRequestAddWithCode200("filter.json")
            addNewFilter(name, isHighlight)
            registerToEventRemoveFail()
            initRequestRemoveWithParamError(id, "filter_param_error.json")
        }.removeFilterFail(filter) {
            isErrorEqual(ErrorType.CHANGESET, 0, "Parameter Error")
            filterModel {
            }.getFilter(id) {
                isEqualTo(id, name, isHighlight)
            }
        }
    }

}