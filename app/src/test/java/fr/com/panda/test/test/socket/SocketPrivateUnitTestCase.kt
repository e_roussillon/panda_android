package fr.com.panda.test.test.socket

import fr.com.panda.test.base.PandaWSUnitTestCase
import fr.com.panda.test.robot.socket.socketPrivateRobot
import fr.com.panda.test.utils.doWait
import org.junit.Test

/**
 * @author Edouard Roussillon
 */

class SocketPrivateUnitTestCase : PandaWSUnitTestCase() {

    @Test
    fun connectToPrivateRoom() {
        socketPrivateRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initJoinPrivateRoom()
            registerJoinedChannelPrivate()
            registerGetSessionSucceed()
            loginUser("dovi@gmail.com", "123456")
            waitForResultJoinPrivateRoom()
            waitForResultJoinPrivateRoom()
        }.checkResult {
            isJoinPrivateRoom()
            isFilterPendingResultSet()
        }
    }

    @Test
    fun connectToPrivateRoomAndWaitHeartbeat() {
        socketPrivateRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initJoinPrivateRoom()
            initHeartbeat()
            registerJoinedChannelPrivate()
            registerGetSessionSucceed()
            loginUser("dovi@gmail.com", "123456")
            waitForResultHeartbeatUsed()
        }.checkResult {
            isJoinPrivateRoom()
        }
    }

    @Test
    fun connectToPrivateRoomAndSendDuplicateUser() {
        socketPrivateRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initRequestLogoutWithCode200()
            initJoinPrivateRoom()
            registerJoinedChannelPrivate()
            registerGetSessionSucceed()
            registerShowSplashScreen()
            loginUser("dovi@gmail.com", "123456")
            waitForResultJoinPrivateRoom()
            sendDuplicate()
            waitForResultIsShowSplashScreen()
        }.checkResult {
            isUserLogout()
            isShowSplashScreen()
            isSocketDisconnected()
        }
    }

    @Test
    fun connectToPrivateRoomAndAskFriend() {
        socketPrivateRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initJoinPrivateRoom()
            registerJoinedChannelPrivate()
            registerGetSessionSucceed()
            registerUpdateFriendship()
            loginUser("dovi@gmail.com", "123456")
            waitForResultJoinPrivateRoom()
            askedFriendship()
            waitForResultIsUpdateFriendship()
        }.checkResult {
            isFriendshipFound()
            isUpdateFriendship()
        }
    }

    @Test
    fun connectToPrivateRoomAndUserConnected() {
        socketPrivateRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initJoinPrivateRoom()
            registerJoinedChannelPrivate()
            registerGetSessionSucceed()
            registerUsersConnected()
            loginUser("dovi@gmail.com", "123456")
            waitForResultJoinPrivateRoom()
            sendUsersConnect()
            waitForResultUserConnected()
        }.checkResult {
            isFriendsConnected(true)
        }
    }

    @Test
    fun connectToPrivateRoomAndUserDisconnect() {
        socketPrivateRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initJoinPrivateRoom()
            registerJoinedChannelPrivate()
            registerGetSessionSucceed()
            registerUsersConnected()
            registerNewDisconnection()
            loginUser("dovi@gmail.com", "123456")
            waitForResultJoinPrivateRoom()
            sendUsersConnect()
            waitForResultUserConnected()
            sendUserDisconnected()
            waitForResultUserDisconnected()
        }.checkResult {
            isFriendsConnected(false)
            isNewConnection()
            isNewDisconnection()
        }
    }

    @Test
    fun connectToPrivateRoomAndUserDisconnectAndUserConnected() {
        socketPrivateRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initJoinPrivateRoom()
            registerJoinedChannelPrivate()
            registerGetSessionSucceed()
            registerUsersConnected()
            registerNewDisconnection()
            loginUser("dovi@gmail.com", "123456")
            waitForResultJoinPrivateRoom()
            sendUsersConnect()
            waitForResultUserConnected()
            sendUserDisconnected()
            waitForResultUserDisconnected()
            sendUserConnected()
            waitForResultUserConnected()
        }.checkResult {
            isFriendsConnected(true)
            isNewConnection()
            isNewDisconnection()
        }
    }

    @Test
    fun connectToPrivateRoomAndGetReported() {
        socketPrivateRobot(this.bag, this.realWebSocketWithMock, this.server) {
            initRequestLoginWithCode200()
            initRequestBackupWithCode200()
            initRequestLogoutWithCode200()
            initJoinPrivateRoom()
            registerJoinedChannelPrivate()
            registerGetSessionSucceed()
            registerUserReported()
            loginUser("dovi@gmail.com", "123456")
            waitForResultJoinPrivateRoom()
            sendReported()
            waitForResultUserReported()
        }.checkResult {
            isUserLogout()
            isUserReported(600)
            isSocketDisconnected()
        }
    }

}