package com.rockspoon.test.mock.android

import mockit.Mock
import mockit.MockUp
import java.lang.String

/**
 * @author Edouard Roussillon
 */

class SystemMock : MockUp<System>() {

    @Mock
    fun loadLibrary(libname: String) {
    }

}