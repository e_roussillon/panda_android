package fr.com.panda.test.robot.db

import fr.com.panda.model.FilterModel
import fr.com.panda.test.utils.isEqual
import java.util.*

/**
 * Created by edouardroussillon on 12/31/16.
 */

fun filterModel(func: FilterModelRobots.() -> Unit) = FilterModelRobots().apply { func() }

data class FilterData(var filterId: Int, var name: String, var isHighlight: Boolean, var timestamp: Date)

fun initFilterModel(filterData: FilterData): FilterModel {
    return FilterModel(
            filterData.filterId,
            filterData.name,
            filterData.isHighlight,
            filterData.timestamp
    )
}

class FilterModelRobots {

    fun insert(filterData: FilterData) {
        initFilterModel(filterData).insert()
    }

    fun remove(filterId: Int) {
        FilterModel(filterId, "", true, Date()).remove()
    }

    fun getFilters(isHighlight: Boolean, func: ResultFiltersModelRobots.() -> Unit) : ResultFiltersModelRobots {
        return ResultFiltersModelRobots(FilterModel.getFilters(isHighlight)).apply { func() }
    }

    fun getFilter(filterId: Int, func: ResultFilterModelRobots.() -> Unit) : ResultFilterModelRobots {
        return ResultFilterModelRobots(FilterModel.getFilter(filterId)).apply { func() }
    }
}

class ResultFiltersModelRobots(val list: List<FilterModel>) {
    fun isSingleResult(filterId: Int, name: String, isHighlight: Boolean, timestamp: Date) {
        isEqual(list.size, 1)
        ResultFilterModelRobots(list[0]).isEqualTo(filterId, name, isHighlight, timestamp)
    }

    fun isEmptyList() {
        isEqual(list.size, 0)
    }
}

class ResultFilterModelRobots(val item: FilterModel?) {
    fun isEqualTo(filterId: Int, name: String, isHighlight: Boolean, timestamp: Date) {
        isEqual(item!!.filterId, filterId)
        isEqual(item!!.name, name)
        isEqual(item!!.isHighlight, isHighlight)
        isEqual(item!!.timestamp.toString(), timestamp.toString())
    }

    fun isEqualTo(filterId: Int, name: String, isHighlight: Boolean) {
        isEqual(item!!.filterId, filterId)
        isEqual(item!!.name, name)
        isEqual(item!!.isHighlight, isHighlight)
    }

    fun isEmpty() {
        isEqual((item == null), true)
    }
}
