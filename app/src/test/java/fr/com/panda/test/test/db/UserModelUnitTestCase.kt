package fr.com.panda.test.test.db

import fr.com.panda.enums.FriendshipType
import fr.com.panda.enums.GenderType
import fr.com.panda.test.base.PandaUnitTestCase
import fr.com.panda.test.robot.db.UserData
import fr.com.panda.test.robot.db.userModelRobot
import fr.com.panda.utils.DateUtil
import org.junit.Test
import java.net.URL
import java.util.*

/**
 * Created by edouardroussillon on 1/8/17.
 */

class UserModelUnitTestCase : PandaUnitTestCase() {


    @Test
    fun testInsertUser_getCurrentUser() {

        val user = UserData(-1,
                "user@gmail.com",
                "user",
                "hello here the details",
                DateUtil.getStringToSmallDate("10-10-1990"),
                GenderType.MALE,
                URL("http://google.com"),
                URL("http://google.com"),
                URL("http://google.com"),
                true,
                Date(),
                FriendshipType.NOT_FRIEND,
                0,
                true,
                Date());

        userModelRobot {
            insertUser(user)
        } getCurrentUser {
            isEqualTo(user)
        }
    }

    @Test
    fun testInsertUser_getUserById() {
        val userId = 1
        val user = UserData(userId,
                "user@gmail.com",
                "user",
                "hello here the details",
                DateUtil.getStringToSmallDate("10-10-1990"),
                GenderType.MALE,
                URL("http://google.com"),
                URL("http://google.com"),
                URL("http://google.com"),
                true,
                Date(),
                FriendshipType.NOT_FRIEND,
                0,
                true,
                Date());

        userModelRobot {
            insertUser(user)
        }.getUserById(userId) {
            isEqualTo(user)
        }
    }

    @Test
    fun testUpdateStatusUser_getUserById() {
        val userId = 1
        val email = "user@gmail.com"
        val pseudo = "user"
        val details = "hello here the details"
        var birthday = DateUtil.getStringToSmallDate("10-10-1990")
        var gender = GenderType.MALE
        var isOnline = true
        var lastSeen = Date()
        var friendshipStatus = FriendshipType.NOT_FRIEND
        var friendshipLastAction = 0
        var reported = true
        var updatedAt = Date()


        val user = UserData(userId,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                friendshipStatus,
                friendshipLastAction,
                reported,
                updatedAt);

        val pseudo2 = "user2"
        var friendshipStatus2 = FriendshipType.BLOCKED_AFTER_BE_FRIEND
        var friendshipLastAction2 = 1

        val user2 = UserData(userId,
                email,
                pseudo2,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                friendshipStatus2,
                friendshipLastAction2,
                reported,
                updatedAt);

        userModelRobot {
            insertUser(user)
            updateUserFriendship(userId, pseudo2, friendshipStatus2, friendshipLastAction2)
        }.getUserById(userId) {
            isEqualTo(user2)
        }
    }

    @Test
    fun testInsertUsers_getCountPendingUser() {
        val email = "user@gmail.com"
        val pseudo = "user"
        val details = "hello here the details"
        var birthday = DateUtil.getStringToSmallDate("10-10-1990")
        var gender = GenderType.MALE
        var isOnline = true
        var lastSeen = Date()
        var friendshipStatus = FriendshipType.PENDING
        var reported = true
        var updatedAt = Date()


        val user1 = UserData(-1,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                friendshipStatus,
                1,
                reported,
                updatedAt);

        val user2 = UserData(1,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                friendshipStatus,
                -1,
                reported,
                updatedAt);

        val user3 = UserData(2,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                friendshipStatus,
                1,
                reported,
                updatedAt);

        userModelRobot {
            insertUser(user1)
            insertUser(user2)
            insertUser(user3)
        } getCountPendingUsers {
            isEqualTo(1)
        }
    }

    @Test
    fun testInsertUsers_getPendingUser() {
        val email = "user@gmail.com"
        val pseudo = "user"
        val details = "hello here the details"
        var birthday = DateUtil.getStringToSmallDate("10-10-1990")
        var gender = GenderType.MALE
        var isOnline = true
        var lastSeen = Date()
        var reported = true
        var updatedAt = Date()


        val user1 = UserData(-1,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                FriendshipType.PENDING,
                1,
                reported,
                updatedAt);

        val user2 = UserData(1,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                FriendshipType.PENDING,
                -1,
                reported,
                updatedAt);

        userModelRobot {
            insertUser(user1)
            insertUser(user2)
        } getPendingUsers {
            isEqualTo(listOf(user2))
        }
    }

    @Test
    fun testInsertUsers_getBlockedUsers() {
        val email = "user@gmail.com"
        val pseudo = "user"
        val details = "hello here the details"
        var birthday = DateUtil.getStringToSmallDate("10-10-1990")
        var gender = GenderType.MALE
        var isOnline = true
        var lastSeen = Date()
        var reported = true
        var updatedAt = Date()


        val user1 = UserData(1,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                FriendshipType.BLOCKED_AFTER_BE_FRIEND,
                -1,
                reported,
                updatedAt);

        val user2 = UserData(2,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                FriendshipType.BLOCKED_BEFORE_BE_FRIEND,
                -1,
                reported,
                updatedAt);

        val user3 = UserData(-1,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                FriendshipType.BLOCKED_BEFORE_BE_FRIEND,
                -1,
                reported,
                updatedAt);

        userModelRobot {
            insertUser(user1)
            insertUser(user2)
            insertUser(user3)
        } getBlockedUsers {
            isEqualTo(listOf(user1, user2))
        }
    }

    @Test
    fun testInsertUsers_getFriendUsers() {
        val email = "user@gmail.com"
        val pseudo = "user"
        val details = "hello here the details"
        var birthday = DateUtil.getStringToSmallDate("10-10-1990")
        var gender = GenderType.MALE
        var isOnline = true
        var lastSeen = Date()
        var reported = true
        var updatedAt = Date()


        val user1 = UserData(-1,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                FriendshipType.FRIEND,
                -1,
                reported,
                updatedAt);

        val user2 = UserData(1,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                FriendshipType.FRIEND,
                -1,
                reported,
                updatedAt);

        userModelRobot {
            insertUser(user1)
            insertUser(user2)
        } getFriendUsers {
            isEqualTo(listOf(user2))
        }
    }

    @Test
    fun testUpdateUser_getUser() {
        val userId = 1
        val email = "user@gmail.com"
        val pseudo = "user"
        val details = "hello here the details"
        var birthday = DateUtil.getStringToSmallDate("10-10-1990")
        var gender = GenderType.MALE
        var isOnline = true
        var lastSeen = Date()
        var reported = true
        var updatedAt = Date()


        val user1 = UserData(userId,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                FriendshipType.FRIEND,
                -1,
                reported,
                updatedAt)

        val user2 = UserData(userId,
                email,
                "user_updated",
                "hello",
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                FriendshipType.BLOCKED_BEFORE_BE_FRIEND,
                -1,
                reported,
                updatedAt)

        userModelRobot {
            insertUser(user1)
            updateUsers(listOf(user2))
        }.getUserById(userId) {
            isEqualTo(user2)
        }
    }

    @Test
    fun testInsertUser_insertUser() {
        val userId = 1
        val pseudo = "user"

        userModelRobot {
        }.setUserPseudo(userId, pseudo) {
            isEqualTo(userId, pseudo)
        }
    }
}