package fr.com.panda.test.test.manager

import fr.com.panda.enums.ErrorType
import fr.com.panda.enums.FriendshipType
import fr.com.panda.enums.GenderType
import fr.com.panda.test.base.PandaUnitTestCase
import fr.com.panda.test.robot.db.UserData
import fr.com.panda.test.robot.db.userModelRobot
import fr.com.panda.test.robot.manager.friendshipManager
import fr.com.panda.utils.DateUtil
import org.junit.Test
import java.util.*

class FriendshipManagerUnitTestCase : PandaUnitTestCase() {

    @Test
    fun friendship_getFriends() {
    val email = "user@gmail.com"
    val pseudo = "user"
    val details = "hello here the details"
    var birthday = DateUtil.getStringToSmallDate("10-10-1990")
    var gender = GenderType.MALE
    var isOnline = true
    var lastSeen = Date()
    var reported = true
    var updatedAt = Date()


    val user1 = UserData(-1,
            email,
            pseudo,
            details,
            birthday,
            gender,
            null,
            null,
            null,
            isOnline,
            lastSeen,
            FriendshipType.FRIEND,
            -1,
            reported,
            updatedAt);

    val user2 = UserData(1,
            email,
            pseudo,
            details,
            birthday,
            gender,
            null,
            null,
            null,
            isOnline,
            lastSeen,
            FriendshipType.FRIEND,
            -1,
            reported,
            updatedAt);

        friendshipManager(this.server) {
            userModelRobot {
                insertUser(user1)
                insertUser(user2)
            }
        } getUsersByFriendshipTypeFriend {
            isEqualTo(listOf(user2))
        }
    }

//    getUsersByFriendshipTypeBlocked

    @Test
    fun friendship_getBlocked() {
        val email = "user@gmail.com"
        val pseudo = "user"
        val details = "hello here the details"
        var birthday = DateUtil.getStringToSmallDate("10-10-1990")
        var gender = GenderType.MALE
        var isOnline = true
        var lastSeen = Date()
        var reported = true
        var updatedAt = Date()


        val user1 = UserData(1,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                FriendshipType.BLOCKED_AFTER_BE_FRIEND,
                -1,
                reported,
                updatedAt)

        val user2 = UserData(2,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                FriendshipType.BLOCKED_BEFORE_BE_FRIEND,
                -1,
                reported,
                updatedAt)

        val user3 = UserData(-1,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                FriendshipType.BLOCKED_BEFORE_BE_FRIEND,
                -1,
                reported,
                updatedAt)

        friendshipManager(this.server) {
            userModelRobot {
                insertUser(user1)
                insertUser(user2)
                insertUser(user3)
            }
        } getUsersByFriendshipTypeBlocked  {
            isEqualTo(listOf(user1, user2))
        }
    }

    @Test
    fun friendship_getPending() {
        val email = "user@gmail.com"
        val pseudo = "user"
        val details = "hello here the details"
        var birthday = DateUtil.getStringToSmallDate("10-10-1990")
        var gender = GenderType.MALE
        var isOnline = true
        var lastSeen = Date()
        var friendshipStatus = FriendshipType.PENDING
        var reported = true
        var updatedAt = Date()


        val user1 = UserData(-1,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                friendshipStatus,
                1,
                reported,
                updatedAt);

        val user2 = UserData(1,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                friendshipStatus,
                -1,
                reported,
                updatedAt);

        val user3 = UserData(2,
                email,
                pseudo,
                details,
                birthday,
                gender,
                null,
                null,
                null,
                isOnline,
                lastSeen,
                friendshipStatus,
                1,
                reported,
                updatedAt);

        friendshipManager(this.server) {
            userModelRobot {
                insertUser(user1)
                insertUser(user2)
                insertUser(user3)
            }
        } getUsersByFriendshipTypePending  {
            isEqualTo(listOf(user2, user3))
        }
    }

    @Test
    fun friendship_updateFriendWithSuccess() {
        val user1 = 2
        val status: Int = FriendshipType.PENDING

        friendshipManager(this.server) {
            registeToEventFriendshipSuccess()
            initRequestUpdateFriendshipWithCode200("user.json")
        }.updateFriendship(user1, status) {
            isEqualTo(user1, status)
        }
    }

    @Test
    fun friendship_updateFriendWithFail() {
        val user1 = 2
        val status: Int = FriendshipType.PENDING

        friendshipManager(this.server) {
            registeToEventFriendshipFailed()
            initRequestUpdateFriendshipWithCode401("token_not_renew.json")
        }.updateFriendship(user1, status) {
            isErrorEqual(ErrorType.AUTH, 3, "token not renew")
        }
    }

    @Test
    fun friendship_updateFriendWithFailedUserIdNotFound() {
        val user1 = -1
        val status: Int = FriendshipType.PENDING

        friendshipManager(this.server) {
            registeToEventFriendshipFailedUserIdNotFound()
        }.updateFriendship(user1, status) {
            isUpdateFriendshipFailedUserIdNotFound()
        }
    }


}