package fr.com.panda.test.robot.socket

import android.content.Context
import android.content.res.Resources
import br.com.concretesolutions.requestmatcher.RequestMatcherRule
import br.com.concretesolutions.requestmatcher.model.HttpMethod
import fr.com.panda.enums.StatusType
import fr.com.panda.event.DisposableBag
import fr.com.panda.manager.BackupManager
import fr.com.panda.manager.ChatManager
import fr.com.panda.manager.SessionManager
import fr.com.panda.manager.SocketManager
import fr.com.panda.model.PublicMessageModel
import fr.com.panda.socket.notification.SocketNotificaterManager
import fr.com.panda.test.mock.RealWebSocketWithMock
import fr.com.panda.test.mock.ResourcesMock
import fr.com.panda.test.utils.*
import org.robolectric.RuntimeEnvironment
import org.mockito.Mockito.`when`
import org.mockito.Mockito.spy
import org.mockito.Mockito.`when`
import org.mockito.Mockito.`when`
import org.mockito.Mockito.`when`
import org.robolectric.Robolectric
import android.app.Activity
import fr.com.panda.model.Location
import fr.com.panda.ui.home.HomeActivity


/**
 * @author Edouard Roussillon
 */

fun socketPublicRobot(bag: DisposableBag, realWebSocketWithMock: RealWebSocketWithMock, server: RequestMatcherRule, func: SocketPublicRobot.() -> Unit) = SocketPublicRobot(bag, realWebSocketWithMock, server).apply { func() }

class SocketPublicRobot(val bag: DisposableBag, val realWebSocketWithMock: RealWebSocketWithMock, val server: RequestMatcherRule) {

    private val sessionManager: SessionManager = SessionManager.sharedInstance
    private val chatManager: ChatManager = ChatManager.sharedInstance
    private val backupManager: BackupManager = BackupManager.sharedInstance
    private val socketNotificaterManager: SocketNotificaterManager = SocketNotificaterManager.sharedInstance
    private val socketManager: SocketManager = SocketManager.sharedInstance
    private var isJoinedChannelGeneral: Boolean = false
    private var countJoinedChannelBroadcast: Int = 0
    private var isJoinedChannelSale: Boolean = false
    private var newGeneralMessage: PublicMessageModel? = null
    private var newSaleMessage: PublicMessageModel? = null
    private var newBroadcastMessage: PublicMessageModel? = null
    lateinit private var spiedResources: Resources


    fun initGSPLocalistation() {
        SessionManager.sharedInstance.user().currentLocalisation = Location(-23.515599, -46.189608)
    }

    fun registerGetSessionSucceed() {
        bag.add(sessionManager.onGetSessionSucceed.add { result ->
            backupManager.backup()
        })
    }

    fun registerBackupSucceed() {
        bag.add(backupManager.onBackupSucceed.add { result ->
            chatManager.findPublicRoom()
        })
    }

    fun registerFindPublicRoomSucceed() {
        bag.add(chatManager.onFindPublicRoomSucceed.add { result ->
            socketManager.joinRooms()
        })
    }

    fun registerJoinedChannelGeneral() {
        bag.add(socketNotificaterManager.onJoinedChannelGeneral.add {
            isJoinedChannelGeneral = true
        })
    }

    fun registerJoinedChannelBroadcast() {
        bag.add(socketNotificaterManager.onJoinedChannelBroadcast.add {
            countJoinedChannelBroadcast++
        })
    }

    fun registerJoinedChannelSale() {
        bag.add(socketNotificaterManager.onJoinedChannelSale.add {
            isJoinedChannelSale = true
        })
    }

    fun registerNewGeneralMessageNotification() {
        bag.add(socketNotificaterManager.onNewGeneralMessageNotification.add { message ->
            newGeneralMessage = message
        })
    }

    fun registerNewSaleMessageNotification() {
        bag.add(socketNotificaterManager.onNewSaleMessageNotification.add { message ->
            newSaleMessage = message
        })
    }

    fun registerNewBroadcastMessageNotification() {
        bag.add(socketNotificaterManager.onNewBroadcastMessageNotification.add { message ->
            newBroadcastMessage = message
        })
    }

    fun registerWelcomeMessage(messageId: Int, message: String) {
        ResourcesMock.resourcesGetText = { id ->
            if (messageId == id) message else  ""
        }
    }


    fun loginUser(email: String, password: String) {
        sessionManager.login(email, password)
    }

    fun sendMessage(isGeneral: Boolean, message: String) {
        socketManager.sendPublicMessage(isGeneral, message)
    }

    fun sendMessageBroadcast(isGeneral: Boolean) {
        if (isGeneral) {
            this.realWebSocketWithMock.sendFile("received_broadcast_general.json")
        } else {
            this.realWebSocketWithMock.sendFile("received_broadcast_sale.json")
        }
    }

    fun sendWelcomeMessage(isGeneral: Boolean) {
        socketManager.sendPublicMessageWelcome(RuntimeEnvironment.application, isGeneral)
    }

    fun waitForResultNewGeneralMessage() {
        doWaitFor {
            this.newGeneralMessage != null
        }
    }

    fun waitForResultNewSaleMessage() {
        doWaitFor {
            this.newSaleMessage != null
        }
    }

    fun waitForResultNewBroadcastMessage() {
        doWaitFor {
            this.newBroadcastMessage != null
        }
    }

    fun waitForResult(isJoinedChannelGeneral: Boolean,
                      countJoinedChannelBroadcast: Int,
                      isJoinedChannelSale: Boolean) {
        doWaitFor {
            this.isJoinedChannelGeneral == isJoinedChannelGeneral && this.countJoinedChannelBroadcast == countJoinedChannelBroadcast && this.isJoinedChannelSale == isJoinedChannelSale
        }
    }

    infix fun checkResult(func: SocketPublicResult.() -> Unit) : SocketPublicResult {
        return SocketPublicResult(isJoinedChannelGeneral, countJoinedChannelBroadcast, isJoinedChannelSale, newGeneralMessage, newSaleMessage, newBroadcastMessage).apply { func() }
    }

    //
    // Request
    //

    fun initRequestLoginWithCode200() {
        this.server.addFixture(200, "login.json")
                .ifRequestMatches()
                .pathIs("/api/v1/auth")
                .methodIs(HttpMethod.POST)
    }

    fun initRequestBackupWithCode200() {
        this.server.addFixture(200, "backup.json")
                .ifRequestMatches()
                .pathIs("/api/v1/auth/backup")
                .methodIs(HttpMethod.GET)
    }

    fun initRequestFindRoomWithCode200() {
        this.server.addFixture(200, "room_find_socket.json")
                .ifRequestMatches()
                .pathIs("/api/v1/find_room")
                .methodIs(HttpMethod.POST)
    }

    fun initJoinPrivateRoom() {
        this.realWebSocketWithMock.addFiles("join_private_channel.json", "join_private_channel_response.json")
    }

    fun initJoinGeneralRoom() {
        this.realWebSocketWithMock.addFiles("join_public_general_channel.json", "join_public_general_channel_response.json")
    }

    fun initJoinSaleRoom() {
        this.realWebSocketWithMock.addFiles("join_public_sale_channel.json", "join_public_sale_channel_response.json")
    }

    fun initJoinBroadcastRoom() {
        this.realWebSocketWithMock.addFiles("join_public_broadcast_1_channel.json", "join_public_broadcast_1_channel_response.json")
        this.realWebSocketWithMock.addFiles("join_public_broadcast_2_channel.json", "join_public_broadcast_2_channel_response.json")
        this.realWebSocketWithMock.addFiles("join_public_broadcast_3_channel.json", "join_public_broadcast_3_channel_response.json")
    }

    fun initSendMessageGeneral() {
        this.realWebSocketWithMock.addPublicMessageFiles("send_general_message.json", "send_general_message_response.json")
    }

    fun initSendMessageSale() {
        this.realWebSocketWithMock.addPublicMessageFiles("send_sale_message.json", "send_sale_message_response.json")
    }

}

class SocketPublicResult(val isJoinedChannelGeneral: Boolean,
                         val countJoinedChannelBroadcast: Int,
                         val isJoinedChannelSale: Boolean,
                         val newGeneralMessage: PublicMessageModel?,
                         val newSaleMessage: PublicMessageModel?,
                         val newBroadcastMessage: PublicMessageModel?) {



    fun isJoinedChannelGeneral() {
        isTrue(isJoinedChannelGeneral)
    }

    fun isJoinedChannelBroadcast() {
        isEqual(countJoinedChannelBroadcast, 3)
    }

    fun isJoinedChannelSale() {
        isTrue(isJoinedChannelSale)
    }

    fun isMessageSave(isGeneral: Boolean, message: String, @StatusType.StatusMode status: Int) {
        var list: List<PublicMessageModel>
        if (isGeneral) {
            list = PublicMessageModel.getAllPublicMessageGeneral()
        } else {
            list = PublicMessageModel.getAllPublicMessageSale()
        }

        isNotNull(list)
        isEqual(list.count(), 1)
        isEqual(list[0].msg, message)
        isEqual(list[0].status, status)
        isFalse(list[0].isWelcomeMessage)
    }

    fun isMessageBroadcast(isGeneral: Boolean) {
        var list: List<PublicMessageModel>
        if (isGeneral) {
            list = PublicMessageModel.getAllPublicMessageGeneral()
        } else {
            list = PublicMessageModel.getAllPublicMessageSale()
        }

        isNotNull(list)
        isEqual(list.count(), 1)
        isEqual(list[0].msg, "hello world")
        isFalse(list[0].isWelcomeMessage)
    }

    fun isMessageWelcome(isGeneral: Boolean) {
        var list: List<PublicMessageModel>
        if (isGeneral) {
            list = PublicMessageModel.getAllPublicMessageGeneral()
        } else {
            list = PublicMessageModel.getAllPublicMessageSale()
        }

        isNotNull(list)
        isEqual(list.count(), 1)
        isEqual(list[0].isWelcomeMessage, true)
    }
}