package com.rockspoon.test.mock.android

import android.os.Looper
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */
class LooperMock : MockUp<Looper>() {
	companion object {
		@Mock
		@JvmStatic
		fun getMainLooper() : Looper {
			return LooperMock().mockInstance
		}
	}
}