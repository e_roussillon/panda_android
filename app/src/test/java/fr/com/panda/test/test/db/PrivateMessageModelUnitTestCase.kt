package fr.com.panda.test.test.db

import fr.com.panda.enums.DataType
import fr.com.panda.enums.FriendshipType
import fr.com.panda.enums.StatusType
import fr.com.panda.test.base.PandaUnitTestCase
import fr.com.panda.test.robot.db.PrivateMessageData
import fr.com.panda.test.robot.db.PrivateMessageFullData
import fr.com.panda.test.robot.db.privateMessageModel
import org.junit.Test
import java.net.URL
import java.util.*

/**
 * Created by edouardroussillon on 12/31/16.
 */

class PrivateMessageModelUnitTestCase : PandaUnitTestCase() {

    @Test
    fun testInsertLight_getResultById() {
        val msg = "hello"
        val roomId = 42
        val fromUserId = 42
        val index = "hello_42"
        val messageId = -1
        val dataId = 42
        val urlThumb = URL("http://www.google.com")
        val urlOriginal = URL("http://www.google.com")
        val urlBlur = URL("http://www.google.com")
        val type = DataType.NONE
        val size = 1F
        val dataLocal = "hello"
        val status = StatusType.SENDING
        val insertedAt = Date()

        val item = PrivateMessageData(msg,
                roomId,
                fromUserId,
                index,
                messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                status,
                insertedAt)

        privateMessageModel {
            insertLite(item)
        }.getMessageById(messageId) {
            isEqualTo(item)
        }
    }

    @Test
    fun testInsertLight2Times_getResultById() {
        val msg = "hello"
        val roomId = 42
        val fromUserId = 42
        val index = "hello_42"
        val messageId = -1
        val dataId = 42
        val urlThumb = URL("http://www.google.com")
        val urlOriginal = URL("http://www.google.com")
        val urlBlur = URL("http://www.google.com")
        val type = DataType.NONE
        val size = 1F
        val dataLocal = "hello"
        val status = StatusType.SENDING
        val insertedAt = Date()

        val item = PrivateMessageData(msg,
                roomId,
                fromUserId,
                index,
                messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                status,
                insertedAt)

        privateMessageModel {
            insertLite(item)
            insertLite(item)
        }.getMessageById(messageId) {
            isEqualTo(item)
        }
    }

    @Test
    fun testInsertLight_checkStatus() {
        val msg = "hello"
        val roomId = 42
        val fromUserId = 42
        val index = "hello_42"
        val messageId = -1
        val dataId = 42
        val urlThumb = URL("http://www.google.com")
        val urlOriginal = URL("http://www.google.com")
        val urlBlur = URL("http://www.google.com")
        val type = DataType.NONE
        val size = 1F
        val dataLocal = "hello"
        val status = StatusType.READ
        val insertedAt = Date()

        val item = PrivateMessageData(msg,
                roomId,
                fromUserId,
                index,
                messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                status,
                insertedAt)

        privateMessageModel {
            insertLite(item)
        }.getMessageById(messageId) {
            isStatusValid(StatusType.SENDING)
        }
    }

    @Test
    fun testInsertLight_getResultByIndex() {
        val msg = "hello"
        val roomId = 42
        val fromUserId = 42
        val index = "hello_42"
        val messageId = -1
        val dataId = 42
        val urlThumb = URL("http://www.google.com")
        val urlOriginal = URL("http://www.google.com")
        val urlBlur = URL("http://www.google.com")
        val type = DataType.IMAGE
        val size = 2F
        val dataLocal = "hello"
        val status = StatusType.SENDING
        val insertedAt = Date()

        val item = PrivateMessageData(msg,
                roomId,
                fromUserId,
                index,
                messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                status,
                insertedAt)

        privateMessageModel {
            insertLite(item)
        }.getMessageByIndex(index) {
            isEqualTo(item)
        }
    }

    @Test
    fun testInsert_getMessagesByRoom() {
        val msg = "hello"
        val roomId = 1
        val fromUserId = 2
        val dataId = 3
        val urlThumb = URL("http://www.google.com")
        val urlOriginal = URL("http://www.google.com")
        val urlBlur = URL("http://www.google.com")
        val type = DataType.URL
        val size = 4F
        val dataLocal = "hello_4_2"
        val status = StatusType.SENDING
        val insertedAt = Date()

        val item1 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "hello_5",
                6,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                status,
                insertedAt)

        val item2 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "hello_7",
                8,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                status,
                insertedAt)

        val item3 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "hello_9",
                10,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                status,
                insertedAt)

        privateMessageModel {
            insert(item1)
            insert(item2)
            insert(item3)
        }.getMessageByRoomId(roomId) {
            isEqualTo(arrayListOf(item1, item2, item3))
        }
    }

    @Test
    fun testInsert_getMessagesPending() {

        val roomId = 1
        val messageId = 2
        val msg = "hello"
        val fromUserId = 3
        val fromUserName = "user_3"
        val dataId = 4
        val urlThumb = URL("http://www.google.com")
        val urlOriginal = URL("http://www.google.com")
        val urlBlur = URL("http://www.google.com")
        val type = DataType.IMAGE
        val size = 5F
        val dataLocal = "localisation"
        val status = StatusType.SENT
        val insertedAt = Date()
        val friendshipStatus = FriendshipType.FRIEND
        val friendshipLastAction = 6
        val toUserId = 7
        val toUserName = "user_7"


        //Should be into result list
        val item1 = PrivateMessageFullData(roomId,
                messageId,
                msg,
                fromUserId,
                fromUserName,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                "index_6",
                status,
                insertedAt,
                friendshipStatus,
                friendshipLastAction,
                toUserId,
                toUserName)

        //Should NOT be into result list
        val item2 = PrivateMessageFullData(roomId,
                messageId,
                msg,
                fromUserId,
                fromUserName,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                "index_7",
                StatusType.READ,
                insertedAt,
                friendshipStatus,
                friendshipLastAction,
                toUserId,
                toUserName)

        //Should be into result list
        //TODO - Change current user from -1 to the right number
        val item3 = PrivateMessageFullData(roomId,
                messageId,
                msg,
                -1,
                fromUserName,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                "index_8",
                StatusType.READ,
                insertedAt,
                friendshipStatus,
                friendshipLastAction,
                toUserId,
                toUserName)

        //Should NOT be into result list
        //TODO - Change current user from -1 to the right number
        val item4 = PrivateMessageFullData(roomId,
                messageId,
                msg,
                -1,
                fromUserName,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                "index_9",
                StatusType.CONFIRMED,
                insertedAt,
                friendshipStatus,
                friendshipLastAction,
                toUserId,
                toUserName)

        privateMessageModel {
            insert(item1)
            insert(item2)
            insert(item3)
            insert(item4)
        }.getPendingMessages {
            isEqualToFull(arrayListOf(item1, item3))
        }
    }


    @Test
    fun testInsert_getMessagesSentOrderAsc() {
        val rowId: Long = -1
        val roomId = 1
        val messageId = 2
        val msg = "hello"
        val fromUserId = 1
        val fromUserName = "user_3"
        val dataId = 4
        val urlThumb = URL("http://www.google.com")
        val urlOriginal = URL("http://www.google.com")
        val urlBlur = URL("http://www.google.com")
        val type = DataType.IMAGE
        val size = 5F
        val dataLocal = "localisation"
        val status = StatusType.SENT
        val friendshipStatus = FriendshipType.FRIEND
        val friendshipLastAction = 6
        val toUserId = 7
        val toUserName = "user_7"


        //Should be into result list
        val item1 = PrivateMessageFullData(
                roomId,
                messageId,
                msg,
                fromUserId,
                fromUserName,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                "index_6",
                status,
                Date(),
                friendshipStatus,
                friendshipLastAction,
                toUserId,
                toUserName)

        //Should be into result list
        val item2 = PrivateMessageFullData(
                roomId,
                messageId,
                msg,
                fromUserId,
                fromUserName,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                "index_7",
                status,
                Date(),
                friendshipStatus,
                friendshipLastAction,
                toUserId,
                toUserName)

        //Should NOT be into result list
        val item3 = PrivateMessageFullData(
                roomId,
                messageId,
                msg,
                -1,
                fromUserName,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                "index_8",
                status,
                Date(),
                friendshipStatus,
                friendshipLastAction,
                toUserId,
                toUserName)

        privateMessageModel {
            insert(item1)
            insert(item2)
            insert(item3)
        }.getSentMessagesOrderAsc {
            isEqualToFull(arrayListOf(item1, item2))
        }
    }

    @Test
    fun testInsert_updateStatus() {
        val rowId: Long = -1
        val roomId = 1
        val messageId = 2
        val msg = "hello"
        val fromUserId = 1
        val fromUserName = "user_3"
        val dataId = 4
        val urlThumb = URL("http://www.google.com")
        val urlOriginal = URL("http://www.google.com")
        val urlBlur = URL("http://www.google.com")
        val type = DataType.IMAGE
        val size = 5F
        val dataLocal = "localisation"
        val status = StatusType.SENT
        val friendshipStatus = FriendshipType.FRIEND
        val friendshipLastAction = 6
        val toUserId = 7
        val toUserName = "user_7"

        val item1 = PrivateMessageFullData(
                roomId,
                messageId,
                msg,
                fromUserId,
                fromUserName,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                "index_6",
                status,
                Date(),
                friendshipStatus,
                friendshipLastAction,
                toUserId,
                toUserName)

        privateMessageModel {
            insert(item1)
            updateStatus(messageId, StatusType.READ)
        }.getMessageById(messageId) {
            isStatusValid(StatusType.READ)
        }
    }

}