package com.rockspoon.test.mock.android

import android.app.Activity
import android.content.Context
import fr.com.panda.test.mock.BuildVersionMock
import fr.com.panda.test.mock.android.ActivityCompatMock
import fr.com.panda.test.mock.android.ContextCompatMock
import fr.com.panda.test.mock.android.HawkMock
import fr.com.panda.test.mock.android.LocationMock
import org.mockito.InjectMocks

/**
 * @author Edouard Roussillon
 */

class AndroidMock {
    @InjectMocks
    val versionMock = BuildVersionMock()

    @InjectMocks
    val processMock = ProcessMock()

    @InjectMocks
    val looperMock = LooperMock()

    @InjectMocks
    val handlerMock = HandlerMock()

    @InjectMocks
    val logWitMock = LogMock()

    @InjectMocks
    val systemWithMock = SystemMock()

    @InjectMocks
    val contextWithMock = ContextMock()

    @InjectMocks
    val contextWrapperWithMock = ContextWrapperMock()

    @InjectMocks
    val serviceWithMock = ServiceMock()

    @InjectMocks
    val bundleWithMock = BundleMock()

    @InjectMocks
    val intentWithMock = IntentMock()

    @InjectMocks
    val activityWithMock = ActivityMock()

    @InjectMocks
    val parcelableMock = ParcelableMock()

    @InjectMocks
    val componentNameMock = ParcelableMock()

    @InjectMocks
    val contextCompatMock = ContextCompatMock()

    @InjectMocks
    val hawkMock = HawkMock()

    @InjectMocks
    val activityCompatMock = ActivityCompatMock()

    @InjectMocks
    val locationMock = LocationMock()

    fun context() : Context {
        return contextWithMock.mockInstance
    }

    fun activity() : Activity {
        return activityWithMock.mockInstance
    }
}