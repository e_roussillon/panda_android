package fr.com.panda.test.robot.manager

import br.com.concretesolutions.requestmatcher.RequestMatcherRule
import br.com.concretesolutions.requestmatcher.model.HttpMethod
import fr.com.panda.event.DisposableBag
import fr.com.panda.manager.BackupManager
import fr.com.panda.model.ErrorResponse
import fr.com.panda.model.FilterModel
import fr.com.panda.model.UserModel
import fr.com.panda.test.utils.*

fun backupManager(server: RequestMatcherRule, func: BackupManagerRobot.() -> Unit) = BackupManagerRobot(server).apply { func() }

class BackupManagerRobot(val server: RequestMatcherRule) {

    private val bag: DisposableBag = DisposableBag()
    private var isBackupSucceed: Boolean = false
    private var error: ErrorResponse? = null

    /**
     * Backup
     */

    fun initRequestBackupWithCode200(json: String) {
        this.server.addFixture(200, json)
                .ifRequestMatches()
                .pathIs("/api/v1/auth/backup")
                .methodIs(HttpMethod.GET)
    }

    fun initRequestBackupWithCode401(json: String) {
        this.server.addFixture(401, json)
                .ifRequestMatches()
                .pathIs("/api/v1/auth/backup")
                .methodIs(HttpMethod.GET)
    }

    fun registeToEventBackupSuccess() {
        bag.add(BackupManager.sharedInstance.onBackupSucceed.add({ v ->
            this.isBackupSucceed = true
        }))
    }

    fun registeToEventBackupFail() {
        bag.add(BackupManager.sharedInstance.onBackupFailed.add({ error ->
            this.error = error
        }))
    }

    infix fun backup(func: ResultBackupManagerRobot.() -> Unit) : ResultBackupManagerRobot {
        BackupManager.sharedInstance.backup()
        doWait(3000)
        return ResultBackupManagerRobot(this.isBackupSucceed, this.error).apply { func() }
    }

    /**
     * Backup Pending
     */

    //    TODO - When socket is read add backup pending

}

class ResultBackupManagerRobot(val isBackupSucceed : Boolean,
                               error: ErrorResponse?) : ResultAPIError(error) {

    fun isBackup(userId: Int) {
        isTrue(isBackupSucceed)

        doWait(2000)
        isNotNull(UserModel.getCurrentUser())

        val filters = FilterModel.getFilters(true)
        isNotNull(filters)
        isEqual(1, filters.size)
    }

}