package fr.com.panda.test.robot.manager

import br.com.concretesolutions.requestmatcher.RequestMatcherRule
import br.com.concretesolutions.requestmatcher.model.HttpMethod
import fr.com.panda.event.DisposableBag
import fr.com.panda.manager.SearchManager
import fr.com.panda.model.ErrorResponse
import fr.com.panda.model.Paginated
import fr.com.panda.model.UserModel
import fr.com.panda.test.utils.ResultAPIError
import fr.com.panda.test.utils.doWait
import fr.com.panda.test.utils.isEqual

fun searchManager(server: RequestMatcherRule, func: SearchManagerRobot.() -> Unit) = SearchManagerRobot(server).apply { func() }

class SearchManagerRobot(val server: RequestMatcherRule) {

    private val bag: DisposableBag = DisposableBag()
    private var paginated: Paginated<UserModel>? = null
    private var error: ErrorResponse? = null

    fun registeToEventSearchSuccess() {
        bag.add(SearchManager.sharedInstance.onSearchSucceed.add({ paginated ->
            this.paginated = paginated
        }))
    }

    fun registeToEventSearchFailed() {
        bag.add(SearchManager.sharedInstance.onSearchFailed.add({ error ->
            this.error = error
        }))
    }

    fun initRequestSearchWithCode200(json: String) {
        this.server.addFixture(200, json)
                .ifRequestMatches()
                .pathIs("/api/v1/find_friendships")
                .methodIs(HttpMethod.POST)
    }

    fun initRequestSearchWithCode401(json: String) {
        this.server.addFixture(401, json)
                .ifRequestMatches()
                .pathIs("/api/v1/find_friendships")
                .methodIs(HttpMethod.POST)
    }

    fun searchWith(speudo: String, func: ResultSearchManagerRobot.() -> Unit) : ResultSearchManagerRobot {
        SearchManager.sharedInstance.searchPseudo(speudo, 1);
        doWait(2000)
        return ResultSearchManagerRobot(this.paginated, this.error).apply { func() }
    }

}

class ResultSearchManagerRobot(val paginated: Paginated<UserModel>?, error: ErrorResponse?) : ResultAPIError(error) {

    fun isEqualTo(paginated: Paginated<UserModel>) {
        isEqual(this.paginated!!.totalPages, paginated.totalPages)
        isEqual(this.paginated!!.totalEntries, paginated.totalEntries)
        isEqual(this.paginated!!.pageSize, paginated.pageSize)
        isEqual(this.paginated!!.pageNumber, paginated.pageNumber)
        isEqual(this.paginated!!.totalPages, paginated.totalPages)

        isEqual(this.paginated!!.entries.count(), paginated.entries.count())

        val item = this.paginated.entries[0]
        val otherItem = paginated.entries[0]

//        isEqual(item.urlThumb, otherItem.urlThumb)
//        isEqual(item.urlOriginal, otherItem.urlOriginal)
//        isEqual(item.urlBlur, otherItem.urlBlur)
        isEqual(item.updatedAt.time, otherItem.updatedAt.time)
        isEqual(item.pseudo, otherItem.pseudo)
//        isEqual(item.lastSeen, otherItem.lastSeen)
        isEqual(item.isOnline, otherItem.isOnline)
        isEqual(item.userId, otherItem.userId)
        isEqual(item.friendshipStatus, otherItem.friendshipStatus)
        isEqual(item.friendshipLastAction, otherItem.friendshipLastAction)
    }


}