package com.rockspoon.test.mock.android

import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class ContextMock : MockUp<Context>() {

    companion object {
        var contextStartService: ((Intent) -> ComponentName?)? = null
        var contextStopService: ((Intent) -> Boolean)? = null
        var contextSendBroadcast: ((Intent) -> Unit)? = null
        var contextRegisterReceiver: ((BroadcastReceiver, IntentFilter) -> Intent)? = null
    }

    @Mock
    fun `$init`() {}

    @Mock
    fun startService(intent: Intent) : ComponentName? {
        return contextStartService?.invoke(intent)
    }

    @Mock
    fun stopService(intent: Intent) : Boolean {
        return contextStopService?.invoke(intent) ?: false
    }

    @Mock
    fun registerReceiver(receiver: BroadcastReceiver, filter: IntentFilter): Intent? {
        return contextRegisterReceiver?.invoke(receiver, filter)
    }

    @Mock
    fun sendBroadcast(intent: Intent) {
        contextSendBroadcast?.invoke(intent)
    }
}