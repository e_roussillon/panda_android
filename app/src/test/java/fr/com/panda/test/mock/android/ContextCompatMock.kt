package fr.com.panda.test.mock.android

import android.content.Context
import android.support.v4.content.ContextCompat
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class ContextCompatMock : MockUp<ContextCompat>() {
    companion object {
        var contextCompatCheckSelfPermission: ((Context, String) -> Int)? = null

        @Mock
        @JvmStatic
        fun checkSelfPermission(context: Context, permission: String): Int {
            return contextCompatCheckSelfPermission?.invoke(context, permission) ?: 0
        }
    }
}