package fr.com.panda.test.mock

import fr.com.panda.utils.BuildVersion
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class BuildVersionMock : MockUp<BuildVersion>() {

    companion object {
        var buildVersionGetSDK: (() -> Int)? = null

        @Mock
        @JvmStatic
        fun getSDK(): Int {
            return buildVersionGetSDK?.invoke() ?: 0
        }
    }


}