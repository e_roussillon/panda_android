package fr.com.panda.test.test.socket

import fr.com.panda.test.base.PandaWSUnitTestCase
import fr.com.panda.test.robot.manager.sessionManager
import fr.com.panda.test.robot.socket.socketPublicRobot
import fr.com.panda.test.robot.socket.socketRefreshTokenRobot
import org.junit.Test

/**
 * @author Edouard Roussillon
 */

class SocketRefreshTokenTestCase : PandaWSUnitTestCase() {


    @Test
    fun connectToPublicRooms() {
        val name = "name filter"
        val isHighlight = true
        val id = 42

        socketRefreshTokenRobot(this.bag, this.realWebSocketWithMock, this.server) {
            eventIsRunningOnEmulator()
            eventRefreshSessionSucceed()
            initJoinPrivateRoom()
            initJoinGeneralRoom()
            initJoinSaleRoom()
            initJoinBroadcastRoom()
            socketPublicRobot(this.bag, this.realWebSocketWithMock, this.server) {
                initGSPLocalistation()
                registerGetSessionSucceed()
                registerBackupSucceed()
                registerFindPublicRoomSucceed()
                registerJoinedChannelBroadcast()
                registerJoinedChannelGeneral()
                registerJoinedChannelSale()
                initRequestLoginWithCode200()
                initRequestBackupWithCode200()
                initRequestFindRoomWithCode200()
                initJoinPrivateRoom()
                initJoinBroadcastRoom()
                initJoinGeneralRoom()
                initJoinSaleRoom()
                loginUser("dovi@gmail.com", "123456")
                waitForResult(isJoinedChannelGeneral = true, countJoinedChannelBroadcast = 3, isJoinedChannelSale = true)
            }
            sessionManager(this.server) {
                registeToEventRefreshSessionSuccess()
                initRequestTokenInvalid()
                initRequestTokenRenew()
                addNewFilterWithSuccess(name, isHighlight)
                waitTokenUpdated("token is renew")
            }
            waitFinishRefresh()
            waitReconnect()
        } checkResult {
            isJoinPrivateRoom()
            isRefreshSessionSucceed(true)
            isFindPublicRoomSucceed(true)
            isConnected()
        }


    }




}