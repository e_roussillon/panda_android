package com.rockspoon.test.mock.android

import android.os.Parcelable
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class ParcelableMock: MockUp<Parcelable>() {

}