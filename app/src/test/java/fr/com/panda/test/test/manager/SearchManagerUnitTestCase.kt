package fr.com.panda.test.test.manager

import fr.com.panda.enums.ErrorType
import fr.com.panda.enums.FriendshipType
import fr.com.panda.model.FriendshipModel
import fr.com.panda.model.Paginated
import fr.com.panda.model.UserModel
import fr.com.panda.test.base.PandaUnitTestCase
import fr.com.panda.test.robot.manager.searchManager
import fr.com.panda.test.robot.manager.sessionManager
import fr.com.panda.utils.DateUtil
import org.junit.Test

class SearchManagerUnitTestCase : PandaUnitTestCase() {

    @Test
    fun search_UserSuccess() {

        var user = UserModel()
        user.updatedAt = DateUtil.timestampToDate(1477244153)
        user.pseudo = "qwerty1"
        user.isOnline = true
        user.userId = 5
        user.friendshipStatus = FriendshipType.NOT_FRIEND

        var paginated = Paginated<UserModel>()
        paginated.totalPages = 1
        paginated.totalEntries = 1
        paginated.pageSize = 30
        paginated.pageNumber = 1
        paginated.entries = listOf(user)

        searchManager(this.server) {
            registeToEventSearchSuccess()
            initRequestSearchWithCode200("search.json")
        }.searchWith(user.pseudo) {
            isEqualTo(paginated)
        }
    }

    @Test
    fun search_UserWithErrorParam() {

        searchManager(this.server) {
            registeToEventSearchFailed()
            initRequestSearchWithCode401("search_error_param.json")
        }.searchWith("qwerty1") {
            isErrorEqual(ErrorType.USER, 1, "page have to start at minimum 1")
        }
    }

}