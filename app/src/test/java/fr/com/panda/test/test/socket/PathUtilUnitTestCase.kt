package fr.com.panda.test.test.socket

import fr.com.panda.test.base.PandaUnitTestCase
import fr.com.panda.test.robot.socket.pathUnit
import org.junit.Test
import java.util.*

class PathUtilUnitTestCase : PandaUnitTestCase() {

    @Test
    fun request_setupURLWithSuccess() {
        val paramToken = "token"
        val token = "test"

        val param = HashMap<String, String>()
        param.put(paramToken, token)

        val url =  "https://localhost:4000/socket/websocket?token=test"

        pathUnit {
        }.setupURL("wss", "localhost", "socket", "websocket", param) {
            isEqualTo(url)
        }

    }

}