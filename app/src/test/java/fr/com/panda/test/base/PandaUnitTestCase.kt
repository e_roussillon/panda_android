package fr.com.panda.test.base

import android.content.Context
import android.os.Build
import fr.com.panda.BuildConfig
import org.junit.Rule
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import br.com.concretesolutions.requestmatcher.LocalTestRequestMatcherRule
import br.com.concretesolutions.requestmatcher.RequestMatcherRule
import com.raizlabs.android.dbflow.config.FlowManager
import fr.com.panda.event.DisposableBag
import fr.com.panda.http.ApiManager
import fr.com.panda.test.mock.ResourcesMock
import fr.com.panda.utils.BackgroundUtil
import fr.com.panda.utils.KeychainUtil
import org.junit.After
import org.junit.Before
import timber.log.Timber


/**
 * Created by edouardroussillon on 12/31/16.
 */
//assetDir = "build/intermediates/classes/test/"
@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class,
        manifest = "src/main/AndroidManifest.xml",
        packageName="fr.com.panda",
        sdk = intArrayOf(Build.VERSION_CODES.M),
        shadows = arrayOf(ResourcesMock::class))
abstract class PandaUnitTestCase {

    val bag = DisposableBag()

    @JvmField
    @Rule
    var dbFlowTestRule = PandaUnitTestRule.create()

    @JvmField
    @Rule
    var server: RequestMatcherRule = LocalTestRequestMatcherRule()

    protected val context: Context
        get() = RuntimeEnvironment.application

    @Before
    open fun setUp() {
        Timber.plant(Timber.DebugTree())
        BackgroundUtil.THREAD_API = 1
        KeychainUtil.sharedInstance.reset()
        ApiManager.API = ApiManager.initAPI(server.url("/").toString())
    }

    @After
    open fun reset() {
        KeychainUtil.sharedInstance.reset()
        ApiManager.API = null
        bag.dispose()
    }
}