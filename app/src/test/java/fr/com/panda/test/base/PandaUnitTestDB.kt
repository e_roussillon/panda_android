package fr.com.panda.test.base

import com.raizlabs.android.dbflow.annotation.Database
import fr.com.panda.utils.PandaDB

/**
 * Created by edouardroussillon on 12/31/16.
 */

@Database(name = PandaDB.NAME_TEST, version = PandaDB.VERSION)
object PandaUnitTestDB {}
