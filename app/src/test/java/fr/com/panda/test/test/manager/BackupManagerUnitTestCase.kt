package fr.com.panda.test.test.manager

import fr.com.panda.enums.ErrorType
import fr.com.panda.test.base.PandaUnitTestCase
import fr.com.panda.test.robot.manager.backupManager
import fr.com.panda.test.robot.manager.sessionManager
import org.junit.Test

class BackupManagerUnitTestCase : PandaUnitTestCase() {

    /**
     * Backup
     */

    @Test
    fun backupWithSuccess() {
        backupManager(this.server) {
            registeToEventBackupSuccess()
            initRequestBackupWithCode200("backup.json")
        } backup {
            isBackup(userId = 1)
        }
    }

    @Test
    fun backupWithFail() {
        backupManager(this.server) {
            registeToEventBackupFail()
            initRequestBackupWithCode401("token_not_renew.json")
            sessionManager(this.server) {
                initRequestLogoutWithCode200("empty.json")
            }
        } backup {
            isErrorEqual(ErrorType.AUTH, 3, "token not renew")
        }
    }



    /**
     * Backup Pending
     */
    //    TODO - When socket is read add backup pending

}

