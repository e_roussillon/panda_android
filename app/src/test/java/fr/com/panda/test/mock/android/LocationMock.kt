package fr.com.panda.test.mock.android

import android.location.Location
import android.os.RemoteException
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class LocationMock : MockUp<Location>() {

    companion object {
        var locationGetAccuracy: (() -> Float)? = null
        var locationGetLatitude: (() -> Double)? = null
        var locationGetLongitude: (() -> Double)? = null
        var locationGetProviders: ((Boolean) -> List<String>)? = null
        var locationGetLastKnownLocation: ((String) -> Location)? = null
    }

    @Mock
    fun getProviders(enabledOnly: Boolean): List<String> {
        return locationGetProviders?.invoke(enabledOnly) ?: emptyList()
    }

    @Mock
    fun getLastKnownLocation(provider: String): Location? {
        return locationGetLastKnownLocation?.invoke(provider) ?: null
    }

    @Mock
    fun getAccuracy(): Float {
        return locationGetAccuracy?.invoke() ?: 0.0F
    }

    @Mock
    fun setLatitude(latitude: Double) {
    }

    @Mock
    fun setLongitude(longitude: Double) {
    }

    @Mock
    fun getLatitude(): Double {
        return locationGetLatitude?.invoke() ?: 0.0
    }

    @Mock
    fun getLongitude(): Double {
        return locationGetLongitude?.invoke() ?: 0.0
    }
}