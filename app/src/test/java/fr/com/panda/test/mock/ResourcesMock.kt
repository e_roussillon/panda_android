package fr.com.panda.test.mock

import android.content.res.Resources
import org.robolectric.shadows.ShadowResources
import org.robolectric.annotation.Implements



/**
 * @author Edouard Roussillon
 */

@Implements(Resources::class)
class ResourcesMock : ShadowResources() {

    companion object {
        var resourcesGetText: ((Int) -> String)? = null
    }

    @Throws(Resources.NotFoundException::class)
    fun getText(id: Int): CharSequence {
        val message = resourcesGetText?.invoke(id) ?: ""
        if (message.isEmpty()) {
            return "PandaDB"
        }

        return message
    }
}