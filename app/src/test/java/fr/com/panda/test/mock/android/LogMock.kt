package com.rockspoon.test.mock.android

import android.util.Log
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class LogMock : MockUp<Log>() {

    companion object {
        @Mock
        @JvmStatic
        fun d(tag: String, msg: String): Int {
            return 0
        }
    }



}