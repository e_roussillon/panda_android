package com.rockspoon.test.mock.android

import android.os.Handler
import android.os.Looper
import mockit.Mock
import mockit.MockUp

/**
 * @author Edouard Roussillon
 */

class HandlerMock : MockUp<Handler>() {

	companion object {
		var handlerPost: ((Runnable) -> Boolean)? = null
	}

	@Mock
	fun `$init`(looper: Looper) {
	}

	@Mock
	fun post(runnable: Runnable) : Boolean {
		return handlerPost?.invoke(runnable) ?: false
	}
}