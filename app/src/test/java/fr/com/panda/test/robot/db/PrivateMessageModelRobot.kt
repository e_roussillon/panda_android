package fr.com.panda.test.robot.db

import fr.com.panda.enums.DataType
import fr.com.panda.enums.FriendshipType
import fr.com.panda.enums.StatusType
import fr.com.panda.model.PrivateMessageModel
import fr.com.panda.test.utils.isEqual
import java.net.URL
import java.util.*

/**
 * Created by edouardroussillon on 12/31/16.
 */

fun privateMessageModel(func: PrivateMessageModelRobot.() -> Unit) = PrivateMessageModelRobot().apply { func() }

data class PrivateMessageData(val msg: String,
                              val roomId: Int,
                              val fromUserId: Int,
                              val index: String,
                              val messageId: Int,
                              val dataId: Int,
                              val urlThumb: URL,
                              val urlOriginal: URL,
                              val urlBlur: URL,
                              @DataType.DataMode val type: Int,
                              val size: Float,
                              val dataLocal: String,
                              @StatusType.StatusMode val status: Int,
                              val insertedAt: Date)

data class PrivateMessageFullData(val roomId: Int,
                                  val messageId: Int,
                                  val msg: String,
                                  val fromUserId: Int,
                                  val fromUserName: String,
                                  val dataId: Int,
                                  val urlThumb: URL,
                                  val urlOriginal: URL,
                                  val urlBlur: URL,
                                  @DataType.DataMode val type:  Int,
                                  val size: Float,
                                  val dataLocal: String,
                                  val index: String,
                                  @StatusType.StatusMode val status: Int,
                                  val insertedAt: Date,
                                  @FriendshipType.FriendshipMode val friendshipStatus: Int,
                                  val friendshipLastAction: Int,
                                  val toUserId: Int,
                                  val toUserName: String)

class PrivateMessageModelRobot {

    private fun initPrivateMessageModel(privateMessageData: PrivateMessageData): PrivateMessageModel {
        return PrivateMessageModel(
                privateMessageData.roomId,
                privateMessageData.messageId,
                privateMessageData.msg,
                privateMessageData.fromUserId,
                privateMessageData.dataId,
                privateMessageData.urlThumb,
                privateMessageData.urlOriginal,
                privateMessageData.urlBlur,
                privateMessageData.type,
                privateMessageData.size,
                privateMessageData.dataLocal,
                privateMessageData.index,
                privateMessageData.status,
                privateMessageData.insertedAt
        )
    }

    private fun initPrivateMessageModel(privateMessageData: PrivateMessageFullData): PrivateMessageModel {
        return PrivateMessageModel(
                privateMessageData.roomId,
                privateMessageData.messageId,
                privateMessageData.msg,
                privateMessageData.fromUserId,
                privateMessageData.fromUserName,
                privateMessageData.dataId,
                privateMessageData.urlThumb,
                privateMessageData.urlOriginal,
                privateMessageData.urlBlur,
                privateMessageData.type,
                privateMessageData.size,
                privateMessageData.dataLocal,
                privateMessageData.index,
                privateMessageData.status,
                privateMessageData.insertedAt,
                privateMessageData.friendshipStatus,
                privateMessageData.friendshipLastAction,
                privateMessageData.toUserId,
                privateMessageData.toUserName
        )
    }

    fun insertLite(privateMessageData: PrivateMessageData) {
        initPrivateMessageModel(privateMessageData).insertLite()
    }

    fun insert(privateMessageData: PrivateMessageData) {
        initPrivateMessageModel(privateMessageData).insert()
    }

    fun insert(privateMessageData: PrivateMessageFullData) {
        initPrivateMessageModel(privateMessageData).insert()
    }

    fun updateStatus(privateMessage: Int, @StatusType.StatusMode status: Int) {
        val msg = ""
        val roomId = -1
        val fromUserId = -1
        val index = ""
        val messageId = privateMessage
        val dataId = -1
        val urlThumb = URL("http://www.google.com")
        val urlOriginal = URL("http://www.google.com")
        val urlBlur = URL("http://www.google.com")
        val type = DataType.NONE
        val size = 0F
        val dataLocal = ""
        val status = status
        val insertedAt = Date()

        val item = PrivateMessageData(msg, roomId, fromUserId, index, messageId, dataId, urlThumb, urlOriginal, urlBlur, type, size, dataLocal, status, insertedAt)
        initPrivateMessageModel(item).updateStatus()
    }

    fun getMessageById(id: Int, func: ReturnPrivateMessageModelRobot.() -> Unit): ReturnPrivateMessageModelRobot {
        return ReturnPrivateMessageModelRobot(PrivateMessageModel.getMessage(id)).apply { func() }
    }

    fun getMessageByIndex(index: String, func: ReturnPrivateMessageModelRobot.() -> Unit): ReturnPrivateMessageModelRobot {
        return ReturnPrivateMessageModelRobot(PrivateMessageModel.getMessageByIndex(index)).apply { func() }
    }

    fun getMessageByRoomId(roomId: Int, func: ReturnPrivateMessagesModelRobot.() -> Unit): ReturnPrivateMessagesModelRobot {
        return ReturnPrivateMessagesModelRobot(PrivateMessageModel.getMessagesByRoom(roomId)).apply { func() }
    }

    fun getPendingMessages(func: ReturnPrivateMessagesModelRobot.() -> Unit): ReturnPrivateMessagesModelRobot {
        return ReturnPrivateMessagesModelRobot(PrivateMessageModel.getPendingMessages()).apply { func() }
    }

    fun getSentMessagesOrderAsc(func: ReturnPrivateMessagesModelRobot.() -> Unit): ReturnPrivateMessagesModelRobot {
        return ReturnPrivateMessagesModelRobot(PrivateMessageModel.getSentMessagesOrderAsc()).apply { func() }
    }

}

class ReturnPrivateMessageModelRobot(val item: PrivateMessageModel?) {

    fun isEqualTo(privateMessageData: PrivateMessageData) {

        isEqual(item!!.msg, privateMessageData.msg)
        isEqual(item!!.roomId, privateMessageData.roomId)
        isEqual(item!!.fromUserId, privateMessageData.fromUserId)
        isEqual(item!!.index, privateMessageData.index)
//        isEqual(item!!.rowId, privateMessageData.rowId)
        isEqual(item!!.messageId, privateMessageData.messageId)
        isEqual(item!!.dataId, privateMessageData.dataId)
        isEqual(item!!.urlThumb, privateMessageData.urlThumb)
        isEqual(item!!.urlOriginal, privateMessageData.urlOriginal)
        isEqual(item!!.urlBlur, privateMessageData.urlBlur)
        isEqual(item!!.type, privateMessageData.type)
        isEqual(item!!.size, privateMessageData.size)
        isEqual(item!!.dataLocal, privateMessageData.dataLocal)
        isEqual(item!!.status, privateMessageData.status)
        isEqual(item!!.insertedAt.toString(), privateMessageData.insertedAt.toString())
    }

    fun isEqualTo(privateMessageData: PrivateMessageFullData) {

//        isEqual(item!!.rowId, privateMessageData.rowId)
        isEqual(item!!.roomId, privateMessageData.roomId)
        isEqual(item!!.messageId, privateMessageData.messageId)
        isEqual(item!!.msg, privateMessageData.msg)
        isEqual(item!!.fromUserId, privateMessageData.fromUserId)
//        isEqual(item!!.fromUserName, privateMessageData.fromUserName)
        isEqual(item!!.dataId, privateMessageData.dataId)
        isEqual(item!!.urlThumb, privateMessageData.urlThumb)
        isEqual(item!!.urlOriginal, privateMessageData.urlOriginal)
        isEqual(item!!.urlBlur, privateMessageData.urlBlur)
        isEqual(item!!.type, privateMessageData.type)
        isEqual(item!!.size, privateMessageData.size)
        isEqual(item!!.dataLocal, privateMessageData.dataLocal)
        isEqual(item!!.index, privateMessageData.index)
        isEqual(item!!.status, privateMessageData.status)
        isEqual(item!!.insertedAt.toString(), privateMessageData.insertedAt.toString())
//        isEqual(item!!.toUserId, privateMessageData.toUserId)
//        isEqual(item!!.toUserName, privateMessageData.toUserName)
//        isEqual(item!!.friendshipStatus, privateMessageData.friendshipStatus)
//        isEqual(item!!.friendshipLastAction, privateMessageData.friendshipLastAction)
//
    }

    fun isStatusValid(@StatusType.StatusMode status: Int) {
        isEqual(item!!.status, status)
    }
}

class ReturnPrivateMessagesModelRobot(val list: List<PrivateMessageModel>) {

    fun isEqualTo(listData: List<PrivateMessageData>) {
        isEqual(listData.size, list.size)
        var isChecked = false

        for (itemData: PrivateMessageData in listData) {
            isChecked = false
            for ((index, item) in list.withIndex()) {
                if (item!!.index == itemData.index) {
                    isChecked = true
                    ReturnPrivateMessageModelRobot(item).isEqualTo(itemData)
                    break
                } else if (!isChecked && (list.size - 1) == index) {
                    //List do not have any item like other item
                    isEqual(true, false)
                }
            }
        }
    }

    fun isEqualToFull(listData: List<PrivateMessageFullData>) {
        isEqual(listData.size, list.size)
        var isChecked = false

        for (itemData: PrivateMessageFullData in listData) {
            isChecked = false
            for ((index, item) in list.withIndex()) {
                if (item!!.index == itemData.index) {
                    isChecked = true
                    ReturnPrivateMessageModelRobot(item).isEqualTo(itemData)
                    break
                } else if (!isChecked && (list.size - 1) == index) {
                    //List do not have any item like other item
                    isEqual(true, false)
                }
            }
        }
    }
}
