package fr.com.panda.test

import android.app.Application
import com.orhanobut.hawk.Hawk
import com.raizlabs.android.dbflow.config.FlowConfig
import com.raizlabs.android.dbflow.config.FlowManager
import fr.com.panda.manager.FilterManager

/**
 * Created by edouardroussillon on 12/31/16.
 */

class PandaUnitTestApp : Application() {

    override fun onCreate() {
        super.onCreate()

        FlowManager.init(FlowConfig.Builder(this).build())

    }

    override fun onTerminate() {
        super.onTerminate()
    }
}