package fr.com.panda.test.test.db

import fr.com.panda.enums.DataType
import fr.com.panda.enums.FriendshipType
import fr.com.panda.enums.GenderType
import fr.com.panda.enums.StatusType
import fr.com.panda.test.base.PandaUnitTestCase
import fr.com.panda.test.robot.db.*
import org.junit.Test
import java.net.URL
import java.util.*

/**
 * Created by edouardroussillon on 1/14/17.
 */

class PrivateChatModelUnitTestCase : PandaUnitTestCase() {

    @Test
    fun privateChat_insertMessage() {

        val roomId: Int = 1
        val userId: Int = 2
        val userName: String = "doudou"
        val roomName: String = ""

        val msg = "hello"
        val fromUserId = 2
        val dataId = 3
        val urlThumb = URL("http://www.google.com")
        val urlOriginal = URL("http://www.google.com")
        val urlBlur = URL("http://www.google.com")
        val type = DataType.URL
        val size = 4F
        val dataLocal = "hello_4_2"
        val insertedAt1 = Date()
        val lastSeen = Date()

        val item1 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "1", //index,
                1,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                StatusType.SENDING,
                insertedAt1)
        val insertedAt2 = Date()
        val item2 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "2", //index,
                2, //messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                DataType.NONE,
                0F, //size
                "", //dataLocal
                StatusType.SENDING,
                insertedAt2) //insertedAt

        val insertedAt3 = Date()
        val item3 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "3", //index,
                3, //messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                DataType.NONE,
                0F, //size
                "", //dataLocal
                StatusType.SENT,
                insertedAt3) //insertedAt

        val insertedAt4 = Date()
        val item4 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "4", //index,
                4, //messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                DataType.NONE,
                0F, //size
                "", //dataLocal
                StatusType.RECEIVED,
                insertedAt4) //insertedAt

        val insertedAt5 = Date()
        val item5 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "5", //index,
                5, //messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                DataType.NONE,
                0F, //size
                "", //dataLocal
                StatusType.READ,
                insertedAt5) //insertedAt

        val insertedAt6 = Date()
        val item6 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "6", //index,
                6, //messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                DataType.NONE,
                0F, //size
                "", //dataLocal
                StatusType.CONFIRMED,
                insertedAt6) //insertedAt

        val user = UserData(userId,
                "user@gmail.com",
                userName,
                "hello here the details",
                Date(),
                GenderType.MALE,
                URL("http://google.com"),
                URL("http://google.com"),
                URL("http://google.com"),
                true,
                Date(),
                FriendshipType.FRIEND,
                0,
                true,
                Date())

        val userLocal = UserData(-1,
                "userLocal@gmail.com",
                "userLocal",
                "hello userLocal",
                Date(),
                GenderType.MALE,
                URL("http://google.com"),
                URL("http://google.com"),
                URL("http://google.com"),
                true,
                Date(),
                FriendshipType.NOT_FRIEND,
                0,
                true,
                Date())

        val privateChatData = PrivateChatData(roomId,
                false,
                userName,
                URL("http://google.com"),
                URL("http://google.com"),
                URL("http://google.com"),
                DataType.NONE,
                FriendshipType.FRIEND,
                -1, //userStatusLastAction
                msg,
                insertedAt4,
                4, //numOfMsgNotRead
                userId,
                true, //userIsOnline
                lastSeen)

        privateChatModel {
            UserModelRobot().insertUser(user)
            UserModelRobot().insertUser(userLocal)
            insertRoom(roomId, roomName)
            insertUsersRoom(roomId, userId)
            PrivateMessageModelRobot().insert(item1)
            PrivateMessageModelRobot().insert(item2)
            PrivateMessageModelRobot().insert(item3)
            PrivateMessageModelRobot().insert(item4)
            PrivateMessageModelRobot().insert(item5)
            PrivateMessageModelRobot().insert(item6)
        } getRooms {
            isEqualTo(listOf(privateChatData))
        }
    }

    @Test
    fun privateChat_getCountMessages() {

        val roomId: Int = 1
        val msg = "hello"
        val fromUserId = 2
        val dataId = 3
        val urlThumb = URL("http://www.google.com")
        val urlOriginal = URL("http://www.google.com")
        val urlBlur = URL("http://www.google.com")

        val item1 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "1", //index,
                1, //messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                DataType.NONE,
                0F, //size
                "", //dataLocal
                StatusType.ERROR,
                Date()) //insertedAt

        val item2 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "2", //index,
                2, //messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                DataType.NONE,
                0F, //size
                "", //dataLocal
                StatusType.SENDING,
                Date()) //insertedAt

        val item3 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "3", //index,
                3, //messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                DataType.NONE,
                0F, //size
                "", //dataLocal
                StatusType.SENT,
                Date()) //insertedAt

        val item4 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "4", //index,
                4, //messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                DataType.NONE,
                0F, //size
                "", //dataLocal
                StatusType.RECEIVED,
                Date()) //insertedAt

        val item5 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "5", //index,
                5, //messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                DataType.NONE,
                0F, //size
                "", //dataLocal
                StatusType.READ,
                Date()) //insertedAt

        val item6 = PrivateMessageData(msg,
                roomId,
                fromUserId,
                "6", //index,
                6, //messageId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                DataType.NONE,
                0F, //size
                "", //dataLocal
                StatusType.CONFIRMED,
                Date()) //insertedAt

        privateChatModel {
            PrivateMessageModelRobot().insert(item1)
            PrivateMessageModelRobot().insert(item2)
            PrivateMessageModelRobot().insert(item3)
            PrivateMessageModelRobot().insert(item4)
            PrivateMessageModelRobot().insert(item5)
            PrivateMessageModelRobot().insert(item6)
        } getMessagesCount {
            isEqualTo(4)
        }
    }

    @Test
    fun privateChat_getRoomId() {

        val roomId: Int = 1
        val userId: Int = 2
        val roomName: String = "room_name"

        privateChatModel {
            insertRoom(roomId, roomName)
            insertUsersRoom(roomId, userId)
        }.getRoom(userId) {
            isEqualTo(roomId)
        }
    }


//    fun getRooms(func: ReturnPrivateChatModelsRobot.() -> Unit): ReturnPrivateChatModelsRobot {
//        return ReturnPrivateChatModelsRobot(PrivateRoomSQL.getRooms()).apply { func }
//    }
//
//    fun getRoom(userId: Int, func: ReturnIntIdPrivateChatModelRobot.() -> Unit): ReturnIntIdPrivateChatModelRobot {
//        return ReturnIntIdPrivateChatModelRobot(PrivateRoomSQL.getRoom(userId)).apply { func }
//    }
//
//    fun getMessagesCount(func: ReturnIntIdPrivateChatModelRobot.() -> Unit): ReturnIntIdPrivateChatModelRobot {
//        return ReturnIntIdPrivateChatModelRobot(PrivateRoomSQL.getMessagesCount()).apply { func }
//    }
//
//    fun insertRoom(roomId: Int, name: String) {
//        PrivateRoomSQL.insertRoom(roomId, name)
//    }
//
//    fun insertUsersRoom(roomId: Int, toUserId: Int) {
//        PrivateRoomUserSQL.insertUsersRoom(roomId, toUserId)
//    }


}