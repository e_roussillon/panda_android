package fr.com.panda.test.robot.db

import fr.com.panda.enums.DataType
import fr.com.panda.enums.StatusType
import fr.com.panda.model.PublicMessageModel
import fr.com.panda.test.utils.isEqual
import java.net.URL
import java.util.*

/**
 * Created by edouardroussillon on 1/10/17.
 */

fun publicMessageModel(func: PublicMessageModelRobot.() -> Unit) = PublicMessageModelRobot().apply { func() }

data class PublicMessageData(var messageId: Int,
                             var msg: String,
                             var fromUserId: Int,
                             var fromUserName: String,
                             var urlThumb: URL,
                             var urlOriginal: URL,
                             var urlBlur: URL,
                             var type: Int, //@DataType.DataMode
                             var index: String,
                             @StatusType.StatusMode var status: Int,
                             var isGeneral: Boolean,
                             var isHighlight: Boolean,
                             var isWelcomeMessage: Boolean,
                             var insertedAt: Date)


class PublicMessageModelRobot {

    private fun initPublicMessageData(publicMessageData: PublicMessageData): PublicMessageModel {
        return PublicMessageModel(
                publicMessageData.messageId,
                publicMessageData.msg,
                publicMessageData.fromUserId,
                publicMessageData.fromUserName,
                publicMessageData.urlThumb,
                publicMessageData.urlOriginal,
                publicMessageData.urlBlur,
                publicMessageData.type,
                publicMessageData.index,
                publicMessageData.status,
                publicMessageData.isGeneral,
                publicMessageData.isHighlight,
                publicMessageData.isWelcomeMessage,
                publicMessageData.insertedAt)
    }

    fun insertPublicMessage(publicMessage: PublicMessageData) {
        initPublicMessageData(publicMessage).insert()
    }

    fun updateMessageWithFilter(filterData: FilterData) {
        PublicMessageModel.updateMessageWithFilter(initFilterModel(filterData))
    }

    fun removeMessageWithFilter(filterData: FilterData) {
        PublicMessageModel.removeMessageWithFilter(initFilterModel(filterData))
    }

    fun insertPublicMessageLight(publicMessage: PublicMessageData) {
        initPublicMessageData(publicMessage).insertLight()
    }

    fun isContainWelcomeMessage(publicMessage: PublicMessageData, func: ResultIsPublicMessageModelRobot.() -> Unit): ResultIsPublicMessageModelRobot {
        return ResultIsPublicMessageModelRobot(initPublicMessageData(publicMessage).isContainWelcomeMessage()).apply { func() }
    }

    infix fun getAllPublicMessageGeneral(func: ResultPublicMessageModelsRobot.() -> Unit): ResultPublicMessageModelsRobot {
        return ResultPublicMessageModelsRobot(PublicMessageModel.getAllPublicMessageGeneral()).apply { func() }
    }

    infix fun getAllPublicMessageSale(func: ResultPublicMessageModelsRobot.() -> Unit): ResultPublicMessageModelsRobot {
        return ResultPublicMessageModelsRobot(PublicMessageModel.getAllPublicMessageSale()).apply { func() }
    }

}

class ResultPublicMessageModelRobot(val publicMessage: PublicMessageModel) {

    fun isEqualTo(item: PublicMessageData) {
        isEqual(item!!.messageId, publicMessage.messageId)
        isEqual(item!!.msg, publicMessage.msg)
        isEqual(item!!.fromUserName, publicMessage.fromUserName)
//        isEqual(item!!.urlBlur, publicMessage.urlBlur)
//        isEqual(item!!.urlThumb, publicMessage.urlThumb)
//        isEqual(item!!.urlOriginal, publicMessage.urlOriginal)
        isEqual(item!!.type, publicMessage.type)
        isEqual(item!!.index, publicMessage.index)
        isEqual(item!!.status, publicMessage.status)
        isEqual(item!!.isGeneral, publicMessage.isGeneral)
        isEqual(item!!.isHighlight, publicMessage.isHighlight)
        isEqual(item!!.isWelcomeMessage, publicMessage.isWelcomeMessage)
        isEqual(item!!.insertedAt.toString(), publicMessage.insertedAt.toString())
    }

}

class ResultIsPublicMessageModelRobot(val isWelcomeMessage: Boolean) {

    fun isWelcomMessage(isWelcomeMessage: Boolean) {
        isEqual(this.isWelcomeMessage, isWelcomeMessage)
    }

}

class ResultPublicMessageModelsRobot(val list: List<PublicMessageModel>) {

    fun isEqualTo(listData: List<PublicMessageData>) {
        isEqual(listData.size, list.size)
        var isChecked = false

        for (itemData: PublicMessageData in listData) {
            isChecked = false
            for ((index, item) in list.withIndex()) {
                if (item!!.index == itemData.index) {
                    isChecked = true
                    ResultPublicMessageModelRobot(item).isEqualTo(itemData)
                    break
                } else if (!isChecked && (list.size - 1) == index) {
                    //List do not have any item like other item
                    isEqual(true, false)
                }
            }
        }
    }
}