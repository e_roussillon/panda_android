package com.rockspoon.test.mock.android

import android.os.Bundle
import android.os.Parcelable
import mockit.Mock
import mockit.MockUp
import java.util.*

/**
 * @author Edouard Roussillon
 */

class BundleMock : MockUp<Bundle>() {
    var mapString = mutableMapOf<String, String>()
    var mapInt = mutableMapOf<String, Int>()
    var mapParcelable = mutableMapOf<String, Parcelable>()

    @Mock
    fun `$init`() {}

    @Mock
    fun putString(key: String, value: String) {
        mapString.put(key, value)
    }

    @Mock
    fun putInt(key: String, value: Int) {
        mapInt.put(key, value)
    }

    @Mock
    fun putParcelable(key: String, value: Parcelable) {
        mapParcelable.put(key, value)
    }

    @Mock
    fun get(key: String): Any {
        if (mapString.size > 0 && mapString.containsKey(key)) {
            return mapString[key]!!
        } else if (mapParcelable.size > 0 && mapParcelable.containsKey(key)) {
            return mapParcelable[key]!!
        }
        return ""
    }

    @Mock
    fun getString(key: String): String? {
        if (mapString.size > 0 && mapString.containsKey(key)) {
            return mapString[key]
        }
        return null
    }

    @Mock
    fun getInt(key: String, defaultValue: Int): Int {
        if (mapInt.size > 0 && mapInt.containsKey(key)) {
            return mapInt[key] ?: defaultValue
        }
        return defaultValue

    }

    @Mock
    fun containsKey(key: String): Boolean {
        if (mapString.size > 0 && mapString.containsKey(key)) {
            return true
        } else if (mapParcelable.size > 0 && mapParcelable.containsKey(key)) {
            return true
        }
        return false
    }



    @Mock
    fun <T : Parcelable> getParcelable(key: String): T? {
        if (mapParcelable.size > 0 && mapParcelable.containsKey(key)) {
            return mapParcelable[key] as T
        }
        return null
    }

}