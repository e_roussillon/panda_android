package fr.com.panda.socket.sender;

import fr.com.panda.model.PublicMessageModel;
import fr.com.panda.socket.connector.ISocketConnector;

public interface ISocketSenderPublic {
    void sendMessage(PublicMessageModel publicMessage, ISocketConnector socketConnector);
    void resendMessage(PublicMessageModel publicMessage, ISocketConnector socketConnector);
    void sendWelcomeMessage(PublicMessageModel publicMessage);
}
