package fr.com.panda.socket.phoenix;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class PhoenixMessage {

    @SerializedName("subject")
    @Expose
    private String subject;

    @SerializedName("body")
    @Expose
    private String body;

    @SerializedName("message")
    @Expose
    private Object message;

    public PhoenixMessage(String subject, String body) {
        this.subject = subject;
        this.body = body;
    }

    public PhoenixMessage(Object message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public Object getMessage() {
        return message;
    }
}
