package fr.com.panda.socket.connector;

import fr.com.panda.enums.ChannelType;
import fr.com.panda.socket.phoenix.PhoenixChannel;
import fr.com.panda.socket.phoenix.PhoenixMessage;
import fr.com.panda.socket.phoenix.PhoenixPayload;
import fr.com.panda.socket.phoenix.PhoenixSocket;

public interface ISocketConnector {
    boolean isSocketConnected();
    boolean startSocket();
    void stopSocket(PhoenixSocket.ICallBackSocketClose callBackSocketClose);
    void forceStopSocket(PhoenixSocket.ICallBackSocketClose callBackSocketClose);
    boolean send(PhoenixPayload data);
    boolean isJoined(String topic);
    void joinChanel(@ChannelType.ChannelMode String channel, int roomId, PhoenixMessage message, PhoenixChannel.IPhoenixChannelCallback callback);
    void leaveAllRoom();
    void leaveRoomPublicRooms();
    void refreshToken(@ChannelType.ChannelMode String channel, int roomId, PhoenixMessage message);
}
