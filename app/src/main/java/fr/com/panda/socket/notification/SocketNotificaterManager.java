package fr.com.panda.socket.notification;

import java.util.List;

import fr.com.panda.event.Event;
import fr.com.panda.model.NotificationImageMessage;
import fr.com.panda.model.NotificationMessage;
import fr.com.panda.model.PrivateMessageModel;
import fr.com.panda.model.PublicMessageModel;
import fr.com.panda.model.UserModel;

public class SocketNotificaterManager {
    static public SocketNotificaterManager sharedInstance = new SocketNotificaterManager();

    public Event<Void> onJoinPublicRoomSucceed = new Event();
    public Event<Void> onJoinPublicRoomFailed = new Event();

    public Event<Void> onJoinPrivateRoomSucceed = new Event();
    public Event<Void> onJoinPrivateRoomFailed = new Event();

    public Event<Void> onFindPrivateRoomSucceed = new Event();
    public Event<Void> onFindPrivateRoomFailed = new Event();

    public Event<Long> onResetTimerNotification = new Event();
    public Event<Integer> onUpdateTimerNotification = new Event();
    public Event<Void> onStopTimerNotification = new Event();

    public Event<PublicMessageModel> onNewGeneralMessageNotification = new Event();
    public Event<PublicMessageModel> onFailGeneralMessageNotification = new Event();

    public Event<PublicMessageModel> onNewSaleMessageNotification = new Event();
    public Event<PublicMessageModel> onFailSaleMessageNotification = new Event();

    public Event<PublicMessageModel> onNewBroadcastMessageNotification = new Event();
    public Event<PublicMessageModel> onFailBroadcastMessageNotification = new Event();

    public Event<NotificationImageMessage> onNewImageUploadingNotification = new Event();
    public Event<NotificationMessage> onNewPrivateMessageNotification = new Event();

    public Event<PrivateMessageModel> onNewPrivateMessageFailNotification = new Event();
    public Event<Void> onGetBackupMessagesNotification = new Event();

    public Event<List<UserModel>> onNewConnection = new Event();
    public Event<UserModel> onNewDisconnection = new Event();
    public Event<UserModel> onUpdateFriendship = new Event();

    public Event<Void> onJoinedChannelPrivate = new Event();
    public Event<Void> onJoinedChannelSale = new Event();
    public Event<Void> onJoinedChannelGeneral = new Event();
    public Event<Void> onJoinedChannelBroadcast = new Event();

    public Event<Void> onClearTextNotification = new Event();

    public Event<Void> onRemoveChatFindRoomNotification = new Event();

    // TODO Add those to iOS too
    public Event<Void> onShowSplashScreen = new Event();
    public Event<Integer> onUserReported = new Event();

    private SocketNotificaterManager() {}

}
