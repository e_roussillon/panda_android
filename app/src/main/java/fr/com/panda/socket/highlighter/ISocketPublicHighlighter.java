package fr.com.panda.socket.highlighter;

import fr.com.panda.enums.ChannelType;
import fr.com.panda.model.PublicMessageModel;

public interface ISocketPublicHighlighter {
    boolean checkIsIncomingPublicMessageHaveToBeBlocked(PublicMessageModel publicMessage, @ChannelType.ChannelMode String channelType);
}
