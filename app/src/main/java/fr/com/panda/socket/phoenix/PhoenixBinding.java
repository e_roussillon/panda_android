package fr.com.panda.socket.phoenix;

import java.util.HashMap;

import fr.com.panda.enums.SocketType;

public class PhoenixBinding {
    private final @SocketType.SocketMode  String event;
    private final IPhoenixMessageCallback callback;

    public PhoenixBinding(final @SocketType.SocketMode  String event, final IPhoenixMessageCallback callback) {
        this.event = event;
        this.callback = callback;
    }

    public @SocketType.SocketMode  String getEvent() {
        return event;
    }

    public IPhoenixMessageCallback getCallback() {
        return callback;
    }

    //
    // MARK: Interface
    //

    public interface IPhoenixMessageCallback {

        void onMessage(HashMap<String, Object> msg);
    }
}
