package fr.com.panda.socket.sender;

import java.util.List;

import fr.com.panda.model.PrivateMessageModel;
import fr.com.panda.socket.connector.ISocketConnector;

public interface ISocketSenderPrivate {

    void sendMessage(PrivateMessageModel privateMessage, ISocketConnector socketConnector, boolean withNotification);
    void sendMessages(List<PrivateMessageModel> messages, ISocketConnector socketConnector);
}
