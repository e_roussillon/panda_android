package fr.com.panda.socket.sender;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.com.panda.enums.ChannelType;
import fr.com.panda.enums.DataType;
import fr.com.panda.enums.SocketType;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.model.PrivateMessageModel;
import fr.com.panda.socket.connector.ISocketConnector;
import fr.com.panda.socket.notification.SocketNotificaterManager;
import fr.com.panda.socket.phoenix.PhoenixMessage;
import fr.com.panda.socket.phoenix.PhoenixPayload;

public class SocketSenderPrivate implements ISocketSenderPrivate {
    private SessionManager sessionManager = SessionManager.sharedInstance;
    private SocketNotificaterManager socketNotificaterManager = SocketNotificaterManager.sharedInstance;

    @Override
    public void sendMessage(PrivateMessageModel privateMessage, ISocketConnector socketConnector, boolean withNotification) {
        int sessionUserId = sessionManager.user().getUserId();
        String pseudo = sessionManager.user().getUserPseudo();

        long rowId = privateMessage.insertLite();

        if (rowId > 0 && sessionUserId > 0) {
            privateMessage.rowId = rowId;

            if (!TextUtils.isEmpty(privateMessage.dataLocal) && privateMessage.type == DataType.IMAGE && privateMessage.dataId == -1 && privateMessage.urlBlur == null) {
                //TODO - WHEN IMPLEMENT IMAGE
//                APIData().uploadImage(dataLocal, progress: { (bytesWritten, totalBytesWritten, totalBytesExpected) in
//                    self.socketNotificaterManager.onNewImageUploadingNotification.dispatch((bytesWritten: bytesWritten, totalBytesWritten: totalBytesWritten, totalBytesExpected: totalBytesExpected, message: privateMessage))
//                }, success: { (data) in
//                    let image = data as! DataModel
//                    privateMessage.type = DataType.Image
//                    privateMessage.dataId = image.dataId
//                    privateMessage.insert()
//                    self.sendMessage(index: index, pseudo: pseudo, roomId: privateMessage.roomId!, userId: userId, userName: userName, msg: privateMessage.msg!, dataId: image.dataId!, socketConnector: socketConnector)
//                }, failure: { (response, isErrorComsumed) -> Bool in
//                    self.socketNotificaterManager.onNewPrivateMessageFailNotification.dispatch(privateMessage)
//                    return true
//                })
            } else {
                this.sendMessage(privateMessage.index, pseudo, privateMessage.roomId, privateMessage.toUserId, privateMessage.toUserName, privateMessage.msg, privateMessage.dataId, socketConnector);
            }

            privateMessage.insert();
            if (withNotification) {
                //TODO - WHEN IMPLEMENT IMAGE
//                this.socketNotificaterManager.onNewPrivateMessageNotification((isNewItem: true, message: privateMessage))
                this.socketNotificaterManager.onClearTextNotification.dispatch(null);
            }
        }
    }

    @Override
    public void sendMessages(@NonNull List<PrivateMessageModel> messages, ISocketConnector socketConnector) {
        if (messages.size() <= 0) {
            return;
        }

        long sessionUserId = sessionManager.user().getUserId();
        List<HashMap<String, Object>> newMessages = new ArrayList();

        for (PrivateMessageModel item : messages) {
            HashMap<String, Object> data = new HashMap();
            data.put("id", item.messageId);
            data.put("status", item.status);
            newMessages.add(data);
            item.updateStatus();
        }

        HashMap<String, Object> disc = new HashMap();
        disc.put("messages", newMessages);

        PhoenixMessage message = new PhoenixMessage(disc);
        PhoenixPayload payload = new PhoenixPayload(ChannelType.PRIVATE + ":" + sessionUserId, SocketType.NEWS_MSG_CONFIRMATION, message);
        socketConnector.send(payload);
    }

    //
    // MARK: PRIVATE
    //

    private void sendMessage(String index, String pseudo, int roomId, int userId, String userName, String msg, int dataId, ISocketConnector socketConnector) {
        int sessionUserId = sessionManager.user().getUserId();

        HashMap<String, Object> disc = new HashMap();
        disc.put("index", index);
        disc.put("user_name", pseudo);
        disc.put("private_room_id", roomId);
        disc.put("to_user_id", userId);
        disc.put("to_user_name", userName);
        disc.put("message", msg);
        disc.put("data_id", dataId);

        PhoenixMessage message = new PhoenixMessage(disc);
        PhoenixPayload payload = new PhoenixPayload(ChannelType.PRIVATE + ":" + sessionUserId, SocketType.NEWS_MSG, message);

        socketConnector.send(payload);
    }
}
