package fr.com.panda.socket.phoenix;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import fr.com.panda.enums.ActionNameType;
import fr.com.panda.enums.ChannelType;
import fr.com.panda.enums.KeyNameType;
import fr.com.panda.enums.SocketType;
import fr.com.panda.manager.AnalyticsManager;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.socket.notification.SocketNotificaterManager;
import fr.com.panda.utils.Constants;
import fr.com.panda.utils.JsonUtil;
import fr.com.panda.utils.PathUtil;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;
import timber.log.Timber;

public class PhoenixSocket {
    private static final int NORMAL_CLOSURE_STATUS = 1000;

    private class PhoenixSocketListener extends WebSocketListener {

        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            super.onOpen(webSocket, response);
            PhoenixSocket.this.conn = webSocket;
            PhoenixSocket.this.sendAnalytics(ActionNameType.ConnectionChat);
            PhoenixSocket.this.numberOfTime = 0;
            Timber.i("socket opened");
            PhoenixSocket.this.onOpen();
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            super.onMessage(webSocket, text);

            Timber.i("socket message: " + text);
            PhoenixPayload plaPhoenixPayload = JsonUtil.fromJson(text, PhoenixPayload.class);
            PhoenixSocket.this.onMessage(plaPhoenixPayload);
        }

        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) { super.onMessage(webSocket, bytes); }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            super.onClosing(webSocket, code, reason);
            PhoenixSocket.this.conn = null;

            Timber.i("socket onClose "+ code +"/"+ reason);

            if (!PhoenixSocket.this.forceDisconnection) {
                PhoenixSocket.this.sendAnalytics(ActionNameType.LostConnectionChat);
                PhoenixSocket.this.numberOfTime += (PhoenixSocket.this.numberOfTime == 0) ? 1 : PhoenixSocket.this.numberOfTime;
                PhoenixSocket.this.onClose();
            }
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            super.onFailure(webSocket, t, response);
            PhoenixSocket.this.conn = null;

            if (response != null && response.code() == 403) {
//                BaseAPIManager.startRefreshToken()
            } else {
                Timber.e("socket Error: "+ t.getMessage());
                PhoenixSocket.this.numberOfTime += (PhoenixSocket.this.numberOfTime == 0) ? 1 : PhoenixSocket.this.numberOfTime;
                PhoenixSocket.this.onClose();
            }
        }
    }

    private final PhoenixSocketListener phoenixSocketListener = new PhoenixSocketListener();
    private OkHttpClient client = new OkHttpClient();

    private SessionManager sessionManager = SessionManager.sharedInstance;
    private AnalyticsManager analyticsManager = AnalyticsManager.sharedInstance;
    private SocketNotificaterManager socketNotificaterManager = SocketNotificaterManager.sharedInstance;
    private WebSocket conn;

    private Timer reconnectTimer = new Timer();
    private Timer heartbeatTimer = new Timer();
    private Timer sendBufferTimer = new Timer();

    private List<PhoenixPayload> sendBuffer = new ArrayList();
    private List<PhoenixChannel> channels = new ArrayList();

    private Request endPoint;

    public static long RECONNECT_IN_SEC = Constants.SOCKET.RECONNECT_IN_SEC;
    public static long HEARTBEAT_IN_SEC = Constants.SOCKET.HEARTBEAT_IN_SEC;
    public static long FLUSH_EVERY_MESSAGE_IN_MSEC = Constants.SOCKET.FLUSH_EVERY_MESSAGE_IN_MSEC;
    public long numberOfTime = 0;

    public static long MESSAGE_REFERENCE = 0;
    private HashMap<String, PhoenixMessage> joinMessages = new HashMap();


    public boolean forceDisconnection = false;


    public PhoenixSocket(String domainAndPort, String path, String transport, String prot, HashMap<String, String> params) {
        super();
        this.updateEndPoint(domainAndPort, path, transport, prot, params);
    }

    public void updateEndPoint(String domainAndPort, String path, String transport, String prot, HashMap<String, String> params) {
        this.endPoint = PathUtil.endpointWithProtocol(prot, domainAndPort, path, transport, params);
        resetBufferTimer();
        reconnect();
    }

    private void sendHeartbeat() {
        final PhoenixMessage heartbeatMessage = new PhoenixMessage("status", SocketType.HEARTBEAT);
        final PhoenixPayload payload = new PhoenixPayload("phoenix", SocketType.HEARTBEAT, heartbeatMessage);
        this.send(payload);
    }

    public void send(PhoenixPayload data) {

        if (this.isConnected()) {
            this.doSendBuffer(data);
        } else {
            if (!TextUtils.isEmpty(data.getEvent()) && data.getEvent().equals(SocketType.JOIN) && data.getEvent().equals(SocketType.LEAVE)) {
                this.sendBuffer.add(data);
            }
        }
    }

    private void doSendBuffer(PhoenixPayload data) {
        if (this.conn == null) {
            return;
        }

        synchronized (this.conn) {
            final String json = data.toJsonString(makeRef());
            this.conn.send(json);
            Timber.i("send to channel: "+ json);
        }
    }

    //
    // MARK: public
    //

    public boolean isConnected() {
        if (this.conn != null) {
            return true;
        } else {
            return false;
        }
    }

    public void close(ICallBackSocketClose callback) {
        if (this.conn != null) {
            synchronized (this.conn) {
                this.conn.close(NORMAL_CLOSURE_STATUS, "Disconnected by client");
                this.client.dispatcher().executorService().shutdown();
                this.sendAnalytics(ActionNameType.DisconnectionChat);
            }
        }

        if (!this.forceDisconnection) {
            this.heartbeatTimer.cancel();
            this.heartbeatTimer = new Timer();

            this.reconnectTimer.cancel();
            this.reconnectTimer = new Timer();
        }

        callback.isClose();
    }

    public void refreshToken(String topic, PhoenixMessage message) {
        if (this.joinMessages.keySet().contains(topic)) {
            PhoenixMessage oldMessage = this.joinMessages.get(topic);

            if (!oldMessage.getSubject().equals(message.getSubject()) || !oldMessage.getBody().equals(message.getBody())) {
                this.joinMessages.remove(topic);
                this.joinMessages.put(topic, message);
                for (PhoenixChannel chan : channels) {
                    if (chan.getTopic().equals(topic)) {
                        rejoin(chan);
                        break;
                    }
                }
            }
        }
    }

    public void leaveAllWithoutMessage() {
        this.channels = new ArrayList();
        this.sendBuffer = new ArrayList();
        this.joinMessages = new HashMap();
    }

    public void leaveRoomPublicRooms() {
        synchronized (this.channels) {
            for (PhoenixChannel channel : this.channels) {
                if (!TextUtils.isEmpty(channel.getTopic()) && !channel.getTopic().contains(ChannelType.PRIVATE)) {
                    HashMap<String, String> dics = new HashMap();
                    dics.put(SocketType.LEAVE, "room");
                    this.leave(channel.getTopic(), new PhoenixMessage(dics));
                }
            }
        }
    }

    //
    // MARK: Room
    //

    public boolean isJoined(String topic) {
        for (PhoenixChannel chan : channels) {
            if (chan.isMember(topic)) {
                return true;
            }
        }

        return false;
    }

    public void join(String topic, PhoenixMessage message, PhoenixChannel.IPhoenixChannelCallback callback) {
        if (!isJoined(topic)) {
            PhoenixChannel chan = new PhoenixChannel(this, topic, callback);
            this.joinMessages.put(topic, message);
            this.channels.add(chan);
            Timber.i("joining topic: -> "+ topic);
            this.rejoin(chan);
        }
    }

    public void leave(String topic, PhoenixMessage message) {
        PhoenixPayload payload = new PhoenixPayload(topic, SocketType.LEAVE, message);
        Timber.i("leave topic: -> "+ topic);
        this.send(payload);

        List<PhoenixChannel> newChannels = new ArrayList();
        synchronized (channels) {
            for (PhoenixChannel channel : channels) {
                if (!channel.isMember(topic)) {
                    newChannels.add(channel);
                }
            }
            this.channels = newChannels;
        }
    }

    private void rejoinAll() {
        synchronized (channels) {
            for (PhoenixChannel chan : channels) {
                this.rejoin(chan);
            }
        }
    }

    private void rejoin(PhoenixChannel chan) {
        chan.reset();

        if (TextUtils.isEmpty(chan.getTopic())) {
            return;
        }

        final PhoenixMessage joinMessage = this.joinMessages.get(chan.getTopic());
        if (joinMessage != null) {
            PhoenixPayload payload = new PhoenixPayload(chan.getTopic(), SocketType.JOIN, joinMessage);
            chan.getCallback().onChannel(chan);
            this.send(payload);
        }
    }

    //
    // MARK: PRIVATE
    //

    private void reconnect() {
        this.close(() -> {
            if (!this.forceDisconnection) {
                this.client.newWebSocket(this.endPoint, this.phoenixSocketListener);
            } else {
                this.heartbeatTimer.cancel();
                this.heartbeatTimer = new Timer();

                this.reconnectTimer.cancel();
                this.reconnectTimer = new Timer();
            }
        });
    }

    //
    // MARK: Timer
    //

    private void resetBufferTimer() {
        sendBufferTimer = new Timer();
        sendBufferTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                flushSendBuffer();
            }
        }, 0, FLUSH_EVERY_MESSAGE_IN_MSEC);
    }

    private void flushSendBuffer() {
        if (isConnected() && sendBuffer.size() > 0) {
            synchronized (sendBuffer) {
                for (PhoenixPayload data : sendBuffer) {
                    doSendBuffer(data);
                }
                sendBuffer = new ArrayList<>();
                resetBufferTimer();
            }
        }
    }

    //
    // MARK: Method call by PhoenixSocketListener
    //

    private void onOpen() {
        this.socketNotificaterManager.onStopTimerNotification.dispatch(null);
        this.reconnectTimer.cancel();
        this.reconnectTimer = new Timer();
        this.heartbeatTimer.cancel();
        this.heartbeatTimer = new Timer();

        this.heartbeatTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        sendHeartbeat();
                    }
                },
                secToMillisecond(HEARTBEAT_IN_SEC), secToMillisecond(HEARTBEAT_IN_SEC));
        rejoinAll();
    }

    private void onClose() {
        long time = this.secToMillisecond(RECONNECT_IN_SEC) * this.numberOfTime;
        this.socketNotificaterManager.onResetTimerNotification.dispatch(time);

        Timber.i("Try to Connect again in : "+ time +" sec");
        this.heartbeatTimer.cancel();
        this.heartbeatTimer = new Timer();

        this.reconnectTimer.cancel();
        this.reconnectTimer = new Timer();

        this.reconnectTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                reconnect();
            }
        }, time + 1, time + 1);
    }

    private void onMessage(PhoenixPayload payload) {
        synchronized (channels) {
            for (PhoenixChannel chan : channels) {
                if (chan.isMember(payload.getTopic())) {
                    chan.trigger(payload.getEvent(), payload.getPayload());
                }
            }
        }
    }

    //
    // MARK: Analytics
    //


    private void sendAnalytics(@ActionNameType.ActionNameMode String action) {
        HashMap<String, Object> param = new HashMap();
        param.put(KeyNameType.UserId, this.sessionManager.user().getUserId());
        param.put(KeyNameType.UserPseudo, this.sessionManager.user().getUserPseudo());
        this.analyticsManager.trackAction(action, param);
    }

    //
    // MARK: Util
    //

    private long secToMillisecond(long sec) {
        return sec * 1000;
    }

    private long makeRef() {
        final long newRef = MESSAGE_REFERENCE + 1;
        MESSAGE_REFERENCE = (newRef == Long.MAX_VALUE) ? 0 : newRef;
        return newRef;
    }

    //
    // MARK: Callback
    //

    public interface ICallBackSocketClose {
        void isClose();
    }
}
