package fr.com.panda.socket.phoenix;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

import fr.com.panda.enums.SocketType;
import fr.com.panda.http.ApiManager;
import fr.com.panda.utils.JsonUtil;

public class PhoenixPayload {

    @SerializedName("topic")
    @Expose
    private String topic;

    @SerializedName("event")
    @Expose
    private @SocketType.SocketMode String event;

    @SerializedName("ref")
    @Expose
    private String ref;

    @SerializedName("payload")
    @Expose
    private HashMap<String, Object> payload;

    public PhoenixPayload(final String topic, final @SocketType.SocketMode String event, PhoenixMessage payload) {
        this(topic, event, JsonUtil.fromJson(JsonUtil.toJson(payload.getMessage() != null ? payload.getMessage() : payload), new HashMap<String, Object>().getClass()));
    }

    public PhoenixPayload(final String topic, final @SocketType.SocketMode String event, final HashMap<String, Object> payload) {
        this.topic = topic;
        this.event = event;
        this.payload = payload;
    }

    public String toJsonString(long ref) {
        this.ref = ref + "";
        return JsonUtil.toJson(this);
    }

    public String getTopic() {
        return topic;
    }

    public @SocketType.SocketMode String getEvent() {
        return event;
    }

    public HashMap<String, Object> getPayload() {
        return payload;
    }
}
