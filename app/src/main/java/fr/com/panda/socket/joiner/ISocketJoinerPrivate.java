package fr.com.panda.socket.joiner;

import fr.com.panda.socket.connector.ISocketConnector;

public interface ISocketJoinerPrivate {
    void joinPrivateChannel(ISocketConnector socketConnector);
    void refreshToken(ISocketConnector socketConnector);
}
