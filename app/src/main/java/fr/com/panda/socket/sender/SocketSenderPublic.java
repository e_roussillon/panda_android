package fr.com.panda.socket.sender;

import android.text.TextUtils;

import java.util.HashMap;

import fr.com.panda.enums.ChannelType;
import fr.com.panda.enums.StatusType;
import fr.com.panda.enums.SocketType;
import fr.com.panda.manager.ChatManager;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.model.PublicMessageModel;
import fr.com.panda.socket.connector.ISocketConnector;
import fr.com.panda.socket.notification.SocketNotificaterManager;
import fr.com.panda.socket.phoenix.PhoenixMessage;
import fr.com.panda.socket.phoenix.PhoenixPayload;

public class SocketSenderPublic implements ISocketSenderPublic {
    private SessionManager sessionManager = SessionManager.sharedInstance;
    private SocketNotificaterManager socketNotificaterManager = SocketNotificaterManager.sharedInstance;
    private ChatManager chatManager = ChatManager.sharedInstance;

    @Override
    public void sendMessage(PublicMessageModel publicMessage, ISocketConnector socketConnector) {
        long rowId = publicMessage.insertLight();

        if (rowId > 0 && !TextUtils.isEmpty(this.chatManager.getFindRooms().token) && this.sessionManager.user().getUserId() > 0) {
            publicMessage.rowId = rowId;
            publicMessage.insert();

            this.send(publicMessage, socketConnector);
            this.socketNotificaterManager.onClearTextNotification.dispatch(null);
        }
    }

    @Override
    public void sendWelcomeMessage(PublicMessageModel publicMessage) {
        if (publicMessage.isWelcomeMessage && !publicMessage.isContainWelcomeMessage()) {
            publicMessage.insert();

            if (publicMessage.isGeneral) {
                this.socketNotificaterManager.onNewGeneralMessageNotification.dispatch(publicMessage);
            } else {
                this.socketNotificaterManager.onNewSaleMessageNotification.dispatch(publicMessage);
            }
        }
    }

    @Override
    public void resendMessage(PublicMessageModel publicMessage, ISocketConnector socketConnector) {
        this.send(publicMessage, socketConnector);
    }

    //
    // MARK: PRIVATE
    //

    private void send(PublicMessageModel publicMessage, ISocketConnector socketConnector) {
        HashMap<String, String> param = new HashMap();
        param.put("message", publicMessage.msg);
        param.put("user", publicMessage.fromUserName);
        param.put("index", publicMessage.index);

        boolean isGeneral = publicMessage.isGeneral;

        PhoenixMessage message = new PhoenixMessage(param);
        @ChannelType.ChannelMode String roomType = isGeneral ? ChannelType.GENERIC : ChannelType.SALE;
        PhoenixPayload payload = new PhoenixPayload(roomType + ":" + this.chatManager.getCurrentRoomId(), SocketType.NEWS_MSG, message);

        if (socketConnector.send(payload)) {
            if (isGeneral) {
                this.socketNotificaterManager.onNewGeneralMessageNotification.dispatch(publicMessage);
            } else {
                this.socketNotificaterManager.onNewSaleMessageNotification.dispatch(publicMessage);
            }
        } else {
            publicMessage.status = StatusType.SENDING;
            publicMessage.insert();
            if (isGeneral) {
                this.socketNotificaterManager.onFailGeneralMessageNotification.dispatch(publicMessage);
            } else {
                this.socketNotificaterManager.onFailSaleMessageNotification.dispatch(publicMessage);
            }
        }
    }
}