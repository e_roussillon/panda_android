package fr.com.panda.socket.phoenix;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.com.panda.enums.SocketType;
import fr.com.panda.socket.connector.ISocketConnector;
import fr.com.panda.socket.connector.SocketConnectorManager;

public class PhoenixChannel {

    private List<PhoenixBinding> bindings = new ArrayList();
    private String topic = "";
    private PhoenixSocket socket;
    private IPhoenixChannelCallback callback;
    private ISocketConnector socketConnectorManager = SocketConnectorManager.connectorManager;

    public PhoenixChannel(PhoenixSocket socket, String topic, IPhoenixChannelCallback callback) {
        this.socket = socket;
        this.topic = topic;
        this.callback = callback;
    }

    public String getTopic() {
        return topic;
    }

    public IPhoenixChannelCallback getCallback() {
        return callback;
    }

    public void reset() {
        bindings = new ArrayList();
    }

    public void on(@SocketType.SocketMode String event, PhoenixBinding.IPhoenixMessageCallback callback) {
        bindings.add(new PhoenixBinding(event, callback));
    }

    public boolean isMember(String topic) {
        return !TextUtils.isEmpty(this.topic) && !TextUtils.isEmpty(topic) && this.topic.equals(topic);
    }

    private void off(@SocketType.SocketMode String event) {
        List<PhoenixBinding> newBindings = new ArrayList();

        for (PhoenixBinding binding:  bindings) {
            if (!binding.getEvent().equals(event)) {
                newBindings.add(new PhoenixBinding(binding.getEvent(), binding.getCallback()));
            }
        }
        bindings = newBindings;
    }

    public void trigger(@SocketType.SocketMode String triggerEvent, HashMap<String, Object> msg) {
        for (PhoenixBinding binding : bindings) {
            if (binding.getEvent().equals(triggerEvent)) {
                binding.getCallback().onMessage(msg);
            }
        }
    }

    private void send(@SocketType.SocketMode String event, PhoenixMessage message) {
        final PhoenixPayload payload = new PhoenixPayload(this.topic, event, message);
        this.socketConnectorManager.send(payload);
    }

    public void leave(PhoenixMessage message) {
        if (this.socket == null) {
            this.reset();
            return;
        }

        this.socket.leave(topic, message);
    }



    //
    // MARK: Interface
    //

    public interface IPhoenixChannelCallback {

        void onChannel(final PhoenixChannel channel);
    }
}
