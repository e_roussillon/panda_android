package fr.com.panda.socket.joiner;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import fr.com.panda.enums.ActionNameType;
import fr.com.panda.enums.ChannelType;
import fr.com.panda.enums.KeyNameType;
import fr.com.panda.enums.SocketType;
import fr.com.panda.enums.StatusType;
import fr.com.panda.manager.AnalyticsManager;
import fr.com.panda.manager.ChatManager;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.manager.SocketManager;
import fr.com.panda.model.PublicMessageModel;
import fr.com.panda.model.ResponsePhoenixModel;
import fr.com.panda.socket.connector.ISocketConnector;
import fr.com.panda.socket.notification.SocketNotificaterManager;
import fr.com.panda.socket.phoenix.PhoenixMessage;
import fr.com.panda.utils.JsonUtil;
import timber.log.Timber;

class SocketJoinerPublic implements ISocketJoinerPublic {

    private AnalyticsManager analyticsManager = AnalyticsManager.sharedInstance;
    private ChatManager chatManager = ChatManager.sharedInstance;
    private SessionManager sessionManager = SessionManager.sharedInstance;
    private SocketNotificaterManager socketNotificaterManager = SocketNotificaterManager.sharedInstance;

    @Override
    public void joinPublicChannel(@ChannelType.ChannelMode String channelType, int roomId, String token, ISocketConnector socketConnector) {
        switch (channelType) {
            case ChannelType.GENERIC:
                this.joinGeneralChannel(roomId, token, socketConnector);
                break;
            case ChannelType.SALE:
                this.joinSaleChannel(roomId, token, socketConnector);
                break;
            case ChannelType.BROADCAST:
                this.joinBroadcastChannel(roomId, token, socketConnector);
                break;
            default:
                break;
        }
    }

    @Override
    public void refreshToken(@ChannelType.ChannelMode String channelType, int roomId, String token, ISocketConnector socketConnector) {
        PhoenixMessage message = new PhoenixMessage(this.sessionManager.user().getToken(), token);
        socketConnector.refreshToken(channelType, roomId, message);
    }


    //
    // Private
    //

    private void joinGeneralChannel(int roomId, String token, ISocketConnector socketConnector) {
        PhoenixMessage message = new PhoenixMessage(this.sessionManager.user().getToken(), token);
        socketConnector.joinChanel(ChannelType.GENERIC, roomId, message, channel -> {
            channel.on(SocketType.NEWS_MSG_ERROR, hashMap -> {
                this.sendAnalytics(ActionNameType.ErrorGeneralMessage, new HashMap());
                this.handleFailMessage(message, true);
            });

            channel.on(SocketType.NEWS_MSG, hashMap -> {
                this.handleSuccessMessage(hashMap, ChannelType.GENERIC);
            });

            channel.on(SocketType.REPLY, hashMap -> {
                ResponsePhoenixModel reponse = JsonUtil.fromJson(hashMap, ResponsePhoenixModel.class);

                if (reponse == null) {
                    return;
                }

                if (reponse.statusOk()) {
                    this.sendAnalytics(ActionNameType.JoinGeneralChat);
                    this.socketNotificaterManager.onJoinedChannelGeneral.dispatch(null);
                    this.socketNotificaterManager.onJoinPublicRoomSucceed.dispatch(null);
                }
            });
        });
    }

    private void joinSaleChannel(int roomId, String token, ISocketConnector socketConnector) {
        PhoenixMessage message = new PhoenixMessage(this.sessionManager.user().getToken(), token);
        socketConnector.joinChanel(ChannelType.SALE, roomId, message, channel -> {

            channel.on(SocketType.NEWS_MSG_ERROR, hashMap -> {
                this.sendAnalytics(ActionNameType.ErrorTradeMessage, new HashMap());
                this.handleFailMessage(message, false);
            });

            channel.on(SocketType.NEWS_MSG, hashMap -> {
                this.handleSuccessMessage(hashMap, ChannelType.SALE);
            });

            channel.on(SocketType.REPLY, hashMap -> {
                ResponsePhoenixModel reponse = JsonUtil.fromJson(hashMap, ResponsePhoenixModel.class);

                if (reponse == null) {
                    return;
                }

                if (reponse.statusOk()) {
                    this.sendAnalytics(ActionNameType.JoinTradeChat);
                    this.socketNotificaterManager.onJoinedChannelSale.dispatch(null);
                    this.socketNotificaterManager.onJoinPublicRoomSucceed.dispatch(null);
                }
            });

        });
    }

    private void joinBroadcastChannel(int roomId, String token, ISocketConnector socketConnector) {
        PhoenixMessage message = new PhoenixMessage(this.sessionManager.user().getToken(), token);
        socketConnector.joinChanel(ChannelType.BROADCAST, roomId, message, channel -> {

            channel.on(SocketType.NEWS_MSG, hashMap -> {
                this.handleSuccessMessage(hashMap, ChannelType.BROADCAST);
            });

            channel.on(SocketType.REPLY, hashMap -> {
                ResponsePhoenixModel reponse = JsonUtil.fromJson(hashMap, ResponsePhoenixModel.class);

                if (reponse == null) {
                    return;
                }

                if (reponse.statusOk()) {
                    this.sendAnalytics(ActionNameType.JoinBroadcastChat);
                    this.socketNotificaterManager.onJoinedChannelBroadcast.dispatch(null);
                }
            });

        });
    }

    //
    // MARK: Generic
    //

    private void handleSuccessMessage(HashMap<String, Object> object, @ChannelType.ChannelMode String channelType) {
        object.replace("send_type", StatusType.SENT);

        PublicMessageModel publicMessage = JsonUtil.fromJson(object, PublicMessageModel.class);

        if (publicMessage == null || publicMessage.msg == null || publicMessage.msg.length() <= 0) {
            return;
        }

        if (channelType != ChannelType.BROADCAST) {
            if (SocketManager.sharedInstance.isPublicMessageHavetoBeBlocked(publicMessage, channelType)) {
                return;
            }
        }

        publicMessage.status = StatusType.SENT;
        publicMessage.insert();

        switch (channelType) {
            case ChannelType.GENERIC:
                this.sendAnalytics(ActionNameType.ReceivedGeneralMessage);
                this.socketNotificaterManager.onNewGeneralMessageNotification.dispatch(publicMessage);
                break;
            case ChannelType.SALE:
                this.sendAnalytics(ActionNameType.ReceivedTradeMessage);
                this.socketNotificaterManager.onNewSaleMessageNotification.dispatch(publicMessage);
                break;
            case ChannelType.BROADCAST:
                this.sendAnalytics(ActionNameType.ReceivedBroadcastMessage);
                this.socketNotificaterManager.onNewBroadcastMessageNotification.dispatch(publicMessage);
                break;
            default:
                Timber.e("[Receive message] - but this message hae been managed!!!");
                break;
        }
    }

    private void handleFailMessage(Object object, boolean isGeneral) {
// TODO - Not need to be user for today
//        guard let message = object as? PhoenixMessage,
//            let (index, _) = PublicMessageSender.socketResponseToErrorMessage(message),
//            let publicMessage = PublicMessageSQL.getPublicMessage(isGeneral, index: index) else {
//                //TODO CHECK ERROR
//                return
//        }
//
//        publicMessage.sendType = SendType.Error
//        PublicMessageSQL.insertMessageLite(publicMessage)
//
//        if isGeneral {
//            self.socketNotificaterManager.onFailGeneralMessageNotification.dispatch(publicMessage)
//        } else {
//            self.socketNotificaterManager.onFailSaleMessageNotification.dispatch(publicMessage)
//        }
    }

    private void sendAnalytics(@ActionNameType.ActionNameMode String actionName) {
        sendAnalytics(actionName, new HashMap());
    }

    private void sendAnalytics(@ActionNameType.ActionNameMode String actionName, HashMap<String, Object> params) {
        HashMap<String, Object> param = new HashMap();
        param.put(KeyNameType.RoomId, this.chatManager.getCurrentRoomId() +"");
        param.put(KeyNameType.RoomName, this.chatManager.getCurrentRoom().originalName);
        param.put(KeyNameType.UserPseudo, this.sessionManager.user().getUserPseudo());

        Iterator it = params.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            param.put((String) pair.getKey(), pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }

        this.analyticsManager.trackAction(actionName, param);
    }

}
