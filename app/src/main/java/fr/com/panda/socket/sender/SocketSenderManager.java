package fr.com.panda.socket.sender;

import java.util.List;

import fr.com.panda.model.PrivateMessageModel;
import fr.com.panda.model.PublicMessageModel;
import fr.com.panda.socket.connector.ISocketConnector;

public class SocketSenderManager implements ISocketSenderPrivate, ISocketSenderPublic {

    private SocketSenderPrivate socketSenderPrivate = new SocketSenderPrivate();
    private SocketSenderPublic socketSenderPublic = new SocketSenderPublic();

    //
    // ISocketSenderPrivate
    //

    @Override
    public void sendMessage(PrivateMessageModel privateMessage, ISocketConnector socketConnector, boolean withNotification) {
        this.socketSenderPrivate.sendMessage(privateMessage, socketConnector, withNotification);
    }

    @Override
    public void sendMessages(List<PrivateMessageModel> messages, ISocketConnector socketConnector) {
        this.socketSenderPrivate.sendMessages(messages, socketConnector);
    }

    //
    // ISocketSenderPublic
    //

    @Override
    public void sendMessage(PublicMessageModel publicMessage, ISocketConnector socketConnector) {
        this.socketSenderPublic.sendMessage(publicMessage, socketConnector);
    }

    @Override
    public void resendMessage(PublicMessageModel publicMessage, ISocketConnector socketConnector) {
        this.socketSenderPublic.resendMessage(publicMessage, socketConnector);
    }

    @Override
    public void sendWelcomeMessage(PublicMessageModel publicMessage) {
        this.socketSenderPublic.sendWelcomeMessage(publicMessage);
    }






}
