package fr.com.panda.socket.highlighter;

import fr.com.panda.enums.ChannelType;

public interface ISocketHighlighter {
    boolean isMessageNeedToBeBlocked(int fromUserId, @ChannelType.ChannelMode String channelType);
}
