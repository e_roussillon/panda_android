package fr.com.panda.socket.joiner;

import fr.com.panda.enums.ChannelType;
import fr.com.panda.socket.connector.ISocketConnector;

public interface ISocketJoinerPublic {
    void joinPublicChannel(@ChannelType.ChannelMode String channelType, int roomId, String token, ISocketConnector socketConnector);
    void refreshToken(@ChannelType.ChannelMode String channelType, int roomId, String token, ISocketConnector socketConnector);
}
