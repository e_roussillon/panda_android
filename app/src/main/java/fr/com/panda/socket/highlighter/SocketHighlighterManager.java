package fr.com.panda.socket.highlighter;

import fr.com.panda.enums.ChannelType;
import fr.com.panda.model.PublicMessageModel;

public class SocketHighlighterManager implements ISocketHighlighter, ISocketPublicHighlighter {
    public static final SocketHighlighterManager sharedInstance = new SocketHighlighterManager();
    private ISocketHighlighter socketHighlighter = SocketHighlighter.sharedInstance;
    private SocketPublicHighlighter socketPublicHighlighter = SocketPublicHighlighter.sharedInstance;

    private SocketHighlighterManager() {
        this.socketPublicHighlighter.socketHighlighter = socketHighlighter;
    }

    //
    // MARK: ISocketHighlighter
    //

    @Override
    public boolean isMessageNeedToBeBlocked(int fromUserId, @ChannelType.ChannelMode String channelType) {
        return socketHighlighter.isMessageNeedToBeBlocked(fromUserId, channelType);
    }

    //
    // MARK: ISocketPublicHighlighter
    //

    @Override
    public boolean checkIsIncomingPublicMessageHaveToBeBlocked(PublicMessageModel publicMessage, @ChannelType.ChannelMode String channelType) {
        return socketPublicHighlighter.checkIsIncomingPublicMessageHaveToBeBlocked(publicMessage, channelType);
    }
}