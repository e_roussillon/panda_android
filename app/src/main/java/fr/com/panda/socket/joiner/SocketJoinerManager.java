package fr.com.panda.socket.joiner;

import java.util.HashMap;

import fr.com.panda.enums.ChannelType;
import fr.com.panda.model.PrivateMessageModel;
import fr.com.panda.socket.connector.ISocketConnector;

public class SocketJoinerManager implements ISocketJoinerPrivate, ISocketJoinerPublic {

    private SocketJoinerPrivate mSocketJoinerPrivate = new SocketJoinerPrivate();
    private SocketJoinerPublic mSocketJoinerPublic = new SocketJoinerPublic();

    static PrivateMessageModel handleNotifMessage(HashMap<String, Object> object)  {
//        let sessionManager = SessionManager.sharedInstance
//        if let privateMessage = Mapper<PrivateMessageModel>().map(object),
//        let roomId = privateMessage.roomId,
//        let toUserName = privateMessage.toUserName,
//        let toUserId = privateMessage.toUserId,
//        let index = privateMessage.index,
//        let fromUserId = privateMessage.fromUserId,
//        let fromUserName = privateMessage.fromUserName,
//        let friendshipStatus = privateMessage.friendshipStatus,
//        let friendshipLastAction = privateMessage.friendshipLastAction {
//
//            let message = PrivateMessageModel.getMessageByIndex(index)
//
//            privateMessage.rowId = message?.rowId
//            privateMessage.dataLocal = message?.dataLocal
//            privateMessage.insert()
//
//            if sessionManager.user.userId == toUserId {
//                //new message
//                PrivateChatModel.insertRoom(roomId, name: fromUserName)
//                PrivateChatModel.insertUsersRoom(roomId, toUserId: fromUserId)
//                UserModel.insert(fromUserId, userName: fromUserName, friendshipStatus: friendshipStatus, friendshipLastAction: friendshipLastAction)
//                return privateMessage
//            } else {
//                //update message
//                PrivateChatModel.insertRoom(roomId, name: toUserName)
//                PrivateChatModel.insertUsersRoom(roomId, toUserId: toUserId)
//                UserModel.insert(toUserId, userName: toUserName, friendshipStatus: friendshipStatus, friendshipLastAction: friendshipLastAction)
//
//                return privateMessage
//            }
//        } else {
//            return nil
//        }
        return null;
    }

    public boolean isJoined(@ChannelType.ChannelMode String  channelType, int roomId, ISocketConnector socketConnector) {
        return socketConnector.isJoined(channelType + ":" + roomId);
    }

    //
    // ISocketJoinerPrivate
    //

    @Override
    public void joinPrivateChannel(ISocketConnector socketConnector) {
        mSocketJoinerPrivate.joinPrivateChannel(socketConnector);
    }

    @Override
    public void refreshToken(ISocketConnector socketConnector) {
        mSocketJoinerPrivate.refreshToken(socketConnector);
    }

    //
    // ISocketJoinerPublic
    //

    @Override
    public void joinPublicChannel(@ChannelType.ChannelMode String channelType, int roomId, String token, ISocketConnector socketConnector) {
        mSocketJoinerPublic.joinPublicChannel(channelType, roomId, token, socketConnector);
    }

    @Override
    public void refreshToken(@ChannelType.ChannelMode String channelType, int roomId, String token, ISocketConnector socketConnector) {
        mSocketJoinerPublic.refreshToken(channelType, roomId, token, socketConnector);
    }
}
