package fr.com.panda.socket.connector;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.com.panda.enums.ChannelType;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.socket.notification.SocketNotificaterManager;
import fr.com.panda.socket.phoenix.PhoenixChannel;
import fr.com.panda.socket.phoenix.PhoenixMessage;
import fr.com.panda.socket.phoenix.PhoenixPayload;
import fr.com.panda.socket.phoenix.PhoenixSocket;
import fr.com.panda.utils.Constants;
import timber.log.Timber;

public class SocketConnectorManager implements ISocketConnector {
    public static final ISocketConnector connectorManager = new SocketConnectorManager();

    private SocketNotificaterManager socketNotificaterManager = SocketNotificaterManager.sharedInstance;
    private SessionManager sessionManager = SessionManager.sharedInstance;

    private PhoenixSocket socket;
//    private var timer: NSTimer?
    private int currentTimer = -1;
    private List<PhoenixPayload> sendBufferSaleMessage = new ArrayList();
    private List<PhoenixPayload> sendBufferGeneralMessage = new ArrayList();

    private SocketConnectorManager() {}

    //
    // ISocketConnector
    //

    @Override
    public boolean isSocketConnected() {
        if (this.socket != null) {
            return socket.isConnected();
        }
        return false;
    }

    @Override
    public boolean startSocket() {
        if (sessionManager.user().isLogin()) {
            HashMap<String, String> disc = new HashMap();
            disc.put("token", sessionManager.user().getToken());

            if (this.socket == null) {
                socket = new PhoenixSocket(Constants.SOCKET.kURL, Constants.SOCKET.kPath, Constants.SOCKET.kTransport, Constants.SOCKET.kProt, disc);
            } else {
                this.socket.forceDisconnection = false;
                this.stopSocket(() -> {
                    this.socket.updateEndPoint(Constants.SOCKET.kURL, Constants.SOCKET.kPath, Constants.SOCKET.kTransport, Constants.SOCKET.kProt, disc);
                });
            }
            return true;
        }
        return false;
    }

    @Override
    public void stopSocket(PhoenixSocket.ICallBackSocketClose callback) {
        if (this.socket != null) {
            this.socket.close(() -> {
                callback.isClose();
            });
        }
    }

    @Override
    public void forceStopSocket(PhoenixSocket.ICallBackSocketClose callback) {
        if (this.socket != null) {
            this.socket.forceDisconnection = true;
            this.socket.numberOfTime = 0;
//            this.timer?.invalidate()
            this.socket.close(() -> {
                    callback.isClose();
            });
        }
    }

    @Override
    public boolean send(PhoenixPayload data) {
        if (this.isSocketConnected()) {
            this.socket.send(data);
            return true;
        } else {
            if (TextUtils.isEmpty(data.getTopic())) {
                return false;
            }

            if (data.getTopic().contains(ChannelType.GENERIC)) {
                this.sendBufferSaleMessage.add(data);
            } else if (data.getTopic().contains(ChannelType.SALE)) {
                this.sendBufferGeneralMessage.add(data);
            }

            return false;
        }
    }

    @Override
    public boolean isJoined(String topic) {
        if (this.socket != null) {
            return socket.isJoined(topic);
        }
        return false;
    }

    @Override
    public void joinChanel(@ChannelType.ChannelMode String channel, int roomId, PhoenixMessage message, PhoenixChannel.IPhoenixChannelCallback callback) {
        if (this.socket != null) {
            this.socket.join(channel + ":" + roomId, message, callback);
        } else {
            Timber.e("ERROR !!! SOCKET NOT CONNECTED YET");
        }
    }

    @Override
    public void leaveAllRoom() {
        if (this.socket != null) {
            this.socket.leaveAllWithoutMessage();
        }
    }

    @Override
    public void leaveRoomPublicRooms() {
        if (this.socket != null) {
            this.socket.leaveRoomPublicRooms();
        }
    }

    @Override
    public void refreshToken(@ChannelType.ChannelMode String channel, int roomId, PhoenixMessage message) {
        if (this.socket != null) {
            this.socket.refreshToken(channel + ":" + roomId, message);
        }
    }
}
