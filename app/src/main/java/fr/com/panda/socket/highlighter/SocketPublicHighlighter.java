package fr.com.panda.socket.highlighter;

import android.text.TextUtils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fr.com.panda.enums.ActionNameType;
import fr.com.panda.enums.ChannelType;
import fr.com.panda.enums.KeyNameType;
import fr.com.panda.manager.AnalyticsManager;
import fr.com.panda.manager.ChatManager;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.model.FilterModel;
import fr.com.panda.model.PublicMessageModel;

public class SocketPublicHighlighter implements ISocketPublicHighlighter {
    public static final SocketPublicHighlighter sharedInstance = new SocketPublicHighlighter();
    private AnalyticsManager analyticsManager = AnalyticsManager.sharedInstance;
    private SessionManager sessionManager = SessionManager.sharedInstance;
    private ChatManager chatManager = ChatManager.sharedInstance;

    public ISocketHighlighter socketHighlighter;

    private SocketPublicHighlighter() {}


    @Override
    public boolean checkIsIncomingPublicMessageHaveToBeBlocked(PublicMessageModel publicMessage, @ChannelType.ChannelMode String channelType) {

        if (publicMessage.fromUserId <= 0  || !TextUtils.isEmpty(publicMessage.fromUserName) || !TextUtils.isEmpty(publicMessage.msg)) {
            return false;
        }

        if (!socketHighlighter.isMessageNeedToBeBlocked(publicMessage.fromUserId, channelType) && !this.isMessageNeedToBeBlockedByFilters(publicMessage.msg, publicMessage.fromUserId, publicMessage.fromUserName, channelType)) {
            publicMessage = this.isMessageNeedToBeHighlighted(publicMessage, publicMessage.msg, publicMessage.fromUserId, publicMessage.fromUserName, channelType);
            return false;
        }

        return true;
    }

    //
    // Private
    //

    private boolean isMessageNeedToBeBlockedByFilters(String message, int userId, String pseudo, @ChannelType.ChannelMode String channelType) {

        List<FilterModel> list = FilterModel.getFilters(true);

        if (list != null && list.size() > 0) {
            for (FilterModel item : list) {
                if (message.toLowerCase().contains(item.getName().toLowerCase())) {

                    HashMap<String, String> param = new HashMap();
                    param.put(KeyNameType.ToUserId, userId+"");
                    param.put(KeyNameType.ToUserPseudo, pseudo);
                    param.put(KeyNameType.Word, item.getName().toLowerCase());
                    param.put(KeyNameType.Message, message.toLowerCase());

                    switch (channelType) {
                        case ChannelType.GENERIC:
                            this.sendAnalytics(ActionNameType.BlockedUserGeneralMessage, param);
                            break;
                        case ChannelType.PRIVATE:
                            this.sendAnalytics(ActionNameType.BlockedUserTradeMessage, param);
                            break;
                        default:
                            break;
                    }

                    return true;
                }
            }
        }

        return false;
    }

    private PublicMessageModel isMessageNeedToBeHighlighted(PublicMessageModel publicMessage, String message, int userId, String pseudo, @ChannelType.ChannelMode String channelType) {
        publicMessage.isHighlight = false;

        List<FilterModel> list = FilterModel.getFilters(true);

        for (FilterModel item : list) {
            if (message.toLowerCase().contains(item.getName().toLowerCase())) {
                publicMessage.isHighlight = true;

                HashMap<String, String> param = new HashMap();
                param.put(KeyNameType.ToUserId, userId+"");
                param.put(KeyNameType.ToUserPseudo, pseudo);
                param.put(KeyNameType.Word, item.getName().toLowerCase());
                param.put(KeyNameType.Message, message.toLowerCase());

                switch (channelType) {
                    case ChannelType.GENERIC:
                        this.sendAnalytics(ActionNameType.BlockedUserGeneralMessage, param);
                        break;
                    case ChannelType.PRIVATE:
                        this.sendAnalytics(ActionNameType.BlockedUserTradeMessage, param);
                        break;
                    default:
                        break;
                }

                break;
            }
        }

        return publicMessage;
    }

    private void sendAnalytics(@ActionNameType.ActionNameMode String actionName, HashMap<String, String> params) {
        HashMap<String, Object> param = new HashMap();
        param.put(KeyNameType.RoomId, this.chatManager.getCurrentRoomId()+"");
        param.put(KeyNameType.RoomName, this.chatManager.getCurrentRoom().originalName);
        param.put(KeyNameType.UserId, this.sessionManager.user().getUserId()+"");
        param.put(KeyNameType.UserPseudo, this.sessionManager.user().getUserPseudo());

        if (params != null && params.size() > 0) {
            Iterator it = params.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                param.put((String)pair.getKey(), (String)pair.getValue());
                it.remove(); // avoids a ConcurrentModificationException
            }
        }

        this.analyticsManager.trackAction(actionName, param);
    }
}
