package fr.com.panda.socket.highlighter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import fr.com.panda.enums.ActionNameType;
import fr.com.panda.enums.ChannelType;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.enums.KeyNameType;
import fr.com.panda.manager.AnalyticsManager;
import fr.com.panda.manager.ChatManager;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.model.UserModel;

public class SocketHighlighter implements ISocketHighlighter {
    public static final SocketHighlighter sharedInstance = new SocketHighlighter();
    private AnalyticsManager analyticsManager = AnalyticsManager.sharedInstance;
    private SessionManager sessionManager = SessionManager.sharedInstance;
    private ChatManager chatManager = ChatManager.sharedInstance;

    private SocketHighlighter() {}

    @Override
    public boolean isMessageNeedToBeBlocked(int fromUserId, @ChannelType.ChannelMode String channelType) {
        UserModel user = UserModel.getUser(fromUserId);

        if (user != null
                && (user.friendshipStatus == FriendshipType.BLOCKED_BEFORE_BE_FRIEND
                || user.friendshipStatus == FriendshipType.BLOCKED_AFTER_BE_FRIEND)
                && user.friendshipLastAction == sessionManager.user().getUserId()) {

            HashMap<String, Object> param = new HashMap();
            param.put(KeyNameType.ToUserId, user.userId +"");
            param.put(KeyNameType.ToUserPseudo, user.pseudo);

            switch (channelType) {
                case ChannelType.GENERIC:
                    this.sendAnalytics(ActionNameType.BlockedUserGeneralMessage, param);
                    break;
                case ChannelType.PRIVATE:
                    this.sendAnalytics(ActionNameType.BlockedUserTradeMessage, param);
                    break;
                default:
                    break;
            }

            return true;
        }

        return false;
    }

    //
    // MARK: Private
    //

    private void sendAnalytics(@ActionNameType.ActionNameMode String actionName, HashMap<String, Object> params) {

        HashMap<String, Object> param = new HashMap();
        param.put(KeyNameType.RoomName, this.chatManager.getCurrentRoom().originalName);
        param.put(KeyNameType.UserId, this.sessionManager.user().getUserId() +"");
        param.put(KeyNameType.UserPseudo, this.sessionManager.user().getUserPseudo());


        if (params != null && params.size() > 0) {
            Iterator it = params.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                param.put((String)pair.getKey(), (String)pair.getValue());
                it.remove(); // avoids a ConcurrentModificationException
            }
        }

        this.analyticsManager.trackAction(actionName, param);
    }
}
