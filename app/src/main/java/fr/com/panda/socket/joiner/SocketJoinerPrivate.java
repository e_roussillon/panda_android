package fr.com.panda.socket.joiner;

import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fr.com.panda.enums.ActionNameType;
import fr.com.panda.enums.ChannelType;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.enums.KeyNameType;
import fr.com.panda.enums.SocketType;
import fr.com.panda.enums.StatusType;
import fr.com.panda.manager.AnalyticsManager;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.manager.SocketManager;
import fr.com.panda.model.BackupModel;
import fr.com.panda.model.NotificationMessage;
import fr.com.panda.model.PrivateMessageModel;
import fr.com.panda.model.ResponsePhoenixModel;
import fr.com.panda.model.UserModel;
import fr.com.panda.socket.connector.ISocketConnector;
import fr.com.panda.socket.notification.SocketNotificaterManager;
import fr.com.panda.socket.phoenix.PhoenixMessage;
import fr.com.panda.utils.DateUtil;
import fr.com.panda.utils.JsonUtil;

class SocketJoinerPrivate implements ISocketJoinerPrivate {

    private AnalyticsManager analyticsManager = AnalyticsManager.sharedInstance;
    private SessionManager sessionManager = SessionManager.sharedInstance;
    private SocketManager socketManager;
    private SocketNotificaterManager socketNotificaterManager = SocketNotificaterManager.sharedInstance;

    @Override
    public void refreshToken(ISocketConnector socketConnector) {
        PhoenixMessage message = new PhoenixMessage(sessionManager.user().getToken(), UserModel.getCurrentUser().pseudo);
        socketConnector.refreshToken(ChannelType.PRIVATE, sessionManager.user().getUserId(), message);
    }

    @Override
    public void joinPrivateChannel(ISocketConnector socketConnector) {
        PhoenixMessage message = new PhoenixMessage(sessionManager.user().getToken(), UserModel.getCurrentUser().pseudo);
        socketConnector.joinChanel(ChannelType.PRIVATE, sessionManager.user().getUserId(), message, channel -> {
            channel.on(SocketType.NEWS_MSG, hashMap -> this.handleSuccessMessage(hashMap));

            channel.on(SocketType.NEWS_MSG_CONFIRMATION, hashMap -> this.handleSuccessMessagesStatus(hashMap));

            channel.on(SocketType.NEWS_USERS_CONNECT, hashMap -> {

                if (hashMap == null || !hashMap.containsKey("list")) {
                    return;
                }

                List<UserModel> users = JsonUtil.fromJson(JsonUtil.toJson(hashMap.get("list")), new TypeToken<List<UserModel>>(){}.getType());

                if (users == null) {
                    return;
                }

                UserModel.updateUsers(users);
                this.socketNotificaterManager.onNewConnection.dispatch(users);
                this.sendAnalytics(ActionNameType.UsersConnect);
            });

            channel.on(SocketType.NEWS_USER_CONNECT, hashMap -> {
                if (hashMap == null || !(hashMap.containsKey("user_id")
                        && hashMap.containsKey("last_seen")
                        && hashMap.get("user_id") instanceof Double
                        && hashMap.get("last_seen") instanceof Double)) {

                    return;
                }

                Double userIdD = (Double) hashMap.get("user_id");
                Double lastSeenD = (Double) hashMap.get("last_seen");

                int userId = userIdD.intValue();
                long lastSeen = lastSeenD.longValue();
                UserModel user = UserModel.getUser(userId);

                if (user == null) {
                    return;
                }

                user.isOnline = true;
                user.lastSeen = DateUtil.timestampToDate(lastSeen);
                user.update();
                List<UserModel> list = new ArrayList();
                list.add(user);
                this.socketNotificaterManager.onNewConnection.dispatch(list);

                HashMap<String, Object> param = new HashMap();
                param.put(KeyNameType.ToUserId, user.userId + "");
                param.put(KeyNameType.ToUserPseudo, user.pseudo);
                this.sendAnalytics(ActionNameType.UserConnect, param);
            });

            channel.on(SocketType.NEWS_USER_DISCONNECT, hashMap -> {
                if (hashMap == null || !(hashMap.containsKey("user_id")
                                        && hashMap.containsKey("last_seen")
                                        && hashMap.get("user_id") instanceof Double
                                        && hashMap.get("last_seen") instanceof Double)) {

                    return;
                }

                Double userIdD = (Double) hashMap.get("user_id");
                Double lastSeenD = (Double) hashMap.get("last_seen");

                int userId = userIdD.intValue();
                long lastSeen = lastSeenD.longValue();
                UserModel user = UserModel.getUser(userId);

                if (user == null) {
                    return;
                }

                user.isOnline = false;
                user.lastSeen = DateUtil.timestampToDate(lastSeen);
                user.update();
                this.socketNotificaterManager.onNewDisconnection.dispatch(user);

                HashMap<String, Object> param = new HashMap();
                param.put(KeyNameType.ToUserId, user.userId + "");
                param.put(KeyNameType.ToUserPseudo, user.pseudo);
                this.sendAnalytics(ActionNameType.UserDisconnect, param);
            });

            channel.on(SocketType.NEWS_USER_FRIENDSHIP, hashMap -> {
                UserModel user = JsonUtil.fromJson(hashMap, UserModel.class);

                if (user == null) {
                    return;
                }

                user.update();
                this.socketNotificaterManager.onUpdateFriendship.dispatch(user);
                this.sendNotifFriendship(user);

                HashMap<String, Object> param = new HashMap();
                param.put(KeyNameType.ToUserId, user.userId + "");
                param.put(KeyNameType.ToUserPseudo, user.pseudo);
                param.put(KeyNameType.FriendshipLastAction, user.friendshipLastAction);
                this.sendAnalytics(ActionNameType.UserFriendship, param);
            });

            channel.on(SocketType.NEWS_USER_DUPLICATE, hashMap -> {
                this.sendAnalytics(ActionNameType.UserDuplicate, new HashMap());
                this.forceDisconnect();
            });

            channel.on(SocketType.NEWS_USER_REPORTED, hashMap -> {
                if (hashMap == null || !(hashMap.containsKey("time")
                                        && hashMap.get("time") instanceof Double)) {
                    return;
                }

                Double timeD = (Double) hashMap.get("time");

                this.sendAnalytics(ActionNameType.UserReported);
                this.disconnectUser(timeD.intValue());
            });

            channel.on(SocketType.NEWS_USER_PENDING, hashMap -> {
                BackupModel backup = JsonUtil.fromJson(hashMap, BackupModel.class);

                if (backup == null) {
                    return;
                }

                backup.insert();

                int userId = this.sessionManager.user().getUserId();
                List<PrivateMessageModel> list = PrivateMessageModel.getPendingMessages();
                List<PrivateMessageModel> newList = new ArrayList();

                for (PrivateMessageModel item: list) {

                    if (item.fromUserId > 0 && userId == item.fromUserId) {
                        item.status = StatusType.CONFIRMED;
                    } else {
                        item.status = StatusType.RECEIVED;
                    }

                    newList.add(item);
                }

                this.sendAnalytics(ActionNameType.UserBackup);
                this.getSocketManger().sendPrivateMessages(newList);
                this.socketNotificaterManager.onGetBackupMessagesNotification.dispatch(null);
            });

            channel.on(SocketType.REPLY, hashMap -> {
                ResponsePhoenixModel reponse = JsonUtil.fromJson(hashMap, ResponsePhoenixModel.class);

                if (reponse == null) {
                    return;
                }

                if (reponse.statusOk()) {
                    this.sendAnalytics(ActionNameType.JoinPrivateChat);
                    this.socketNotificaterManager.onJoinedChannelPrivate.dispatch(null);
                } if (reponse.response != null && reponse.response.containsKey("time")) {
                    this.disconnectUser((int) reponse.response.get("time"));
                } else {
//                    TODO - Need to see how to do
//                    BaseAPIManager.startRefreshToken()
                }
            });

        });
    }

    //
    // MARK: Generic
    //

    private void handleSuccessMessagesStatus(HashMap<String, Object> object) {
        List<PrivateMessageModel> newList = new ArrayList();

        List<HashMap<String, Object>> messages = (List<HashMap<String, Object>>)object.get("messages");
        for (HashMap<String, Object> message: messages) {
            PrivateMessageModel msg = this.savePrivateMessage(message);
            if (null != msg) {
                newList.add(msg);
            }
        }

        int userId = this.sessionManager.user().getUserId();

        List<PrivateMessageModel> listToSave = new ArrayList();

        for (PrivateMessageModel message : newList) {
            if (message.fromUserId == userId && message.status == StatusType.READ) {
                message.status = StatusType.CONFIRMED;
                listToSave.add(message);
            } else if (message.fromUserId != userId && (message.status == StatusType.RECEIVED || message.status == StatusType.SENT)) {
                message.status = StatusType.READ;
                listToSave.add(message);
            }
        }

        this.getSocketManger().sendPrivateMessages(listToSave);
        for (PrivateMessageModel item : newList) {
            this.socketNotificaterManager.onNewPrivateMessageNotification.dispatch(new NotificationMessage(false, item));
        }
    }

    private PrivateMessageModel savePrivateMessage(HashMap<String, Object> object) {
        int id = 0;
        @StatusType.StatusMode int status = -2;
        PrivateMessageModel privateMessage = null;

        if (object.get("id") instanceof Integer) {
            id = (int)object.get("id");
            privateMessage = PrivateMessageModel.getMessage(id);
        }

        if (object.get("status") instanceof Integer) {
            status = StatusType.fromId((int) object.get("status"));
        }

        if (id != 0 && privateMessage != null) {
            privateMessage.status = status;
            privateMessage.updateStatus();
            return privateMessage;
        }

        return null;
    }

    private void handleSuccessMessage(HashMap<String, Object> object) {
        PrivateMessageModel privateMessage = SocketJoinerManager.handleNotifMessage(object);

        if (null == privateMessage || privateMessage.toUserId > 0) {
            return;
        }

        if (this.sessionManager.user().getUserId() == privateMessage.toUserId) {
            privateMessage.status = StatusType.RECEIVED;
            List<PrivateMessageModel> privateMessages = new ArrayList();
            privateMessages.add(privateMessage);
            this.getSocketManger().sendPrivateMessages(privateMessages);
            this.socketNotificaterManager.onNewPrivateMessageNotification.dispatch(new NotificationMessage(true, privateMessage));
            this.sendNotifMessage(privateMessage);
        } else {
            this.socketNotificaterManager.onNewPrivateMessageNotification.dispatch(new NotificationMessage(false, privateMessage));
        }
    }

    private void disconnectUser(int time) {
        sessionManager.logout();
        SocketManager.sharedInstance.forceStopSocket(() -> {});
        socketNotificaterManager.onUserReported.dispatch(time);
    }

    private void forceDisconnect() {
        sessionManager.logout();
        SocketManager.sharedInstance.forceStopSocket(() -> {});
        socketNotificaterManager.onShowSplashScreen.dispatch(null);
    }

    private void sendNotifFriendship(UserModel user) {
        String pseudo = user.pseudo;
        int userId = user.userId;

        if (!TextUtils.isEmpty(pseudo) && userId > 0 && user.friendshipStatus == FriendshipType.PENDING) {

//      TODO - need to apply for android
//            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//            if let window = appDelegate.window,
//            let root = window.rootViewController,
//            let nav = root as? BaseNavigationController {
//
//                if let home = nav.viewControllers.last as? HomeViewController {
//                    if let navPre = home.presentedViewController as? BaseNavigationController {
//                        if let fly = navPre.viewControllers.last as? FlyingProfileViewController where fly.user == user {
//                            //DO NOTHING WE ARE IN THE RIGHT PLACE
//                        } else {
//                            UILocalNotification.showFriendship(pseudo, userId: userId)
//                        }
//                    } else if home.homePageDelegate.current != 0 {
//                        UILocalNotification.showFriendship(pseudo, userId: userId)
//                    }
//                } else if let navPre = nav.presentedViewController as? BaseNavigationController,
//                        let fly = navPre.viewControllers.last as? FlyingProfileViewController {
//
//                    if fly.user != user {
//                        UILocalNotification.showFriendship(pseudo, userId: userId)
//                    }
//                } else {
//                    UILocalNotification.showFriendship(pseudo, userId: userId)
//                }
//
//            } else {
//                UILocalNotification.showFriendship(pseudo, userId: userId)
//            }

        }
    }

    private void sendNotifMessage(PrivateMessageModel message) {
//      TODO - need to apply for android
//        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        if let window = appDelegate.window,
//        let root = window.rootViewController,
//        let nav = root as? BaseNavigationController,
//                let userIdFrom = message.fromUserId {
//
//            let userFrom = UserModel.getUser(userIdFrom)
//
//            if let chat = nav.viewControllers.last as? ChatViewController where userFrom == chat.user {
//                //DO NOTHING WE ARE IN THE RIGHT PLACE
//            } else if let navPre = nav.presentedViewController as? BaseNavigationController,
//                    let chat = navPre.viewControllers.last as? ChatViewController where userFrom == chat.user {
//                //DO NOTHING WE ARE IN THE RIGHT PLACE
//            } else {
//                UILocalNotification.showPrivateMessage(message)
//            }
//
////            if let _ = home.presentedViewController {
////                UILocalNotification.showPrivateMessage(false, privateMessage: message)
////            } else if home.homePageDelegate.current != 1 {
////                UILocalNotification.showPrivateMessage(false, privateMessage: message)
////            }
////
//
//        } else {
//            UILocalNotification.showPrivateMessage(message)
//        }
    }

    private void sendAnalytics(@ActionNameType.ActionNameMode String action) {
        sendAnalytics(action, new HashMap());
    }

    private void sendAnalytics(@ActionNameType.ActionNameMode String action, HashMap<String, Object> params) {
        HashMap<String, Object> param = new HashMap();
        param.put(KeyNameType.UserId, this.sessionManager.user().getUserId() +"");
        param.put(KeyNameType.UserPseudo, this.sessionManager.user().getUserPseudo());

        Iterator it = params.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            param.put((String) pair.getKey(), pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }

        this.analyticsManager.trackAction(action, param);
    }

    private SocketManager getSocketManger() {
        if (socketManager == null) {
            socketManager = SocketManager.sharedInstance;
        }
        return socketManager;
    }
}
