package fr.com.panda.model;

/**
 * @author Edouard Roussillon
 */

public class NotificationImageMessage {

    private long bytesWritten = 0;
    private long totalBytesWritten = 0;
    private long totalBytesExpected = 0;
    private PrivateMessageModel message;

    public NotificationImageMessage(long bytesWritten, long totalBytesWritten, long totalBytesExpected, PrivateMessageModel message) {
        this.bytesWritten = bytesWritten;
        this.totalBytesWritten = totalBytesWritten;
        this.totalBytesExpected = totalBytesExpected;
        this.message = message;
    }

    public long getBytesWritten() {
        return bytesWritten;
    }

    public long getTotalBytesWritten() {
        return totalBytesWritten;
    }

    public long getTotalBytesExpected() {
        return totalBytesExpected;
    }

    public PrivateMessageModel getMessage() {
        return message;
    }
}
