package fr.com.panda.model;

/**
 * @author Edouard Roussillon
 */

public class Location {
    private String longitude = "0.0";
    private String latitude = "0.0";

    public Location(double latitude, double longitude) {
        this.longitude = String.valueOf(longitude);
        this.latitude = String.valueOf(latitude);
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public static Location fromAndroidLocalizaion(android.location.Location location) {
        if (location == null) {
            return null;
        }

        return new Location(location.getLatitude(), location.getLongitude());
    }
}
