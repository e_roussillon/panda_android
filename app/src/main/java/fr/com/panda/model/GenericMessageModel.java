package fr.com.panda.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.net.URL;
import java.util.Date;

import fr.com.panda.enums.DataType;
import fr.com.panda.enums.StatusType;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class GenericMessageModel {

    @SerializedName("row_id")
    @Expose
    public long rowId;

    @SerializedName("id")
    @Expose
    public int roomId;

    @SerializedName("room_id")
    @Expose
    public int messageId;

    @SerializedName("msg")
    @Expose
    public String msg;

    @SerializedName("from_user_id")
    @Expose
    public int fromUserId;

    @SerializedName("from_user_name")
    @Expose
    public String fromUserName;

    @SerializedName("data_id")
    @Expose
    public int dataId = -1;

    @SerializedName("url_thumb")
    @Expose
    public URL urlThumb;

    @SerializedName("url_original")
    @Expose
    public URL urlOriginal;

    @SerializedName("url_blur")
    @Expose
    public URL urlBlur;

    @SerializedName("type")
    @Expose
    @DataType.DataMode
    public int type = DataType.NONE;

    @SerializedName("size")
    @Expose
    public float size;

    @SerializedName("data_local")
    @Expose
    public String dataLocal;

    @SerializedName("index")
    @Expose
    public String index;

    @SerializedName("status")
    @Expose
    @StatusType.StatusMode
    public int status = StatusType.SENDING;

    @SerializedName("inserted_at")
    @Expose
    public Date insertedAt;

    @SerializedName("uploading")
    @Expose
    public float uploading = 0L;

    @SerializedName("is_failed")
    @Expose
    public boolean isFailed = false;

    public GenericMessageModel(int roomId,
                               int messageId,
                               String msg,
                               int fromUserId,
                               int dataId,
                               URL urlThumb,
                               URL urlOriginal,
                               URL urlBlur,
                               @DataType.DataMode int type,
                               float size,
                               String dataLocal,
                               String index,
                               @StatusType.StatusMode int status,
                               Date insertedAt) {
        this.roomId = roomId;
        this.messageId = messageId;
        this.msg = msg;
        this.fromUserId = fromUserId;
        this.dataId = dataId;
        this.urlThumb = urlThumb;
        this.urlOriginal = urlOriginal;
        this.urlBlur = urlBlur;
        this.type = type;
        this.size = size;
        this.dataLocal = dataLocal;
        this.index = index;
        this.status = status;
        this.insertedAt = insertedAt;
    }

    public GenericMessageModel(int roomId,
                               int messageId,
                               String msg,
                               int fromUserId,
                               String fromUserName,
                               int dataId,
                               URL urlThumb,
                               URL urlOriginal,
                               URL urlBlur,
                               @DataType.DataMode int type,
                               float size,
                               String dataLocal,
                               String index,
                               @StatusType.StatusMode int status,
                               Date insertedAt) {
        this.roomId = roomId;
        this.messageId = messageId;
        this.msg = msg;
        this.fromUserId = fromUserId;
        this.fromUserName = fromUserName;
        this.dataId = dataId;
        this.urlThumb = urlThumb;
        this.urlOriginal = urlOriginal;
        this.urlBlur = urlBlur;
        this.type = type;
        this.size = size;
        this.dataLocal = dataLocal;
        this.index = index;
        this.status = status;
        this.insertedAt = insertedAt;
    }
}
