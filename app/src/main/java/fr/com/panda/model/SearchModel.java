package fr.com.panda.model;

/**
 * @author Edouard Roussillon
 */

public class SearchModel {

    private Paginated<UserModel> paginated;
    private String searchText;

    public SearchModel(Paginated<UserModel> paginated, String searchText) {
        this.paginated = paginated;
        this.searchText = searchText;
    }

    public Paginated<UserModel> getPaginated() {
        return paginated;
    }

    public String getSearchText() {
        return searchText;
    }
}
