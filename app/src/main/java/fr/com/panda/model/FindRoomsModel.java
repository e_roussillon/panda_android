package fr.com.panda.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class FindRoomsModel {

    @SerializedName("token")
    @Expose
    public String token;

    @SerializedName("rooms")
    @Expose
    public List<PublicRoomModel> rooms;

}
