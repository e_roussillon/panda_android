package fr.com.panda.model;

import java.net.URL;
import java.util.Date;
import java.util.List;

import fr.com.panda.enums.DataType;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.model.sql.PrivateRoomSQL;
import fr.com.panda.model.sql.PrivateRoomUserSQL;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class PrivateChatModel {

    public int roomId;
    public boolean isGroup;
    public String nameRoom;
    public URL urlThumb;
    public URL urlOriginal;
    public URL urlBlur;
    public @DataType.DataMode int type;
    public @FriendshipType.FriendshipMode int userStatus;
    public int userStatusLastAction;
    public String msg;
    public Date msgSent;
    public int numOfMsgNotRead;
    public int userId;
    public boolean userIsOnline;
    public Date userLastSeen;

    public PrivateChatModel(int roomId, boolean isGroup, String nameRoom, URL urlThumb, URL urlOriginal, URL urlBlur, @DataType.DataMode int type, @FriendshipType.FriendshipMode int userStatus, int userStatusLastAction, String msg, Date msgSent, int numOfMsgNotRead, int userId, boolean userIsOnline, Date userLastSeen) {
        this.roomId = roomId;
        this.isGroup = isGroup;
        this.nameRoom = nameRoom;
        this.urlThumb = urlThumb;
        this.urlOriginal = urlOriginal;
        this.urlBlur = urlBlur;
        this.type = type;
        this.userStatus = userStatus;
        this.userStatusLastAction = userStatusLastAction;
        this.msg = msg;
        this.msgSent = msgSent;
        this.numOfMsgNotRead = numOfMsgNotRead;
        this.userId = userId;
        this.userIsOnline = userIsOnline;
        this.userLastSeen = userLastSeen;
    }

    public static List<PrivateChatModel> getRooms() {
        return PrivateRoomSQL.getRooms();
    }

    public static int getRoom(int userId) {
        return PrivateRoomSQL.getRoom(userId);
    }

    public static int getMessagesCount() {
        return PrivateRoomSQL.getMessagesCount();
    }

    public static void insertRoom(int roomId, String name) {
        PrivateRoomSQL.insertRoom(roomId, name);
    }

    public static void insertUsersRoom(int roomId, int toUserId) {
        PrivateRoomUserSQL.insertUsersRoom(roomId, toUserId);
    }

}
