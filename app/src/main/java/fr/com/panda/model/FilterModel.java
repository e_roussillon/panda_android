package fr.com.panda.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

import fr.com.panda.model.sql.FilterSQL;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class FilterModel {

    @SerializedName("id")
    @Expose
    private int filterId;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("is_highlight")
    @Expose
    private boolean isHighlight;

    @SerializedName("timestamp")
    @Expose
    private Date timestamp = new Date();

    public FilterModel(int filterId, String name, boolean isHighlight, Date timestamp) {
        this.filterId = filterId;
        this.name = name;
        this.isHighlight = isHighlight;
        this.timestamp = timestamp;
    }

    public int getFilterId() {
        return filterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHighlight() {
        return isHighlight;
    }

    public Date getTimestamp() {
        if (null == timestamp) {
            timestamp = new Date();
        }
        return timestamp;
    }

    public void insert() {
        FilterSQL.insertFilter(this);
    }

    public void remove() {
        if (this.filterId > 0) {
            FilterSQL.removeFilter(this.filterId);
        }
    }

    public static List<FilterModel> getFilters(boolean isHighlight) {
        return FilterSQL.getFilters(isHighlight);
    }

    public static FilterModel getFilter(int filterId) {
        if (filterId > 0) {
            return FilterSQL.getFilter(filterId);
        }
        return null;
    }

    //
    // Binding
    //
    private int bindingPosition = -1;
    private boolean showRemoveButton = false;
    private boolean first = false;
    private boolean last = false;

    public String getBindingName() {
        return first ? name : "." + name;
    }

    public int getBindingPosition() {
        return bindingPosition;
    }

    public void setBindingPosition(int bindingPosition) {
        this.bindingPosition = bindingPosition;
    }

    public boolean isShowRemoveButton() {
        return showRemoveButton;
    }

    public void setShowRemoveButton(boolean showRemoveButton) {
        this.showRemoveButton = showRemoveButton;
    }

    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }


}
