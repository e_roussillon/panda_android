package fr.com.panda.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.net.URL;
import java.util.Date;
import java.util.List;

import fr.com.panda.PandaApp;
import fr.com.panda.R;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.enums.GenderType;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.model.sql.UserSQL;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class UserModel implements Parcelable {

    @SerializedName("id")
    @Expose
    public int userId = -1;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("pseudo")
    @Expose
    public String pseudo;

    @SerializedName("gender")
    @Expose
    @GenderType.GenderMode
    public int gender = GenderType.NONE;

    @SerializedName("birthday")
    @Expose
    public Date birthday;

    @SerializedName("description")
    @Expose
    public String details = "";

    @SerializedName("url_blur")
    @Expose
    public URL urlBlur;

    @SerializedName("url_thumb")
    @Expose
    public URL urlThumb;

    @SerializedName("url_original")
    @Expose
    public URL urlOriginal;

    @SerializedName("is_online")
    @Expose
    public boolean isOnline;

    @SerializedName("last_seen")
    @Expose
    public Date lastSeen;

    @SerializedName("friendship_status")
    @Expose
    @FriendshipType.FriendshipMode
    public int friendshipStatus = FriendshipType.NOT_FRIEND;

    @SerializedName("friendship_last_action")
    @Expose
    public int friendshipLastAction;

    @SerializedName("reported")
    @Expose
    public boolean reported;

    @SerializedName("updated_at")
    @Expose
    public Date updatedAt;

    public UserModel() {
    }

    public UserModel(int userId,
                     String email,
                     String pseudo,
                     String details,
                     @Nullable Date birthday,
                     @GenderType.GenderMode @Nullable int gender,
                     @Nullable URL urlBlur,
                     @Nullable URL urlThumb,
                     @Nullable URL urlOriginal,
                     boolean isOnline,
                     @Nullable Date lastSeen,
                     @FriendshipType.FriendshipMode @Nullable int friendshipStatus,
                     int friendshipLastAction,
                     boolean reported,
                     Date updatedAt) {
        this.userId = userId;
        this.email = email;
        this.pseudo = pseudo;
        this.gender = gender;
        this.birthday = birthday;
        this.details = details;
        this.urlBlur = urlBlur;
        this.urlThumb = urlThumb;
        this.urlOriginal = urlOriginal;
        this.isOnline = isOnline;
        this.lastSeen = lastSeen;
        this.friendshipStatus = friendshipStatus;
        this.friendshipLastAction = friendshipLastAction;
        this.reported = reported;
        this.updatedAt = updatedAt;
    }

    //
    //MARK: Methods
    //

    public String getGenderType() {

        switch (this.gender) {
            case GenderType.FEMALE:
                return PandaApp.getContext().getString(R.string.generic_label_female);
            case GenderType.MALE:
                return PandaApp.getContext().getString(R.string.generic_label_male);
            default:
                return PandaApp.getContext().getString(R.string.generic_label_none);
        }
    }


    //
    //MARK: Loader
    //

    public static UserModel initModel(int userId, String pseudo) {
        UserModel userModel = new UserModel();
        userModel.userId = userId;
        userModel.pseudo = pseudo;
        return userModel;
    }

    public static UserModel getCurrentUser() {
        final int user = SessionManager.sharedInstance.user().getUserId();
        return UserSQL.getUser(user);
    }

    public static UserModel getUser(int userId) {
        return UserSQL.getUser(userId);
    }

    public static void insert(int userId, String userName, @FriendshipType.FriendshipMode int friendshipStatus, int friendshipLastAction) {
        UserSQL.insertUser(userId, userName, friendshipStatus, friendshipLastAction);
    }

    public static List<UserModel> getUsersByFriendshipTypeFriend() {
        return UserSQL.getUsersByFriendshipTypeFriend();
    }

    public static List<UserModel> getUsersByFriendshipTypeBlocked() {
        return UserSQL.getUsersByFriendshipTypeBlocked();
    }

    public static List<UserModel> getUsersByFriendshipTypePending() {
        return UserSQL.getUsersByFriendshipTypePending();
    }

    public static void updateUsers(List<UserModel> users) {
        for (UserModel user : users) {
            user.update();
        }
    }

    public static long getCountUsersByFriendshipTypePending() {
        return UserSQL.getCountUsersByFriendshipTypePending();
    }

    public void update() {
        UserSQL.updateUser(this);
    }

    //
    // Parcelable
    //

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.userId);
        dest.writeString(this.email);
        dest.writeString(this.pseudo);
        dest.writeInt(this.gender);
        dest.writeLong(this.birthday != null ? this.birthday.getTime() : -1);
        dest.writeString(this.details);
        dest.writeSerializable(this.urlBlur);
        dest.writeSerializable(this.urlThumb);
        dest.writeSerializable(this.urlOriginal);
        dest.writeByte(this.isOnline ? (byte) 1 : (byte) 0);
        dest.writeLong(this.lastSeen != null ? this.lastSeen.getTime() : -1);
        dest.writeInt(this.friendshipStatus);
        dest.writeInt(this.friendshipLastAction);
        dest.writeByte(this.reported ? (byte) 1 : (byte) 0);
        dest.writeLong(this.updatedAt != null ? this.updatedAt.getTime() : -1);
    }

    protected UserModel(Parcel in) {
        this.userId = in.readInt();
        this.email = in.readString();
        this.pseudo = in.readString();
        this.gender = GenderType.fromId(in.readInt());
        long tmpBirthday = in.readLong();
        this.birthday = tmpBirthday == -1 ? null : new Date(tmpBirthday);
        this.details = in.readString();
        this.urlBlur = (URL) in.readSerializable();
        this.urlThumb = (URL) in.readSerializable();
        this.urlOriginal = (URL) in.readSerializable();
        this.isOnline = in.readByte() != 0;
        long tmpLastSeen = in.readLong();
        this.lastSeen = tmpLastSeen == -1 ? null : new Date(tmpLastSeen);
        this.friendshipStatus = FriendshipType.fromId(in.readInt());
        this.friendshipLastAction = in.readInt();
        this.reported = in.readByte() != 0;
        long tmpUpdatedAt = in.readLong();
        this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel source) {
            return new UserModel(source);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
}