package fr.com.panda.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;

import java.util.List;

import fr.com.panda.utils.PandaDB;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class BackupModel {

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("backup")
    @Expose
    public List<String> backup;

    public void insert() {
        if (null == backup) {
            return;
        }

        DatabaseWrapper database = FlowManager.getDatabase(PandaDB.NAME).getWritableDatabase();
        database.beginTransaction();
        final int count = backup.size();
        for (int i = 0; i < count; i++) {
            database.execSQL(backup.get(i));
        }
        database.setTransactionSuccessful();
        database.endTransaction();
    }

}
