package fr.com.panda.model;

public class LogoutReported {

    public int time = 0;
    public boolean logout = false;

    public LogoutReported(int time, boolean logout) {
        this.time = time;
        this.logout = logout;
    }
}
