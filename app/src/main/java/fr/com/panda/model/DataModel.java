package fr.com.panda.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.net.URL;

import fr.com.panda.enums.DataType;

/**
 * Created by edouardroussillon on 12/29/16.
 */

public class DataModel {

    @SerializedName("id")
    @Expose
    public int dataId = -1;

    @SerializedName("url_blur")
    @Expose
    public URL urlBlur;

    @SerializedName("url_original")
    @Expose
    public URL urlOriginal;

    @SerializedName("url_thumb")
    @Expose
    public URL urlThumb;

    @SerializedName("type")
    @Expose
    public DataType type;

}
