package fr.com.panda.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.net.URL;
import java.util.Date;
import java.util.List;

import fr.com.panda.enums.DataType;
import fr.com.panda.enums.StatusType;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.model.sql.PublicMessageSQL;
import fr.com.panda.utils.DateUtil;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class PublicMessageModel extends GenericMessageModel {

    @SerializedName("is_general")
    @Expose
    public boolean isGeneral;

    @SerializedName("is_highlight")
    @Expose
    public boolean isHighlight;

    @SerializedName("is_welcome_message")
    @Expose
    public boolean isWelcomeMessage;

    public PublicMessageModel(int messageId,
                              String msg,
                              int fromUserId,
                              String fromUserName,
                              URL urlThumb,
                              URL urlOriginal,
                              URL urlBlur,
                              @DataType.DataMode int type,
                              String index,
                              @StatusType.StatusMode int status,
                              boolean isGeneral,
                              boolean isHighlight,
                              boolean isWelcomeMessage,
                              Date insertedAt) {
        super(-1, messageId, msg, fromUserId, fromUserName, -1, urlThumb, urlOriginal, urlBlur, type, 0, "", index, status, insertedAt);
        this.isGeneral = isGeneral;
        this.isHighlight = isHighlight;
        this.isWelcomeMessage = isWelcomeMessage;
    }

    public static PublicMessageModel initMessageLite(String msg,
                                                     boolean isGeneral) {
        return PublicMessageModel.initMessageLite(msg, isGeneral, StatusType.SENDING, false);
    }

    public static PublicMessageModel initMessageLite(String msg,
                                                     boolean isGeneral,
                                                     @StatusType.StatusMode int status,
                                                     boolean isWelcomeMessage) {

        String fromUserName = SessionManager.sharedInstance.user().getUserPseudo();
        long insertedAt = DateUtil.timestamp();
        String indexTemp = fromUserName + "_" + insertedAt;

        return new PublicMessageModel(-1, msg, SessionManager.sharedInstance.user().getUserId(), fromUserName, null, null, null, DataType.NONE, indexTemp, status, isGeneral, false, isWelcomeMessage, DateUtil.timestampToDate(insertedAt));
    }

    public static void updateMessageWithFilter(FilterModel filter) {
        PublicMessageSQL.updateMessageWithFilter(filter);
    }

    public static void removeMessageWithFilter(FilterModel filter) {
        PublicMessageSQL.removeMessageWithFilter(filter);
    }

    public static List<PublicMessageModel> getAllPublicMessageGeneral() {
        return PublicMessageSQL.getAllPublicMessage(true);
    }

    public static List<PublicMessageModel> getAllPublicMessageSale() {
        return PublicMessageSQL.getAllPublicMessage(false);
    }

    public void insert() {
        PublicMessageSQL.insert(this);
    }

    public long insertLight() {
        return PublicMessageSQL.insertLite(this);
    }

    public boolean isContainWelcomeMessage() {
        return PublicMessageSQL.isContainMessage(this.msg);
    }
}
