package fr.com.panda.model.sql;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.NotNull;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

import fr.com.panda.utils.PandaDB;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.model.FilterModel;
import fr.com.panda.utils.DateUtil;

/**
 * Created by edouardroussillon on 12/29/16.
 */


@Table(database = PandaDB.class, name = "user_filters")
public class FilterSQL extends BaseModel {

    @PrimaryKey(autoincrement = true)
    @Column(name = "_id")
    long idField; // package-private recommended, not required

    @Column(name = "user_filters_id")
    @Unique(onUniqueConflict = ConflictAction.REPLACE)
    @NotNull
    public int filterId;

    @Column(name = "name")
    @NotNull
    public String name;

    @Column(name = "is_highlight")
    @NotNull
    public boolean isHighlight;

    @Column(name = "user_id")
    @NotNull
    int userIdField;

    @Column(name = "updated_at")
    @NotNull
    String timestampField;

    public FilterSQL() {
    }

    private FilterSQL(int filterId, String name, boolean isHighlight, int userIdField, String timestampField) {
        this.filterId = filterId;
        this.name = name;
        this.isHighlight = isHighlight;
        this.userIdField = userIdField;
        this.timestampField = timestampField;
    }

    private static FilterSQL filterModelToFilterSQL(FilterModel filter) {
        final int USER_ID = SessionManager.sharedInstance.user().getUserId();
        return new FilterSQL(filter.getFilterId(), filter.getName(), filter.isHighlight(), USER_ID, DateUtil.getDateToFullString(filter.getTimestamp()));
    }

    private static FilterModel filterSQLToFilterModel(FilterSQL filter) {
        return new FilterModel(filter.filterId, filter.name, filter.isHighlight, DateUtil.getFullStringToDate(filter.timestampField));
    }

    private static List<FilterModel> filtersSQLToFiltersModel(List<FilterSQL> filters) {
        List<FilterModel> newList = new ArrayList();

        if (null != filters) {
            final long count = filters.size();

            for (int i = 0; i < count; i++) {
                final FilterSQL filterSQL = filters.get(i);
                newList.add(filterSQLToFilterModel(filterSQL));
            }
        }

        return newList;
    }

    public static void insertFilter(final FilterModel filter) {
        filterModelToFilterSQL(filter).save();
    }

    public static FilterModel getFilter(int filterId) {
        final int USER_ID = SessionManager.sharedInstance.user().getUserId();

        final FilterSQL item = SQLite.select()
                .from(FilterSQL.class)
                .where(FilterSQL_Table.user_filters_id.is(filterId))
                .and(FilterSQL_Table.user_id.is(USER_ID))
                .orderBy(FilterSQL_Table.name, true)
                .querySingle();

        if (null != item) {
            return filterSQLToFilterModel(item);
        } else {
            return null;
        }
    }

    public static List<FilterModel> getFilters(boolean isHighlight) {
        final int USER_ID = SessionManager.sharedInstance.user().getUserId();

        final List<FilterSQL> list = SQLite.select()
                .from(FilterSQL.class)
                .where(FilterSQL_Table.is_highlight.is(isHighlight))
                .and(FilterSQL_Table.user_id.is(USER_ID))
                .orderBy(FilterSQL_Table.name, true)
                .queryList();

        return filtersSQLToFiltersModel(list);
    }


    public static void removeFilter(int filterId) {
        final int USER_ID = SessionManager.sharedInstance.user().getUserId();

        SQLite.delete(FilterSQL.class)
                .where(FilterSQL_Table.user_filters_id.is(filterId))
                .and(FilterSQL_Table.user_id.is(USER_ID))
                .execute();
    }
}
