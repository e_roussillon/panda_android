package fr.com.panda.model;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class RequestFail {

    public ErrorResponse errorResponse;
    public boolean consumed;
    public int status;

    public RequestFail(ErrorResponse errorResponse, int status, boolean consumed) {
        this.errorResponse = errorResponse;
        this.status = status;
        this.consumed = consumed;
    }
}
