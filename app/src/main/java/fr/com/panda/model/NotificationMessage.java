package fr.com.panda.model;

/**
 * @author Edouard Roussillon
 */

public class NotificationMessage {

    private boolean isNewItem;
    private PrivateMessageModel message;

    public NotificationMessage(boolean isNewItem, PrivateMessageModel message) {
        this.isNewItem = isNewItem;
        this.message = message;
    }

    public boolean isNewItem() {
        return isNewItem;
    }

    public PrivateMessageModel getMessage() {
        return message;
    }
}
