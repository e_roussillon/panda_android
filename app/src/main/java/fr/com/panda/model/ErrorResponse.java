package fr.com.panda.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

import fr.com.panda.enums.ErrorType;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class ErrorResponse {

    @SerializedName("type")
    @Expose
    @ErrorType.ErrorMode
    public int type;

    @SerializedName("msg")
    @Expose
    public String msg;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("details")
    @Expose
    public Map<String, Object> details;

    @SerializedName("fields")
    @Expose
    public List<FieldModel> fields;

}
