package fr.com.panda.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class TokenModel {

    @SerializedName("token")
    @Expose
    public String token;

    @SerializedName("refresh_token")
    @Expose
    public String refreshToken;

}
