package fr.com.panda.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class PublicRoomModel {

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("original_name")
    @Expose
    public String originalName;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("id")
    @Expose
    public int publicRoomId;

}
