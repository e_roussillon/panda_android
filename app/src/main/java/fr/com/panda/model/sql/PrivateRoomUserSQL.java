package fr.com.panda.model.sql;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.annotation.UniqueGroup;
import com.raizlabs.android.dbflow.sql.language.ConditionGroup;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

import fr.com.panda.utils.PandaDB;
import fr.com.panda.utils.DateUtil;

/**
 * Created by edouardroussillon on 12/30/16.
 */

@Table(database = PandaDB.class, name = "private_room_users",
        uniqueColumnGroups = {@UniqueGroup(groupNumber = 1, uniqueConflict = ConflictAction.REPLACE)})
public class PrivateRoomUserSQL extends BaseModel {

    @Column
    @PrimaryKey(autoincrement = true)
    long id;

    @Column(name = "user_id")
    @Unique(unique = false, uniqueGroups = 1)
    int userIdField;

    @Column(name = "room_id")
    @Unique(unique = false, uniqueGroups = 1)
    int roomIdField;

    @Column(name = "updated_at")
    long timestampField;

    //TODO ADD USER ID
    private static int USER_ID = -1;

    public PrivateRoomUserSQL() {
    }

    private PrivateRoomUserSQL(int userIdField, int roomIdField, long timestampField) {
        this.userIdField = userIdField;
        this.roomIdField = roomIdField;
        this.timestampField = timestampField;
    }

    //
    //MARK: SQL
    //

    public static void insertUsersRoom(int roomId, int toUserId) {
        final int userId = USER_ID;
        final List<PrivateRoomUserSQL> list = SQLite.select()
                .from(PrivateRoomUserSQL.class)
                .where(PrivateRoomUserSQL_Table.room_id.is(roomId))
                .and(ConditionGroup.clause(PrivateRoomUserSQL_Table.user_id.is(userId)).or(PrivateRoomUserSQL_Table.user_id.is(toUserId)))
                .queryList();

        if (null != list || list.size() < 2) {
            new PrivateRoomUserSQL(userId, roomId, DateUtil.timestamp()).save();
            new PrivateRoomUserSQL(toUserId, roomId, DateUtil.timestamp()).save();
        }
    }
}