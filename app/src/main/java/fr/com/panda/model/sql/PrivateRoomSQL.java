package fr.com.panda.model.sql;

import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.com.panda.utils.PandaDB;
import fr.com.panda.enums.DataType;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.enums.StatusType;
import fr.com.panda.model.PrivateChatModel;
import fr.com.panda.utils.DateUtil;

/**
 * Created by edouardroussillon on 12/29/16.
 */

@Table(database = PandaDB.class, name = "private_rooms")
public class PrivateRoomSQL extends BaseModel {

    @PrimaryKey(autoincrement = true)
    @Column(name = "_id")
    long idField;

    @Column(name = "private_rooms_id")
    @Unique(onUniqueConflict = ConflictAction.REPLACE)
    int privateRoomIdField;

    @Column(name = "name")
    String nameField;

    @Column(name = "is_group")
    boolean isGroupField;

    @Column(name = "data_id")
    int dataIdField;

    @Column(name = "updated_at")
    long timestampField = new Date().getTime();
    ;

    //TODO ADD USER ID
    private static int USER_ID = -1;

    //
    //MARK: SQL
    //

    public PrivateRoomSQL() {
    }

    private PrivateRoomSQL(int privateRoomIdField, String nameField, boolean isGroupField, int dataIdField, long timestampField) {
        this.privateRoomIdField = privateRoomIdField;
        this.nameField = nameField;
        this.isGroupField = isGroupField;
        this.dataIdField = dataIdField;
        this.timestampField = timestampField;
    }

    public static List<PrivateChatModel> getRooms() {
        final int currentUserId = USER_ID;

        final String sql = "SELECT " +
                "p_r.private_rooms_id, " +
                "p_r.is_group, " +
                "case when p_r.is_group = 1 then p_r.name else u.pseudo end AS name, " +
                "case when u.url_thumb is null then '' else u.url_thumb end AS url_thumb, " +
                "case when u.url_original is null then '' else u.url_original end AS url_original, " +
                "case when u.url_blur is null then '' else url_blur end AS url_blur, " +
                "u.status AS user_status, " +
                "u.last_action AS user_status_last_action, " +
                "p_m_l.msg, " +
                "p_m_l.type, " +
                "p_m_l.inserted_at AS msg_sent, " +
                "case when p_m_c.num_of_msg_not_read is null then 0 else p_m_c.num_of_msg_not_read end AS num_of_msg_not_read, " +
                "u.users_id, " +
                "u.is_online, " +
                "u.last_seen " +
                "FROM private_rooms AS p_r " +
                "JOIN private_room_users AS p_r_u " +
                "ON p_r.private_rooms_id = p_r_u.room_id " +
                "JOIN users AS u " +
                "ON p_r_u.user_id = u.users_id " +
                "JOIN ( SELECT p_m_1.room_id, " +
                "p_m_1.inserted_at, " +
                "p_m_1.msg, " +
                "p_m_1.type " +
                "FROM private_messages p_m_1 " +
                "LEFT JOIN private_messages p_m_2 " +
                "ON (p_m_1.room_id = p_m_2.room_id " +
                "AND p_m_1.private_messages_id < p_m_2.private_messages_id) " +
                "GROUP BY p_m_1.room_id " +
                ") AS p_m_l ON p_m_l.room_id = p_r.private_rooms_id " +
                "LEFT JOIN ( SELECT p_m.room_id, " +
                "COUNT(p_m.msg) AS num_of_msg_not_read " +
                "FROM private_messages AS p_m " +
                "WHERE p_m.from_user_id <> " + currentUserId + " " +
                "AND p_m.status < " + StatusType.READ + " " +
                "GROUP BY p_m.room_id " +
                ") AS p_m_c ON p_m_c.room_id = p_r.private_rooms_id " +
                "WHERE u.users_id <> " + currentUserId + " " +
                "AND (u.status <> " + FriendshipType.BLOCKED_BEFORE_BE_FRIEND + " " +
                "AND u.status <> " + FriendshipType.BLOCKED_AFTER_BE_FRIEND + ") " +
                "ORDER BY p_m_l.inserted_at DESC";


        final Cursor cur = FlowManager.getDatabase(PandaDB.NAME).getWritableDatabase().rawQuery(sql, null);

        List<PrivateChatModel> list = new ArrayList();

        cur.moveToFirst();
        try {
            if (cur.getCount() > 0) {
                do {
                    final int roomId = cur.getInt(0);
                    final int isGroup = cur.getInt(1);
                    final String nameRoom = cur.getString(2);
                    final URL urlThumb = TextUtils.isEmpty(cur.getString(3)) ? null : new URL(cur.getString(3));
                    final URL urlOriginal = TextUtils.isEmpty(cur.getString(4)) ? null : new URL(cur.getString(4));
                    final URL urlBlur = TextUtils.isEmpty(cur.getString(5)) ? null : new URL(cur.getString(5));
                    final @FriendshipType.FriendshipMode int userStatus = FriendshipType.fromId(cur.getInt(6));
                    final int userStatusLastAction = cur.getInt(7) == 0 ? -1 : cur.getInt(7);
                    final String msg = cur.getString(8);
                    final @DataType.DataMode int type = DataType.fromId(cur.getInt(9));
                    final Date msgSent = cur.getLong(10) == 0 ? null : DateUtil.timestampToDate(cur.getLong(10));
                    final int numOfMsgNotRead = cur.getInt(11);
                    final int userId = cur.getInt(12);
                    final int isOnline = cur.getInt(13);
                    final Date last_seen = cur.getLong(14) == 0 ? null : DateUtil.timestampToDate(cur.getLong(14));


                    list.add(new PrivateChatModel(roomId,
                            isGroup == 1 ? true : false,
                            nameRoom,
                            urlThumb,
                            urlOriginal,
                            urlBlur,
                            type,
                            userStatus,
                            userStatusLastAction,
                            msg,
                            msgSent,
                            numOfMsgNotRead,
                            userId,
                            isOnline == 1 ? true : false,
                            last_seen));
                } while (cur.moveToNext());
            }
        } catch (Exception e) {
            Log.e("PrivateMessageSQL", e.getMessage());
        } finally {
            cur.close();
        }

        return list;
    }


    public static int getRoom(int userId) {
        int result = -1;
        final String sql = "SELECT private_rooms_id, user_id from private_rooms JOIN private_room_users on private_rooms_id = room_id where user_id == " + userId + " OR user_id == " + USER_ID;
        final Cursor cur = FlowManager.getDatabase(PandaDB.NAME).getWritableDatabase().rawQuery(sql, null);

        cur.moveToFirst();
        try {
            if (cur.getCount() > 0) {
                do {
                    final int roomId = cur.getInt(0);
                    final int userIdDB = cur.getInt(1);
                    if (userIdDB != USER_ID) {
                        result = roomId;
                    }
                } while (cur.moveToNext());
            }
        } catch (Exception e) {
            Log.e("PrivateMessageSQL", e.getMessage());
        } finally {
            cur.close();
        }

        return result;
    }

    public static void insertRoom(int roomId, String name) {
        new PrivateRoomSQL(roomId, name, false, 0, DateUtil.timestamp()).save();
    }

    public static int getMessagesCount() {
        int result = 0;
        final String sql = "SELECT COUNT(p_m.msg) as num_of_msg_not_read FROM private_messages AS p_m WHERE p_m.from_user_id <> " + USER_ID + " and p_m.status < " + StatusType.READ;
        final Cursor cur = FlowManager.getDatabase(PandaDB.NAME).getWritableDatabase().rawQuery(sql, null);

        cur.moveToFirst();

        try {
            if (cur.getCount() > 0) {
                do {
                    result = cur.getInt(0);
                } while (cur.moveToNext());
            }
        } catch (Exception e) {
            Log.e("PrivateMessageSQL", e.getMessage());
        } finally {
            cur.close();
        }


        return result;
    }
}
