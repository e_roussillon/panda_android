package fr.com.panda.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class Paginated<T> {

    @SerializedName("total_pages")
    @Expose
    public int totalPages;

    @SerializedName("total_entries")
    @Expose
    public int totalEntries;

    @SerializedName("page_size")
    @Expose
    public int pageSize;

    @SerializedName("page_number")
    @Expose
    public int pageNumber;

    @SerializedName("entries")
    @Expose
    public List<T> entries;

}
