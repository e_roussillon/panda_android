package fr.com.panda.model.sql;

import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.NotNull;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.sql.language.ConditionGroup;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.queriable.StringQuery;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.com.panda.utils.PandaDB;
import fr.com.panda.utils.PrivateMessageInsertException;
import fr.com.panda.enums.DataType;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.enums.StatusType;
import fr.com.panda.model.PrivateMessageModel;
import fr.com.panda.utils.DateUtil;

/**
 * Created by edouardroussillon on 12/29/16.
 */

@Table(database = PandaDB.class, name = "private_messages")
public class PrivateMessageSQL extends BaseModel {

    @PrimaryKey(autoincrement = true)
    @Column(name = "_id")
    long idField;

    @Column(name = "private_messages_id")
    int privateMessageIdField = -1;

    @Column(name = "room_id")
    @NotNull
    int roomIdField;

    @Column(name = "from_user_id")
    @NotNull
    int fromUserIdField;

    @Column(name = "msg")
    @NotNull
    String msgField;

    @Column(name = "status")
    @NotNull
    int statusField;

    @Column(name = "index_row")
    @NotNull
    @Unique(onUniqueConflict = ConflictAction.REPLACE)
    String indexField;

    @Column(name = "data_id")
    @NotNull
    int dataIdField = -1;

    @Column(name = "url_thumb")
    @NotNull
    String urlThumbField = "";

    @Column(name = "url_original")
    @NotNull
    String urlOriginalField = "";

    @Column(name = "url_blur")
    @NotNull
    String urlBlurField = "";

    @Column(name = "type")
    @NotNull
    @DataType.DataMode
    int typeField = DataType.NONE;

    @Column(name = "size")
    @NotNull
    float sizeField = 0F;

    @Column(name = "data_local")
    @NotNull
    String dataLocalField = "";

    @Column(name = "inserted_at")
    @NotNull
    long timestampField = new Date().getTime();

    public PrivateMessageSQL() {
    }

    private PrivateMessageSQL(int privateMessageIdField,
                              int roomIdField,
                              int fromUserIdField,
                              String msgField,
                              int statusField,
                              String indexField,
                              int dataIdField,
                              String urlThumbField,
                              String urlOriginalField,
                              String urlBlurField,
                              int typeField,
                              float sizeField,
                              String dataLocalField,
                              long timestampField) {
        this.privateMessageIdField = privateMessageIdField;
        this.roomIdField = roomIdField;
        this.fromUserIdField = fromUserIdField;
        this.msgField = msgField;
        this.statusField = statusField;
        this.indexField = indexField;
        this.dataIdField = dataIdField;
        this.urlThumbField = urlThumbField;
        this.urlOriginalField = urlOriginalField;
        this.urlBlurField = urlBlurField;
        this.typeField = typeField;
        this.sizeField = sizeField;
        this.dataLocalField = dataLocalField;
        this.timestampField = timestampField;
    }

    //TODO ADD USER ID
    private static int USER_ID = -1;

    private static PrivateMessageSQL privateMessageModelToPrivateMessageSQL(PrivateMessageModel privateMessageModel) {
        @StatusType.StatusMode int status = privateMessageModel.status;
        String urlThumb = null == privateMessageModel.urlThumb ? "" : privateMessageModel.urlThumb.toString();
        String urlOriginal = null == privateMessageModel.urlOriginal ? "" : privateMessageModel.urlOriginal.toString();
        String urlBlur = null == privateMessageModel.urlBlur ? "" : privateMessageModel.urlBlur.toString();
        @DataType.DataMode int type = privateMessageModel.type;
        long insertedAt = null == privateMessageModel.insertedAt ? DateUtil.timestamp() : DateUtil.timestamp(privateMessageModel.insertedAt);

        return new PrivateMessageSQL(privateMessageModel.messageId,
                privateMessageModel.roomId,
                privateMessageModel.fromUserId,
                privateMessageModel.msg,
                status,
                privateMessageModel.index,
                privateMessageModel.dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                privateMessageModel.size,
                privateMessageModel.dataLocal,
                insertedAt);
    }

    private static PrivateMessageModel privateMessageSQLToPrivateMessageModel(PrivateMessageSQL privateMessageSQL) {

        try {
            URL urlThumbField = TextUtils.isEmpty(privateMessageSQL.urlThumbField) ? null : new URL(privateMessageSQL.urlThumbField);
            URL urlOriginalField = TextUtils.isEmpty(privateMessageSQL.urlOriginalField) ? null : new URL(privateMessageSQL.urlOriginalField);
            URL urlBlurField = TextUtils.isEmpty(privateMessageSQL.urlBlurField) ? null : new URL(privateMessageSQL.urlBlurField);

            return new PrivateMessageModel(privateMessageSQL.roomIdField,
                    privateMessageSQL.privateMessageIdField,
                    privateMessageSQL.msgField,
                    privateMessageSQL.fromUserIdField,
                    privateMessageSQL.dataIdField,
                    urlThumbField,
                    urlOriginalField,
                    urlBlurField,
                    DataType.fromId(privateMessageSQL.typeField),
                    privateMessageSQL.sizeField,
                    privateMessageSQL.dataLocalField,
                    privateMessageSQL.indexField,
                    StatusType.fromId(privateMessageSQL.statusField),
                    DateUtil.timestampToDate(privateMessageSQL.timestampField));


        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<PrivateMessageModel> privateMessageSQLsToPrivatesMessageModels(List<PrivateMessageSQL> privateMessages) {
        List<PrivateMessageModel> newList = new ArrayList();
        if (null != privateMessages) {
            final long count = privateMessages.size();


            for (int i = 0; i < count; i++) {
                final PrivateMessageSQL privateMessageSQL = privateMessages.get(i);
                final PrivateMessageModel privateMessageModel = privateMessageSQLToPrivateMessageModel(privateMessageSQL);
                if (null != privateMessageModel) {
                    newList.add(privateMessageModel);
                }
            }
        }
        return newList;
    }

    //
    //MARK: SQL
    //

    public static long insertLite(PrivateMessageModel message) {

        final String msg = message.msg;
        final int roomId = message.roomId;
        final int fromUserId = message.fromUserId;
        final String index = message.index;

        if (TextUtils.isEmpty(msg) && roomId <= 0 && fromUserId <= 0 && TextUtils.isEmpty(index)) {
            final String print = "roomId : " + roomId +
                    " fromUserId : " + fromUserId +
                    " index : " + index +
                    " msg : " + msg;
            new PrivateMessageInsertException("impossible to add - " + print);
            return -1;
        } else {

            final int dataId = message.dataId;
            final String urlThumb = null == message.urlThumb ? "" : message.urlThumb.toString();
            final String urlOriginal = null == message.urlOriginal ? "" : message.urlBlur.toString();
            final String urlBlur = null == message.urlBlur ? "" : message.urlBlur.toString();
            final @DataType.DataMode int type = message.type;
            final String dataLocal = TextUtils.isEmpty(message.dataLocal) ? "" : message.dataLocal;
            final float size = message.size;

            final PrivateMessageSQL privateMessageSQL = new PrivateMessageSQL(-1,
                    roomId,
                    fromUserId,
                    msg,
                    StatusType.SENDING,
                    index,
                    dataId,
                    urlThumb,
                    urlOriginal,
                    urlBlur,
                    type,
                    size,
                    dataLocal,
                    DateUtil.timestamp());
            privateMessageSQL.save();
            return getMessageIdByIndex(index);
        }
    }

    public static void insert(PrivateMessageModel message) {

        final String msg = message.msg;
        final int roomId = message.roomId;
        final int fromUserId = message.fromUserId;
        final String index = message.index;

        if (TextUtils.isEmpty(msg) && roomId <= 0 && fromUserId <= 0 && TextUtils.isEmpty(index)) {
            final String print = "roomId : " + roomId +
                    " fromUserId : " + fromUserId +
                    " index : " + index +
                    " msg : " + msg;
            new PrivateMessageInsertException("impossible to add - " + print);
        } else {

            final int messageId = message.messageId <= 0 ? -1 : message.messageId;
            final @StatusType.StatusMode int status = message.status;
            final int dataId = message.dataId;
            final String urlThumb = null == message.urlThumb ? "" : message.urlThumb.toString();
            final String urlOriginal = null == message.urlOriginal ? "" : message.urlBlur.toString();
            final String urlBlur = null == message.urlBlur ? "" : message.urlBlur.toString();
            final @DataType.DataMode int type = message.type;
            final String dataLocal = TextUtils.isEmpty(message.dataLocal) ? "" : message.dataLocal;
            final float size = message.size;
            final long date = null == message.insertedAt ? DateUtil.timestamp() : DateUtil.timestamp(message.insertedAt);

            final PrivateMessageSQL privateMessageSQL = new PrivateMessageSQL(messageId,
                    roomId,
                    fromUserId,
                    msg,
                    status,
                    index,
                    dataId,
                    urlThumb,
                    urlOriginal,
                    urlBlur,
                    type,
                    size,
                    dataLocal,
                    date);
            privateMessageSQL.save();
        }
    }

    @NotNull
    public static PrivateMessageModel getMessage(int privateMessageId) {
        PrivateMessageSQL privateMessageSQL = SQLite.select()
                .from(PrivateMessageSQL.class)
                .where(PrivateMessageSQL_Table.private_messages_id.is(privateMessageId))
                .querySingle();

        if (null == privateMessageSQL) {
            return null;
        } else {
            return privateMessageSQLToPrivateMessageModel(privateMessageSQL);
        }
    }

    @NotNull
    public static PrivateMessageModel getMessageByIndex(String index) {
        PrivateMessageSQL privateMessageSQL = SQLite.select()
                .from(PrivateMessageSQL.class)
                .where(PrivateMessageSQL_Table.index_row.is(index))
                .querySingle();

        if (null == privateMessageSQL) {
            return null;
        } else {
            return privateMessageSQLToPrivateMessageModel(privateMessageSQL);
        }
    }

    public static List<PrivateMessageModel> getMessagesByRoom(int roomId) {
        List<PrivateMessageSQL> newList = SQLite.select()
                .from(PrivateMessageSQL.class)
                .where(PrivateMessageSQL_Table.room_id.is(roomId))
                .queryList();

        return privateMessageSQLsToPrivatesMessageModels(newList);
    }

    public static List<PrivateMessageModel> getPendingMessages() {
        List<PrivateMessageSQL> newList = SQLite.select().from(PrivateMessageSQL.class)
                .where(ConditionGroup.clause(PrivateMessageSQL_Table.status.is(StatusType.SENT)).and(PrivateMessageSQL_Table.from_user_id.isNot(USER_ID)))
                .or(ConditionGroup.clause(PrivateMessageSQL_Table.status.is(StatusType.READ)).and(PrivateMessageSQL_Table.from_user_id.is(USER_ID)))
                .queryList();

        return privateMessageSQLsToPrivatesMessageModels(newList);
    }

    public static List<PrivateMessageModel> getSentMessagesOrderAsc() {
        List<PrivateMessageSQL> newList = SQLite.select().from(PrivateMessageSQL.class)
                .where(ConditionGroup.clause(PrivateMessageSQL_Table.status.is(StatusType.SENT)).and(PrivateMessageSQL_Table.from_user_id.isNot(USER_ID)))
                .orderBy(PrivateMessageSQL_Table.inserted_at, true)
                .queryList();

        return privateMessageSQLsToPrivatesMessageModels(newList);
    }

    public static void updateStatus(@NotNull int messageId, @NotNull @StatusType.StatusMode int status) {
        final PrivateMessageSQL privateMessageSQL = SQLite.select()
                .from(PrivateMessageSQL.class)
                .where(PrivateMessageSQL_Table.private_messages_id.is(messageId))
                .querySingle();

        if (null != privateMessageSQL) {
            privateMessageSQL.statusField = status;
            privateMessageSQL.update();
        }
    }

    public static List<PrivateMessageModel> getMessagesNotSent() {
//        let sessionManager = SessionManager.sharedInstance
//        let sessionUserId = sessionManager.user.userId
//        let pseudo = sessionManager.user.userPseudo
        int sessionUserId = USER_ID;
        String pseudo = "test";
        List<PrivateMessageModel> list = new ArrayList();

        String sql = "SELECT " +
                "pm.index_row," +
                "pm.room_id," +
                "u.users_id," +
                "u.pseudo," +
                "pm.status as message_status," +
                "u.status," +
                "u.last_actio," +
                "pm.private_messages_id," +
                "pm.msg," +
                "pm.data_id," +
                "pm.data_local," +
                "pm.url_thumb," +
                "pm.url_original," +
                "pm.url_blur," +
                "pm.type," +
                "pm.size," +
                "pm.inserted_at" +
                "FROM private_messages AS pm" +
                "INNER JOIN private_room_users AS pru" +
                "ON pru.room_id = pm.room_id AND pru.user_id <> " + sessionUserId +
                "INNER JOIN users AS u" +
                "ON u.users_id = pru.user_id" +
                "WHERE pm.status = " + StatusType.SENDING + " AND pm.from_user_id = " + sessionUserId;

        Cursor cur = new StringQuery(PrivateMessageSQL.class, sql).query();

        cur.moveToFirst();
        try {
            while (cur.moveToNext()) {
                final String index = cur.getString(0);
                final int roomId = cur.getInt(1);
                final int userId = cur.getInt(2);
                final String userName = cur.getString(3);
                final int status = cur.getInt(4);
                final int friendshipStatus = cur.getInt(5);
                final int friendshipLastAction = cur.getInt(6);
                final int privateMessagesId = cur.getInt(7);
                final String msg = cur.getString(8);
                final int dataId = cur.getInt(9);
                final String dataLocal = cur.getString(10);
                final URL urlThumb = TextUtils.isEmpty(cur.getString(11)) ? null : new URL(cur.getString(11));
                final URL urlOriginal = TextUtils.isEmpty(cur.getString(12)) ? null : new URL(cur.getString(12));
                final URL urlBlur = TextUtils.isEmpty(cur.getString(13)) ? null : new URL(cur.getString(13));
                final int type = cur.getInt(14);
                final float size = cur.getFloat(15);
                final long insertedAt = cur.getLong(16);

                PrivateMessageModel privateMessageModel = new PrivateMessageModel(roomId,
                        privateMessagesId,
                        msg,
                        sessionUserId,
                        pseudo,
                        dataId,
                        urlThumb,
                        urlOriginal,
                        urlBlur,
                        DataType.fromId(type),
                        size,
                        dataLocal,
                        index,
                        StatusType.fromId(status),
                        DateUtil.timestampToDate(insertedAt),
                        FriendshipType.fromId(friendshipStatus),
                        friendshipLastAction,
                        userId,
                        userName);

                list.add(privateMessageModel);
            }
        } catch (Exception e) {
            Log.e("PrivateMessageSQL", e.getMessage());
        } finally {
            cur.close();
        }

        return list;
    }

//    static func getMessagesNotSent() -> [PrivateMessageModel] {
//        let sessionManager = SessionManager.sharedInstance
//        let sessionUserId = sessionManager.user.userId
//        let pseudo = sessionManager.user.userPseudo
//
//        let sql = " ".join(["SELECT",
//        "pm.index_row,",
//                "pm.room_id,",
//                "u.users_id,",
//                "u.pseudo,",
//                "pm.status as message_status,",
//                "u.status,",
//                "u.last_action,",
//                "pm.private_messages_id,",
//                "pm.msg,",
//                "pm.data_id,",
//                "pm.data_local,",
//                "pm.url_thumb,",
//                "pm.url_original,",
//                "pm.url_blur,",
//                "pm.type,",
//                "pm.size,",
//                "pm.inserted_at",
//                "FROM private_messages AS pm",
//                "INNER JOIN private_room_users AS pru",
//                "ON pru.room_id = pm.room_id AND pru.user_id <> \(sessionUserId)",
//                "INNER JOIN users AS u",
//                "ON u.users_id = pru.user_id",
//                "WHERE pm.status = \(StatusType.Sending.rawValue) AND pm.from_user_id = \(sessionUserId)"])
//
//
//        do {
//            let stmt = try DBUtil.sharedInstance.prepare(sql)
//            var list: [PrivateMessageModel] = []
//            for row in stmt {
//                if let index = row[0] as? String,
//                        let roomId = row[1] as? Int64,
//                        let userId = row[2] as? Int64,
//                        let userName = row[3] as? String,
//                        let status = row[4] as? Int64 {
//
//                    let friendshipStatus = row[5] as? Int64
//                    let friendshipLastAction = row[6] as? Int64
//                    let privateMessagesId = row[7] as? Int64
//                    let msg = row[8] as? String
//                    let dataId = row[9] as? Int64
//                    let dataLocal = row[10] as? String
//                    let urlThumb = row[11] as? String
//                    let urlOriginal = row[12] as? String
//                    let urlBlur = row[13] as? String
//                    let type = row[14] as? Int64
//                    let size = row[15] as? Float64
//                    let insertedAt = row[16] as? Double
//
//                    let message = PrivateMessageModel.initMessage(roomId: Int(roomId),
//                            messageId: Int(privateMessagesId ?? -1),
//                    msg: msg ?? "",
//                            urlThumb: urlThumb ?? "",
//                            urlOriginal: urlOriginal ?? "",
//                            urlBlur: urlBlur ?? "",
//                            type: DataType(rawValue: Int(type ?? -1)) ?? DataType.None,
//                            size: Float(size ?? 0.0),
//                    dataId:  Int(dataId ?? -1),
//                    dataLocal: dataLocal ?? "",
//                            fromUserId: sessionUserId,
//                            fromUserName: pseudo,
//                            toUserId: Int(userId) ?? -1,
//                            toUserName: userName,
//                            friendshipStatus: FriendshipType(rawValue: Int(friendshipStatus ?? -1)) ?? FriendshipType.NotFriend,
//                            friendshipLastAction: Int(friendshipLastAction ?? -1),
//                    insertedAt: insertedAt ?? NSDate().timeIntervalSince1970,
//                            index: index,
//                            status: StatusType(rawValue: Int(status)) ?? StatusType.Sending)
//
//                    list.append(message)
//                }
//            }
//
//            return list
//        } catch {
//            print("error \(error)")
//            return []
//        }
//    }


    private static long getMessageIdByIndex(String index) {
        return SQLite.select()
                .from(PrivateMessageSQL.class)
                .where(PrivateMessageSQL_Table.index_row.is(index))
                .querySingle()
                .idField;
    }
}


