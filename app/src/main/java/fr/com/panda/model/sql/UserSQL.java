package fr.com.panda.model.sql;

import android.text.TextUtils;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.NotNull;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.sql.language.ConditionGroup;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.com.panda.manager.SessionManager;
import fr.com.panda.utils.PandaDB;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.enums.GenderType;
import fr.com.panda.model.UserModel;
import fr.com.panda.utils.DateUtil;

/**
 * Created by edouardroussillon on 12/29/16.
 */

@Table(database = PandaDB.class, name = "users")
public class UserSQL extends BaseModel {

    @PrimaryKey(autoincrement = true)
    @Column(name = "_id")
    long idField;

    @Column(name = "users_id")
    @Unique(onUniqueConflict = ConflictAction.REPLACE)
    @NotNull
    int userIdField;

    @Column(name = "email")
    String emailField;

    @Column(name = "pseudo")
    @NotNull
    String pseudoField;

    @Column(name = "description")
    String descriptionField = "";

    @Column(name = "birthday")
    String birthdayField = "";

    @Column(name = "gender")
    int genderField;

    @Column(name = "url_thumb")
    String dataUrlThumbField;

    @Column(name = "url_original")
    String dataUrlOriginalField;

    @Column(name = "url_blur")
    String dataUrlBlurField;

    @Column(name = "is_online")
    boolean isOnlineField = false;

    @Column(name = "last_seen")
    long lastSeenField;

    @Column(name = "status")
    @FriendshipType.FriendshipMode
    int friendshipStatusField = FriendshipType.NOT_FRIEND;

    @Column(name = "last_action")
    int friendshipLastActionField = -1;

    @Column(name = "reported")
    boolean reportedField = false;

    @Column(name = "updated_at")
    long timestampField = -1;

    private static int USER_ID = SessionManager.sharedInstance.user().getUserId();

    public UserSQL() {
    }

    public UserSQL(int userIdField,
                   String emailField,
                   String pseudoField,
                   String descriptionField,
                   String birthdayField,
                   int genderField,
                   String dataUrlThumbField,
                   String dataUrlOriginalField,
                   String dataUrlBlurField,
                   boolean isOnlineField,
                   long lastSeenField,
                   @FriendshipType.FriendshipMode int friendshipStatusField,
                   int friendshipLastActionField,
                   boolean reportedField,
                   long timestampField) {
        this.userIdField = userIdField;
        this.emailField = emailField;
        this.pseudoField = pseudoField;
        this.descriptionField = descriptionField;
        this.birthdayField = birthdayField;
        this.genderField = genderField;
        this.dataUrlThumbField = dataUrlThumbField;
        this.dataUrlOriginalField = dataUrlOriginalField;
        this.dataUrlBlurField = dataUrlBlurField;
        this.isOnlineField = isOnlineField;
        this.lastSeenField = lastSeenField;
        this.friendshipStatusField = friendshipStatusField;
        this.friendshipLastActionField = friendshipLastActionField;
        this.reportedField = reportedField;
        this.timestampField = timestampField;
    }

    private static UserSQL userModelToUserSQL(UserModel userModel) {

        String urlThumb = null == userModel.urlThumb ? "" : userModel.urlThumb.toString();
        String urlOriginal = null == userModel.urlOriginal ? "" : userModel.urlOriginal.toString();
        String urlBlur = null == userModel.urlBlur ? "" : userModel.urlBlur.toString();
        String birthday = null == userModel.birthday ? "" : DateUtil.getSmallDateToString(userModel.birthday);
        @GenderType.GenderMode int gender = userModel.gender;
        long lastSeen = null == userModel.lastSeen ? 0 : DateUtil.timestamp(userModel.lastSeen);
        @FriendshipType.FriendshipMode int status = userModel.friendshipStatus;
        long updatedAt = null == userModel.updatedAt ? DateUtil.timestamp() : DateUtil.timestamp(userModel.updatedAt);

        return new UserSQL(userModel.userId,
                userModel.email,
                userModel.pseudo,
                userModel.details,
                birthday,
                gender,
                urlThumb,
                urlOriginal,
                urlBlur,
                userModel.isOnline,
                lastSeen,
                status,
                userModel.friendshipLastAction,
                userModel.reported,
                updatedAt);
    }

    private static UserModel userSQLToUserModel(UserSQL userSQL) {
        if (null == userSQL) {
            return null;
        }

        try {
            URL urlThumb = TextUtils.isEmpty(userSQL.dataUrlThumbField) ? null : new URL(userSQL.dataUrlThumbField);
            URL urlOriginal = TextUtils.isEmpty(userSQL.dataUrlOriginalField) ? null : new URL(userSQL.dataUrlOriginalField);
            URL urlBlur = TextUtils.isEmpty(userSQL.dataUrlBlurField) ? null : new URL(userSQL.dataUrlBlurField);
            Date insertedAt = userSQL.timestampField == 0 ? new Date() : DateUtil.timestampToDate(userSQL.timestampField);
            Date birthday = TextUtils.isEmpty(userSQL.birthdayField) ? null : DateUtil.getStringToSmallDate(userSQL.birthdayField);
            Date lastSeen = userSQL.lastSeenField == 0 ? null : DateUtil.timestampToDate(userSQL.lastSeenField);

            return new UserModel(userSQL.userIdField,
                    userSQL.emailField,
                    userSQL.pseudoField,
                    userSQL.descriptionField,
                    birthday,
                    GenderType.fromId(userSQL.genderField),
                    urlThumb,
                    urlOriginal,
                    urlBlur,
                    userSQL.isOnlineField,
                    lastSeen,
                    FriendshipType.fromId(userSQL.friendshipStatusField),
                    userSQL.friendshipLastActionField,
                    userSQL.reportedField,
                    insertedAt);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static List<UserModel> userSQLsToUserModels(List<UserSQL> userSQLs) {
        List<UserModel> newList = new ArrayList();
        if (null != userSQLs) {
            final long count = userSQLs.size();

            for (int i = 0; i < count; i++) {
                final UserSQL userSQL = userSQLs.get(i);
                final UserModel userModel = userSQLToUserModel(userSQL);
                if (null != userModel) {
                    newList.add(userModel);
                }
            }
        }
        return newList;
    }

    //
    //MARK: SQL
    //

    public static UserModel getUser(int userId) {
        final UserSQL userSQL = SQLite.select()
                .from(UserSQL.class)
                .where(UserSQL_Table.users_id.is(userId))
                .querySingle();
        return userSQLToUserModel(userSQL);
    }

    public static List<UserModel> getUsersByFriendshipTypeFriend() {
        final int user = USER_ID;
        final List<UserSQL> list = SQLite.select()
                .from(UserSQL.class)
                .where(UserSQL_Table.status.is(FriendshipType.FRIEND))
                .and(UserSQL_Table.users_id.isNot(user))
                .orderBy(UserSQL_Table.pseudo, true)
                .queryList();
        return userSQLsToUserModels(list);
    }

    public static List<UserModel> getUsersByFriendshipTypeBlocked() {
        final int user = USER_ID;
        final List<UserSQL> list = SQLite.select()
                .from(UserSQL.class)
                .where(ConditionGroup.clause(UserSQL_Table.status.is(FriendshipType.BLOCKED_BEFORE_BE_FRIEND)).or(UserSQL_Table.status.is(FriendshipType.BLOCKED_AFTER_BE_FRIEND)))
                .and(UserSQL_Table.last_action.is(user))
                .and(UserSQL_Table.users_id.isNot(user))
                .orderBy(UserSQL_Table.pseudo, true)
                .queryList();
        return userSQLsToUserModels(list);
    }

    public static List<UserModel> getUsersByFriendshipTypePending() {
        final int user = USER_ID;
        final List<UserSQL> list = SQLite.select()
                .from(UserSQL.class)
                .where(UserSQL_Table.status.is(FriendshipType.PENDING))
                .and(UserSQL_Table.users_id.isNot(user))
                .orderBy(UserSQL_Table.pseudo, true)
                .queryList();
        return userSQLsToUserModels(list);
    }

    public static long getCountUsersByFriendshipTypePending() {
        final int user = USER_ID;
        final List<UserSQL> list = SQLite.select()
                .from(UserSQL.class)
                .where(UserSQL_Table.status.is(FriendshipType.PENDING))
                .and(UserSQL_Table.last_action.isNot(user))
                .and(UserSQL_Table.users_id.isNot(user))
                .orderBy(UserSQL_Table.pseudo, true)
                .queryList();
        if (null == list) {
            return 0;
        } else {
            return list.size();
        }
    }

    public static void updateUser(UserModel user) {
        userModelToUserSQL(user).save();
    }

    public static void insertUser(int userId, String userName, @FriendshipType.FriendshipMode int friendshipStatus, int friendshipLastAction) {
        final UserSQL userSQL = SQLite.select()
                .from(UserSQL.class)
                .where(UserSQL_Table.users_id.is(userId))
                .querySingle();

        if (null == userSQL) {
            userModelToUserSQL(new UserModel(userId,
                    "",
                    userName,
                    "",
                    null,
                    GenderType.NONE,
                    null,
                    null,
                    null,
                    false,
                    null,
                    friendshipStatus,
                    friendshipLastAction,
                    false,
                    new Date())).save();
        } else {
            userSQL.pseudoField = userName;
            userSQL.friendshipStatusField = friendshipStatus;
            userSQL.friendshipLastActionField = friendshipLastAction;
            userSQL.save();
        }
    }
}
