package fr.com.panda.model.sql;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.com.panda.utils.PandaDB;
import fr.com.panda.enums.DataType;
import fr.com.panda.enums.StatusType;
import fr.com.panda.model.FilterModel;
import fr.com.panda.model.PublicMessageModel;
import fr.com.panda.utils.DateUtil;

/**
 * Created by edouardroussillon on 12/29/16.
 */

@Table(database = PandaDB.class, name = "public_messages")
public class PublicMessageSQL extends BaseModel {

    @Column(name = "_id")
    @PrimaryKey(autoincrement = true)
    long idField;

    @Column(name = "public_messages_id")
    int publicMessageIdField = -1;

    @Column(name = "is_general")
    boolean isGeneralField;

    @Column(name = "msg")
    String msgField;

    @Column(name = "from_user_id")
    int fromUserIdField;

    @Column(name = "from_user_name")
    String fromUserNameField;

    @Column(name = "index_row")
    @Unique(onUniqueConflict = ConflictAction.REPLACE)
    String indexField;

    @Column(name = "is_read")
    boolean isReadField = false;

    @Column(name = "url_thumb")
    String urlThumbField = "";

    @Column(name = "url_original")
    String urlOriginalField = "";

    @Column(name = "url_blur")
    String urlBlurField = "";

    @Column(name = "type")
    @DataType.DataMode
    int typeField = DataType.NONE;

    @Column(name = "status")
    int statusField = StatusType.ERROR;

    @Column(name = "inserted_at")
    long insertedAtField = new Date().getTime();

    @Column(name = "is_highlight")
    boolean isHighlightField = false;

    @Column(name = "is_welcome_message")
    boolean isWelcomeMessageField = false;

    public PublicMessageSQL() {
    }

    public PublicMessageSQL(long idField, int publicMessageIdField, boolean isGeneralField, String msgField, int fromUserIdField, String fromUserNameField, String indexField, boolean isReadField, String urlThumbField, String urlOriginalField, String urlBlurField, int typeField, int statusField, long insertedAtField, boolean isHighlightField, boolean isWelcomeMessageField) {
        this.idField = idField;
        this.publicMessageIdField = publicMessageIdField;
        this.isGeneralField = isGeneralField;
        this.msgField = msgField;
        this.fromUserIdField = fromUserIdField;
        this.fromUserNameField = fromUserNameField;
        this.indexField = indexField;
        this.isReadField = isReadField;
        this.urlThumbField = urlThumbField;
        this.urlOriginalField = urlOriginalField;
        this.urlBlurField = urlBlurField;
        this.typeField = typeField;
        this.statusField = statusField;
        this.insertedAtField = insertedAtField;
        this.isHighlightField = isHighlightField;
        this.isWelcomeMessageField = isWelcomeMessageField;
    }

    public PublicMessageSQL(int publicMessageIdField, boolean isGeneralField, String msgField, int fromUserIdField, String fromUserNameField, String indexField, boolean isReadField, String urlThumbField, String urlOriginalField, String urlBlurField, int typeField, int statusField, long insertedAtField, boolean isHighlightField, boolean isWelcomeMessageField) {
        this.publicMessageIdField = publicMessageIdField;
        this.isGeneralField = isGeneralField;
        this.msgField = msgField;
        this.fromUserIdField = fromUserIdField;
        this.fromUserNameField = fromUserNameField;
        this.indexField = indexField;
        this.isReadField = isReadField;
        this.urlThumbField = urlThumbField;
        this.urlOriginalField = urlOriginalField;
        this.urlBlurField = urlBlurField;
        this.typeField = typeField;
        this.statusField = statusField;
        this.insertedAtField = insertedAtField;
        this.isHighlightField = isHighlightField;
        this.isWelcomeMessageField = isWelcomeMessageField;
    }

    private static PublicMessageSQL publicMessageModelToPublicMessageSQL(PublicMessageModel publicMessageModel) {

        String urlThumb = null == publicMessageModel.urlThumb ? "" : publicMessageModel.urlThumb.toString();
        String urlOriginal = null == publicMessageModel.urlOriginal ? "" : publicMessageModel.urlOriginal.toString();
        String urlBlur = null == publicMessageModel.urlBlur ? "" : publicMessageModel.urlBlur.toString();
        @DataType.DataMode int type = DataType.fromId(publicMessageModel.type);
        @StatusType.StatusMode int status = publicMessageModel.status;
        long insertedAt = null == publicMessageModel.insertedAt ? DateUtil.timestamp() : DateUtil.timestamp(publicMessageModel.insertedAt);

        return new PublicMessageSQL(publicMessageModel.messageId,
                publicMessageModel.isGeneral,
                publicMessageModel.msg,
                publicMessageModel.fromUserId,
                publicMessageModel.fromUserName,
                publicMessageModel.index,
                false,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                status,
                insertedAt,
                publicMessageModel.isHighlight,
                publicMessageModel.isWelcomeMessage);
    }

    private static PublicMessageSQL publicMessageModelToPublicMessageSQLFull(PublicMessageModel publicMessageModel) {

        String urlThumb = null == publicMessageModel.urlThumb ? "" : publicMessageModel.urlThumb.toString();
        String urlOriginal = null == publicMessageModel.urlOriginal ? "" : publicMessageModel.urlOriginal.toString();
        String urlBlur = null == publicMessageModel.urlBlur ? "" : publicMessageModel.urlBlur.toString();
        @DataType.DataMode int type = DataType.fromId(publicMessageModel.type);
        @StatusType.StatusMode int status = publicMessageModel.status;
        long insertedAt = null == publicMessageModel.insertedAt ? DateUtil.timestamp() : DateUtil.timestamp(publicMessageModel.insertedAt);

        return new PublicMessageSQL(publicMessageModel.rowId,
                publicMessageModel.messageId,
                publicMessageModel.isGeneral,
                publicMessageModel.msg,
                publicMessageModel.fromUserId,
                publicMessageModel.fromUserName,
                publicMessageModel.index,
                false,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                status,
                insertedAt,
                publicMessageModel.isHighlight,
                publicMessageModel.isWelcomeMessage);
    }

    private static PublicMessageModel publicMessageSQLToPublicMessageModel(PublicMessageSQL publicMessageSQL) {
        PublicMessageModel mPublicMessageModel = null;

        try {
            URL urlThumb = TextUtils.isEmpty(publicMessageSQL.urlThumbField) ? null : new URL(publicMessageSQL.urlThumbField);
            URL urlOriginal = TextUtils.isEmpty(publicMessageSQL.urlOriginalField) ? null : new URL(publicMessageSQL.urlOriginalField);
            URL urlBlur = TextUtils.isEmpty(publicMessageSQL.urlBlurField) ? null : new URL(publicMessageSQL.urlBlurField);
            Date insertedAt = publicMessageSQL.insertedAtField == 0 ? new Date() : DateUtil.timestampToDate(publicMessageSQL.insertedAtField);

            mPublicMessageModel = new PublicMessageModel(publicMessageSQL.publicMessageIdField,
                    publicMessageSQL.msgField,
                    publicMessageSQL.fromUserIdField,
                    publicMessageSQL.fromUserNameField,
                    urlThumb,
                    urlOriginal,
                    urlBlur,
                    DataType.fromId(publicMessageSQL.typeField),
                    publicMessageSQL.indexField,
                    StatusType.fromId(publicMessageSQL.statusField),
                    publicMessageSQL.isGeneralField,
                    publicMessageSQL.isHighlightField,
                    publicMessageSQL.isWelcomeMessageField,
                    insertedAt);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return mPublicMessageModel;
    }

    private static List<PublicMessageModel> publicMessageSQLsToPublicMessageModels(List<PublicMessageSQL> publicMessages) {
        List<PublicMessageModel> newList = new ArrayList();
        if (null != publicMessages) {
            final long count = publicMessages.size();


            for (int i = 0; i < count; i++) {
                final PublicMessageSQL publicMessageSQL = publicMessages.get(i);
                final PublicMessageModel publicMessageModel = publicMessageSQLToPublicMessageModel(publicMessageSQL);
                if (null != publicMessageModel) {
                    newList.add(publicMessageModel);
                }
            }
        }
        return newList;
    }

    //
    //MARK: SQL
    //

    public static List<PublicMessageModel> getAllPublicMessage(boolean isGeneral) {

        final List<PublicMessageSQL> list = SQLite.select()
                .from(PublicMessageSQL.class)
                .where(PublicMessageSQL_Table.is_general.is(isGeneral))
                .orderBy(PublicMessageSQL_Table.inserted_at, true)
                .limit(100)
                .queryList();

        return publicMessageSQLsToPublicMessageModels(list);
    }

    @Nullable
    public static PublicMessageModel getPublicMessage(boolean isGeneral, String index) {

        final PublicMessageSQL publicMessageSQL = SQLite.select()
                .from(PublicMessageSQL.class)
                .where(PublicMessageSQL_Table.is_general.is(isGeneral))
                .and(PublicMessageSQL_Table.index_row.is(index))
                .querySingle();

        return publicMessageSQLToPublicMessageModel(publicMessageSQL);
    }

    public static void updateMessageWithFilter(FilterModel filter) {
        final List<PublicMessageSQL> list = SQLite.select()
                .from(PublicMessageSQL.class)
                .queryList();

        for (PublicMessageSQL item : list) {
            if (!TextUtils.isEmpty(item.msgField) && item.msgField.toLowerCase().contains(filter.getName().toLowerCase())) {
                item.isHighlightField = filter.isHighlight();
                item.save();
            }
        }
    }

    public static void removeMessageWithFilter(FilterModel filter) {
        final List<PublicMessageSQL> list = SQLite.select()
                .from(PublicMessageSQL.class)
                .queryList();

        for (PublicMessageSQL item : list) {
            if (!TextUtils.isEmpty(item.msgField) && item.msgField.toLowerCase().contains(filter.getName().toLowerCase())) {
                item.isHighlightField = false;
                item.save();
            }
        }
    }


    public static void insert(PublicMessageModel message) {
        PublicMessageSQL publicMessage = publicMessageModelToPublicMessageSQLFull(message);
        publicMessage.save();
    }

    public static long insertLite(PublicMessageModel message) {
        PublicMessageSQL publicMessage = publicMessageModelToPublicMessageSQL(message);
        publicMessage.save();

        final PublicMessageSQL publicMessageSQL = SQLite.select()
                .from(PublicMessageSQL.class)
                .where(PublicMessageSQL_Table.index_row.is(message.index))
                .querySingle();

        return publicMessageSQL.idField;
    }

    public static boolean isContainMessage(String message) {
        final long count = SQLite.select()
                .from(PublicMessageSQL.class)
                .where(PublicMessageSQL_Table.is_welcome_message.is(true))
                .and(PublicMessageSQL_Table.msg.is(message))
                .count();

        return (count > 0);
    }
}
