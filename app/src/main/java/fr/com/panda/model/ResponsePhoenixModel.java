package fr.com.panda.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class ResponsePhoenixModel {

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("response")
    @Expose
    public Map<String, Object> response;

    public boolean statusOk() {
        if (null != status && status.equals("ok")) {
            return true;
        }

        return false;
    }
}
