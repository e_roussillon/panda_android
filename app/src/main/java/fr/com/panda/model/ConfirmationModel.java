package fr.com.panda.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import fr.com.panda.enums.StatusType;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class ConfirmationModel {

    @SerializedName("id")
    @Expose
    public int confirmationModelId;

    @SerializedName("status")
    @Expose
    @StatusType.StatusMode
    public int status;

}
