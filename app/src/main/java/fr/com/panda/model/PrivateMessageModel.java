package fr.com.panda.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.net.URL;
import java.util.Date;
import java.util.List;

import fr.com.panda.enums.DataType;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.enums.StatusType;
import fr.com.panda.model.sql.PrivateMessageSQL;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class PrivateMessageModel extends GenericMessageModel {

    @SerializedName("friendship_status")
    @Expose
    @FriendshipType.FriendshipMode
    public int friendshipStatus;

    @SerializedName("friendship_last_action")
    @Expose
    public int friendshipLastAction;

    @SerializedName("to_user_id")
    @Expose
    public int toUserId;

    @SerializedName("to_user_name")
    @Expose
    public String toUserName;

    public PrivateMessageModel(int roomId,
                               int messageId,
                               String msg,
                               int fromUserId,
                               String fromUserName,
                               int dataId,
                               URL urlThumb,
                               URL urlOriginal,
                               URL urlBlur,
                               @DataType.DataMode int type,
                               float size,
                               String dataLocal,
                               String index,
                               @StatusType.StatusMode int status,
                               Date insertedAt,
                               @FriendshipType.FriendshipMode int friendshipStatus,
                               int friendshipLastAction,
                               int toUserId,
                               String toUserName) {
        super(roomId,
                messageId,
                msg,
                fromUserId,
                fromUserName,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                index,
                status,
                insertedAt);

        this.friendshipStatus = friendshipStatus;
        this.friendshipLastAction = friendshipLastAction;
        this.toUserId = toUserId;
        this.toUserName = toUserName;
    }

    public PrivateMessageModel(int roomId,
                               int messageId,
                               String msg,
                               int fromUserId,
                               int dataId,
                               URL urlThumb,
                               URL urlOriginal,
                               URL urlBlur,
                               @DataType.DataMode int type,
                               float size,
                               String dataLocal,
                               String index,
                               @StatusType.StatusMode int status,
                               Date insertedAt) {
        super(roomId,
                messageId,
                msg,
                fromUserId,
                dataId,
                urlThumb,
                urlOriginal,
                urlBlur,
                type,
                size,
                dataLocal,
                index,
                status,
                insertedAt);
    }


    @Nullable
    public static PrivateMessageModel getMessage(int id) {
        return PrivateMessageSQL.getMessage(id);
    }

    @Nullable
    public static PrivateMessageModel getMessageByIndex(String index) {
        return PrivateMessageSQL.getMessageByIndex(index);
    }

    public static List<PrivateMessageModel> getPendingMessages() {
        return PrivateMessageSQL.getPendingMessages();
    }

    public static List<PrivateMessageModel> getSentMessagesOrderAsc() {
        return PrivateMessageSQL.getSentMessagesOrderAsc();
    }

    public static List<PrivateMessageModel> getMessagesByRoom(int roomId) {
        return PrivateMessageSQL.getMessagesByRoom(roomId);
    }

    public static List<PrivateMessageModel> getMessagesNotSent() {
        return PrivateMessageSQL.getMessagesNotSent();
    }

    public void insert() {
        PrivateMessageSQL.insert(this);
    }

    public void updateStatus() {
        PrivateMessageSQL.updateStatus(this.messageId, this.status);
    }

    public long insertLite() {
        return PrivateMessageSQL.insertLite(this);
    }

}
