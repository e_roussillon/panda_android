package fr.com.panda.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import fr.com.panda.enums.FriendshipType;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class FriendshipModel {

    @SerializedName("user_id")
    @Expose
    public int minUserId;

    @SerializedName("to_user_id")
    @Expose
    public int maxUserId;

    @SerializedName("status")
    @Expose
    public FriendshipType status;

    @SerializedName("last_action")
    @Expose
    public int userLastAction;



}
