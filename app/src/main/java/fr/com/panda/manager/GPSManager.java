package fr.com.panda.manager;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.List;

import fr.com.panda.enums.GPSStateType;
import fr.com.panda.event.Event;
import fr.com.panda.model.Location;
import fr.com.panda.utils.BuildVersion;
import fr.com.panda.utils.SimulatorUtil;

public class GPSManager {
    private LocalisationManager manager;

    public
    @GPSStateType.GPSStateMode
    int gpsState = GPSStateType.NOT_DETERMINED;

    public Event<Location> onGetLocalisation = new Event();
    public Event<Void> onGetLocalisationDenied = new Event();
    public Event<Void> onGetLocalisationNotDetermined = new Event();
    public Event<Void> onGetLocalisationFail = new Event();

    public GPSManager() {
        this.manager = new LocalisationManager(this);
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        this.manager.checkResult(requestCode, permissions, grantResults);
    }

    public void resetLocalisation() {
        this.manager.lastLocalisation = null;
    }

    public @GPSStateType.GPSStateMode int getGPSState(Activity activity) {
        if (this.gpsState == GPSStateType.NOT_DETERMINED) {
            getLastLocalisation(activity);
        }

        return this.gpsState;
    }

    @Nullable
    public Location getCurrentLocalisation() {
        if (this.manager.lastLocalisation != null) {
            return this.manager.lastLocalisation;
        } else {
            return SessionManager.sharedInstance.user().getCurrentLocalisation();
        }
    }

    public void getLastLocalisation(Activity activity) {

        if (SimulatorUtil.isRunningOnEmulator()) {
            this.manager.lastLocalisation = new Location(-23.515599, -46.189608);
            this.onGetLocalisation.dispatch(this.manager.lastLocalisation);
        } else {

            if (this.manager.lastLocalisation == null) {
                this.manager.startPermission(activity);
            } else {
                this.onGetLocalisation.dispatch(this.manager.lastLocalisation);
            }
        }
    }

    public void showDialogForAccessToPermission(Activity activity) {
        if (BuildVersion.getSDK() >= Build.VERSION_CODES.M) {
            Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            i.addCategory(Intent.CATEGORY_DEFAULT);
            i.setData(Uri.parse("package:" + activity.getPackageName()));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            activity.startActivity(i);
        } else {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            activity.startActivity(intent);
        }
    }


    private void updateState(@GPSStateType.GPSStateMode int gpsState) {
        this.gpsState = gpsState;

        switch (gpsState) {
            case GPSStateType.DENIED:
                this.onGetLocalisationDenied.dispatch(null);
                break;
            case GPSStateType.NOT_DETERMINED:
                this.onGetLocalisationNotDetermined.dispatch(null);
                break;
            case GPSStateType.FAILED:
                this.onGetLocalisationFail.dispatch(null);
                break;
            case GPSStateType.LOOKING_FOR:
                break;
            case GPSStateType.GOT_IT:
                this.onGetLocalisation.dispatch(this.manager.lastLocalisation);
                break;
        }
    }

    private class LocalisationManager {
        private LocationManager service;
        private GPSManager gpsManager;
        private String provider;
        private final int PERMISSIONS_REQUEST_FINE_LOCATION = 666;
        private Location lastLocalisation;

        private boolean accessFineLocation = false;
        private boolean resultAccessFineLocation = false;


        public LocalisationManager(GPSManager gpsManager) {
            this.gpsManager = gpsManager;
        }

        private void startPermission(Activity activity) {
            this.gpsManager.updateState(GPSStateType.LOOKING_FOR);
            this.service = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

            if (BuildVersion.getSDK() >= Build.VERSION_CODES.M
                    && ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    this.gpsManager.updateState(GPSStateType.DENIED);
                } else {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_FINE_LOCATION);
                }

                return;
            } else if (BuildVersion.getSDK() < Build.VERSION_CODES.M && service.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                this.gpsManager.updateState(GPSStateType.DENIED);
                return;
            }

            startGSP();
        }

        private Location getLastKnownLocation() throws SecurityException {
            List<String> providers = service.getProviders(true);
            android.location.Location bestLocation = null;
            for (String provider : providers) {
                android.location.Location l = service.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    // Found best last known location: %s", l);
                    bestLocation = l;
                }
            }
            return Location.fromAndroidLocalizaion(bestLocation);
        }

        private void startGSP() {
            try {
                Location location = getLastKnownLocation();
                if (location == null) {
                    this.gpsManager.updateState(GPSStateType.FAILED);
                } else {
                    this.lastLocalisation = location;
                    SessionManager.sharedInstance.user().setCurrentLocalisation(this.lastLocalisation);
                    this.gpsManager.updateState(GPSStateType.GOT_IT);
                }
            } catch (SecurityException e) {
                this.gpsManager.updateState(GPSStateType.FAILED);
            }
        }


        private void checkResult(int requestCode, String permissions[], int[] grantResults) {
            switch (requestCode) {
                case PERMISSIONS_REQUEST_FINE_LOCATION: {
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        accessFineLocation = true;
                    } else {
                        accessFineLocation = false;
                    }
                    resultAccessFineLocation = true;
                    break;
                }
            }

            this.checkResultRequestLocalisation();
        }

        private void checkResultRequestLocalisation() {
            if (resultAccessFineLocation && !accessFineLocation) {
                this.gpsManager.updateState(GPSStateType.DENIED);
            } else if (resultAccessFineLocation && accessFineLocation) {
                this.startGSP();
            }
        }
    }



}
