package fr.com.panda.manager;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.Date;
import java.util.HashMap;

import fr.com.panda.enums.ErrorType;
import fr.com.panda.enums.api.LoginError;
import fr.com.panda.event.Event;
import fr.com.panda.event.interfaces.IHandler;
import fr.com.panda.event.interfaces.IHandlerWithReturn;
import fr.com.panda.http.ApiEventGlobal;
import fr.com.panda.http.ApiManager;
import fr.com.panda.model.EmptyModel;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.model.Location;
import fr.com.panda.model.RequestFail;
import fr.com.panda.model.TokenModel;
import fr.com.panda.model.UserModel;
import fr.com.panda.socket.connector.SocketConnectorManager;
import fr.com.panda.utils.Constants;
import fr.com.panda.utils.DateUtil;
import fr.com.panda.utils.KeychainUtil;
import timber.log.Timber;

public class SessionManager {

    public static final SessionManager sharedInstance = new SessionManager();
    private User user = new User();

    private SessionManager() {}

    public Event<Void> onGetSessionSucceed = new Event();
    public Event<ErrorResponse> onGetSessionFailed = new Event();
    public Event<Void> onGetSessionFailedNotFoundEmailOrPassword = new Event();
    public Event<Void> onGetSessionFailedRateLimit = new Event();
    public Event<Void> onGetSessionFailedNotActive = new Event();

    public Event<Void> onRefreshSessionSucceed = new Event();
    public Event<ErrorResponse> onRefreshSessionFailed = new Event();

    public Event<Void> onResetPasswordSessionSucceed = new Event();
    public Event<Void> onResetPasswordSessionFailed = new Event();

    public User user() {
        return user;
    }

    public void uploadPushToken(@Nullable String token) {
        if (!TextUtils.isEmpty(token) && this.user().isLogin()) {
            APILogin.pushToken(token, emptyModel -> {
                this.user().setPushToken(token);
            }, requestFail -> {
                Timber.e("Error when uploading Push token", requestFail);
                return true;
            });
        } else {
            this.user().setPushToken("");
        }
    }

    public void updateUserId(int userId) {
        this.user().setUserId(userId);
    }

    public void login(String user, String password) {

        APILogin.login(user, password, tokenModel -> {
            this.checkToken(tokenModel);
        }, requestFail -> {
            this.clearUser();

            if (null == requestFail || null == requestFail.errorResponse) {
                this.onGetSessionFailed.dispatch(null);
                return false;
            }

            @LoginError.LoginErrorMode int code = LoginError.fromId(requestFail.errorResponse.code);

            if (requestFail.errorResponse.type == ErrorType.AUTH) {
                switch (code) {
                    case LoginError.ERROR_EMAIL_OR_PASSWORD_EMPTY:
                        this.onGetSessionFailedNotFoundEmailOrPassword.dispatch(null);
                        return true;
                    case LoginError.ERROR_RATE_LIMIT:
                        this.onGetSessionFailedRateLimit.dispatch(null);
                        return true;
                    case LoginError.ERROR_NOT_ACTIVE:
                        this.onGetSessionFailedNotActive.dispatch(null);
                        return true;
                }
            }

            this.onGetSessionFailed.dispatch(requestFail.errorResponse);
            return false;
        });
    }

    public void register(String pseudo, String email, Date birthday, String password) {
        APILogin.register(pseudo, email, birthday, password, tokenModel -> {
            this.checkToken(tokenModel);
        }, requestFail -> {
            this.clearUser();
            this.onGetSessionFailed.dispatch(requestFail.errorResponse);
            return true;
        });
    }

    public void logout() {
        this.clearUser();
        APILogin.logout(success -> {
            Timber.e("Logout with success");
        });
    }

    public void refreshToken() {
        APILogin.newToken(this.user().getTokenRefresh(), tokenModel -> {
            if (this.loginUser(tokenModel, false)) {
                SocketManager.sharedInstance.updatedToken = true;
                SocketConnectorManager.connectorManager.startSocket();
                ApiEventGlobal.sharedInstance.onRefreshSessionSucceed();
                //INFO - When event `SessionManager.onRefreshSessionSucceed` is dispatch Activity need to all `ChatManager.refreshPublicRoom`
                this.onRefreshSessionSucceed.dispatch(null);
            } else {
                ApiEventGlobal.sharedInstance.onRefreshSessionFail();
                this.onRefreshSessionFailed.dispatch(null);
            }

        }, requestFail -> {
            ApiEventGlobal.sharedInstance.onRefreshSessionFail();
            this.onRefreshSessionFailed.dispatch(requestFail.errorResponse);
            return true;
        });

    }

    public void resetPassword(String email) {

        APILogin.resetPassword(email, emptyView -> this.onResetPasswordSessionSucceed.dispatch(null)
                , requestFail -> {
            this.onResetPasswordSessionFailed.dispatch(null);
            return true;
        });
    }

    //
    //MARK: GENERIC
    //

    private void checkToken(TokenModel tokenModel) {
        if (null != tokenModel && this.loginUser(tokenModel, true)) {
            this.onGetSessionSucceed.dispatch(null);
        } else {
            this.onGetSessionFailed.dispatch(null);
        }
    }

    private boolean loginUser(TokenModel tokenModel, boolean uploadPushToken /**: Bool = true**/) {
        if (TextUtils.isEmpty(tokenModel.token) || TextUtils.isEmpty(tokenModel.refreshToken)) {
            this.logout();
            return false;
        }

        this.user().setToken(tokenModel.token);
        this.user().setTokenRefresh(tokenModel.refreshToken);
        this.user().setJustLogin(true);
        if (uploadPushToken) {
            this.uploadPushToken(this.user().getPushToken());
        }
        return true;
    }

    private void clearUser() {
        this.user().reset();
    }

    private static class APILogin {

        //
        //MARK: Login
        //

        public static void login(String user, String password, final IHandler<TokenModel> success, final IHandlerWithReturn<RequestFail> fail) {
            final HashMap<String, Object> disc = new HashMap<>();
            disc.put("email", user);
            disc.put("platform_device", Constants.PLATFORM);

            ApiManager.run(ApiManager.getApi().loginUser(password, disc), success, fail);
        }

        //
        //MARK: Register
        //

        public static void register(String pseudo, String email, Date birthday, String password, IHandler<TokenModel> success, IHandlerWithReturn<RequestFail> fail) {
            final HashMap<String, Object> user = new HashMap<>();
            user.put("pseudo", pseudo.trim().toLowerCase());
            user.put("email", email.trim().toLowerCase());
            user.put("birthday", DateUtil.getSmallDateToString(birthday));
            final HashMap<String, Object> disc = new HashMap<>();
            disc.put("user", user);

            ApiManager.run(ApiManager.getApi().registerUser(password, disc), success, fail);
        }

        //
        //MARK: New Token
        //

        public static void newToken(String refreshToken, final IHandler<TokenModel> success, final IHandlerWithReturn<RequestFail> fail) {
            ApiManager.run(ApiManager.getApi().renewToken("Bearer " + refreshToken), success, fail);
        }

        //
        //MARK: Push Token
        //

        public static void pushToken(String token, final IHandler<EmptyModel> success, final IHandlerWithReturn<RequestFail> fail) {
            final HashMap<String, Object> disc = new HashMap<>();
            disc.put("token", token);
            disc.put("os", Constants.PLATFORM);

            ApiManager.run(ApiManager.getApi().pushToken(disc), success, fail);
        }

        //
        //MARK: Logout
        //

        public static void logout(final IHandler<Void> success) {
            ApiManager.run(ApiManager.getApi().logout(), tokenModel -> {
                success.dispatch(null);
            }, requestFail -> {
                success.dispatch(null);
                return true;
            });
        }

        //
        //MARK: ResetPassword
        //

        public static void resetPassword(String email, final IHandler<EmptyModel> success, final IHandlerWithReturn<RequestFail> fail) {
            final HashMap<String, Object> disc = new HashMap<>();
            disc.put("email", email);

            ApiManager.run(ApiManager.getApi().resetPassword(disc), success, fail);
        }
    }

    public class User {
        private KeychainUtil keychainUtil = KeychainUtil.sharedInstance;


        private User() {}

        public String getToken() {
            return keychainUtil.getToken();
        }

        public void setToken(String token) {
            keychainUtil.setToken(token);
        }

        public String getPushToken() {
            return keychainUtil.getPushToken();
        }

        public void setPushToken(String pushToken) {
            keychainUtil.setPushToken(pushToken);
        }

        public String getTokenRefresh() {
            return keychainUtil.getTokenRefresh();
        }

        public void setTokenRefresh(String tokenRefresh) {
            keychainUtil.setTokenRefresh(tokenRefresh);
        }

        public boolean isJustLogin() {
            return keychainUtil.getJustLogin();
        }

        public void setJustLogin(boolean justLogin) {
            keychainUtil.setJustLogin(justLogin);
        }

        public boolean isWelcomeScreen() {
            return keychainUtil.isWelcomeScreen();
        }

        public void setWelcomeScreen(boolean welcomeScreen) {
            keychainUtil.setWelcomeScreen(welcomeScreen);
        }

        public int getUserId() {
            return keychainUtil.getUserId();
        }

        public void setUserId(int userId) {
            keychainUtil.setUserId(userId);
        }

        public Location getCurrentLocalisation() {
            return keychainUtil.getCurrentLocalisation();
        }

        public void setCurrentLocalisation(Location location) {
            keychainUtil.setCurrentLocalisation(location);
        }

        public boolean isLogin() {
            return keychainUtil.isLogin();
        }

        public String getUserPseudo() {
            final UserModel user = UserModel.getCurrentUser();

            if (null != user && !TextUtils.isEmpty(user.pseudo)) {
                return user.pseudo;
            } else {
                return "";
            }
        }

        public void reset() {
            keychainUtil.reset();
        }

        public void resetDB() {
            keychainUtil.resetDB();
        }

        public void resetWelcomeScreen() {
            keychainUtil.resetWelcomeScreen();
        }

        public void resetLocalisation() {
            keychainUtil.resetLocalisation();
        }

        public void resetPublicChat() {
            keychainUtil.resetPublicChat();
        }
    }
}
