package fr.com.panda.manager;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.List;

import fr.com.panda.enums.StatusType;
import fr.com.panda.event.Event;
import fr.com.panda.event.interfaces.IHandler;
import fr.com.panda.event.interfaces.IHandlerWithReturn;
import fr.com.panda.http.ApiManager;
import fr.com.panda.model.ConfirmationModel;
import fr.com.panda.model.EmptyModel;
import fr.com.panda.model.FindRoomsModel;
import fr.com.panda.model.Location;
import fr.com.panda.model.PrivateMessageModel;
import fr.com.panda.model.PublicRoomModel;
import fr.com.panda.model.RequestFail;
import fr.com.panda.socket.connector.SocketConnectorManager;
import fr.com.panda.socket.notification.SocketNotificaterManager;
import fr.com.panda.utils.KeychainUtil;

//TODO - NEED TO TEST THIS CLASS

public class ChatManager {
    public static final ChatManager sharedInstance = new ChatManager();

    private SocketNotificaterManager socketNotificaterManager = SocketNotificaterManager.sharedInstance;
    private SessionManager sessionManager = SessionManager.sharedInstance;

    public Event<FindRoomsModel> onFindPublicRoomSucceed = new Event();
    public Event<Void> onFindPublicRoomFailedGPS = new Event();
    public Event<Void> onFindPublicRoomFailed = new Event();

    private boolean isCountryFind = false;

    public boolean isCountryFind() {
        return isCountryFind;
    }

    public int getCurrentRoomId() {
        return KeychainUtil.sharedInstance.getCurrentRoomId();
    }

    public void setCurrentRoomId(int currentRoomId) {
        KeychainUtil.sharedInstance.setCurrentRoomId(currentRoomId);
    }

    @Nullable
    public PublicRoomModel getCurrentRoom() {
        return KeychainUtil.sharedInstance.getCurrentRoom();
    }

    public void setCurrentRoom(PublicRoomModel publicRoomModel) {
        KeychainUtil.sharedInstance.setCurrentRoom(publicRoomModel);
    }

    public FindRoomsModel getFindRooms() {
        return KeychainUtil.sharedInstance.getFindRooms();
    }

    public void setFindRooms(FindRoomsModel findRoomsModel) {
        KeychainUtil.sharedInstance.setFindRooms(findRoomsModel);
    }

    private ChatManager() {

    }

    public void findPublicRoom() {

        APIChat.findPublicRoom(findRoomsModel -> {
            this.isCountryFind = true;
            this.setFindRooms(findRoomsModel);
            List<PublicRoomModel> rooms = findRoomsModel.rooms;
            final int count = rooms.size();

            for (int i = 0; i < count; i++) {
                PublicRoomModel room = rooms.get(i);

                if (!TextUtils.isEmpty(room.type) && room.type.equals("city")) {
                    this.setCurrentRoomId(room.publicRoomId);
                    this.setCurrentRoom(room);
                    SocketManager.sharedInstance.joinRooms();
                    break;
                }
            }

            this.onFindPublicRoomSucceed.dispatch(this.getFindRooms());
        }, event -> {
            this.sessionManager.user().resetLocalisation();
            this.isCountryFind = false;

            this.onFindPublicRoomFailed.dispatch(null);
            return true;
        }, event -> {
            this.sessionManager.user().resetLocalisation();
            this.isCountryFind = false;

            this.onFindPublicRoomFailedGPS.dispatch(null);
            return true;
        });
    }

    //INFO - When event `SessionManager.onRefreshSessionSucceed` is dispatch Activity need to all `ChatManager.refreshPublicRoom`
    public void refreshPublicRoom(Activity activity) {
        GPSManager gpdManager = new GPSManager();
        gpdManager.onGetLocalisation.addOnce(RefreshPublicRoom::onGetLocalisation);
        gpdManager.getLastLocalisation(activity);
    }

    public void receivedPrivareMessages(List<ConfirmationModel> messages) {
        APIChat.confirmationsPrivateMessages(messages, success -> {}, event -> true);
    }

    private static class RefreshPublicRoom {
        public static void onGetLocalisation(Location location) {
            ChatManager.sharedInstance.findPublicRoom();
        }
    }

    private static class APIChat {

        public static void findPublicRoom(final IHandler<FindRoomsModel> success, final IHandlerWithReturn<RequestFail> fail, final IHandlerWithReturn<Void> failLocalisation) {
            final Location loc = new GPSManager().getCurrentLocalisation();

            if (loc == null) {
                failLocalisation.onEvent(null);
            } else {
                HashMap<String, Object> disc = new HashMap();
                disc.put("latitude", loc.getLatitude());
                disc.put("longitude", loc.getLongitude());

                ApiManager.run(ApiManager.getApi().findRoom(disc), findRoomsModel -> {
                    success.dispatch(findRoomsModel);
                }, fail);
            }
        }

        public static void confirmationsPrivateMessages(final List<ConfirmationModel> messages, final IHandler<EmptyModel> success, final IHandlerWithReturn<RequestFail> fail) {
            final HashMap<String, Object> disc = new HashMap<>();
            disc.put("messages", messages);

            ApiManager.run(ApiManager.getApi().confirmationMessages(disc), emptyModel -> {
                final int count = messages.size();

                for (int i = 0; i < count; i++) {
                    ConfirmationModel message = messages.get(i);

                    if (message.confirmationModelId > 0) {
                        @StatusType.StatusMode int status = StatusType.fromId(message.status);

                        PrivateMessageModel privateMessage = PrivateMessageModel.getMessage(message.confirmationModelId);

                        if (privateMessage != null)  {
                            privateMessage.status = status;
                            privateMessage.updateStatus();
                        }
                    }
                }

                success.dispatch(new EmptyModel());
            }, fail);
        }
    }
}
