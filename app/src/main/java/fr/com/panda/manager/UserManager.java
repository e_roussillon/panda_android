package fr.com.panda.manager;


import java.util.HashMap;

import fr.com.panda.event.Event;
import fr.com.panda.event.interfaces.IHandler;
import fr.com.panda.event.interfaces.IHandlerWithReturn;
import fr.com.panda.http.ApiManager;
import fr.com.panda.model.EmptyModel;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.model.RequestFail;
import fr.com.panda.model.UserModel;

public class UserManager {
    public static UserManager sharedInstance = new UserManager();

    public Event<UserModel> onUpdateUserSucceed = new Event<UserModel>();
    public Event<ErrorResponse> onUpdateUserFailed = new Event<ErrorResponse>();

    public Event<UserModel> onGetUserSucceed = new Event<UserModel>();
    public Event<ErrorResponse> onGetUserFailed = new Event<ErrorResponse>();

    public Event<UserModel> onReportUserSucceed = new Event<UserModel>();
    public Event<ErrorResponse> onReportUserFailed = new Event<ErrorResponse>();

    public void updateLocalUser(UserModel userModel) {
        userModel.update();
    }

    public void reportUser(int userId) {
        APIUser.reportUserBy(userId, empty -> {
            final UserModel user = UserModel.getUser(userId);
            user.reported = true;
            user.update();
            this.onReportUserSucceed.dispatch(user);
        }, requestFail -> {
            this.onReportUserFailed.dispatch(requestFail.errorResponse);
            return true;
        });
    }

    public void getUserBy(int userId) {
        APIUser.getUserBy(userId, user -> {
            this.updateLocalUser(user);
            this.onGetUserSucceed.dispatch(user);
        }, requestFail -> {
            this.onGetUserFailed.dispatch(requestFail.errorResponse);
            return true;
        });
    }

    private static class APIUser {

        public static void reportUserBy(int userId, final IHandler<EmptyModel> success, final IHandlerWithReturn<RequestFail> fail) {
            final HashMap<String, Object> reported = new HashMap<>();
            reported.put("user_id_reported", userId);

            final HashMap<String, Object> disc = new HashMap<>();
            disc.put("user_report", reported);

            ApiManager.run(ApiManager.getApi().reportUser(disc), success, fail);
        }

        public static void getUserBy(int userId, final IHandler<UserModel> success, final IHandlerWithReturn<RequestFail> fail) {
            ApiManager.run(ApiManager.getApi().getUserBy(userId), success, fail);
        }
    }



}
