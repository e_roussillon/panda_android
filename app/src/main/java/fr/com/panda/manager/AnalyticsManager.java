package fr.com.panda.manager;

import android.text.TextUtils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import fr.com.panda.enums.ActionNameType;
import fr.com.panda.enums.ScreeNameType;
import fr.com.panda.model.UserModel;
import fr.com.panda.utils.EnvironmentUtil;
import timber.log.Timber;

public class AnalyticsManager {
    public static final AnalyticsManager sharedInstance = new AnalyticsManager();

    private AnalyticsManager() {}

    public void trackUserLogin() {
        if (EnvironmentUtil.isTargetPanda()) {
            UserModel user = UserModel.getCurrentUser();

            if (user.userId > 0 && !TextUtils.isEmpty(user.pseudo) && !TextUtils.isEmpty(user.email)) {
                HashMap<String, Object> disc = new HashMap();
                disc.put("pseudo", user.pseudo);
                disc.put("email", user.email);
                this.printTrackActionUser(user.userId +"", disc);
//                SEGAnalytics.sharedAnalytics().identify("\(userId)", traits: disc)
            }
        }
    }

    public void trackScreen(@ScreeNameType.ScreeNameMode String screen, HashMap<String, Object> params) {
        if (EnvironmentUtil.isTargetPanda() || EnvironmentUtil.isTargetPandaBeta()) {
            this.printTrackScreen(screen, params);
//            Answers.logContentViewWithName(screen.rawValue, contentType: nil, contentId: nil, customAttributes: params)

            this.sendSegment(screen, params);
        }
    }

    public void trackAction(@ActionNameType.ActionNameMode String action, HashMap<String, Object> params) {
        if (EnvironmentUtil.isTargetPanda() || EnvironmentUtil.isTargetPandaBeta()) {
            this.printTrackAction(action, params);
//            Answers.logCustomEventWithName(action.rawValue, customAttributes: params)

            this.sendSegment(action, params);
        }
    }

    private void printTrackScreen(@ScreeNameType.ScreeNameMode String screen, HashMap<String, Object>  params) {
        Timber.i("***** Analytics (ScreenName) *****");
        Timber.i("----- SCREEN NAME -----");
        Timber.i("       *  "+ screen);

        if (params != null && params.size() > 0) {
            Timber.i("----- PARAM -----");

            Iterator it = params.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                Timber.i("       * "+ pair.getKey() + " -> " + pair.getValue());
                it.remove(); // avoids a ConcurrentModificationException
            }
        }
    }

    private void printTrackAction(@ActionNameType.ActionNameMode String action, HashMap<String, Object> params) {
        Timber.i("***** Analytics (ActionName) *****");
        Timber.i("----- ACTION NAME -----");
        Timber.i("       *  "+ action);
        if (params != null && params.size() > 0) {
            Timber.i("----- PARAM -----");

            Iterator it = params.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                Timber.i("       * "+ pair.getKey() + " -> " + pair.getValue());
                it.remove(); // avoids a ConcurrentModificationException
            }
        }
    }

    private void printTrackActionUser(String userId, HashMap<String, Object> params) {
        Timber.i("***** Analytics (UserID) *****");
        Timber.i("----- UserId -----");
        Timber.i("       *  "+ userId);

        if (params != null && params.size() > 0) {
            Timber.i("----- PARAM -----");

            Iterator it = params.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                Timber.i("       * "+ pair.getKey() + " -> " + pair.getValue());
                it.remove(); // avoids a ConcurrentModificationException
            }
        }
    }

    private void sendSegment(String track, HashMap<String, Object> params) {
        if (EnvironmentUtil.isTargetPanda()) {
//            if let p = params {
//                SEGAnalytics.sharedAnalytics().track(track, properties: p)
//            } else {
//                SEGAnalytics.sharedAnalytics().track(track)
//            }
        }
    }
}
