package fr.com.panda.manager;

import java.util.List;

import fr.com.panda.event.Event;
import fr.com.panda.event.interfaces.IHandler;
import fr.com.panda.event.interfaces.IHandlerWithReturn;
import fr.com.panda.http.ApiManager;
import fr.com.panda.model.BackupModel;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.model.PrivateMessageModel;
import fr.com.panda.model.RequestFail;
import fr.com.panda.model.UserModel;
import timber.log.Timber;

public class BackupManager {

    public static final BackupManager sharedInstance = new BackupManager();
    private SessionManager sessionManager = SessionManager.sharedInstance;

    public Event<Void> onBackupSucceed = new Event();
    public Event<ErrorResponse> onBackupFailed = new Event();

    private BackupManager() {

    }

    public void backup() {
        APIBackup.backup(backup -> {
            this.checkBackup(backup);
        }, error -> {
            this.sessionManager.logout();
            this.onBackupFailed.dispatch(error.errorResponse);
            return true;
        });
    }

    public void backupPending() {
        APIBackup.backupPending(backup -> {
            this.backupPending(backup);
        }, error -> {
            Timber.e("Cannot backup data pending", error);
            return true;
        });
    }

    private void checkBackup(BackupModel backup) {
        if (this.backupUser(backup)) {
            this.onBackupSucceed.dispatch(null);
        } else {
            this.sessionManager.logout();
            this.onBackupFailed.dispatch(null);
        }
    }

    private boolean backupUser(BackupModel backupModel) {
        if (null == backupModel || backupModel.userId <= 0) {
            return false;
        }

        backupModel.insert();
        this.sessionManager.updateUserId(backupModel.userId);
        SocketManager.sharedInstance.startSocket();
        return true;
    }

    private void backupPending(BackupModel backup) {
        if (null == backup) {
            return;
        }

        List<PrivateMessageModel> listMessagesBefore = PrivateMessageModel.getSentMessagesOrderAsc();
        List<UserModel> listFriendsBefore = UserModel.getUsersByFriendshipTypePending();
        backup.insert();
        List<PrivateMessageModel> listMessagesAfter = PrivateMessageModel.getSentMessagesOrderAsc();
        List<UserModel> listFriendsAfter = UserModel.getUsersByFriendshipTypePending();

        listMessagesAfter.removeAll(listMessagesBefore);
        listFriendsAfter.removeAll(listFriendsBefore);


//        TODO - WHEN CHAT IS READ UPDATE THIS PART
//        var listIds: [(id: Int, status: StatusType)] = []
//        for privateMessage in listMessagesAfter {
//            if let messageId = privateMessage.messageId {
//                listIds.append((id: messageId, status: StatusType.Received))
//                UILocalNotification.showPrivateMessage(privateMessage)
//            }
//        }
//
//        for user in listFriendsAfter {
//            if let pseudo = user.pseudo,
//            let userId = user.userId {
//                UILocalNotification.showFriendship(pseudo, userId: userId)
//            }
//        }
//
//        if listIds.count > 0 {
//            ChatManager.sharedInstance.receivedPrivareMessages(listIds)
//        }
    }

    private static class APIBackup {
        public static void backup(final IHandler<BackupModel> success, final IHandlerWithReturn<RequestFail> fail) {
            ApiManager.run(ApiManager.getApi().getBackup(), success, fail);
        }

        public static void backupPending(final IHandler<BackupModel> success, final IHandlerWithReturn<RequestFail> fail) {
            ApiManager.run(ApiManager.getApi().getBackupPending(), success, fail);
        }
    }
}
