package fr.com.panda.manager;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ImageView;

import org.apache.commons.lang3.text.WordUtils;

import java.net.URL;
import java.util.Date;
import java.util.List;

import fr.com.panda.R;
import fr.com.panda.enums.ChannelType;
import fr.com.panda.enums.DataType;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.enums.StatusType;
import fr.com.panda.model.PrivateMessageModel;
import fr.com.panda.model.PublicMessageModel;
import fr.com.panda.model.PublicRoomModel;
import fr.com.panda.model.UserModel;
import fr.com.panda.socket.connector.ISocketConnector;
import fr.com.panda.socket.connector.SocketConnectorManager;
import fr.com.panda.socket.highlighter.SocketHighlighterManager;
import fr.com.panda.socket.joiner.SocketJoinerManager;
import fr.com.panda.socket.notification.SocketNotificaterManager;
import fr.com.panda.socket.phoenix.PhoenixSocket;
import fr.com.panda.socket.sender.SocketSenderManager;

public class SocketManager {

    public static final SocketManager sharedInstance = new SocketManager();
    private SocketJoinerManager socketJoiner = new SocketJoinerManager();
    private SocketSenderManager socketSender = new SocketSenderManager();
    private ISocketConnector socketConnector = SocketConnectorManager.connectorManager;
    private SocketHighlighterManager socketHighlighter = SocketHighlighterManager.sharedInstance;
    private ChatManager chatManager = ChatManager.sharedInstance;
    private SessionManager sessionManager = SessionManager.sharedInstance;
    private SocketNotificaterManager socketNotification = SocketNotificaterManager.sharedInstance;

    public boolean updatedToken = false;

    private SocketManager() {}

    //
    // Connector
    //

    public boolean isSocketConnected() {
        return socketConnector.isSocketConnected();
    }

    public boolean startSocket() {
        boolean result = socketConnector.startSocket();
        joinRooms();
        return result;
    }

    public void stopSocket(PhoenixSocket.ICallBackSocketClose callBackSocketClose) {
        socketConnector.stopSocket(callBackSocketClose);
    }

    public void forceStopSocket(PhoenixSocket.ICallBackSocketClose callBackSocketClose) {
        socketConnector.forceStopSocket(callBackSocketClose);
    }

    //
    // Send Message
    //

    public void sendPrivateMessages(List<PrivateMessageModel> messages) {
        socketSender.sendMessages(messages, this.socketConnector);
    }

    public void sendPrivateMessage(int roomId, String message, @Nullable ImageView image, @FriendshipType.FriendshipMode int friendshipStatus, int friendshipLastAction, UserModel toUser) {
//        TODO - need to send how set light version
//        let message = PrivateMessageModel.initMessageLite(roomId: roomId,
//                msg: message,
//                friendshipStatus: friendshipStatus,
//                friendshipLastAction: friendshipLastAction,
//                toUser: toUser,
//                image: image)
        socketSender.sendMessage(null /*change for PrivateMessageModel*/, this.socketConnector, true);
    }

    public void resendPrivateMessages(PrivateMessageModel message) {
        socketSender.sendMessage(message, this.socketConnector,false);
    }

    public void sendPublicMessage(boolean isGeneralTopic, String msg) {
        PublicMessageModel message = PublicMessageModel.initMessageLite(msg, isGeneralTopic);
        socketSender.sendMessage(message, this.socketConnector);
    }

    public void sendPublicMessageWelcome(Context mContext, boolean isGeneralTopic) {

        if (null != this.chatManager.getCurrentRoom() && !TextUtils.isEmpty(this.chatManager.getCurrentRoom().originalName)) {
            final String title = this.chatManager.getCurrentRoom().originalName;
            if (isGeneralTopic) {
                final String msg = mContext.getResources().getString(R.string.welcome_message_general).replace("%1", WordUtils.capitalize(title));

                PublicMessageModel message = PublicMessageModel.initMessageLite(msg, isGeneralTopic, StatusType.SENT, true);
                socketSender.sendWelcomeMessage(message);
            } else {
                final String msg = mContext.getResources().getString(R.string.welcome_message_sale).replace("%1", WordUtils.capitalize(title));

                PublicMessageModel message = PublicMessageModel.initMessageLite(msg, isGeneralTopic, StatusType.SENT, true);
                socketSender.sendWelcomeMessage(message);
            }
        }
    }

    public void resendPublicMessage(PublicMessageModel publicMessageModel) {
        socketSender.resendMessage(publicMessageModel, this.socketConnector);
    }

    //
    // Join Channel
    //

    public void leaveRooms() {
        socketConnector.leaveAllRoom();
    }

    public void leaveRoomPublicRooms() {
        socketConnector.leaveRoomPublicRooms();
    }

    public void joinRooms() {

        if (null != this.sessionManager.user() && this.sessionManager.user().getUserId() > 0) {
            final int userId = this.sessionManager.user().getUserId();

            if (!this.isJoined(ChannelType.PRIVATE, userId)) {
                this.joinPrivateChannel();
            } else if (updatedToken) {
                this.refreshTokenPrivateChannel();
            }

            this.socketNotification.onJoinPrivateRoomSucceed.dispatch(null);
        } else {
            this.socketNotification.onJoinPrivateRoomFailed.dispatch(null);
        }

        if (!TextUtils.isEmpty(this.chatManager.getFindRooms().token)
                && null != this.chatManager.getFindRooms()
                && this.chatManager.getCurrentRoomId() > 0
                && this.chatManager.isCountryFind()) {

            String token = this.chatManager.getFindRooms().token;
            List<PublicRoomModel> rooms = this.chatManager.getFindRooms().rooms;
            int id = this.chatManager.getCurrentRoomId();

            if (!this.isJoined(ChannelType.GENERIC, id)) {
                this.joinGeneralChannel(id, token);
            } else if (updatedToken) {
                this.refreshTokenGeneralChannel(id, token);
            }

            if (!this.isJoined(ChannelType.SALE, id)) {
                this.joinSaleChannel(id, token);
            } else if (updatedToken) {
                this.refreshTokenSaleChannel(id, token);
            }

            for (PublicRoomModel room: rooms) {
                if (room.publicRoomId <= 0) {
                    return;
                }

                final int roomId = room.publicRoomId;

                if (!this.isJoined(ChannelType.BROADCAST, roomId)) {
                    this.joinBroadcastChannel(roomId, token);
                } else if (updatedToken) {
                    this.refreshTokenBroadcastChannel(roomId, token);
                }
            }

        } else {
            this.socketNotification.onJoinPublicRoomFailed.dispatch(null);
        }

        if (updatedToken) {
            updatedToken = false;
        }
    }

    public boolean isJoined(@ChannelType.ChannelMode String channelType, int roomId) {
        return socketJoiner.isJoined(channelType, roomId, this.socketConnector);
    }

    public void joinGeneralChannel(int roomId, String token) {
        socketJoiner.joinPublicChannel(ChannelType.GENERIC, roomId, token, this.socketConnector);
    }

    public void refreshTokenGeneralChannel(int roomId, String token) {
        socketJoiner.refreshToken(ChannelType.GENERIC, roomId, token, this.socketConnector);
    }

    public void joinSaleChannel(int roomId, String token) {
        socketJoiner.joinPublicChannel(ChannelType.SALE, roomId, token, this.socketConnector);
    }

    public void refreshTokenSaleChannel(int roomId, String token) {
        socketJoiner.refreshToken(ChannelType.SALE, roomId, token, this.socketConnector);
    }

    public void joinBroadcastChannel(int roomId, String token) {
        socketJoiner.joinPublicChannel(ChannelType.BROADCAST, roomId, token, this.socketConnector);
    }

    public void refreshTokenBroadcastChannel(int roomId, String token) {
        socketJoiner.refreshToken(ChannelType.BROADCAST, roomId, token, this.socketConnector);
    }

    public void joinPrivateChannel() {
        socketJoiner.joinPrivateChannel(this.socketConnector);
    }

    public void refreshTokenPrivateChannel() {
        socketJoiner.refreshToken(this.socketConnector);
    }

    //
    // Message Highlighter
    //

    public boolean isPublicMessageHavetoBeBlocked(PublicMessageModel publicMessage, @ChannelType.ChannelMode String channelType) {
        return socketHighlighter.checkIsIncomingPublicMessageHaveToBeBlocked(publicMessage, channelType);
    }

}
