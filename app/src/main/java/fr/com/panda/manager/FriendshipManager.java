package fr.com.panda.manager;

import java.util.HashMap;
import java.util.List;

import fr.com.panda.enums.FriendshipType;
import fr.com.panda.event.Event;
import fr.com.panda.event.interfaces.IHandler;
import fr.com.panda.event.interfaces.IHandlerWithReturn;
import fr.com.panda.http.ApiManager;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.model.RequestFail;
import fr.com.panda.model.UserModel;

public class FriendshipManager {
    static public FriendshipManager sharedInstance = new FriendshipManager();

    public Event<UserModel> onUpdateFriendshipSucceed = new Event();
    public Event<ErrorResponse> onUpdateFriendshipFailed = new Event();
    public Event<ErrorResponse> onUpdateFriendshipFailedUserIdNotFound = new Event();

    private FriendshipManager() {}

    public List<UserModel> getUsersByFriendshipTypeFriend() {
        return UserModel.getUsersByFriendshipTypeFriend();
    }

    public List<UserModel> getUsersByFriendshipTypeBlocked() {
        return UserModel.getUsersByFriendshipTypeBlocked();
    }

    public List<UserModel> getUsersByFriendshipTypePending() {
        return UserModel.getUsersByFriendshipTypePending();
    }

    public void updateFriendship(int userId, @FriendshipType.FriendshipMode int status) {
        if (userId <= 0) {
            this.onUpdateFriendshipFailedUserIdNotFound.dispatch(null);
            return;
        }

        APIFriendship.updateFriendship(userId, status, userModel -> {
            userModel.update();
            this.onUpdateFriendshipSucceed.dispatch(userModel);
        }, event -> {
            this.onUpdateFriendshipFailed.dispatch(event.errorResponse);
            return true;
        });
    }

    private static class APIFriendship {

        public static void updateFriendship(int toUserId, @FriendshipType.FriendshipMode int status, final IHandler<UserModel> success, final IHandlerWithReturn<RequestFail> fail) {
            final HashMap<String, Object> disc = new HashMap<>();
            disc.put("to_user_id", toUserId);
            disc.put("status", status);

            ApiManager.run(ApiManager.getApi().updateFriendship(disc), success, fail);
        }

    }
}
