package fr.com.panda.manager;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.HashMap;

import fr.com.panda.event.Event;
import fr.com.panda.event.interfaces.IHandler;
import fr.com.panda.event.interfaces.IHandlerWithReturn;
import fr.com.panda.http.ApiManager;
import fr.com.panda.model.EmptyModel;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.model.FilterModel;
import fr.com.panda.model.PublicMessageModel;
import fr.com.panda.model.RequestFail;

/**
 * Created by edouardroussillon on 1/16/17.
 */

public class FilterManager {

    public static final FilterManager sharedInstance = new FilterManager();

    public Event<FilterModel> onAddFilterSucceed = new Event();
    public Event<ErrorResponse> onAddFilterFailed = new Event();

    public Event<FilterModel> onUpdateFilterSucceed = new Event();
    public Event<ErrorResponse> onUpdateFilterFailed = new Event();
    public Event<ErrorResponse> onUpdateFilterFailedMissingParam = new Event();

    public Event<Void> onRemoveFilterSucceed = new Event();
    public Event<ErrorResponse> onRemoveFilterFailed = new Event();


    public void addFilter(String name, boolean isHighlight) {
        APIFilter.addFilter(name, isHighlight, filterModel -> {

            final FilterModel filter = updateOfInsertFilter(filterModel);

            if (null != filter) {
                PublicMessageModel.updateMessageWithFilter(filter);
                this.onAddFilterSucceed.dispatch(filter);
            } else {
                this.onAddFilterFailed.dispatch(null);
            }

        }, requestFail -> {
            this.onAddFilterFailed.dispatch(requestFail.errorResponse);
            return true;
        });
    }

    public void updateFilter(FilterModel filterModel, String name) {

        if (null == filterModel || filterModel.getFilterId() == 0 || TextUtils.isEmpty(name)) {
            this.onUpdateFilterFailedMissingParam.dispatch(null);
        }

        APIFilter.updateFilter(filterModel.getFilterId(), name, filterModel.isHighlight(), filterModelEvent -> {
            final FilterModel filter = updateOfInsertFilter(filterModelEvent);

            if (null != filter) {
                PublicMessageModel.updateMessageWithFilter(filter);
                this.onUpdateFilterSucceed.dispatch(filter);
            } else {
                this.onUpdateFilterFailed.dispatch(null);
            }
        }, requestFail -> {
            this.onUpdateFilterFailed.dispatch(requestFail.errorResponse);
            return true;
        });
    }


    public void removeFilter(final FilterModel filterModel) {

        if (filterModel.getFilterId() != 0) {
            PublicMessageModel.removeMessageWithFilter(filterModel);

            APIFilter.removeFilter(filterModel.getFilterId(), filterModelEvent -> {
                filterModel.remove();
                this.onRemoveFilterSucceed.dispatch(null);
            }, requestFail -> {
                this.onRemoveFilterFailed.dispatch(requestFail.errorResponse);
                return true;
            });
        }
    }


    @Nullable
    private FilterModel updateOfInsertFilter(FilterModel filterModel) {
        if (null == filterModel) {
            return null;
        } else {
            filterModel.insert();
            return filterModel;
        }
    }

    private static class APIFilter {
        //
        //MARK: Filter
        //

        public static void addFilter(String name, boolean isHighlight, final IHandler<FilterModel> success, final IHandlerWithReturn<RequestFail> fail) {
            final HashMap<String, Object> filter = new HashMap<>();
            filter.put("name", name);
            filter.put("is_highlight", isHighlight);
            final HashMap<String, Object> disc = new HashMap<>();
            disc.put("user_filter", filter);

            ApiManager.run(ApiManager.getApi().addFilter(disc), success, fail);
        }

        public static void updateFilter(int filterId, String name, boolean isHighlight, final IHandler<FilterModel> success, final IHandlerWithReturn<RequestFail> fail) {
            final HashMap<String, Object> filter = new HashMap<>();
            filter.put("name", name);
            filter.put("is_highlight", isHighlight);
            final HashMap<String, Object> disc = new HashMap<>();
            disc.put("id", filterId);
            disc.put("user_filter", filter);

            ApiManager.run(ApiManager.getApi().updateFilter(filterId, disc), success, fail);
        }

        public static void removeFilter(int filterId, final IHandler<EmptyModel> success, final IHandlerWithReturn<RequestFail> fail) {
            final HashMap<String, Object> disc = new HashMap<>();
            disc.put("id", filterId);

            ApiManager.run(ApiManager.getApi().removeFilter(filterId), success, fail);
        }
    }

}
