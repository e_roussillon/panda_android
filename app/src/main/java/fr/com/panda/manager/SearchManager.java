package fr.com.panda.manager;

import java.util.HashMap;

import fr.com.panda.event.Event;
import fr.com.panda.event.interfaces.IHandler;
import fr.com.panda.event.interfaces.IHandlerWithReturn;
import fr.com.panda.http.ApiManager;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.model.Paginated;
import fr.com.panda.model.RequestFail;
import fr.com.panda.model.SearchModel;
import fr.com.panda.model.UserModel;

public class SearchManager {
    public final static SearchManager sharedInstance = new SearchManager();

    public Event<SearchModel> onSearchSucceed = new Event();
    public Event<ErrorResponse> onSearchFailed = new Event();

    private SearchManager() {
    }

    public void searchPseudo(final String pseudo, int page) {
        APISearch.search(pseudo, page, paginated -> {
            this.onSearchSucceed.dispatch(new SearchModel(paginated, pseudo));
        }, error -> {
            this.onSearchFailed.dispatch(error.errorResponse);
            return true;
        });
    }

    private static class APISearch {

        public static void search(String pseudo, int page, final IHandler<Paginated<UserModel>> success, final IHandlerWithReturn<RequestFail> fail) {
            final HashMap<String, Object> disc = new HashMap<>();
            disc.put("pseudo", pseudo.toLowerCase());
            disc.put("page", page);

            ApiManager.run(ApiManager.getApi().search(disc), success, fail);
        }
    }
}
