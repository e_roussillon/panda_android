package fr.com.panda.http.util;

import java.io.IOException;

import fr.com.panda.model.RequestFail;

/**
 * Created by edouardroussillon on 1/16/17.
 */

public class HttpException extends IOException {

    private RequestFail requestFail = null;

    public HttpException(RequestFail requestFail) {
        this.requestFail = requestFail;
    }

    public RequestFail getRequestFail() {
        return requestFail;
    }
}
