package fr.com.panda.http;

import java.util.ArrayList;
import java.util.List;

import fr.com.panda.enums.ErrorType;
import fr.com.panda.enums.api.APIHandler;
import fr.com.panda.enums.api.AuthError;
import fr.com.panda.event.DisposableBag;
import fr.com.panda.event.Event;
import fr.com.panda.http.util.HttpException;
import fr.com.panda.http.util.HttpTask;
import fr.com.panda.model.LogoutReported;
import fr.com.panda.utils.BackgroundUtil;

public class ApiEventGlobal {
    public static final ApiEventGlobal sharedInstance = new ApiEventGlobal();

    public Event<Void> onGenericEventError = new Event();
    public Event<Void> onGenericEventForceLogout = new Event();
    public Event<LogoutReported> onGenericEventForceLogoutReported = new Event();

    private final DisposableBag bag = new DisposableBag();
    private final List<HttpTask> requestsQueue = new ArrayList();
    private boolean isRefreshingToken = false;

    @APIHandler.APIHandlerMode
    public int handlerError(HttpTask task, HttpException e) {

        if (null == task || null == e.getRequestFail() || null == e.getRequestFail().errorResponse || (e.getRequestFail().status != 401 && e.getRequestFail().status != 409)) {
            return APIHandler.NOT_CONSUMED;
        }

        if (e.getRequestFail().errorResponse.type == ErrorType.AUTH) {
            switch (AuthError.fromId(e.getRequestFail().errorResponse.code)) {
                case AuthError.USER_REPORTED:

                    if (e.getRequestFail().errorResponse.details.get("time") instanceof Integer) {
                        Integer time = (Integer) e.getRequestFail().errorResponse.details.get("time");
                        onGenericEventForceLogoutReported.dispatch(new LogoutReported(time, !task.isLogin()));
                    } else {
                        onGenericEventForceLogout.dispatch(null);
                    }
                    return APIHandler.CONSUMED;
                case AuthError.TOKEN_INVALID:

                    this.isRefreshingToken = true;
                    task.setKeepIntoStack(true);
                    this.addTask(task);
                    return APIHandler.RENEW_TOKEN;
                case AuthError.TOKEN_NOT_FOUND:

                    onGenericEventForceLogout.dispatch(null);
                    return APIHandler.CONSUMED;
                case AuthError.USER_DUPLICATED:

                    onGenericEventForceLogout.dispatch(null);
                    return APIHandler.CONSUMED;
                default:
                    return APIHandler.NOT_CONSUMED;
            }
        }

        return APIHandler.NOT_CONSUMED;
    }

    public void addTask(HttpTask task) {
        if (this.isRefreshingToken && !task.isRefreshToken()) {
            requestsQueue.add(task);
        } else {
            runTask(task);
        }
    }

    public void removeTask(HttpTask task) {
        if (!task.isKeepIntoStack()) {
            requestsQueue.remove(task);
        }
    }

    public void runNextInQueue() {
        if (requestsQueue.size() > 0) {
            runTask(requestsQueue.get(0));
        }
    }

    private void runTask(HttpTask task) {
        task.setKeepIntoStack(false);
        BackgroundUtil.API().execute(task);
    }

    public void onRefreshSessionSucceed() {
        this.isRefreshingToken = false;
        this.runNextInQueue();
    }

    public void onRefreshSessionFail() {
        this.isRefreshingToken = false;
        this.requestsQueue.clear();
        this.onGenericEventForceLogout.dispatch(null);
    }
}
