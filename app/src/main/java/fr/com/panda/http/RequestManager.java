package fr.com.panda.http;

import java.io.IOException;

import fr.com.panda.enums.api.APIHandler;
import fr.com.panda.event.interfaces.IHandler;
import fr.com.panda.event.interfaces.IHandlerWithReturn;
import fr.com.panda.http.util.HttpException;
import fr.com.panda.http.util.HttpTask;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.model.RequestFail;
import retrofit2.Call;
import retrofit2.Response;

class RequestManager {
    public static final RequestManager sharedInstance = new RequestManager();

    private ApiEventGlobal apiEventGlobal = ApiEventGlobal.sharedInstance;

    public <T> void runRequest(Call<T> call, IHandler<T> success, IHandlerWithReturn<RequestFail> fail) {

        final HttpTask task = new HttpTask<T>(call, success, fail) {
            @Override
            protected void doWork() {
                try {
                    Response<T> response = this.getApi().execute();
                    T result = response.body();
                    getSuccess().dispatch(result);
                } catch (HttpException e) {
                    final @APIHandler.APIHandlerMode int apiHandlerMode = apiEventGlobal.handlerError(this, e);

                    if (apiHandlerMode == APIHandler.CONSUMED) {
                        final RequestFail newRequestFail = new RequestFail(e.getRequestFail().errorResponse, e.getRequestFail().status, true);
                        if (!fail.onEvent(newRequestFail)) {
                            apiEventGlobal.onGenericEventError.dispatch(null);
                        }
                    } else if (apiHandlerMode == APIHandler.NOT_CONSUMED) {
                        final RequestFail newRequestFail = new RequestFail(e.getRequestFail().errorResponse, e.getRequestFail().status, false);
                        if (!fail.onEvent(newRequestFail)) {
                            apiEventGlobal.onGenericEventError.dispatch(null);
                        }
                    } else {
                        SessionManager.sharedInstance.refreshToken();
                    }
                } catch (IOException e) {
                    e.printStackTrace();

                    if (!fail.onEvent(new RequestFail(null, 500, false))) {
                        apiEventGlobal.onGenericEventError.dispatch(null);
                    }
                } finally {
                    apiEventGlobal.removeTask(this);
                }
            }
        };

        apiEventGlobal.addTask(task);
    }


}
