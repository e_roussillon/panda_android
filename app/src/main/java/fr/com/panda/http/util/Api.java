package fr.com.panda.http.util;

import java.util.HashMap;

import fr.com.panda.model.BackupModel;
import fr.com.panda.model.EmptyModel;
import fr.com.panda.model.FilterModel;
import fr.com.panda.model.FindRoomsModel;
import fr.com.panda.model.Paginated;
import fr.com.panda.model.TokenModel;
import fr.com.panda.model.UserModel;
import fr.com.panda.utils.Constants;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by edouardroussillon on 1/15/17.
 */

public interface Api {

    //Login
    @POST("/api/v1/auth")
    Call<TokenModel> loginUser(@Header("Password") String password, @Body HashMap<String, Object> body);

    @POST("/api/v1/register")
    Call<TokenModel> registerUser(@Header("Password") String password, @Body HashMap<String, Object> body);

    //Logout
    @POST("/api/v1/logout")
    Call<EmptyModel> logout();

    //Renew Token
    @POST("/api/v1/auth/renew")
    Call<TokenModel> renewToken(@Header("authorization_refesh") String refreshToken);

    //Push Notification - Token
    @POST("/api/v1/push_token")
    Call<EmptyModel> pushToken(@Body HashMap<String, Object> body);

//    //Register
//    @POST("api/v1/register")
//    Observable<SessionVO> getGuest();
//

    @POST("api/v1/reset_password")
    Call<EmptyModel> resetPassword(@Body HashMap<String, Object> body);

    //Backup
    @GET("api/v1/auth/backup")
    Call<BackupModel> getBackup();

    //Backup pending
    @GET("api/v1/pending/backup")
    Call<BackupModel> getBackupPending();

    //User
    @GET("api/v1/users/{id}")
    Call<UserModel> getUserBy(@Path("id") int id);

    //Report User
    @POST("api/v1/report")
    Call<EmptyModel> reportUser(@Body HashMap<String, Object> body);

    //PublicRoom
    @POST("api/v1/find_room")
    Call<FindRoomsModel> findRoom(@Body HashMap<String, Object> body);

    //Confirm Message
    @POST("api/v1/confirmation/messages")
    Call<EmptyModel> confirmationMessages(@Body HashMap<String, Object> body);

    //add Filter
    @POST("/api/v1/filters")
    Call<FilterModel> addFilter(@Body HashMap<String, Object> body);

    @PUT("/api/v1/filters/{filterId}")
    Call<FilterModel> updateFilter(@Path("filterId") int filterId, @Body HashMap<String, Object> body);

    @DELETE("/api/v1/filters/{filterId}")
    Call<EmptyModel> removeFilter(@Path("filterId") int filterId);

    //Friendship
    @POST("api/v1/update_friendship")
    Call<UserModel> updateFriendship(@Body HashMap<String, Object> body);

    @POST("api/v1/find_friendships")
    Call<Paginated<UserModel>> search(@Body HashMap<String, Object> body);
//
//    //Update Image
//    @POST("api/v1/upload/image")
//    Observable<SessionVO> getGuest();
//
//    //Token
//    @POST("api/v1/auth/renew")
//    Observable<SessionVO> getGuest();
//
//    //PRIVATE Chat
//    @POST("api/chat/get_all_conversations")
//    Observable<SessionVO> getGuest();
//
//    @POST("api/chat/create_conversations")
//    Observable<SessionVO> getGuest();
//
//    //Friend USer
//    @POST("api/friend/users")
//    Observable<SessionVO> getGuest();
}