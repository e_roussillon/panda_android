package fr.com.panda.http.interceptor;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.io.IOException;

import fr.com.panda.http.util.HttpException;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.model.RequestFail;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import timber.log.Timber;

/**
 * Created by edouardroussillon on 1/16/17.
 */

public class ResponseInterceptor implements Interceptor {

    private Gson gson;

    @Override
    public Response intercept(Chain chain) throws IOException {
        final Request request = chain.request();
        final Response response = chain.proceed(request);

        final int code = response.code();

        if (code >= 500) {
            throw new HttpException(new RequestFail(null, code, false));
        }

        if (code < 200 || code > 299) {
            final ResponseBody body = response.body();

            try {
                final ErrorResponse errorResponse = gson.fromJson(body.charStream(), ErrorResponse.class);
                Timber.i("Error body: %s", errorResponse);
                throw new HttpException(new RequestFail(errorResponse, code, false));
            } catch (JsonParseException e) {
                throw new HttpException(new RequestFail(null, code, false));
            }
        }

        return response;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }
}
