package fr.com.panda.http.util;

import java.util.HashMap;

import fr.com.panda.event.interfaces.IHandler;
import fr.com.panda.event.interfaces.IHandlerWithReturn;
import fr.com.panda.http.ApiManager;
import fr.com.panda.model.RequestFail;
import needle.CancelableTask;
import retrofit2.Call;

public abstract class HttpTask<T> extends CancelableTask {

    private Call<T> apiOriginal;
    private IHandler<T> success;
    private IHandlerWithReturn<RequestFail> fail;
    private boolean keepIntoStack = false;

    public HttpTask(Call<T> api, IHandler<T> success, IHandlerWithReturn<RequestFail> fail) {
        this.apiOriginal = api;
        this.success = success;
        this.fail = fail;
    }

    public Call<T> getApiOriginal() {
        return apiOriginal;
    }

    public boolean isLogin() {
        return apiOriginal.request().url().url().getPath().equals(ApiManager.getApi().loginUser("", new HashMap<>()).request().url().url().getPath());
    }

    public boolean isRefreshToken() {
        return apiOriginal.request().url().url().getPath().equals(ApiManager.getApi().renewToken("").request().url().url().getPath());
    }

    public Call<T> getApi() {
        return apiOriginal.clone();
    }

    public IHandler<T> getSuccess() {
        return success;
    }

    public IHandlerWithReturn<RequestFail> getFail() {
        return fail;
    }

    public boolean isKeepIntoStack() {
        return keepIntoStack;
    }

    public void setKeepIntoStack(boolean keepIntoStack) {
        this.keepIntoStack = keepIntoStack;
    }
}
