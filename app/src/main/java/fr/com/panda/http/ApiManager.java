package fr.com.panda.http;

import java.util.concurrent.TimeUnit;

import fr.com.panda.BuildConfig;
import fr.com.panda.event.interfaces.IHandler;
import fr.com.panda.event.interfaces.IHandlerWithReturn;
import fr.com.panda.http.interceptor.RequestInterceptor;
import fr.com.panda.http.interceptor.ResponseInterceptor;
import fr.com.panda.http.util.Api;
import fr.com.panda.model.RequestFail;
import fr.com.panda.utils.Constants;
import fr.com.panda.utils.JsonUtil;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;
import static okhttp3.logging.HttpLoggingInterceptor.Level.NONE;

/**
 * Created by edouardroussillon on 1/16/17.
 */

public class ApiManager {
    private static RequestManager requestManager = RequestManager.sharedInstance;


    public static Api API = null;
    private static TimeUnit UNIT = TimeUnit.SECONDS;
    private static long TIMEOUT = Constants.API.kTimeoutSec;
    private static boolean LOGGING = BuildConfig.DEBUG;
    private static String BASE_URL = Constants.API.kAPI;

    public static Api getApi() {
        if (null == API) {
            API = initAPI(BASE_URL);
        }
        return API;
    }

    public static Api initAPI(String baseUrl) {
        final OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder().connectTimeout(TIMEOUT, UNIT);

        // Interceptors
        final RequestInterceptor requestInterceptor = new RequestInterceptor();
        final ResponseInterceptor responseInterceptor = new ResponseInterceptor();
        final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(message -> Timber.i(message)).setLevel(LOGGING ? BODY : NONE);

        okHttpBuilder.addInterceptor(requestInterceptor)
                     .addInterceptor(responseInterceptor)
                     .addInterceptor(loggingInterceptor);


        final OkHttpClient client = okHttpBuilder.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(JsonUtil.GSON))
                .client(client)
                .build();

        responseInterceptor.setGson(JsonUtil.GSON);

        return retrofit.create(Api.class);
    }

    public static <T> void run(Call<T> call, IHandler<T> success, IHandlerWithReturn<RequestFail> fail) {
        requestManager.runRequest(call, success, fail);
    }


}
