package fr.com.panda.http.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import fr.com.panda.utils.DateUtil;

public class DateDeserializer implements JsonDeserializer<Date> {

    private static final String[] DATE_FORMATS = new String[] {
            DateUtil.SMALL_DATE
    };

    @Override
    public Date deserialize(JsonElement jsonElement, Type typeOF,
                            JsonDeserializationContext context) throws JsonParseException {

        for (String format : DATE_FORMATS) {
            try {
                return new SimpleDateFormat(format).parse(jsonElement.getAsString());
            } catch (ParseException e) {
                //Check is number
                try {
                    return DateUtil.timestampToDate(jsonElement.getAsLong());
                } catch (UnsupportedOperationException ex) {
                    throw new JsonParseException("Unparseable date: \"" + jsonElement.getAsString()
                            + "\". Supported formats: " + Arrays.toString(DATE_FORMATS));
                }
            }
        }

        return null;
    }
}
