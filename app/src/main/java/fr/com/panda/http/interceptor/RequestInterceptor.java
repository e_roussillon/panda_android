package fr.com.panda.http.interceptor;

import java.io.IOException;

import fr.com.panda.manager.SessionManager;
import fr.com.panda.utils.Constants;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by edouardroussillon on 1/16/17.
 */

public class RequestInterceptor implements Interceptor {

    private SessionManager sessionManager = SessionManager.sharedInstance;

    @Override
    public Response intercept(Chain chain) throws IOException {
        final Request request = chain.request();
        final Request.Builder builder = request.newBuilder();

        builder.addHeader("Content-Type", Constants.HEADER.JSON);
        builder.addHeader("Accept-Language", Constants.HEADER.LANGUAGE);
        builder.addHeader("App-Id", Constants.HEADER.APP_ID);
        builder.addHeader("Device-Id", "");
        builder.addHeader("Accept-Encoding", Constants.HEADER.ENCODING);

        if (sessionManager.user().isLogin() == true) {
            builder.addHeader("authorization", "Bearer " + sessionManager.user().getToken());
        }



//        if (!TextUtils.isEmpty(token)) builder.addHeader("authorization", "Bearer " + token);
//        builder.addHeader("Agent", buildAgent());
//        builder.addHeader("Origin", origin.origin());
        return chain.proceed(builder.build());
    }
}
