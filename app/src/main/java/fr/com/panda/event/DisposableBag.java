package fr.com.panda.event;

import java.util.HashSet;
import java.util.Set;

import fr.com.panda.event.interfaces.IDisposable;

/**
 * Created by edouardroussillon on 12/26/16.
 */

public class DisposableBag {
    private Set<IDisposable> disposables = new HashSet();

    public void add(IDisposable disposable) {
        this.disposables.add(disposable);
    }

    public void dispose() {
        for (IDisposable item : disposables) {
            item.dispose();
        }
        this.disposables.clear();
    }
}
