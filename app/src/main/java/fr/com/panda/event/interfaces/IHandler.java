package fr.com.panda.event.interfaces;

/**
 * Created by edouardroussillon on 12/26/16.
 */

public interface IHandler<T> {
    void dispatch(T event);
}
