package fr.com.panda.event;

import java.util.HashSet;
import java.util.Set;

import fr.com.panda.event.interfaces.IDisposable;
import fr.com.panda.event.interfaces.IHandler;

/**
 * Created by edouardroussillon on 12/26/16.
 */

public class Event<T> {
    private Set<IHandler> listeners = new HashSet();
    private Set<IHandler> listenersOnce = new HashSet();

    public IDisposable add(IHandler<T> handler) {
        final Listener listener = new Listener(this, handler);
        listeners.add(listener);
        return listener;
    }

    public void addOnce(IHandler<T> handler) {
        final Listener listener = new Listener(this, handler);
        listenersOnce.add(listener);
    }

    public void dispatch(T event) {
        for (IHandler item : listeners) {
            item.dispatch(event);
        }

        for (IHandler item : listenersOnce) {
            item.dispatch(event);
        }

        listenersOnce.clear();
    }

    public Set<IHandler> getListeners() {
        return listeners;
    }

    public void setListeners(Set<IHandler> listeners) {
        this.listeners = listeners;
    }


    public void removeListener(Listener<T> listener) {
        listeners.remove(listener);
    }
}
