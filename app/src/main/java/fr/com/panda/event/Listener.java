package fr.com.panda.event;

import java.util.HashSet;
import java.util.Set;

import fr.com.panda.event.interfaces.IDisposable;
import fr.com.panda.event.interfaces.IHandler;

public class Listener<T> implements IHandler<T>, IDisposable {

    private IHandler handler;
    private Event<T> event;

    public Listener(Event<T> event, IHandler handler) {
        this.event = event;
        this.handler = handler;
    }

    @Override
    public void dispatch(T event) {
        this.handler.dispatch(event);
    }

    @Override
    public void dispose() {
        event.removeListener(this);
        event = null;
        handler = null;
    }
}
