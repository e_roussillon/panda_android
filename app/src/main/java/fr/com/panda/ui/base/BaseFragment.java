package fr.com.panda.ui.base;

import android.content.DialogInterface;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;

import fr.com.panda.R;
import fr.com.panda.model.UserModel;

/**
 * Created by edouardroussillon on 12/26/16.
 */

public abstract class BaseFragment extends Fragment {

    protected BaseActivity getBaseActivity() {
        if (this.getActivity() != null && this.getActivity() instanceof BaseActivity) {
            return (BaseActivity) this.getActivity();
        }
        return null;
    }

    //
    // Launcher
    //

    public void launchChat(UserModel userModel){
        final BaseActivity baseActivity = this.getBaseActivity();
        if (baseActivity != null) {
            baseActivity.launchChat(userModel);
        }
    }

    public void launchProfile(UserModel userModel){
        final BaseActivity baseActivity = this.getBaseActivity();
        if (baseActivity != null) {
            baseActivity.launchProfile(userModel);
        }
    }

    public void launchSplash() {
        final BaseActivity baseActivity = this.getBaseActivity();
        if (baseActivity != null) {
            baseActivity.launchSplash();
        }
    }

    //
    // Dialog
    //

    public void showDialogAlert(@StringRes int message) {
        final BaseActivity baseActivity = this.getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showDialogAlert(message);
        }
    }

    public void showDialogAlert(@StringRes int message, @StringRes int positiveMessage, final DialogInterface.OnClickListener positiveListener) {
        final BaseActivity baseActivity = this.getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showDialogAlert(message, positiveMessage, positiveListener);
        }
    }

    public void showDialogAlert(String message) {
        final BaseActivity baseActivity = this.getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showDialogAlert(message);
        }
    }

    public void showDialogAlert(String message, @StringRes int positiveMessage, final DialogInterface.OnClickListener positiveListener) {
        final BaseActivity baseActivity = this.getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showDialogAlert(message, positiveMessage, positiveListener);
        }
    }

    public void showDialogMessage(@StringRes int message) {
        final BaseActivity baseActivity = this.getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showDialogMessage(message);
        }
    }

    public void showDialogMessage(@StringRes int title, @StringRes int message) {
        final BaseActivity baseActivity = this.getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showDialogMessage(title, message);
        }
    }

    public void showDialogMessage(String message) {
        final BaseActivity baseActivity = this.getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showDialogMessage(message);
        }
    }

    public void showDialogMessage(String title, String message) {
        final BaseActivity baseActivity = this.getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showDialogMessage(title, message);
        }
    }

    //
    // Loader
    //

    public void showLoader() {
        final BaseActivity baseActivity = this.getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showLoader();
        }
    }

    public void removeLoader() {
        final BaseActivity baseActivity = this.getBaseActivity();
        if (baseActivity != null) {
            baseActivity.removeLoader();
        }
    }
}
