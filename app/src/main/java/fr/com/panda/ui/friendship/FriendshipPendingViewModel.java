package fr.com.panda.ui.friendship;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.com.panda.R;
import fr.com.panda.databinding.FriendshipItemAdapterBinding;
import fr.com.panda.databinding.FriendshipPendingViewGroupBinding;
import fr.com.panda.enums.FriendshipTabType;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.manager.FriendshipManager;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.model.UserModel;
import fr.com.panda.ui.base.util.EmptyStateViewModel;
import fr.com.panda.utils.BackgroundUtil;

/**
 * @author Edouard Roussillon
 */

public class FriendshipPendingViewModel extends FriendshipViewModel implements FriendshipViewModel.FriendshipItemClicked {

    private TabFriendshipFragment fragment;
    private FriendshipPendingViewGroupBinding binding;
    private FriendshipRecyclerAdapter friendshipRecyclerAdapter;
    private FriendshipManager friendshipManager = FriendshipManager.sharedInstance;

    public FriendshipPendingViewModel(TabFriendshipFragment fragment, ViewGroup viewGroup) {
        super(FriendshipTabType.PENDING, R.drawable.ic_dislike_big, R.string.friend_empty_state_pending);

        LayoutInflater inflater = (LayoutInflater) fragment.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.fragment = fragment;
        this.binding = FriendshipPendingViewGroupBinding.inflate(inflater, viewGroup, true);
    }

    @Override
    public void bind() {
        super.bind();

        this.friendshipRecyclerAdapter = new FriendshipRecyclerAdapter(this, this.friendshipTabMode);

        this.binding.friendshipPendingRecycler.setLayoutManager(new LinearLayoutManager(this.binding.getRoot().getContext()));
        this.binding.friendshipPendingRecycler.setAdapter(this.friendshipRecyclerAdapter);
        this.binding.setViewModel(this);
    }

    @Override
    public void start() {
        super.start();

        this.reloadList();
        this.getBag().add(friendshipManager.onUpdateFriendshipSucceed.add(this::onUpdateFriendshipSucceed));
        this.getBag().add(friendshipManager.onUpdateFriendshipFailed.add(this::onUpdateFriendshipFailed));
        this.getBag().add(friendshipManager.onUpdateFriendshipFailedUserIdNotFound.add(this::onUpdateFriendshipFailedUserIdNotFound));
    }

    @Override
    public void unbind() {
        super.unbind();

        this.fragment = null;
        this.binding = null;
        this.friendshipRecyclerAdapter = null;
    }

    @Override
    public View getRoot() {
        return binding.getRoot();
    }

    @Override
    public void reloadList() {
        this.friendshipRecyclerAdapter.reloadList();
        this.emptyStateViewModel.setVisibility(this.friendshipRecyclerAdapter.showEmptyState());
    }

    //
    // Hanlder
    //

    private void onUpdateFriendshipSucceed(UserModel userModel) {
        BackgroundUtil.UI().execute(() -> {
            if (this.fragment != null) {
                this.fragment.removeLoader();
                this.reloadList();
            }
        });
    }

    private void onUpdateFriendshipFailed(ErrorResponse errorResponse) {
        BackgroundUtil.UI().execute(() -> {
            if (this.fragment != null) {
                this.fragment.removeLoader();
            }
        });
    }

    private void onUpdateFriendshipFailedUserIdNotFound(ErrorResponse errorResponse) {
        BackgroundUtil.UI().execute(() -> {
            if (this.fragment != null) {
                this.fragment.removeLoader();
            }
        });
    }


    //
    // FriendshipViewModel.FriendshipItemClicked
    //

    @Override
    public void launchChat(UserModel userModel) {
        if (this.fragment != null) {
            this.fragment.launchChat(userModel);
        }
    }

    @Override
    public void launchProfile(UserModel userModel) {
        if (this.fragment != null) {
            this.fragment.launchProfile(userModel);
        }
    }

    @Override
    public void updateFriendship(int userId, @FriendshipType.FriendshipMode int friendshipType) {
        if (this.fragment != null) {
            this.fragment.showLoader();

            BackgroundUtil.API().execute(() -> {
                friendshipManager.updateFriendship(userId, friendshipType);
            });
        }
    }
}