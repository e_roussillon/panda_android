package fr.com.panda.ui.friendship;

import android.support.annotation.DrawableRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringRes;
import android.view.View;

import fr.com.panda.R;
import fr.com.panda.databinding.FriendshipItemAdapterBinding;
import fr.com.panda.enums.FriendshipTabType;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.manager.FriendshipManager;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.model.UserModel;
import fr.com.panda.ui.base.ViewModel;
import fr.com.panda.ui.base.util.EmptyStateViewModel;

/**
 * @author Edouard Roussillon
 */

public class FriendshipViewModel extends ViewModel {

    protected @FriendshipTabType.FriendshipTabMode int friendshipTabMode;
    private UserModel userModel;
    private Boolean isSearch = false;
    private FriendshipItemClicked friendshipItemClicked;
    protected EmptyStateViewModel emptyStateViewModel;
    private SessionManager sessionManager = SessionManager.sharedInstance;

    public FriendshipViewModel(@FriendshipTabType.FriendshipTabMode int friendshipTabMode, @DrawableRes int emptyStateImage, @StringRes int emptyStateMessage) {
        this.friendshipTabMode = friendshipTabMode;
        this.emptyStateViewModel = new EmptyStateViewModel(emptyStateImage, emptyStateMessage);
    }

    public FriendshipViewModel(UserModel userModel, FriendshipItemClicked friendshipItemClicked) {
        this.userModel = userModel;
        this.friendshipItemClicked = friendshipItemClicked;
    }

    public FriendshipViewModel(FriendshipItemClicked friendshipItemClicked, UserModel userModel, Boolean isSearch) {
        this.userModel = userModel;
        this.isSearch = isSearch;
        this.friendshipItemClicked = friendshipItemClicked;
    }

    public void reloadList() {

    }

    @Override
    public View getRoot() {
        return null;
    }

    @Override
    public void unbind() {
        super.unbind();

        this.emptyStateViewModel = null;
    }

    //
    // Bind
    //

    public String getPseudo() {
        return userModel.pseudo;
    }

    public Boolean showLastSeen() { return userModel.friendshipStatus == FriendshipType.FRIEND; }

    public String getLastSeen() {
        return "need to set time";
    }

    public String getBirthday(){
        return "need to set birthday";
    }

    public Boolean isFriend() {
        return userModel.friendshipStatus == FriendshipType.FRIEND || this.isSearch;
    }

    public Boolean isNotFriend() {
        return !this.isSearch && userModel.friendshipStatus != FriendshipType.FRIEND;
    }

    public Boolean isPending() {
        return userModel.friendshipStatus == FriendshipType.PENDING;
    }

    public Boolean notAskedFriendShip() { return userModel.friendshipStatus == FriendshipType.PENDING && userModel.friendshipLastAction != sessionManager.user().getUserId(); }

    public int getTitleButtonLight() {
        return this.isPending() ? R.string.friendship_action_accept_title : R.string.friendship_action_unblock_title;
    }

    public int getTitleButtonDark() {
        return this.notAskedFriendShip() ? R.string.friendship_action_refuse_title : R.string.friendship_action_cancel_title;
    }

    public View.OnClickListener onClickMessage = v -> {
        if (this.friendshipItemClicked != null) {
            this.friendshipItemClicked.launchChat(userModel);
        }
    };

    public View.OnClickListener onClickCell = v -> {
        if (this.friendshipItemClicked != null) {
            this.friendshipItemClicked.launchProfile(userModel);
        }
    };

    public View.OnClickListener onClickAccept = v -> {
        if (this.friendshipItemClicked != null) {
            this.friendshipItemClicked.updateFriendship(userModel.userId, FriendshipType.FRIEND);
        }
    };

    public View.OnClickListener onClickRefuse = v -> {
        if (this.friendshipItemClicked != null) {
            if (this.notAskedFriendShip()) {
                this.friendshipItemClicked.updateFriendship(userModel.userId, FriendshipType.REFUSED_BEFORE_BE_FRIEND);
            } else {
                this.friendshipItemClicked.updateFriendship(userModel.userId, FriendshipType.CANCEL_REQUEST);
            }
        }
    };

    public EmptyStateViewModel getEmptyStateViewModel() {
        return emptyStateViewModel;
    }

    public interface FriendshipItemClicked {
        void launchChat(UserModel userModel);
        void launchProfile(UserModel userModel);
        void updateFriendship(int userId, @FriendshipType.FriendshipMode int friendshipType);
    }



}
