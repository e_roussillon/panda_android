package fr.com.panda.ui.profile.adapter;


import fr.com.panda.databinding.FlyingProfileItemBinding;
import fr.com.panda.ui.base.adapter.BaseItemViewHolder;

/**
 * @author Edouard Roussillon
 */

public class FlyingProfileItemHolder extends BaseItemViewHolder {

    private FlyingProfileItemBinding flyingProfileItemBinding;

    public FlyingProfileItemHolder(FlyingProfileItemBinding flyingProfileItemBinding) {
        super(flyingProfileItemBinding.getRoot());

        this.flyingProfileItemBinding = flyingProfileItemBinding;
    }

    @Override
    public void bind(FlyingItemViewModel flyingItemViewModel) {
        super.bind(flyingItemViewModel);

        this.flyingProfileItemBinding.setViewModel(flyingItemViewModel.getFlyingProfileItemViewModel());
    }
}
