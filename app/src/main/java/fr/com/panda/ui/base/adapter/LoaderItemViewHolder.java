package fr.com.panda.ui.base.adapter;

import fr.com.panda.databinding.ViewItemLoaderBinding;
import fr.com.panda.ui.base.adapter.BaseItemViewHolder;
import fr.com.panda.ui.search.SearchItemViewModel;

/**
 * @author Edouard Roussillon
 */

public class LoaderItemViewHolder extends BaseItemViewHolder {

    private ViewItemLoaderBinding viewLoaderBinding;

    public LoaderItemViewHolder(ViewItemLoaderBinding viewLoaderBinding) {
        super(viewLoaderBinding.getRoot());
        this.viewLoaderBinding = viewLoaderBinding;
    }

    @Override
    public void bind(SearchItemViewModel searchItemViewModel) {}
}
