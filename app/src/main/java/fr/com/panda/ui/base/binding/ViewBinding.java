package fr.com.panda.ui.base.binding;

import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.graphics.Typeface;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

/**
 * @author Edouard Roussillon
 */

public class ViewBinding {

    @BindingAdapter("visible")
    public static void setVisible(View v, Boolean showView) {
        v.setVisibility(showView ? View.VISIBLE : View.GONE);
    }





}
