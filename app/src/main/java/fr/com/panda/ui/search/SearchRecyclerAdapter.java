package fr.com.panda.ui.search;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import fr.com.panda.R;
import fr.com.panda.databinding.FriendshipItemAdapterBinding;
import fr.com.panda.databinding.ViewHeaderBinding;
import fr.com.panda.databinding.ViewItemLoaderBinding;
import fr.com.panda.ui.base.adapter.BaseItemViewHolder;
import fr.com.panda.ui.base.adapter.HeaderItemViewHolder;
import fr.com.panda.ui.base.adapter.LoaderItemViewHolder;
import fr.com.panda.ui.friendship.FriendshipItemViewHolder;

/**
 * @author Edouard Roussillon
 */

public class SearchRecyclerAdapter extends RecyclerView.Adapter<BaseItemViewHolder> {

    private List<SearchItemViewModel> list = new ArrayList();
    private LayoutInflater layoutInflater;

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addNewItems(List<SearchItemViewModel> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addLoader() {
        if (this.list.size() > 0 && this.list.get(this.list.size() - 1).isLoader()) {
            return;
        }
        this.list.add(new SearchItemViewModel());
        notifyItemInserted(this.list.size() -1);
    }

    public void removeLoader() {
        if (this.list.size() > 0 && this.list.get(this.list.size() - 1).isLoader()) {
            this.list.remove(this.list.size() - 1);
            notifyItemRemoved(this.list.size() -1);
        }
    }

    public void replaceItem(List<SearchItemViewModel> list) {
        if (null == list) {
            this.list = new ArrayList();
        } else {
            this.list = list;
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {

        final SearchItemViewModel item = list.get(position);

        if (item.isLoader()) return R.layout.view_item_loader;

        return item.isSection() ? R.layout.view_header : R.layout.friendship_item_adapter;
    }

    @Override
    public BaseItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == layoutInflater) layoutInflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case R.layout.view_item_loader:
                return new LoaderItemViewHolder(ViewItemLoaderBinding.inflate(layoutInflater, parent, false));
            case R.layout.view_header:
                return new HeaderItemViewHolder(ViewHeaderBinding.inflate(layoutInflater, parent, false));
            default:
                return new FriendshipItemViewHolder(FriendshipItemAdapterBinding.inflate(layoutInflater, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(BaseItemViewHolder holder, int position) {
        holder.bind(list.get(position));
    }
}
