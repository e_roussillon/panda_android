package fr.com.panda.ui.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author Edouard Roussillon
 */

public abstract class ViewModelFragment<VM extends ViewModel, B extends ViewDataBinding> extends BaseFragment {

    private String STATE_VIEW_MODEL = "state.VIEW_MODEL_FRAGMENT";
    protected VM viewModel;

    protected B binding;

    protected abstract VM onCreateViewModel(B binding);

    protected abstract @LayoutRes int getLayout();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.binding = DataBindingUtil.inflate(inflater, getLayout(), container, false);
        this.viewModel = onCreateViewModel(this.binding);

        this.onBindViewModel();
        if (savedInstanceState != null) {
            this.onRestoreInstanceStateViewModel(savedInstanceState);
        }

        return this.binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();

        this.onStartViewModel();
    }

    @Override
    public void onResume() {
        super.onResume();

        this.onResumeViewModel();
    }

    @Override
    public void onStop() {
        super.onStop();

        this.onStopViewModel();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        this.onSaveInstanceStateViewModel(outState);
    }

    @CallSuper
    @Override
    public void onDestroy() {
        super.onDestroy();

        this.onUnbindViewModel();
    }

    //
    // ViewModel Lifecycle
    //

    @CallSuper
    protected void onBindViewModel() {
        this.viewModel.bind();
    }

    @CallSuper
    protected void onStartViewModel() { this.viewModel.start(); }

    @CallSuper
    protected void onResumeViewModel() { this.viewModel.resume(); }

    @CallSuper
    protected void onRestoreInstanceStateViewModel(Bundle savedInstanceState) { this.viewModel.restoreInstance(savedInstanceState.getParcelable(STATE_VIEW_MODEL)); }

    @CallSuper
    protected void onStopViewModel() { this.viewModel.stop(); }

    @CallSuper
    protected void onSaveInstanceStateViewModel(Bundle outState) { outState.putParcelable(STATE_VIEW_MODEL, this.viewModel.saveInstanceState()); }

    @CallSuper
    protected void onUnbindViewModel() {
        this.viewModel.unbind();
    }

}