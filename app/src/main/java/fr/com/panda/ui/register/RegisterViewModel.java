package fr.com.panda.ui.register;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.databinding.Bindable;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.DatePicker;

import com.codetroopers.betterpickers.OnDialogDismissListener;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import fr.com.panda.BR;
import fr.com.panda.R;
import fr.com.panda.databinding.RegisterActivityBinding;
import fr.com.panda.enums.ErrorType;
import fr.com.panda.enums.FieldErrorType;
import fr.com.panda.enums.RegisterErrorType;
import fr.com.panda.enums.ValidatorType;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.model.FieldModel;
import fr.com.panda.ui.base.ViewModel;
import fr.com.panda.utils.BackgroundUtil;
import fr.com.panda.utils.DateUtil;
import fr.com.panda.utils.ValidatorUtil;

/**
 * @author Edouard Roussillon
 */

public class RegisterViewModel extends ViewModel {

    private RegisterActivity registerActivity;
    private RegisterActivityBinding binding;

    public RegisterViewModel(RegisterActivity registerActivity, RegisterActivityBinding binding) {
        this.registerActivity = registerActivity;
        this.binding = binding;
    }

    private CharSequence editTextPseudo = "";
    private Date editTextBirthday = null;
    private CharSequence editTextEmail = "";
    private CharSequence editTextPassword = "";

    @Bindable
    public CharSequence getEditTextPseudo() {
        return editTextPseudo;
    }

    public void setEditTextPseudo(CharSequence editTextPseudo) {
        this.editTextPseudo = editTextPseudo;
    }

    @Bindable
    public CharSequence getEditTextBirthday() {
        return editTextBirthday == null ? "" : DateUtil.getMediumStyleDate(editTextBirthday);
    }

    public void setEditTextBirthday(Date date) {
        this.editTextBirthday = date;
        notifyPropertyChanged(BR.editTextBirthday);
    }

    @Bindable
    public CharSequence getEditTextEmail() {
        return editTextEmail;
    }

    public void setEditTextEmail(CharSequence editTextEmail) {
        this.editTextEmail = editTextEmail;
    }

    @Bindable
    public CharSequence getEditTextPassword() {
        return editTextPassword;
    }

    public void setEditTextPassword(CharSequence editTextPassword) {
        this.editTextPassword = editTextPassword;
    }

    //
    // Bind
    //

    @Override
    public View getRoot() {
        return this.binding.getRoot();
    }

    @Override
    public void start() {
        super.start();

        this.getBag().add(this.getSessionManager().onGetSessionSucceed.add(this::onGetSessionSucceed));
        this.getBag().add(this.getSessionManager().onGetSessionFailed.add(this::onGetSessionFailed));
    }

    @Override
    public void restoreInstance(ViewModel.State state) {
        super.restoreInstance(state);

        if (state instanceof State) {
            State stateRegister = (State) state;
            this.editTextPseudo = stateRegister.pseudo;
            this.editTextBirthday = stateRegister.birthday;
            this.editTextEmail = stateRegister.email;
            this.editTextPassword = stateRegister.password;
        }
    }

    @Override
    public ViewModel.State saveInstanceState() {
        return new State(this);
    }

    @Override
    public void unbind() {
        super.unbind();

        this.registerActivity = null;
        this.binding = null;
    }

    //
    // Valid
    //

    private boolean isValid() {
        if (this.binding == null) {
            return false;
        }

        boolean isValid = ValidatorUtil.isFieldValid(this.binding.inputLayoutPseudo, ValidatorType.IS_NOT_EMPTY);
        isValid = ValidatorUtil.isFieldValid(this.binding.inputLayoutBirthday, ValidatorType.IS_NOT_EMPTY) && isValid;
        isValid = ValidatorUtil.isFieldValid(this.binding.inputLayoutEmail, ValidatorType.IS_NOT_EMPTY) && isValid;
        return ValidatorUtil.isFieldValid(this.binding.inputLayoutPassword, ValidatorType.IS_NOT_EMPTY) && isValid;
    }

    //
    // Click
    //

    public View.OnClickListener onClickDate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (registerActivity == null)
                return;

            registerActivity.hideKeyboard();

            if (editTextBirthday == null) {
                editTextBirthday = new Date();
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(editTextBirthday);

            CalendarDatePickerDialogFragment dialog = new CalendarDatePickerDialogFragment();
            dialog.setOnDateSetListener(dateSetListener);
            dialog.setThemeCustom(R.style.PickersDialogs);
            dialog.setPreselectedDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            dialog.show(registerActivity.getSupportFragmentManager(), "DATE_PICKER_TAG");
        }
    };

    public View.OnClickListener onClickRegister = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (registerActivity == null)
                return;

            registerActivity.hideKeyboard();

            if (isValid()) {
                getSessionManager().register(editTextPseudo.toString(), editTextEmail.toString(), editTextBirthday, editTextPassword.toString());
            }
        }
    };

    public View.OnClickListener onClickLogin = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (registerActivity == null)
                return;

            registerActivity.hideKeyboard();
            registerActivity.launchLogin();
        }
    };

    public View.OnClickListener onClickInfo = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (registerActivity == null)
                return;

            registerActivity.hideKeyboard();
            registerActivity.showDialogMessage(R.string.register_screen_alert_title_birthday, R.string.register_screen_alert_message_birthday);
        }
    };

    //
    // Date
    //

    private CalendarDatePickerDialogFragment.OnDateSetListener dateSetListener = (dialog, year, monthOfYear, dayOfMonth) -> {
        // Set date from user input.
        Calendar date = Calendar.getInstance();
        date.set(Calendar.YEAR, year);
        date.set(Calendar.MONTH, monthOfYear);
        date.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        this.setEditTextBirthday(date.getTime());
    };

    //
    // Handler
    //

    private void onGetSessionSucceed(Void v) {
        BackgroundUtil.UI().execute(() -> {
            if (this.registerActivity != null) {
                this.registerActivity.removeLoader();
                this.registerActivity.launchSplash();
            }
        });
    }

    private void onGetSessionFailed(ErrorResponse response) {
        BackgroundUtil.UI().execute(() -> {
            if (this.registerActivity != null && this.binding != null) {
                this.registerActivity.removeLoader();
                this.registerActivity.animLogoError();

                if (response != null && response.fields != null && response.type == ErrorType.CHANGESET) {
                    List<FieldModel> fields = response.fields;

                    for (FieldModel item : fields) {
                        if (item.message != null && item.message.size() > 0) {
                            String name = item.field;
                            String msg = item.message.get(0);

                            switch (name) {
                                case RegisterErrorType.ERROR_PSEUDO:
                                    switch (msg) {
                                        case FieldErrorType.ERROR_BLANK:
                                            this.binding.inputPseudo.setError(this.registerActivity.getString(R.string.generic_error_empty_field));
                                            break;
                                        case FieldErrorType.ERROR_USED:
                                            this.binding.inputPseudo.setError(this.registerActivity.getString(R.string.register_screen_error_pseudo));
                                            break;
                                        case FieldErrorType.ERROR_MIN_4_CHARACTER:
                                        case FieldErrorType.ERROR_MAX_30_CHARACTER:
                                            this.binding.inputPseudo.setError(this.registerActivity.getString(R.string.register_screen_error_empty_pseudo_max_min));
                                            break;
                                        default:
                                            this.binding.inputPseudo.setError(this.registerActivity.getString(R.string.register_screen_error_pseudo_invalid));
                                            break;
                                    }
                                    break;
                                case RegisterErrorType.ERROR_EMAIL:
                                    switch (msg) {
                                        case FieldErrorType.ERROR_BLANK:
                                            this.binding.inputEmail.setError(this.registerActivity.getString(R.string.generic_error_empty_field));
                                            break;
                                        case FieldErrorType.ERROR_USED:
                                            this.binding.inputEmail.setError(this.registerActivity.getString(R.string.register_screen_error_email));
                                            break;
                                        default:
                                            this.binding.inputEmail.setError(this.registerActivity.getString(R.string.register_screen_error_email_invalid));
                                            break;
                                    }
                                    break;
                                case RegisterErrorType.ERROR_BIRTHDAY:
                                    this.registerActivity.showDialogAlert(R.string.register_screen_error_message_birthday);
                                    break;
                                case RegisterErrorType.ERROR_PASSWORD:
                                    switch (msg) {
                                        case FieldErrorType.ERROR_MIN_6_CHARACTER:
                                            this.binding.inputPassword.setError(this.registerActivity.getString(R.string.register_screen_error_password_to_small));
                                            break;
                                        default:
                                            this.binding.inputPassword.setError(this.registerActivity.getString(R.string.generic_error_empty_field));
                                            break;
                                    }
                            }
                            break;
                        }
                    }
                }
            }
        });
    }

    //
    // State
    //

    private static class State extends ViewModel.State {
        public static final Creator<State> CREATOR = new Creator<State>() {
            @Override
            public State createFromParcel(Parcel in) {
                return new State(in);
            }

            @Override
            public State[] newArray(int size) {
                return new State[size];
            }
        };


        @Nullable
        public CharSequence pseudo;
        @Nullable
        public CharSequence email;
        @Nullable
        public CharSequence password;
        @Nullable
        public Date birthday;

        public State(RegisterViewModel registerViewModel) {
            super(registerViewModel);
            this.pseudo = registerViewModel.editTextPseudo;
            this.email = registerViewModel.editTextEmail;
            this.password = registerViewModel.editTextPassword;
            this.birthday = registerViewModel.editTextBirthday;
        }

        public State(android.os.Parcel parcel) {
            super(parcel);
            if (parcel != null) {
                this.pseudo = parcel.readString();
                this.email = parcel.readString();
                this.password = parcel.readString();
                long birth = parcel.readLong();
                if (birth > 0) {
                    this.birthday = DateUtil.timestampToDate(birth);
                } else {
                    this.birthday = null;
                }
            }
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (dest != null) {
                dest.writeString(pseudo.toString());
                dest.writeString(email.toString());
                dest.writeString(password.toString());
                dest.writeLong(birthday == null ? 0L :  DateUtil.timestamp(birthday));
            }
        }
    }
}
