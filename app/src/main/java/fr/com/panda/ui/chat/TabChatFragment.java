package fr.com.panda.ui.chat;


import android.support.v4.app.Fragment;

import fr.com.panda.R;
import fr.com.panda.databinding.TabChatFragmentBinding;
import fr.com.panda.ui.base.ViewModelFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabChatFragment extends ViewModelFragment<TabChatViewModel, TabChatFragmentBinding> {

    public static TabChatFragment newInstance() {
        TabChatFragment f = new TabChatFragment();

        return f;
    }

    @Override
    protected TabChatViewModel onCreateViewModel(TabChatFragmentBinding binding) {
        return new TabChatViewModel(this, binding);
    }

    @Override
    protected int getLayout() {
        return R.layout.tab_chat_fragment;
    }

    @Override
    protected void onBindViewModel() {
        super.onBindViewModel();

        this.binding.setViewModel(this.viewModel);
    }
}
