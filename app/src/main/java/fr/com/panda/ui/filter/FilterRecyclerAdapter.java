package fr.com.panda.ui.filter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.com.panda.R;
import fr.com.panda.databinding.FilterItemAdapterBinding;
import fr.com.panda.model.FilterModel;

/**
 * @author Edouard Roussillon
 */

public class FilterRecyclerAdapter extends RecyclerView.Adapter<FilterRecyclerAdapter.FilterHolder> {

    private boolean isHighlighted;
    private FilterClick filterClick;
    private List<FilterModel> list = new ArrayList<>();
    private FilterModel itemAddFilter;

    public FilterRecyclerAdapter(Context context, FilterClick filterClick, boolean isHighlighted, boolean showDelete) {
        this.itemAddFilter = new FilterModel(-1, context.getString(R.string.filter_add_new_filter_title), isHighlighted, new Date());
        this.itemAddFilter.setFirst(true);

        this.isHighlighted = isHighlighted;
        this.filterClick = filterClick;
        this.reLoadList(showDelete);
    }

    public void reLoadList(boolean show) {
        List<FilterModel> tmpFilter = FilterModel.getFilters(this.isHighlighted);
        this.list = new ArrayList<>();
        if (!show) {
            this.list.add(0, this.itemAddFilter);
        }

        for (FilterModel item: tmpFilter) {
            item.setShowRemoveButton(show);
            this.list.add(item);
        }

        if (this.list.size() > 1) {
            this.list.get(this.list.size()-1).setLast(true);
        }

        this.notifyDataSetChanged();
    }

    @Override
    public FilterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        FilterItemAdapterBinding itemBinding = FilterItemAdapterBinding.inflate(layoutInflater, parent, false);
        return new FilterHolder(itemBinding, this.filterClick);
    }

    @Override
    public void onBindViewHolder(FilterHolder holder, int position) {
        FilterModel item = this.list.get(position);
        item.setBindingPosition(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }

    public boolean showEmptyState() {
        return this.list.size() == 1;
    }

    public boolean showEmptyState(boolean show) {
        if (show) {
            return this.list.size() == 0;
        } else {
            return this.showEmptyState();
        }
    }

    public static class FilterHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final FilterItemAdapterBinding binding;
        private FilterClick filterClick;

        public FilterHolder(FilterItemAdapterBinding binding, FilterClick filterClick) {
            super(binding.getRoot());
            this.binding = binding;
            this.filterClick = filterClick;
        }

        public void bind(FilterModel item) {
            binding.setViewModel(item);
            binding.getRoot().setOnClickListener(this);
            binding.buttonRemove.setOnClickListener(this);
            binding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {
            if (binding.getViewModel().isShowRemoveButton()) {
                filterClick.onRemoveFilter(this.binding);
            } else {
                filterClick.onClickFilter(this.binding);
            }
        }
    }

    public interface FilterClick {
        void onClickFilter(FilterItemAdapterBinding binding);
        void onRemoveFilter(FilterItemAdapterBinding binding);
    }
}
