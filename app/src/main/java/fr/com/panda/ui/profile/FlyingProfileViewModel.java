package fr.com.panda.ui.profile;

import android.content.DialogInterface;
import android.databinding.Bindable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import fr.com.panda.R;
import fr.com.panda.databinding.FlyingProfileActivityBinding;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.manager.FriendshipManager;
import fr.com.panda.manager.UserManager;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.model.UserModel;
import fr.com.panda.ui.base.ViewModel;
import fr.com.panda.ui.profile.adapter.FlyingActionItemViewModel;
import fr.com.panda.utils.BackgroundUtil;

/**
 * @author Edouard Roussillon
 */

public class FlyingProfileViewModel extends ViewModel implements FlyingActionItemViewModel.FlyingAction {

    private UserManager userManager = UserManager.sharedInstance;
    private FriendshipManager friendshipManager = FriendshipManager.sharedInstance;
    private FlyingProfileActivity flyingProfileActivity;
    private FlyingProfileActivityBinding flyingProfileActivityBinding;
    private FlyingProfileAdapter flyingProfileAdapter;
    private UserModel userModel;

    public FlyingProfileViewModel(FlyingProfileActivity flyingProfileActivity, FlyingProfileActivityBinding flyingProfileActivityBinding) {
        this.flyingProfileActivity = flyingProfileActivity;
        this.flyingProfileActivityBinding = flyingProfileActivityBinding;
    }

    @Override
    public void bind() {
        super.bind();

        this.getBag().add(userManager.onGetUserSucceed.add(this::onGetUserSucceed));
        this.getBag().add(userManager.onGetUserFailed.add(this::onGetUserFailed));

        this.getBag().add(userManager.onReportUserSucceed.add(this::onReportUserSucceed));
        this.getBag().add(userManager.onReportUserFailed.add(this::onReportUserFailed));

        this.getBag().add(friendshipManager.onUpdateFriendshipSucceed.add(this::onUpdateFriendshipSucceed));
        this.getBag().add(friendshipManager.onUpdateFriendshipFailed.add(this::onUpdateFriendshipFailed));
        this.getBag().add(friendshipManager.onUpdateFriendshipFailedUserIdNotFound.add(this::onUpdateFriendshipFailedUserIdNotFound));

        this.flyingProfileActivityBinding.profileRecycler.setLayoutManager(new LinearLayoutManager(this.flyingProfileActivity));
    }

    @Override
    public void unbind() {
        super.unbind();

        this.flyingProfileActivity = null;
        this.flyingProfileActivityBinding = null;
        this.userModel = null;
    }

    @Override
    public View getRoot() {
        return null;
    }

    public void updateUser(UserModel userModel) {
        this.userModel = userModel;
        this.appTitle();

        final UserModel user = UserModel.getUser(userModel.userId);
        this.userModel = user;

        if (user != null) {
            this.updateAdapter();
        } else {
            this.flyingProfileActivity.showLoader();
            this.userManager.getUserBy(userModel.userId);
        }
    }

    private void appTitle() {
        this.flyingProfileActivityBinding.chatToolbar.setTitle(this.userModel.pseudo);
    }

    //
    // FlyingActionItemViewModel.FlyingAction
    //

    @Override
    public void updateFriendship(@FriendshipType.FriendshipMode int friendship) {
        if (null != this.flyingProfileActivity) {
            switch (friendship) {
                case FriendshipType.BLOCKED_AFTER_BE_FRIEND:
                case FriendshipType.BLOCKED_BEFORE_BE_FRIEND:
                    this.flyingProfileActivity.showDialogMessage(R.string.dialog_block_user_title, R.string.dialog_block_user_message,
                            (dialog, positiveWhich) -> {
                                uploadFriendship(userModel.userId, friendship);
                            },
                            (dialog, negativeWhich) -> {
                                dialog.dismiss();
                            });
                    break;
                case FriendshipType.REPORTED:
                    this.flyingProfileActivity.showDialogMessage(R.string.dialog_report_user_title, R.string.dialog_report_user_message,
                            (dialog, positiveWhich) -> {
                                userReported(userModel.userId);
                            },
                            (dialog, negativeWhich) -> {
                                dialog.dismiss();
                            });
                    break;
                default:
                    uploadFriendship(userModel.userId, friendship);
            }
        }
    }

    private void uploadFriendship(int userId, @FriendshipType.FriendshipMode int friendship) {
        if (null != this.flyingProfileActivity) {
            this.flyingProfileActivity.showLoader();
            this.friendshipManager.updateFriendship(userId, friendship);
        }
    }

    private void userReported(int userId) {
        if (null != this.flyingProfileActivity) {
            this.flyingProfileActivity.showLoader();
            this.userManager.reportUser(userId);
        }
    }

    //
    // Handler GetUser
    //

    private void onGetUserSucceed(UserModel userModel) {
        BackgroundUtil.UI().execute(() -> {
            this.flyingProfileActivity.removeLoader();
            this.userModel = userModel;
            this.updateAdapter();
        });
    }

    private void onGetUserFailed(ErrorResponse errorResponse) {
        BackgroundUtil.UI().execute(() -> {
            this.flyingProfileActivity.removeLoader();
        });
    }

    //
    // Handler Reported
    //

    private void onReportUserSucceed(UserModel userModel) {
        BackgroundUtil.UI().execute(() -> {
            this.flyingProfileActivity.removeLoader();
            this.userModel = userModel;
            this.updateAdapter();
        });
    }

    private void onReportUserFailed(ErrorResponse errorResponse) {
        BackgroundUtil.UI().execute(() -> {
            this.flyingProfileActivity.removeLoader();
        });
    }

    //
    // Handler UpdateFriendship
    //

    private void onUpdateFriendshipSucceed(UserModel userModel) {
        BackgroundUtil.UI().execute(() -> {
            if (null != this.flyingProfileActivity) {
                this.flyingProfileActivity.removeLoader();
                this.flyingProfileAdapter.updateAdapter(this.flyingProfileActivity, userModel);
            }
        });
    }

    private void onUpdateFriendshipFailed(ErrorResponse errorResponse) {
        BackgroundUtil.UI().execute(() -> {
            if (null != this.flyingProfileActivity) {
                this.flyingProfileActivity.removeLoader();
            }
        });
    }

    private void onUpdateFriendshipFailedUserIdNotFound(ErrorResponse errorResponse) {
        BackgroundUtil.UI().execute(() -> {
            if (null != this.flyingProfileActivity) {
                this.flyingProfileActivity.removeLoader();
            }
        });
    }

    //
    // Private
    //

    private void updateAdapter() {
        this.flyingProfileAdapter = new FlyingProfileAdapter(flyingProfileActivity, this.userModel, this);
        this.flyingProfileActivityBinding.profileRecycler.setAdapter(this.flyingProfileAdapter);
    }
}
