package fr.com.panda.ui.search;

import android.databinding.Bindable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

import fr.com.panda.R;
import fr.com.panda.databinding.TabFriendshipFragmentBinding;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.manager.FriendshipManager;
import fr.com.panda.manager.SearchManager;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.model.SearchModel;
import fr.com.panda.model.UserModel;
import fr.com.panda.ui.base.ViewModel;
import fr.com.panda.ui.base.util.recycler.EndlessRecyclerViewScrollListener;
import fr.com.panda.BR;
import fr.com.panda.ui.friendship.FriendshipViewModel;
import fr.com.panda.ui.friendship.TabFriendshipFragment;
import fr.com.panda.utils.BackgroundUtil;

/**
 * @author Edouard Roussillon
 */

public class SearchViewModel extends ViewModel implements FriendshipViewModel.FriendshipItemClicked {

    private SearchManager searchManager = SearchManager.sharedInstance;

    @Nullable
    private TabFriendshipFragment fragment;
    @Nullable
    private TabFriendshipFragmentBinding binding;

    private boolean showSearchContent = false;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener scrollListener;
    private SearchRecyclerAdapter adapter = new SearchRecyclerAdapter();
    private int page = 1;
    private int totalPages = 1;
    private String searchText = "";
    private MaterialSearchView.SearchViewListener searchViewListener;

    public SearchViewModel(TabFriendshipFragment fragment, TabFriendshipFragmentBinding binding,  MaterialSearchView.SearchViewListener searchViewListener) {
        this.fragment = fragment;
        this.binding = binding;
        this.searchViewListener = searchViewListener;

        this.linearLayoutManager = new LinearLayoutManager(this.fragment.getContext());
        this.scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {

            @Override
            public void onLoadMore(int totalItemsCount, RecyclerView view) {
                addLoader();
                page++;
                //TODO - Make possible to cancel request
                searchManager.searchPseudo(searchText, page);
            }
        };
        this.binding.searchRecyclerView.setLayoutManager(this.linearLayoutManager);
        this.binding.searchRecyclerView.addOnScrollListener(this.scrollListener);
        this.binding.searchRecyclerView.setAdapter(this.adapter);
    }

    @Override
    public View getRoot() {
        return null;
    }

    //
    // ViewModel Cycle
    //


    @Override
    public void bind() {
        super.bind();
    }

    @Override
    public void start() {
        super.start();

        this.getBag().add(this.searchManager.onSearchSucceed.add(this::onSearchSucceed));
        this.getBag().add(this.searchManager.onSearchFailed.add(this::onSearchFailed));
    }

    @Override
    public void stop() {
        super.stop();
    }

    @Override
    public void unbind() {
        super.unbind();

        this.fragment = null;
        this.binding = null;
    }

    //
    // Bind
    //

    @Bindable
    public MaterialSearchView.OnQueryTextListener onQueryText = new MaterialSearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            //Do some magic
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            searchText = newText;
            resetAdapter();

            if (searchText.length() < 3) {
                return false;
            }

            updateLocalList();
            addLoader();
            page = 1;
            //TODO - Make possible to cancel request
            searchManager.searchPseudo(searchText, page);
            return true;
        }
    };

    @Bindable
    public MaterialSearchView.SearchViewListener onSearchView = new MaterialSearchView.SearchViewListener() {
        @Override
        public void onSearchViewShown() {
            setShowSearchContent(true);
            searchViewListener.onSearchViewShown();
        }

        @Override
        public void onSearchViewClosed() {
            setShowSearchContent(false);
            searchViewListener.onSearchViewClosed();
        }
    };

    @Bindable
    public boolean getShowSearchContent() {
        return showSearchContent;
    }


    public void setShowSearchContent(Boolean showSearchContent) {
        this.showSearchContent = showSearchContent;
        notifyPropertyChanged(BR.showSearchContent);
        binding.getViewModel().setShowBottomBar(this.showSearchContent ? View.GONE : View.VISIBLE);
    }

    //
    // Handler
    //

    private void onSearchSucceed(SearchModel searchModel) {
        if (!searchModel.getSearchText().equals(this.searchText)) return;

        this.totalPages = searchModel.getPaginated().totalPages;
        List<SearchItemViewModel> newList = new ArrayList();

        if (page == 1) {
            if(searchModel.getPaginated().entries.size() > 0) {
                newList.add(0, new SearchItemViewModel(R.string.search_other_title));
            }
        }

        for (UserModel item: searchModel.getPaginated().entries) {
            newList.add(createSearchItem(item));
        }

        BackgroundUtil.UI().execute(() -> {
            this.removeLoader();
            this.updateAdapter(newList);
        });
    }

    private void onSearchFailed(ErrorResponse error) {
        BackgroundUtil.UI().execute(() -> {
            this.resetAdapter();
        });
    }

    //
    // Private
    //

    private void addLoader() {
        this.adapter.addLoader();
    }

    private void removeLoader() {
        this.adapter.removeLoader();
    }

    private void updateLocalList() {
        List<SearchItemViewModel> newList = new ArrayList();

        final List<SearchItemViewModel> pendingList = searchIntoList(FriendshipManager.sharedInstance.getUsersByFriendshipTypePending(), searchText);
        if (pendingList.size() > 0) {
            newList.add(new SearchItemViewModel(R.string.search_pending_title));
            newList.addAll(pendingList);
        }

        final List<SearchItemViewModel> friendList = searchIntoList(FriendshipManager.sharedInstance.getUsersByFriendshipTypeFriend(), searchText);
        if (friendList.size() > 0) {
            newList.add(new SearchItemViewModel(R.string.search_accepted_title));
            newList.addAll(pendingList);
        }

        this.resetAdapter(newList);
    }

    private List<SearchItemViewModel> searchIntoList(List<UserModel> list, String text) {
        List<SearchItemViewModel> newList = new ArrayList();
        for (UserModel item: list) {
            if (item.pseudo.toLowerCase().contains(text.toLowerCase())) {
                newList.add(createSearchItem(item));
            }
        }

        return newList;
    }

    private void updateAdapter(List<SearchItemViewModel> newList) {
        this.adapter.addNewItems(newList);
    }

    private void resetAdapter(List<SearchItemViewModel> list) {
        this.adapter.replaceItem(list);
    }

    private void resetAdapter() {
        this.resetAdapter(new ArrayList());
    }

    private SearchItemViewModel createSearchItem(UserModel userModel) {
        return new SearchItemViewModel(this, userModel);
    }

    //
    // FriendshipViewModel.FriendshipItemClicked
    //


    @Override
    public void launchChat(UserModel userModel) {
        this.fragment.launchChat(userModel);
    }

    @Override
    public void launchProfile(UserModel userModel) {
        this.fragment.launchProfile(userModel);
    }

    @Override
    public void updateFriendship(int userId, @FriendshipType.FriendshipMode int friendshipType) {
        //should do nothing
    }
}
