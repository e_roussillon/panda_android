package fr.com.panda.ui.chat;

import android.support.annotation.IdRes;
import android.view.View;

import com.raizlabs.android.dbflow.annotation.NotNull;
import com.roughike.bottombar.OnTabSelectListener;

import fr.com.panda.R;
import fr.com.panda.databinding.TabChatFragmentBinding;
import fr.com.panda.ui.base.ViewModel;

/**
 * @author Edouard Roussillon
 */

public class TabChatViewModel extends ViewModel {

    @NotNull
    private TabChatFragment fragment;
    @NotNull
    private TabChatFragmentBinding binding;

    public TabChatViewModel(TabChatFragment fragment, TabChatFragmentBinding binding) {
        this.fragment = fragment;
        this.binding = binding;
    }

    @Override
    public View getRoot() {
        return this.binding.getRoot();
    }

    @Override
    public void bind() {
        super.bind();

        this.binding.chatBottomBar.setOnTabSelectListener(onTabClick);
    }

    @Override
    public void unbind() {
        super.unbind();

        this.fragment = null;
        this.binding = null;
    }

    //
    // Click
    //

    private OnTabSelectListener onTabClick = (tabId) -> {
        switch (tabId) {
            case R.id.tab_general:
                break;
            case R.id.tab_trade:
                break;
            case R.id.tab_private:
                break;
        }
    };

}
