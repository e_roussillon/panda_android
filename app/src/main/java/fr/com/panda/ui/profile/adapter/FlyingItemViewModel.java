package fr.com.panda.ui.profile.adapter;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import fr.com.panda.enums.FriendshipType;
import fr.com.panda.ui.base.util.HeaderViewModel;

/**
 * @author Edouard Roussillon
 */

public class FlyingItemViewModel {

    private HeaderViewModel headerViewModel;
    private FlyingActionItemViewModel flyingActionItemViewModel;
    private FlyingProfileItemViewModel flyingProfileItemViewModel;


    public FlyingItemViewModel() {
        this.headerViewModel = null;
        this.flyingActionItemViewModel = null;
        this.flyingProfileItemViewModel = null;
    }

    public FlyingItemViewModel(@StringRes int headerMessage) {
        this.headerViewModel = new HeaderViewModel(headerMessage);
        this.flyingActionItemViewModel = null;
        this.flyingProfileItemViewModel = null;
    }

    public FlyingItemViewModel(@StringRes int titleCategory, String message) {
        this.headerViewModel = null;
        this.flyingActionItemViewModel = null;
        this.flyingProfileItemViewModel = new FlyingProfileItemViewModel(titleCategory, message);
    }

    public FlyingItemViewModel(boolean actionPositive, @StringRes int title, @DrawableRes int ressourceIcon, @FriendshipType.FriendshipMode int tag, FlyingActionItemViewModel.FlyingAction flyingAction) {
        this.headerViewModel = null;
        this.flyingActionItemViewModel = new FlyingActionItemViewModel(title, actionPositive, ressourceIcon, tag, flyingAction);
        this.flyingProfileItemViewModel = null;
    }

    public boolean isSection() { return null == flyingProfileItemViewModel && null == flyingActionItemViewModel && null != headerViewModel; }
    public boolean isAction() { return null == flyingProfileItemViewModel && null != flyingActionItemViewModel && null == headerViewModel; }
    public boolean isProfile() { return null != flyingProfileItemViewModel && null == flyingActionItemViewModel && null == headerViewModel; }

    public HeaderViewModel getHeaderViewModel() {
        return headerViewModel;
    }

    public FlyingProfileItemViewModel getFlyingProfileItemViewModel() {
        return flyingProfileItemViewModel;
    }

    public FlyingActionItemViewModel getFlyingActionItemViewModel() {
        return flyingActionItemViewModel;
    }
}
