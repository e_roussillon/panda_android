package fr.com.panda.ui.base;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;

import com.devspark.robototextview.inflater.RobotoInflater;
import com.raizlabs.android.dbflow.annotation.NotNull;

import fr.com.panda.R;
import fr.com.panda.databinding.ViewLoaderBinding;
import fr.com.panda.event.DisposableBag;
import fr.com.panda.http.ApiEventGlobal;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.model.LogoutReported;
import fr.com.panda.model.UserModel;
import fr.com.panda.ui.chat.chat.ChatActivity;
import fr.com.panda.ui.profile.FlyingProfileActivity;
import fr.com.panda.ui.splash.SplashActivity;
import fr.com.panda.utils.BackgroundUtil;
import fr.com.panda.utils.DateUtil;

/**
 * Created by edouardroussillon on 12/26/16.
 */

public class BaseActivity extends AppCompatActivity {

    @Nullable
    private ViewLoaderBinding loaderBinding = null;

    @NotNull
    private ApiEventGlobal apiEventGlobal = ApiEventGlobal.sharedInstance;

    //
    // Lifecycle
    //

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        RobotoInflater.attach(this);
        super.onCreate(savedInstanceState);
    }


    //
    // Dialog
    //

    public void showDialogAlert(@StringRes int message) {
        showDialogMessage(R.string.dialog_title, message, R.string.dialog_button_ok, null, -1, null);
    }

    public void showDialogAlert(@StringRes int message, @StringRes int positiveMessage, final DialogInterface.OnClickListener positiveListener) {
        showDialogMessage(R.string.dialog_title, message, positiveMessage, positiveListener, -1, null);
    }

    public void showDialogAlert(String message) {
        showDialogMessage(getString(R.string.dialog_title), message, R.string.dialog_button_ok, null, -1, null);
    }

    public void showDialogAlert(String message, @StringRes int positiveMessage, final DialogInterface.OnClickListener positiveListener) {
        showDialogMessage(getString(R.string.dialog_title), message, positiveMessage, positiveListener, -1, null);
    }

    public void showDialogMessage(@StringRes int message) {
        showDialogMessage(-1, message, R.string.dialog_button_ok, null, -1, null);
    }

    public void showDialogMessage(@StringRes int title, @StringRes int message) {
        showDialogMessage(title, message, R.string.dialog_button_ok, null, -1, null);
    }

    public void showDialogMessage(@StringRes int title, @StringRes int message, final DialogInterface.OnClickListener positiveListener, final DialogInterface.OnClickListener negativeListener) {
        showDialogMessage(title, message, R.string.dialog_button_ok, positiveListener, R.string.dialog_button_cancel, negativeListener);
    }

    public void showDialogMessage(String message) {
        showDialogMessage(null, message, R.string.dialog_button_ok, null, -1, null);
    }

    public void showDialogMessage(String title, String message) {
        showDialogMessage(title, message, R.string.dialog_button_ok, null, -1, null);
    }

    public void showDialogMessage(@StringRes int title, @StringRes int messsage,
                                  @StringRes int positiveMessage, final DialogInterface.OnClickListener positiveListener,
                                  @StringRes int negativeMessage, final DialogInterface.OnClickListener negativeListener) {
        BackgroundUtil.UI().execute(() -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            if (title != -1) {
                builder.setTitle(title);
            }

            builder.setMessage(messsage);
            builder.setPositiveButton(positiveMessage, positiveListener);

            if (negativeMessage != -1) {
                builder.setNegativeButton(negativeMessage, negativeListener);
            }

            showDialog(builder);
        });
    }

    public void showDialogMessage(String title, String messsage,
                                  @StringRes int positiveMessage, final DialogInterface.OnClickListener positiveListener,
                                  @StringRes int negativeMessage, final DialogInterface.OnClickListener negativeListener) {
        BackgroundUtil.UI().execute(() -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            if (title != null) {
                builder.setTitle(title);
            }

            builder.setMessage(messsage);
            builder.setPositiveButton(positiveMessage, positiveListener);

            if (negativeMessage != -1) {
                builder.setNegativeButton(negativeMessage, negativeListener);
            }

            showDialog(builder);
        });
    }

    private void showDialog(AlertDialog.Builder builder) {
        builder.setCancelable(false);

        AppCompatDialog appCompatDialog = builder.create();
        appCompatDialog.show();
    }


    //
    // Loader
    //

    public void showLoader() {
        if (loaderBinding == null) {
            ViewGroup view = (ViewGroup) findViewById(android.R.id.content);
            loaderBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.view_loader, view, true);
        }
    }

    public void removeLoader() {
        if (loaderBinding != null) {
            ViewGroup view = (ViewGroup) findViewById(android.R.id.content);
            view.removeView(loaderBinding.getRoot());
            loaderBinding = null;
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (loaderBinding == null) {
            return super.dispatchTouchEvent(ev);
        }

        return true;
    }

    //
    // Launcher
    //

    public void launchChat(UserModel userModel){
        Intent intent = new Intent(this, ChatActivity.class);
        this.startActivity(intent);
    }

    public void launchProfile(UserModel userModel) {
        this.startActivity(FlyingProfileActivity.initIntent(this, userModel));
    }

    public void launchSplash() {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        this.startActivity(intent);
    }

    //
    // Http Error
    //

    protected void initErrorHttpHandle(DisposableBag bag) {
        bag.add(apiEventGlobal.onGenericEventError.add(this::onGenericEventError));
        bag.add(apiEventGlobal.onGenericEventForceLogout.add(this::onGenericEventForceLogout));
        bag.add(apiEventGlobal.onGenericEventForceLogoutReported.add(this::onGenericEventForceLogoutReported));
    }

    private void onGenericEventError(Void v) {
        this.showDialogMessage(R.string.dialog_title_error_server, R.string.dialog_message_error_server);
    }

    private void onGenericEventForceLogout(Void v) {
        SessionManager.sharedInstance.logout();
        this.showDialogAlert(R.string.dialog_message_logout_froced, R.string.dialog_button_ok, (dialog, which) -> {
            launchSplash();
        });
    }

    private void onGenericEventForceLogoutReported(final LogoutReported logoutReported) {
        SessionManager.sharedInstance.logout();
        this.showDialogAlert(getString(R.string.dialog_message_reported).replace("%1", DateUtil.convertSecondToHHMMString(logoutReported.time)),
                R.string.dialog_button_ok, (dialog, which) -> {
                    if (logoutReported.logout) {
                        launchSplash();
                    }
        });
    }
}
