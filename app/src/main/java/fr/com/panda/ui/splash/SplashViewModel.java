package fr.com.panda.ui.splash;

import android.support.annotation.Nullable;
import android.view.View;

import com.raizlabs.android.dbflow.annotation.NotNull;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import fr.com.panda.manager.BackupManager;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.ui.base.ViewModel;

/**
 * @author Edouard Roussillon
 */

public class SplashViewModel extends ViewModel {

    private static int LAUNCH_LOGIN_IN_SEC = 3;

    @Nullable private SplashActivity splashActivity;
    private BackupManager backupManager = BackupManager.sharedInstance;
    private Timer timerLaunchLogin = new Timer();

    public SplashViewModel(@NotNull SplashActivity splashActivity) {
        this.splashActivity = splashActivity;
        this.getBag().add(this.backupManager.onBackupSucceed.add(this::onBackupSucceed));
        this.getBag().add(this.backupManager.onBackupFailed.add(this::onBackupFailed));
    }

    @Override
    public View getRoot() {
        return null;
    }

    @Override
    public void start() {
        super.start();

        if (this.getSessionManager().user().isJustLogin()) {
            this.initBackup();
        } else {
            this.timerLaunchLogin.cancel();
            this.timerLaunchLogin = new Timer();

            this.timerLaunchLogin.schedule(new TimerTask() {
                                                        @Override
                                                        public void run() {
                                                            if (splashActivity != null) {
                                                                splashActivity.launchLogin();
                                                            }
                                                        }
                                                    }, TimeUnit.SECONDS.toMillis(LAUNCH_LOGIN_IN_SEC));
        }
    }

    @Override
    public void unbind() {
        super.unbind();

        this.splashActivity = null;
    }

    //
    //MARK: Backup
    //

    private void initBackup() {
        this.backupManager.backup();
    }

    //
    // Handler
    //

    private void onBackupSucceed(Void v) {
        this.getSessionManager().user().setJustLogin(false);
        if (this.splashActivity != null) {
            this.splashActivity.launchHome();
        }
    }

    private void onBackupFailed(ErrorResponse response) {
        if (this.splashActivity != null) {
            this.splashActivity.launchLogin();
        }
    }
}
