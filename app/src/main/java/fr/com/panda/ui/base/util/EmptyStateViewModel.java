package fr.com.panda.ui.base.util;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringRes;

import fr.com.panda.BR;

/**
 * @author Edouard Roussillon
 */

public class EmptyStateViewModel extends BaseObservable {


    private @DrawableRes int emptyStateImage;
    private @StringRes int emptyStateMessage;
    private boolean visibility = true;

    public EmptyStateViewModel(@DrawableRes int emptyStateImage, @StringRes int emptyStateMessage) {
        this.setEmptyStateImage(emptyStateImage);
        this.setEmptyStateMessage(emptyStateMessage);
    }

    @Bindable
    public int getEmptyStateImage() {
        return emptyStateImage;
    }

    public void setEmptyStateImage(int emptyStateImage) {
        this.emptyStateImage = emptyStateImage;
        notifyPropertyChanged(BR.emptyStateImage);
    }

    @Bindable
    public int getEmptyStateMessage() {
        return emptyStateMessage;
    }

    public void setEmptyStateMessage(int emptyStateMessage) {
        this.emptyStateMessage = emptyStateMessage;
        notifyPropertyChanged(BR.emptyStateMessage);
    }

    @Bindable
    public boolean isVisibility() {
        return visibility;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
        notifyPropertyChanged(BR.visibility);
    }
}
