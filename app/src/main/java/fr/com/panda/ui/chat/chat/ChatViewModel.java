package fr.com.panda.ui.chat.chat;

import android.view.View;

import fr.com.panda.databinding.ChatActivityBinding;
import fr.com.panda.ui.base.ViewModel;

/**
 * @author Edouard Roussillon
 */

public class ChatViewModel extends ViewModel {

    private ChatActivity chatActivity;
    private ChatActivityBinding chatActivityBinding;

    public ChatViewModel(ChatActivity chatActivity, ChatActivityBinding chatActivityBinding) {
        this.chatActivity = chatActivity;
        this.chatActivityBinding = chatActivityBinding;
    }

    @Override
    public void unbind() {
        super.unbind();

        this.chatActivity = null;
        this.chatActivityBinding = null;
    }

    @Override
    public View getRoot() {
        return null;
    }
}
