package fr.com.panda.ui.splash;

import android.content.Intent;
import android.support.annotation.Nullable;

import fr.com.panda.R;
import fr.com.panda.databinding.SplashActivityBinding;
import fr.com.panda.manager.BackupManager;
import fr.com.panda.ui.base.ViewModel;
import fr.com.panda.ui.base.ViewModelActivity;
import fr.com.panda.ui.home.HomeActivity;
import fr.com.panda.ui.login.LoginActivity;

public class SplashActivity extends ViewModelActivity<SplashViewModel, SplashActivityBinding> {

    private BackupManager backupManager = BackupManager.sharedInstance;

    @Override
    protected SplashViewModel onCreateViewModel(SplashActivityBinding binding) {
        return new SplashViewModel(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.splash_activity;
    }

    @Override
    protected void onBindViewModel() {
        super.onBindViewModel();

        this.binding.setViewModel(this.viewModel);
    }

    //
    // Intent
    //

    public void launchLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        this.startActivity(intent);
    }

    public void launchHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        this.startActivity(intent);
    }
}
