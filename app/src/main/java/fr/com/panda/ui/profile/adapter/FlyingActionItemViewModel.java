package fr.com.panda.ui.profile.adapter;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.view.View;

import fr.com.panda.R;
import fr.com.panda.enums.FriendshipType;

/**
 * @author Edouard Roussillon
 */

public class FlyingActionItemViewModel {

    private @StringRes int title;
    private boolean actionPositive;
    private @DrawableRes int resourceIcon;
    private @FriendshipType.FriendshipMode int tag;
    private FlyingAction flyingAction;

    public FlyingActionItemViewModel(@StringRes int title, boolean actionPositive, @DrawableRes int resourceIcon, @FriendshipType.FriendshipMode int tag, FlyingAction flyingAction) {
        this.title = title;
        this.actionPositive = actionPositive;
        this.flyingAction = flyingAction;
        this.tag = tag;
        this.resourceIcon = resourceIcon;
    }

    public int getTitle() {
        return title;
    }

    public boolean isActionPositive() {
        return actionPositive;
    }

    public boolean isActionNegative() {
        return !actionPositive;
    }

    public @DrawableRes int getResourceIcon() {
        return resourceIcon;
    }

    public @ColorRes int getResourceIconColor() {
        return actionPositive ? R.color.colorPrimaryDark : R.color.colorPrimary;
    }

    public View.OnClickListener onClickButton = v -> {
        flyingAction.updateFriendship(tag);
    };

    public interface FlyingAction {
        void updateFriendship(@FriendshipType.FriendshipMode int friendship);
    }

}
