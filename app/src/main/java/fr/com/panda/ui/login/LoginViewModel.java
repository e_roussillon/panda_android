package fr.com.panda.ui.login;

import android.databinding.Bindable;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;

import fr.com.panda.BR;
import fr.com.panda.R;
import fr.com.panda.databinding.DialogEditTextBinding;
import fr.com.panda.databinding.LoginActivityBinding;
import fr.com.panda.enums.ValidatorType;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.ui.base.ViewModel;
import fr.com.panda.ui.base.util.DialogEditTextModel;
import fr.com.panda.utils.BackgroundUtil;
import fr.com.panda.utils.ValidatorUtil;

/**
 * @author Edouard Roussillon
 */

public class LoginViewModel extends ViewModel {

    @Nullable
    private LoginActivity loginActivity;
    @Nullable
    private LoginActivityBinding binding;

    public LoginViewModel(LoginActivity loginActivity, LoginActivityBinding binding) {
        this.loginActivity = loginActivity;
        this.binding = binding;
    }

    private CharSequence editTextEmail = "";
    private CharSequence editTextPassword = "";


    @Bindable
    public CharSequence getEditTextEmail() {
        return editTextEmail;
    }

    public void setEditTextEmail(CharSequence editTextEmail) {
        this.editTextEmail = editTextEmail;
        notifyPropertyChanged(BR.editTextEmail);
    }

    @Bindable
    public CharSequence getEditTextPassword() {
        return editTextPassword;
    }

    public void setEditTextPassword(CharSequence editTextPassword) {
        this.editTextPassword = editTextPassword;
        notifyPropertyChanged(BR.editTextPassword);
    }

    //
    // Bind
    //

    @Override
    public View getRoot() {
        return this.binding.getRoot();
    }

    @Override
    public void bind() {
        super.bind();
    }

    @Override
    public void start() {
        super.start();

        this.getBag().add(this.getSessionManager().onGetSessionSucceed.add(this::onGetSessionSucceed));
        this.getBag().add(this.getSessionManager().onGetSessionFailed.add(this::onGetSessionFailed));
        this.getBag().add(this.getSessionManager().onGetSessionFailedNotFoundEmailOrPassword.add(this::onGetSessionFailedNotFoundEmailOrPassword));
        this.getBag().add(this.getSessionManager().onGetSessionFailedRateLimit.add(this::onGetSessionFailedRateLimit));
        this.getBag().add(this.getSessionManager().onGetSessionFailedNotActive.add(this::onGetSessionFailedNotActive));

        this.getBag().add(this.getSessionManager().onResetPasswordSessionSucceed.add(this::onResetPasswordSessionSucceed));
        this.getBag().add(this.getSessionManager().onResetPasswordSessionFailed.add(this::onResetPasswordSessionFailed));
    }

    @Override
    public void restoreInstance(ViewModel.State state) {
        super.restoreInstance(state);

        if (state instanceof State) {
            State stateLogin = (State) state;
            editTextEmail = stateLogin.email;
            editTextPassword = stateLogin.password;
        }
    }

    @Override
    public ViewModel.State saveInstanceState() {
        return new State(this);
    }

    @Override
    public void unbind() {
        super.unbind();

        this.loginActivity = null;
        this.binding = null;
    }

    //
    // Validator
    //

    private boolean isValid() {
        if (this.binding == null) {
            return false;
        }

        boolean isValid = ValidatorUtil.isFieldValid(this.binding.inputLayoutEmail, ValidatorType.IS_NOT_EMPTY);
        return ValidatorUtil.isFieldValid(this.binding.inputLayoutPassword, ValidatorType.IS_NOT_EMPTY) && isValid;
    }

    //
    // Click
    //

    public View.OnClickListener onClickLogin = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (loginActivity == null)
                return;

            loginActivity.hideKeyboard();

            if (isValid()) {
                loginActivity.showLoader();
                getSessionManager().login(editTextEmail.toString(), editTextPassword.toString());
            }
        }
    };

    public View.OnClickListener onClickRegister = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (loginActivity == null)
                return;

            loginActivity.hideKeyboard();
            loginActivity.launchRegister();
        }
    };

    public View.OnClickListener onClickForgot = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (loginActivity == null)
                return;

            loginActivity.hideKeyboard();

            final AlertDialog.Builder builder = new AlertDialog.Builder(loginActivity);

            final DialogEditTextModel dialogEditTextModel = new DialogEditTextModel();
            dialogEditTextModel.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            dialogEditTextModel.setText(editTextEmail);
            dialogEditTextModel.setHint(R.string.generic_label_email);
            View view = LayoutInflater.from(builder.getContext()).inflate(R.layout.dialog_edit_text, null, false);

            final DialogEditTextBinding dialogEditTextBinding = DialogEditTextBinding.bind(view);
            dialogEditTextBinding.setViewModel(dialogEditTextModel);

            builder.setView(view);
            builder.setTitle(R.string.dialog_reset_password_title);
            builder.setMessage(R.string.dialog_reset_password_message);
            builder.setPositiveButton(R.string.dialog_button_ok, (dialog, whichButton) -> {
                getSessionManager().resetPassword(dialogEditTextModel.getText().toString());
            });

            builder.setNegativeButton(R.string.dialog_button_cancel, (dialog, whichButton) -> {
                    dialog.cancel();
            });

            builder.setCancelable(false);

            AppCompatDialog appCompatDialog = builder.create();
            appCompatDialog.show();
        }
    };

    //
    // Handler
    //

    private void onGetSessionSucceed(Void v) {
        if (this.loginActivity != null) {
            BackgroundUtil.UI().execute(() -> {
                this.loginActivity.removeLoader();
                this.loginActivity.launchSplash();
            });
        }
    }

    private void onGetSessionFailed(ErrorResponse response) {
        if (this.loginActivity != null) {
            BackgroundUtil.UI().execute(() -> {
                this.loginActivity.removeLoader();
                this.loginActivity.animLogoError();
            });
        }
    }

    private void onGetSessionFailedNotFoundEmailOrPassword(Void v) {
        if (this.loginActivity != null && this.binding != null) {
            BackgroundUtil.UI().execute(() -> {
                this.loginActivity.removeLoader();
                this.loginActivity.animLogoError();
                this.binding.inputLayoutEmail.setError(this.loginActivity.getString(R.string.generic_error_empty_field));
                this.binding.inputLayoutPassword.setError(this.loginActivity.getString(R.string.generic_error_empty_field));
                this.sendAnalytics();
            });
        }
    }

    private void onGetSessionFailedRateLimit(Void v) {
        if (this.loginActivity != null) {
            BackgroundUtil.UI().execute(() -> {
                this.loginActivity.removeLoader();
                this.loginActivity.animLogoError();
                this.loginActivity.showDialogAlert(R.string.dialog_message_rate_limit);
            });
        }
    }

    private void onGetSessionFailedNotActive(Void v) {
        if (this.loginActivity != null) {
            BackgroundUtil.UI().execute(() -> {
                this.loginActivity.removeLoader();
                this.loginActivity.animLogoError();
                this.loginActivity.showDialogAlert(R.string.dialog_message_not_active);
            });
        }
    }

    private void onResetPasswordSessionSucceed(Void v) {
        if (this.loginActivity != null) {
            BackgroundUtil.UI().execute(() -> {
                this.loginActivity.removeLoader();
                this.loginActivity.showDialogMessage(R.string.dialog_reset_password_success);
            });
        }
    }

    private void onResetPasswordSessionFailed(Void v) {
        if (this.loginActivity != null) {
            BackgroundUtil.UI().execute(() -> {
                this.loginActivity.removeLoader();
                this.loginActivity.showDialogMessage(R.string.dialog_reset_password_fail);
            });
        }
    }

    //
    // MARK: Analytics
    //

    private void sendAnalytics() {
//        final HashMap<String, Object> param = new HashMap<>();
//        param.put("email", user);
//
//
//
//        if let emailError = self.textFieldEmail.labelError.text where emailError.length() > 0 {
//            param.put(KeyNameType.EmailError, emailError)
//        }
//
//        if let passwordError = self.textFieldPassword.labelError.text where passwordError.length() > 0 {
//            param[KeyName.PasswordError.rawValue] = passwordError
//        }
//        self.analyticsManager.trackAction(ActionName.MissingInfoSignIn, params: param)
    }

    //
    // State
    //

    private static class State extends ViewModel.State {

        public static final Creator<State> CREATOR = new Creator<State>() {
            @Override
            public State createFromParcel(Parcel in) {
                return new State(in);
            }

            @Override
            public State[] newArray(int size) {
                return new State[size];
            }
        };

        @Nullable
        private CharSequence email;
        @Nullable
        private CharSequence password;

        public State(LoginViewModel loginViewModel) {
            super(loginViewModel);
            this.email = loginViewModel.editTextEmail;
            this.password = loginViewModel.editTextPassword;
        }

        public State(android.os.Parcel parcel) {
            super(parcel);
            this.email = parcel.readString();
            this.password = parcel.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (dest != null) {
                dest.writeString(email.toString());
                dest.writeString(password.toString());
            }
        }
    }
}
