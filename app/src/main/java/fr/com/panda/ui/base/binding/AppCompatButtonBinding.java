package fr.com.panda.ui.base.binding;

import android.databinding.BindingAdapter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;

import fr.com.panda.R;

/**
 * @author Edouard Roussillon
 */

public class AppCompatButtonBinding {
    @BindingAdapter("drawStart")
    public static void setDrawStart(AppCompatButton v, @DrawableRes int drawableId) {
        setDrawStartAndColor(v, drawableId, R.color.colorPrimary);
    }

    @BindingAdapter("drawColor")
    public static void setDrawColor(AppCompatButton v, @ColorRes int drawableColor) {
        setDrawStartAndColor(v, R.drawable.user_accepted, drawableColor);
    }

    @BindingAdapter({"drawStart", "drawColor"})
    public static void setDrawStartAndColor(AppCompatButton v, @DrawableRes int drawableId, @ColorRes int drawableColor) {
        final Drawable drawable;
        final @ColorInt int color = ContextCompat.getColor(v.getContext(), drawableColor);

        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            drawable = v.getContext().getResources().getDrawable(drawableId, v.getContext().getTheme());
        } else {
            drawable = v.getContext().getResources().getDrawable(drawableId);
        }

        drawable.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN));
        v.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
    }
}
