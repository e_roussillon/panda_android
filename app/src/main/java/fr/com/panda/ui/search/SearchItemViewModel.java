package fr.com.panda.ui.search;

import android.support.annotation.StringRes;

import fr.com.panda.model.UserModel;
import fr.com.panda.ui.base.util.HeaderViewModel;
import fr.com.panda.ui.friendship.FriendshipViewModel;

/**
 * @author Edouard Roussillon
 */

public class SearchItemViewModel {

    private HeaderViewModel headerViewModel;
    private FriendshipViewModel friendshipItemViewModel;

    public SearchItemViewModel() {
        this.headerViewModel = null;
        this.friendshipItemViewModel = null;
    }

    public SearchItemViewModel(@StringRes int headerMessage) {
        this.headerViewModel = new HeaderViewModel(headerMessage);
        this.friendshipItemViewModel = null;
    }

    public SearchItemViewModel(FriendshipViewModel.FriendshipItemClicked friendshipItemClicked, UserModel userModel) {
        this.headerViewModel = null;
        this.friendshipItemViewModel = new FriendshipViewModel(friendshipItemClicked, userModel, true);
    }


    public boolean isSection() { return null == friendshipItemViewModel && null != headerViewModel; }
    public boolean isLoader() { return null == friendshipItemViewModel && null == headerViewModel; }


    public HeaderViewModel getHeaderViewModel() {
        return headerViewModel;
    }

    public FriendshipViewModel getFriendshipItemViewModel() {
        return friendshipItemViewModel;
    }
}
