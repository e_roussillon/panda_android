package fr.com.panda.ui.base.binding;

import android.databinding.BindingAdapter;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;

/**
 * @author Edouard Roussillon
 */

public class AppCompatTextViewBinding {

    @BindingAdapter("typeface")
    public static void setTypeface(AppCompatTextView v, String style) {
        switch (style) {
            case "bold":
                v.setTypeface(null, Typeface.BOLD);
                break;
            default:
                v.setTypeface(null, Typeface.NORMAL);
                break;
        }
    }

}
