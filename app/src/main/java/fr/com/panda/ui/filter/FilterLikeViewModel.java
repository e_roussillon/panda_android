package fr.com.panda.ui.filter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.com.panda.R;
import fr.com.panda.databinding.FilterLikeViewGroupBinding;
import fr.com.panda.ui.base.util.EmptyStateViewModel;
import fr.com.panda.ui.base.util.HeaderViewModel;

/**
 * @author Edouard Roussillon
 */

public class FilterLikeViewModel extends FilterViewModel {

    public FilterLikeViewModel(TabFilterFragment fragment, ViewGroup viewGroup) {
        super(true, R.string.dialog_filter_message_want);

        LayoutInflater inflater = (LayoutInflater) fragment.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.fragment = fragment;
        this.binding = FilterLikeViewGroupBinding.inflate(inflater, viewGroup, true);
    }

    @Override
    public void bind() {
        super.bind();
        this.headerViewModel = new HeaderViewModel(R.string.filter_header_want);
        this.emptyStateViewModel = new EmptyStateViewModel(R.drawable.ic_like_big, R.string.filter_empty_state_like);

        this.getBinding().filterLikeRecycler.setLayoutManager(this.linearLayoutManager);
        this.getBinding().filterLikeRecycler.setAdapter(this.filterRecyclerAdapter);

        this.getBinding().setViewModel(this);

        this.emptyStateViewModel.setVisibility(this.filterRecyclerAdapter.showEmptyState());
    }

    @Override
    protected Toolbar getToolbar() {
        return this.getBinding().filterLikeToolbar;
    }

    //
    // Get-Set
    //

    @Nullable
    public FilterLikeViewGroupBinding getBinding() {
        return (FilterLikeViewGroupBinding) this.binding;
    }
}
