package fr.com.panda.ui.base.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import fr.com.panda.ui.profile.adapter.FlyingItemViewModel;
import fr.com.panda.ui.search.SearchItemViewModel;

/**
 * @author Edouard Roussillon
 */

public class BaseItemViewHolder extends RecyclerView.ViewHolder {

    public BaseItemViewHolder(View itemView) {
        super(itemView);
    }

    public void bind(SearchItemViewModel searchItemViewModel) {}

    public void bind(FlyingItemViewModel flyingItemViewModel) {}
}
