package fr.com.panda.ui.profile;

import android.support.annotation.Nullable;
import android.view.View;

import fr.com.panda.databinding.ProfileActivityBinding;
import fr.com.panda.ui.base.ViewModel;

/**
 * @author Edouard Roussillon
 */

public class ProfileViewModel extends ViewModel {

    @Nullable
    private ProfileActivity profileActivity;
    @Nullable
    private ProfileActivityBinding binding;

    public ProfileViewModel(ProfileActivity profileActivity, ProfileActivityBinding binding) {
        this.profileActivity = profileActivity;
        this.binding = binding;
    }

    @Override
    public View getRoot() {
        return this.binding.getRoot();
    }

    @Override
    public void unbind() {
        super.unbind();

        this.profileActivity = null;
        this.binding = null;
    }
}
