package fr.com.panda.ui.register;

import android.support.annotation.Nullable;

import fr.com.panda.R;
import fr.com.panda.databinding.RegisterActivityBinding;
import fr.com.panda.ui.base.ViewModel;
import fr.com.panda.ui.base.ViewModelActivity;

/**
 * @author Edouard Roussillon
 */

public class RegisterActivity extends ViewModelActivity<RegisterViewModel, RegisterActivityBinding> {

    @Override
    protected RegisterViewModel onCreateViewModel(RegisterActivityBinding binding) {
        return new RegisterViewModel(this, binding);
    }

    @Override
    protected int getLayout() {
        return R.layout.register_activity;
    }


    @Override
    protected void onBindViewModel() {
        super.onBindViewModel();

        this.binding.setViewModel(this.viewModel);
    }

    //
    // Animation
    //

    public void animLogoError() {

    }

    //
    // Intent
    //

    public void launchLogin() {
        this.finish();
    }
}
