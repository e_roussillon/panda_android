package fr.com.panda.ui.friendship;


import android.view.MenuItem;

import fr.com.panda.R;
import fr.com.panda.databinding.TabFriendshipFragmentBinding;
import fr.com.panda.ui.base.ViewModelFragment;

public class TabFriendshipFragment extends ViewModelFragment<TabFriendshipViewModel, TabFriendshipFragmentBinding> {

    public static TabFriendshipFragment newInstance() {
        TabFriendshipFragment f = new TabFriendshipFragment();

        return f;
    }

    @Override
    protected TabFriendshipViewModel onCreateViewModel(TabFriendshipFragmentBinding binding) {
        return new TabFriendshipViewModel(this, binding);
    }

    @Override
    protected int getLayout() {
        return R.layout.tab_friendship_fragment;
    }

    @Override
    protected void onBindViewModel() {
        super.onBindViewModel();

        this.binding.friendshipToolbar.inflateMenu(R.menu.menu_search);
        final MenuItem item = this.binding.friendshipToolbar.getMenu().findItem(R.id.menu_filter_remove);

        this.binding.searchView.setMenuItem(item);
        this.binding.friendshipToolbar.setTitle(R.string.friendship_screen_title);
        this.binding.setViewModel(this.viewModel);
    }



}
