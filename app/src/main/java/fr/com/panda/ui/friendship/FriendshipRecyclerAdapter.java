package fr.com.panda.ui.friendship;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import fr.com.panda.databinding.FriendshipItemAdapterBinding;
import fr.com.panda.enums.FriendshipTabType;
import fr.com.panda.model.UserModel;

/**
 * @author Edouard Roussillon
 */

public class FriendshipRecyclerAdapter extends RecyclerView.Adapter<FriendshipRecyclerAdapter.FriendshipHolder> {

    private @FriendshipTabType.FriendshipTabMode int listType;
    private FriendshipViewModel.FriendshipItemClicked friendshipItemClicked;
    private List<UserModel> list = new ArrayList<>();

    public FriendshipRecyclerAdapter(FriendshipViewModel.FriendshipItemClicked friendshipItemClicked, @FriendshipTabType.FriendshipTabMode int listType) {
        this.friendshipItemClicked = friendshipItemClicked;
        this.listType = listType;
    }

    private void loadList() {
        switch (listType) {
            case FriendshipTabType.ACCEPTED:
                list = UserModel.getUsersByFriendshipTypeFriend();
                break;
            case FriendshipTabType.PENDING:
                list = UserModel.getUsersByFriendshipTypePending();
                break;
            case FriendshipTabType.BLOCKED:
                list = UserModel.getUsersByFriendshipTypeBlocked();
                break;
        }
    }

    public void reloadList() {
        this.loadList();
        this.notifyDataSetChanged();
    }

    @Override
    public FriendshipHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        FriendshipItemAdapterBinding itemBinding = FriendshipItemAdapterBinding.inflate(layoutInflater, parent, false);
        return new FriendshipHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(FriendshipHolder holder, int position) {
        UserModel item = this.list.get(position);
//        item.setBindingPosition(position);
        holder.bind(friendshipItemClicked, item);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public boolean showEmptyState() {
        return list.size() == 0;
    }

    public static class FriendshipHolder extends RecyclerView.ViewHolder {

        private final FriendshipItemAdapterBinding binding;

        public FriendshipHolder(FriendshipItemAdapterBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(FriendshipViewModel.FriendshipItemClicked friendshipItemClicked, UserModel item) {
            binding.setViewModel(new FriendshipViewModel(item, friendshipItemClicked));
            binding.executePendingBindings();
        }
    }
}
