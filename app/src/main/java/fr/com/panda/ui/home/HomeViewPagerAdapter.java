package fr.com.panda.ui.home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import fr.com.panda.ui.chat.TabChatFragment;
import fr.com.panda.ui.filter.TabFilterFragment;
import fr.com.panda.ui.friendship.TabFriendshipFragment;

/**
 * @author Edouard Roussillon
 */

public class HomeViewPagerAdapter extends FragmentStatePagerAdapter {

    public HomeViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return TabFriendshipFragment.newInstance();
            case 1:
                return TabChatFragment.newInstance();
            default:
                return TabFilterFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
