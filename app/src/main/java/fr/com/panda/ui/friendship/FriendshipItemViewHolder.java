package fr.com.panda.ui.friendship;


import fr.com.panda.databinding.FriendshipItemAdapterBinding;
import fr.com.panda.ui.base.adapter.BaseItemViewHolder;
import fr.com.panda.ui.search.*;

/**
 * @author Edouard Roussillon
 */

public class FriendshipItemViewHolder extends BaseItemViewHolder {

    private FriendshipItemAdapterBinding friendshipItemAdapterBinding;

    public FriendshipItemViewHolder(FriendshipItemAdapterBinding friendshipItemAdapterBinding) {
        super(friendshipItemAdapterBinding.getRoot());
        this.friendshipItemAdapterBinding = friendshipItemAdapterBinding;
    }

    public void bind(FriendshipViewModel friendshipItemViewModel) {
        friendshipItemAdapterBinding.setViewModel(friendshipItemViewModel);
    }

    @Override
    public void bind(SearchItemViewModel searchItemViewModel) {
        friendshipItemAdapterBinding.setViewModel(searchItemViewModel.getFriendshipItemViewModel());
    }
}
