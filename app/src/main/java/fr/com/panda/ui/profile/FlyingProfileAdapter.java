package fr.com.panda.ui.profile;

import android.content.Context;
import android.databinding.ViewDataBinding;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

import fr.com.panda.R;
import fr.com.panda.databinding.FlyingProfileItemActionBinding;
import fr.com.panda.databinding.FlyingProfileItemBinding;
import fr.com.panda.databinding.ViewHeaderBinding;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.enums.ValidatorType;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.model.UserModel;
import fr.com.panda.ui.base.adapter.BaseItemViewHolder;
import fr.com.panda.ui.base.adapter.HeaderItemViewHolder;
import fr.com.panda.ui.profile.adapter.FlyingActionItemHolder;
import fr.com.panda.ui.profile.adapter.FlyingActionItemViewModel;
import fr.com.panda.ui.profile.adapter.FlyingItemViewModel;
import fr.com.panda.ui.profile.adapter.FlyingProfileItemHolder;
import fr.com.panda.utils.DateUtil;

/**
 * @author Edouard Roussillon
 */

public class FlyingProfileAdapter extends RecyclerView.Adapter<BaseItemViewHolder> {

    private LayoutInflater layoutInflater;

    private List<FlyingItemViewModel> list = new ArrayList();
    private SessionManager sessionManager = SessionManager.sharedInstance;
    private FlyingActionItemViewModel.FlyingAction flyingAction;

    private boolean actionSection = false;
    private boolean buttonBlockedSection = false;

    public FlyingProfileAdapter(Context context, @NonNull UserModel user, FlyingActionItemViewModel.FlyingAction flyingAction) {
        this.flyingAction = flyingAction;

        this.updateAdapter(context, user);
    }

    public void updateAdapter(Context context, @NonNull UserModel user) {
        this.list = new ArrayList();

        this.actionSection = getActionSection(user);
        this.buttonBlockedSection = getButtonsBottomSection(user);

        if (this.actionSection) {
            list.add(new FlyingItemViewModel(R.string.flying_profile_header_none));
            list.addAll(initActionButton(user));
        }

        list.add(new FlyingItemViewModel(R.string.flying_profile_header_details));
        list.addAll(this.getDetailsUser(context, user));

        if (this.buttonBlockedSection) {
            list.add(new FlyingItemViewModel(R.string.flying_profile_header_none));
            list.addAll(this.initButtonBlocked(user));
        }

        if (!user.reported) {
            if (!this.buttonBlockedSection) {
                list.add(new FlyingItemViewModel(R.string.flying_profile_header_none));
            }
            list.add(this.addIconReportedUser());
        }

        this.notifyDataSetChanged();
    }

    private List<FlyingItemViewModel> initActionButton(UserModel user) {
        List<FlyingItemViewModel> list = new ArrayList();
        switch (user.friendshipStatus) {
            case FriendshipType.NOT_FRIEND:
            case FriendshipType.CANCEL_REQUEST:
            case FriendshipType.REFUSED_AFTER_BE_FRIEND:
                list.add(this.addIconAddUser());
                break;
            case FriendshipType.PENDING:
                if (user.friendshipLastAction == sessionManager.user().getUserId()) {
                    list.add(this.addIconPendingUser());
                } else {
                    list.add(this.addIconsAcceptFriend(0));
                    list.add(this.addIconsAcceptFriend(1));
                }
                break;
            case FriendshipType.REFUSED_BEFORE_BE_FRIEND:
                list.add(this.addIconAddUser());
                break;
            case FriendshipType.FRIEND:
                list.add(this.addIconRemoveUser());
                break;
        }

        return list;
    }

    private List<FlyingItemViewModel> getDetailsUser(Context context, UserModel user) {
        List<FlyingItemViewModel> list = new ArrayList();

        list.add(new FlyingItemViewModel(R.string.generic_label_gender, user.getGenderType()));

        if (user.birthday != null) {
            list.add(new FlyingItemViewModel(R.string.generic_label_birthday, DateUtil.getMediumStyleDate(user.birthday)));
        } else {
            list.add(new FlyingItemViewModel(R.string.generic_label_birthday, context.getString(R.string.generic_label_none)));
        }

        if (!TextUtils.isEmpty(user.details)) {
            list.add(new FlyingItemViewModel(R.string.generic_label_description, user.details));
        } else {
            list.add(new FlyingItemViewModel(R.string.generic_label_description, context.getString(R.string.generic_label_none)));
        }
        return list;
    }


    private List<FlyingItemViewModel> initButtonBlocked(UserModel user) {
        List<FlyingItemViewModel> list = new ArrayList();

        switch (user.friendshipStatus) {
            case FriendshipType.BLOCKED_AFTER_BE_FRIEND:
            case FriendshipType.BLOCKED_BEFORE_BE_FRIEND:
                if (user.friendshipLastAction == sessionManager.user().getUserId()) {
                    list.add(this.addIconUnblockUser());
                }
                break;
            case FriendshipType.FRIEND:
                list.add(this.addIconBlockedUserAfterBeFriend());
                break;
            default:
                list.add(this.addIconBlockedUserBeforeBeFriend());
                break;
        }

        return list;
    }

    private FlyingItemViewModel addIconsAcceptFriend(int row) {
        switch (row) {
            case 0:
                return new FlyingItemViewModel(false, R.string.friendship_action_accept_title, R.drawable.user_add, FriendshipType.FRIEND, this.flyingAction);
            case 1:
                return new FlyingItemViewModel(false, R.string.friendship_action_refuse_title, R.drawable.user_remove, FriendshipType.REFUSED_BEFORE_BE_FRIEND, this.flyingAction);
            default:
                return this.addIconBlockedUserBeforeBeFriend();
        }
    }

    private FlyingItemViewModel addIconPendingUser() {
        return new FlyingItemViewModel(false, R.string.friendship_action_cancel_title, R.drawable.user_add, FriendshipType.CANCEL_REQUEST, this.flyingAction);
    }

    private FlyingItemViewModel addIconAddUser() {
        return new FlyingItemViewModel(true, R.string.friendship_action_add_title, R.drawable.user_add, FriendshipType.PENDING, this.flyingAction);
    }

    private FlyingItemViewModel addIconRemoveUser() {
        return new FlyingItemViewModel(false, R.string.friendship_action_remove_title, R.drawable.user_remove, FriendshipType.REFUSED_AFTER_BE_FRIEND, this.flyingAction);
    }

    private FlyingItemViewModel addIconBlockedUserBeforeBeFriend() {
        return this.addIconBlockWith(FriendshipType.BLOCKED_BEFORE_BE_FRIEND);
    }

    private FlyingItemViewModel addIconBlockedUserAfterBeFriend() {
        return this.addIconBlockWith(FriendshipType.BLOCKED_AFTER_BE_FRIEND);
    }

    private FlyingItemViewModel addIconBlockWith(@FriendshipType.FriendshipMode int tag) {
        return new FlyingItemViewModel(false, R.string.friendship_action_block_title, R.drawable.user_blocked, tag, this.flyingAction);
    }

    private FlyingItemViewModel addIconUnblockUser() {
        return new FlyingItemViewModel(true, R.string.friendship_action_unblock_title, R.drawable.user_unblock, FriendshipType.UNBLOCKED, this.flyingAction);
    }

    private FlyingItemViewModel addIconReportedUser() {
        return new FlyingItemViewModel(false, R.string.friendship_action_report_title, R.drawable.user_reported, FriendshipType.REPORTED, this.flyingAction);
    }

    public boolean getActionSection(UserModel user) {
        switch (user.friendshipStatus) {
            case FriendshipType.BLOCKED_BEFORE_BE_FRIEND:
            case FriendshipType.BLOCKED_AFTER_BE_FRIEND:
                return false;
            default:
                return true;
        }
    }

    public boolean getButtonsBottomSection(UserModel user) {
        final int lastAction = user.friendshipLastAction;

        if ((user.friendshipStatus == FriendshipType.BLOCKED_BEFORE_BE_FRIEND &&  lastAction != sessionManager.user().getUserId())
                || (user.friendshipStatus == FriendshipType.BLOCKED_AFTER_BE_FRIEND &&  lastAction != sessionManager.user().getUserId())) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        final FlyingItemViewModel item = list.get(position);

        if (item.isSection()) {
            return R.layout.view_header;
        } else if (item.isAction()) {
            return R.layout.flying_profile_item_action;
        } else {
            return R.layout.flying_profile_item;
        }
    }

    @Override
    public BaseItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        switch (viewType) {
            case R.layout.view_header:
                return new HeaderItemViewHolder(ViewHeaderBinding.inflate(layoutInflater, parent, false));
            case R.layout.flying_profile_item_action:
                return new FlyingActionItemHolder(FlyingProfileItemActionBinding.inflate(layoutInflater, parent, false));
            case R.layout.flying_profile_item:
                return new FlyingProfileItemHolder(FlyingProfileItemBinding.inflate(layoutInflater, parent, false));
        }

        return null;
    }

    @Override
    public void onBindViewHolder(BaseItemViewHolder holder, int position) {
        holder.bind(list.get(position));
    }
}
