package fr.com.panda.ui.friendship;

import android.databinding.Bindable;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import fr.com.panda.R;
import fr.com.panda.databinding.TabFriendshipFragmentBinding;
import fr.com.panda.ui.base.ViewModelTab;
import fr.com.panda.ui.search.SearchViewModel;
import fr.com.panda.BR;

/**
 * @author Edouard Roussillon
 */

public class TabFriendshipViewModel extends ViewModelTab implements MaterialSearchView.SearchViewListener {

    @Nullable
    private TabFriendshipFragment fragment;
    @Nullable
    private TabFriendshipFragmentBinding binding;

    private int showBottomBar = View.VISIBLE;

    public SearchViewModel searchViewModel;

    public TabFriendshipViewModel(TabFriendshipFragment fragment, TabFriendshipFragmentBinding binding) {
        this.fragment = fragment;
        this.binding = binding;
        this.searchViewModel = new SearchViewModel(fragment, binding, this);
        this.listViewModelChild.add(searchViewModel);
    }

    @Override
    protected ViewGroup getParent() {
        return this.binding.friendshipContainer;
    }

    @Override
    public View getRoot() {
        return this.binding.getRoot();
    }

    @Override
    public void bind() {
        super.bind();

        if (this.binding != null) {
            this.binding.friendshipBottomBar.setOnTabSelectListener(this);
        }
    }

    @Override
    public void unbind() {
        super.unbind();

        this.fragment = null;
        this.binding = null;
    }

    @Bindable
    public int getShowBottomBar() {
        return showBottomBar;
    }

    public void setShowBottomBar(int showBottomBar) {
        this.showBottomBar = showBottomBar;
        notifyPropertyChanged(BR.showBottomBar);
    }

    //
    // Click
    //

    @Override
    public void onTabSelected(@IdRes int tabId) {
        switch (tabId) {
            case R.id.tab_friend:
                setCurrentViewModel(new FriendshipAcceptedViewModel(this.fragment, this.getParent()));
                break;
            case R.id.tab_pending:
                setCurrentViewModel(new FriendshipPendingViewModel(this.fragment, this.getParent()));
                break;
            case R.id.tab_blocked:
                setCurrentViewModel(new FriendshipBlockedViewModel(this.fragment, this.getParent()));
                break;
        }
    }

    //
    // MaterialSearchView.SearchViewListener
    //

    @Override
    public void onSearchViewShown() {
        //Should do nothing
    }

    @Override
    public void onSearchViewClosed() {
        if (getCurrentViewModel() instanceof FriendshipViewModel) {
            ((FriendshipViewModel) getCurrentViewModel()).reloadList();
        }
    }
}
