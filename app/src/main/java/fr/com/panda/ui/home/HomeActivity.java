package fr.com.panda.ui.home;

import fr.com.panda.R;
import fr.com.panda.databinding.HomeActivityBinding;
import fr.com.panda.ui.base.ViewModelActivity;

/**
 * @author Edouard Roussillon
 */

public class HomeActivity extends ViewModelActivity<HomeViewModel, HomeActivityBinding> {

    @Override
    protected HomeViewModel onCreateViewModel(HomeActivityBinding binding) {
        return new HomeViewModel(this, binding);
    }

    @Override
    protected int getLayout() {
        return R.layout.home_activity;
    }

}
