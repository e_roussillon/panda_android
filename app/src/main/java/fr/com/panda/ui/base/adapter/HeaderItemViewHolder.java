package fr.com.panda.ui.base.adapter;

import fr.com.panda.databinding.ViewHeaderBinding;
import fr.com.panda.ui.base.adapter.BaseItemViewHolder;
import fr.com.panda.ui.base.util.HeaderViewModel;
import fr.com.panda.ui.profile.adapter.FlyingItemViewModel;
import fr.com.panda.ui.search.SearchItemViewModel;

/**
 * @author Edouard Roussillon
 */

public class HeaderItemViewHolder extends BaseItemViewHolder {

    private ViewHeaderBinding viewHeaderBinding;

    public HeaderItemViewHolder(ViewHeaderBinding viewHeaderBinding) {
        super(viewHeaderBinding.getRoot());
        this.viewHeaderBinding = viewHeaderBinding;
    }

    public void bind(HeaderViewModel headerViewModel) {
        this.viewHeaderBinding.setHeader(headerViewModel);
    }

    @Override
    public void bind(SearchItemViewModel searchItemViewModel) {
        this.viewHeaderBinding.setHeader(searchItemViewModel.getHeaderViewModel());
    }

    @Override
    public void bind(FlyingItemViewModel flyingItemViewModel) {
        this.viewHeaderBinding.setHeader(flyingItemViewModel.getHeaderViewModel());
    }
}
