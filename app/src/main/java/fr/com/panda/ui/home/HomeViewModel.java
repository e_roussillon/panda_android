package fr.com.panda.ui.home;

import android.databinding.Bindable;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.view.View;

import fr.com.panda.BR;
import fr.com.panda.databinding.HomeActivityBinding;
import fr.com.panda.ui.base.ViewModel;

/**
 * @author Edouard Roussillon
 */

public class HomeViewModel extends ViewModel {

    @Nullable
    private HomeActivity homeActivity;
    @Nullable
    private HomeActivityBinding binding;

    private HomeViewPagerAdapter adapter;

    private boolean enablePageScroll = true;

    public HomeViewModel(HomeActivity homeActivity, HomeActivityBinding bindin) {
        this.homeActivity = homeActivity;
        this.binding = bindin;
    }

    //
    // Bind
    //

    @Override
    public View getRoot() {
        return this.binding.getRoot();
    }

    @Override
    public void bind() {
        super.bind();

        adapter = new HomeViewPagerAdapter(this.homeActivity.getSupportFragmentManager());
        this.binding.viewPager.setAdapter(adapter);
        this.binding.viewPager.setCurrentItem(1);
    }

    @Override
    public void resume() {
        super.resume();

        if (!this.getSessionManager().user().isLogin()) {
//            this.homeActivity.finish();
            this.homeActivity.launchSplash();
        }
    }

    @Override
    public void unbind() {
        super.unbind();

        this.homeActivity = null;
        this.binding = null;
    }

    @Bindable
    public boolean isEnablePageScroll() {
        return enablePageScroll;
    }

    public void setEnablePageScroll(boolean enablePageScroll) {
        this.enablePageScroll = enablePageScroll;
        notifyPropertyChanged(BR.enablePageScroll);
    }

    //
    // State
    //

    @Override
    public void restoreInstance(ViewModel.State state) {
        super.restoreInstance(state);

        if (state instanceof State) {
            State stateHome = (State) state;
            this.binding.viewPager.setCurrentItem(stateHome.position);
        }
    }

    @Override
    public ViewModel.State saveInstanceState() {
        return new State(this);
    }



    private static class State extends ViewModel.State {

        public static final Creator<State> CREATOR = new Creator<State>() {
            @Override
            public State createFromParcel(Parcel in) {
                return new State(in);
            }

            @Override
            public State[] newArray(int size) {
                return new State[size];
            }
        };

        @Nullable
        private int position;

        public State(HomeViewModel homeViewModel) {
            super(homeViewModel);
            this.position = homeViewModel.binding.viewPager.getCurrentItem();
        }

        public State(android.os.Parcel parcel) {
            super(parcel);
            this.position = parcel.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (dest != null) {
                dest.writeInt(position);
            }
        }
    }
}
