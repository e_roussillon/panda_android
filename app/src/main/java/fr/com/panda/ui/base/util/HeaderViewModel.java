package fr.com.panda.ui.base.util;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.StringRes;

/**
 * @author Edouard Roussillon
 */

public class HeaderViewModel extends BaseObservable {

    private @StringRes int headerMessage;

    public HeaderViewModel(@StringRes int headerMessage) {
        this.headerMessage = headerMessage;
    }

    @Bindable
    public int getHeaderMessage() {
        return headerMessage;
    }
}
