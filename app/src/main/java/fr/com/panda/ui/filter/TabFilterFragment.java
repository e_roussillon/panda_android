package fr.com.panda.ui.filter;


import fr.com.panda.R;
import fr.com.panda.databinding.TabFilterFragmentBinding;
import fr.com.panda.enums.ErrorType;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.ui.base.ViewModelFragment;

public class TabFilterFragment extends ViewModelFragment<TabFilterViewModel, TabFilterFragmentBinding> {

    public static TabFilterFragment newInstance() {
        TabFilterFragment f = new TabFilterFragment();
        return f;
    }

    @Override
    protected TabFilterViewModel onCreateViewModel(TabFilterFragmentBinding binding) {
        return new TabFilterViewModel(this, binding);
    }

    @Override
    protected int getLayout() {
        return R.layout.tab_filter_fragment;
    }

    @Override
    protected void onBindViewModel() {
        super.onBindViewModel();

        this.binding.setViewModel(this.viewModel);
    }

    //
    // Handler
    //

    public void showAlertFilter(ErrorResponse errorResponse) {
        if (errorResponse != null && errorResponse.fields != null && errorResponse.fields.size() > 0 && errorResponse.type == ErrorType.CHANGESET) {
            errorResponse.fields.stream().filter(item ->
                    item.field.equals("name_user_id")
            ).forEach(item ->
                    this.showDialogAlert(R.string.dialog_filter_message_error_duplica
            ));
        }
    }
}
