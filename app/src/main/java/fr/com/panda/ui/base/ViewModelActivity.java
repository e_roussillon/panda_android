package fr.com.panda.ui.base;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * @author Edouard Roussillon
 */

public abstract class ViewModelActivity<VM extends ViewModel, B extends ViewDataBinding> extends BaseActivity {

    private String STATE_VIEW_MODEL = "state.VIEW_MODEL_ACTIVITY";
    public VM viewModel;

    protected B binding;

    protected abstract VM onCreateViewModel(B binding);

    protected abstract @LayoutRes int getLayout();

    @CallSuper
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.binding = DataBindingUtil.setContentView(this, this.getLayout());
        this.viewModel = onCreateViewModel(this.binding);

        this.onBindViewModel();
    }

    @CallSuper
    @Override
    protected void onStart() {
        super.onStart();

        this.onStartViewModel();
        this.initErrorHttpHandle(this.viewModel.getBag());
    }

    @CallSuper
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        this.onRestoreInstanceStateViewModel(savedInstanceState);
    }

    @CallSuper
    @Override
    protected void onResume() {
        super.onResume();

        this.onResumeViewModel();
    }

    @CallSuper
    @Override
    protected void onStop() {
        super.onStop();

        this.onStopViewModel();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        this.onSaveInstanceStateViewModel(outState);
    }

    @CallSuper
    @Override
    protected void onDestroy() {
        super.onDestroy();

        this.onUnbindViewModel();
    }

    //
    // Keyboard
    //

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //
    // ViewModel Lifecycle
    //

    @CallSuper
    protected void onBindViewModel() {
        this.viewModel.bind();
    }

    @CallSuper
    protected void onStartViewModel() { this.viewModel.start(); }

    @CallSuper
    protected void onRestoreInstanceStateViewModel(Bundle savedInstanceState) { this.viewModel.restoreInstance(savedInstanceState.getParcelable(STATE_VIEW_MODEL)); }

    @CallSuper
    protected  void onResumeViewModel() { this.viewModel.resume(); }

    @CallSuper
    protected void onStopViewModel() { this.viewModel.stop(); }

    @CallSuper
    protected void onSaveInstanceStateViewModel(Bundle outState) { outState.putParcelable(STATE_VIEW_MODEL, this.viewModel.saveInstanceState()); }

    @CallSuper
    protected void onUnbindViewModel() {
        this.viewModel.unbind();
    }


}
