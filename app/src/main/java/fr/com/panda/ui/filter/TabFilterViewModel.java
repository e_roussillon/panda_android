package fr.com.panda.ui.filter;

import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import fr.com.panda.R;
import fr.com.panda.databinding.TabFilterFragmentBinding;
import fr.com.panda.ui.base.ViewModelTab;

/**
 * @author Edouard Roussillon
 */

public class TabFilterViewModel extends ViewModelTab {

    @Nullable
    private TabFilterFragment fragment;
    @Nullable
    private TabFilterFragmentBinding binding;

    TabFilterViewModel(TabFilterFragment fragment, TabFilterFragmentBinding binding) {
        this.fragment = fragment;
        this.binding = binding;
    }

    @Override
    protected ViewGroup getParent() {
        return this.binding.filterContainer;
    }

    @Override
    public View getRoot() {
        return this.binding.getRoot();
    }

    @Override
    public void bind() {
        super.bind();

        if (this.binding != null) {
            this.binding.filterBottomBar.setOnTabSelectListener(this);
        }
    }

    @Override
    public void unbind() {
        super.unbind();

        this.fragment = null;
        this.binding = null;
    }

    //
    // Click
    //

    @Override
    public void onTabSelected(@IdRes int tabId) {
        switch (tabId) {
            case R.id.tab_like:
                setCurrentViewModel(new FilterLikeViewModel(this.fragment, this.getParent()));
                break;
            case R.id.tab_dislike:
                setCurrentViewModel(new FilterDislikeViewModel(this.fragment, this.getParent()));
                break;
        }
    }
}
