package fr.com.panda.ui.base;

import android.os.Parcel;
import android.support.annotation.CallSuper;
import android.support.annotation.IdRes;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.raizlabs.android.dbflow.annotation.NotNull;
import com.roughike.bottombar.OnTabSelectListener;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;

import fr.com.panda.utils.ParcelUtil;

/**
 * @author Edouard Roussillon
 */

public abstract class ViewModelTab extends ViewModel implements OnTabSelectListener {

    @IntDef({STATE_BIND, STATE_START, STATE_RESUME, STATE_STOP, STATE_UNBIND})
    @Retention(RetentionPolicy.SOURCE)
    public @interface StateViewModel {}

    private Map<String, ViewModel.State> tabsState = new HashMap();

    @Nullable
    private ViewModel currentViewModel;
    protected abstract ViewGroup getParent();

    private static final int STATE_BIND   = 0;
    private static final int STATE_START  = 1;
    private static final int STATE_RESUME = 2;
    private static final int STATE_STOP   = 3;
    private static final int STATE_UNBIND = 4;

    private @StateViewModel int stateViewModel = STATE_BIND;


    @CallSuper
    @Override
    public void bind() {
        super.bind();
        this.stateViewModel = STATE_BIND;

        if (this.currentViewModel != null) {
            this.currentViewModel.bind();
        }
    }

    @CallSuper
    @Override
    public void start() {
        super.start();
        this.stateViewModel = STATE_START;

        if (this.currentViewModel != null) {
            this.currentViewModel.start();
        }
    }

    @CallSuper
    @Override
    public void resume() {
        super.resume();
        this.stateViewModel = STATE_RESUME;

        if (this.currentViewModel != null) {
            this.currentViewModel.resume();
        }
    }

    @CallSuper
    @Override
    public void stop() {
        super.stop();
        this.stateViewModel = STATE_STOP;

        if (this.currentViewModel != null) {
            this.currentViewModel.stop();
        }
    }

    @CallSuper
    @Override
    public void unbind() {
        super.unbind();
        this.stateViewModel = STATE_UNBIND;

        if (this.currentViewModel != null) {
            this.currentViewModel.unbind();
        }
    }

    //
    // CurrentViewModel
    //

    @Nullable
    public ViewModel getCurrentViewModel() {
        return currentViewModel;
    }

    public void setCurrentViewModel(@NotNull ViewModel currentViewModel) {

        if (this.currentViewModel != null) {
            tabsState.put(this.currentViewModel.getClass().getName(), unbindLifecycleGroupViewModel(getParent(), this.currentViewModel.getRoot()));
        }

        this.bindLifecycleGroupViewModel(currentViewModel);
        this.currentViewModel = currentViewModel;
    }

    private void bindLifecycleGroupViewModel(@NotNull ViewModel currentViewModel) {
        ViewModel.State restoreInstance = tabsState.get(currentViewModel.getClass().getName());

        switch (this.stateViewModel) {
            case STATE_BIND:
                currentViewModel.bind();
                break;
            case STATE_START:
                currentViewModel.bind();
                currentViewModel.start();
                break;
            case STATE_RESUME:
                currentViewModel.bind();
                currentViewModel.start();
                currentViewModel.resume();
                break;
            case STATE_STOP:
                currentViewModel.stop();
                break;
            case STATE_UNBIND:
                currentViewModel.unbind();
                break;
        }

        if (restoreInstance != null) {
            currentViewModel.restoreInstance(restoreInstance);
        }
    }

    private ViewModel.State unbindLifecycleGroupViewModel(ViewGroup parent, View view) {
        currentViewModel.stop();
        final ViewModel.State state = currentViewModel.saveInstanceState();
        currentViewModel.unbind();
        parent.removeView(view);
        return state;
    }


    //
    // State
    //

    @CallSuper
    @Override
    public State saveInstanceState() {
        super.saveInstanceState();
        if (this.currentViewModel != null) {
            tabsState.put(this.currentViewModel.getClass().getName(), currentViewModel.saveInstanceState());
        }

        return new State(this);
    }

    @CallSuper
    @Override
    public void restoreInstance(ViewModel.State state) {
        super.restoreInstance(state);

        if (state instanceof State) {
            ViewModelTab.State stateViewTab = (ViewModelTab.State) state;
            this.tabsState = stateViewTab.tabsState;

            if (this.currentViewModel != null && this.tabsState != null) {
                ViewModel.State statesView = this.tabsState.get(this.currentViewModel.getClass().getName());
                if (statesView != null) {
                    this.currentViewModel.restoreInstance(statesView);
                }
            }
        }
    }

    public static class State extends ViewModel.State {

        public static final Creator<State> CREATOR = new Creator<State>() {
            @Override
            public State createFromParcel(Parcel in) {
                return new State(in);
            }

            @Override
            public State[] newArray(int size) {
                return new State[size];
            }
        };

        @Nullable
        private Map<String, ViewModel.State> tabsState;

        public State(ViewModelTab viewModelTab) {
            super(viewModelTab);

            this.tabsState = viewModelTab.tabsState;
        }

        public State(android.os.Parcel parcel) {
            super(parcel);

            tabsState = ParcelUtil.readMap(parcel);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (dest != null) {
                ParcelUtil.writeMap(dest, flags, this.tabsState);
            }
        }


    }
}
