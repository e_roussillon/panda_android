package fr.com.panda.ui.filter;

import android.databinding.ViewDataBinding;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import fr.com.panda.R;
import fr.com.panda.databinding.DialogEditTextBinding;
import fr.com.panda.databinding.FilterItemAdapterBinding;
import fr.com.panda.manager.FilterManager;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.model.FilterModel;
import fr.com.panda.ui.base.ViewModel;
import fr.com.panda.ui.base.util.DialogEditTextModel;
import fr.com.panda.ui.base.util.EmptyStateViewModel;
import fr.com.panda.ui.base.util.HeaderViewModel;
import fr.com.panda.utils.BackgroundUtil;

/**
 * @author Edouard Roussillon
 */

public abstract class FilterViewModel extends ViewModel implements FilterRecyclerAdapter.FilterClick, Toolbar.OnMenuItemClickListener {

    protected FilterManager filterManager = FilterManager.sharedInstance;
    protected boolean isHighlighted = true;
    @Nullable
    protected TabFilterFragment fragment;
    @Nullable
    protected ViewDataBinding binding;
    @Nullable
    protected LinearLayoutManager linearLayoutManager;
    @Nullable
    protected FilterRecyclerAdapter filterRecyclerAdapter;

    public EmptyStateViewModel emptyStateViewModel;
    public HeaderViewModel headerViewModel;

    protected DialogEditTextBinding dialogEditTextBinding;
    protected @StringRes int dialogMessage;
    protected boolean showRemove = false;

    public FilterViewModel(boolean isHighlighted, @StringRes int dialogMessage) {
        this.isHighlighted = isHighlighted;
        this.dialogMessage = dialogMessage;
    }

    @Override
    public void bind() {
        super.bind();

        this.filterRecyclerAdapter = new FilterRecyclerAdapter(this.fragment.getContext(), this, this.isHighlighted, this.showRemove);
        this.linearLayoutManager = new LinearLayoutManager(this.fragment.getContext());

        this.getToolbar().setTitle(R.string.filter_screen_title);
        this.showFilterRemove();
        this.getToolbar().setOnMenuItemClickListener(this);
    }

    @Override
    public void start() {
        super.start();

        this.getBag().add(filterManager.onAddFilterSucceed.add(this::onAddFilterSucceed));
        this.getBag().add(filterManager.onAddFilterFailed.add(this::onAddFilterFailed));

        this.getBag().add(filterManager.onUpdateFilterSucceed.add(this::onUpdateFilterSucceed));
        this.getBag().add(filterManager.onUpdateFilterFailed.add(this::onUpdateFilterFailed));
        this.getBag().add(filterManager.onUpdateFilterFailedMissingParam.add(this::onUpdateFilterFailedMissingParam));

        this.getBag().add(filterManager.onRemoveFilterSucceed.add(this::onRemoveFilterSucceed));
        this.getBag().add(filterManager.onRemoveFilterFailed.add(this::onRemoveFilterFailed));
    }

    @Override
    public void unbind() {
        super.unbind();

        this.filterManager = null;
        this.fragment = null;
        this.binding = null;
        this.linearLayoutManager = null;
        this.filterRecyclerAdapter = null;
        this.emptyStateViewModel = null;
        this.headerViewModel = null;
        this.dialogEditTextBinding = null;
    }

    @Override
    public View getRoot() {
        return binding.getRoot();
    }

    //
    // Get-Set
    //

    public void setShowRemove(boolean showRemove) {
        this.showRemove = showRemove;
        this.filterRecyclerAdapter.reLoadList(this.showRemove);
    }


    //
    // Toolbar.OnMenuItemClickListener
    //

    @Override
    public void onClickFilter(FilterItemAdapterBinding binding) {
        if (this.fragment == null) {
            return;
        }

        if (binding.getViewModel().getBindingPosition() == 0) {
            this.onAddFilter(binding);
        } else {
            this.onUpdateFilter(binding);
        }

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int id = item.getItemId();
        switch(id) {
            case R.id.menu_filter_remove:
                this.showFilterSave();
                return true;
            case R.id.menu_filter_save:
                this.showFilterRemove();
            default:
                return false;
        }
    }

    private void showFilterSave() {
        getToolbar().getMenu().clear();
        getToolbar().inflateMenu(R.menu.menu_filter_save);
        setShowRemove(true);
    }

    private void showFilterRemove() {
        getToolbar().getMenu().clear();
        getToolbar().inflateMenu(R.menu.menu_filter_remove);
        setShowRemove(false);
    }

    protected abstract Toolbar getToolbar();

    protected void onAddFilter(FilterItemAdapterBinding binding) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.fragment.getContext());

        final DialogEditTextModel dialogEditTextModel = new DialogEditTextModel();
        dialogEditTextModel.setInputType(InputType.TYPE_CLASS_TEXT);
        dialogEditTextModel.setHint(R.string.dialog_add_filter_title);
        View view = LayoutInflater.from(builder.getContext()).inflate(R.layout.dialog_edit_text, null, false);

        dialogEditTextBinding = DialogEditTextBinding.bind(view);
        dialogEditTextBinding.setViewModel(dialogEditTextModel);

        builder.setView(view);
        builder.setTitle(R.string.dialog_add_filter_title);
        builder.setMessage(dialogMessage);
        builder.setPositiveButton(R.string.dialog_button_ok, (dialog, whichButton) -> {
            if (TextUtils.isEmpty(dialogEditTextModel.getText().toString()))
                return;

            fragment.showLoader();
            filterManager.addFilter(dialogEditTextBinding.getViewModel().getText().toString(), this.isHighlighted);
        });

        builder.setNegativeButton(R.string.dialog_button_cancel, (dialog, whichButton) -> {
            dialog.cancel();
        });

        builder.setCancelable(false);

        AppCompatDialog appCompatDialog = builder.create();
        appCompatDialog.show();
    }

    protected void onUpdateFilter(FilterItemAdapterBinding binding) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.fragment.getContext());

        final DialogEditTextModel dialogEditTextModel = new DialogEditTextModel();
        dialogEditTextModel.setInputType(InputType.TYPE_CLASS_TEXT);
        dialogEditTextModel.setText(binding.getViewModel().getName());
        dialogEditTextModel.setHint(R.string.dialog_update_filter_title);
        View view = LayoutInflater.from(builder.getContext()).inflate(R.layout.dialog_edit_text, null, false);

        dialogEditTextBinding = DialogEditTextBinding.bind(view);
        dialogEditTextBinding.setViewModel(dialogEditTextModel);

        builder.setView(view);
        builder.setTitle(R.string.dialog_update_filter_title);
        builder.setMessage(dialogMessage);
        builder.setPositiveButton(R.string.dialog_button_ok, (dialog, whichButton) -> {
            if (TextUtils.isEmpty(dialogEditTextBinding.getViewModel().getText().toString()))
                return;

            binding.getViewModel().setName(dialogEditTextBinding.getViewModel().getText().toString());

            fragment.showLoader();
            filterManager.updateFilter(binding.getViewModel(), dialogEditTextModel.getText().toString());
        });

        builder.setNegativeButton(R.string.dialog_button_cancel, (dialog, whichButton) -> {
            dialog.cancel();
        });

        builder.setCancelable(false);

        AppCompatDialog appCompatDialog = builder.create();
        appCompatDialog.show();
    }

    //
    // FilterRecyclerAdapter.FilterClick
    //

    @Override
    public void onRemoveFilter(FilterItemAdapterBinding binding) {
        if (this.fragment == null) {
            return;
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(this.fragment.getContext());

        builder.setTitle(R.string.dialog_remove_filter_title);
        builder.setMessage(R.string.dialog_filter_message_remove);
        builder.setPositiveButton(R.string.dialog_button_ok, (dialog, whichButton) -> {
            fragment.showLoader();
            filterManager.removeFilter(binding.getViewModel());
        });

        builder.setNegativeButton(R.string.dialog_button_cancel, (dialog, whichButton) -> {
            dialog.cancel();
        });

        builder.setCancelable(false);

        AppCompatDialog appCompatDialog = builder.create();
        appCompatDialog.show();
    }

    //
    // Handler
    //

    private void onAddFilterSucceed(FilterModel filterModel) {
        BackgroundUtil.UI().execute(() -> {
            this.fragment.removeLoader();
            this.filterRecyclerAdapter.reLoadList(showRemove);
            this.emptyStateViewModel.setVisibility(this.filterRecyclerAdapter.showEmptyState());
        });
    }

    private void onAddFilterFailed(ErrorResponse errorResponse) {
        BackgroundUtil.UI().execute(() -> {
            this.fragment.removeLoader();
            this.fragment.showAlertFilter(errorResponse);
        });
    }

    private void onUpdateFilterSucceed(FilterModel filterModel) {
        BackgroundUtil.UI().execute(() -> {
            this.fragment.removeLoader();
            this.filterRecyclerAdapter.reLoadList(showRemove);
        });
    }

    private void onUpdateFilterFailed(ErrorResponse errorResponse) {
        BackgroundUtil.UI().execute(() -> {
            this.fragment.removeLoader();
            this.fragment.showAlertFilter(errorResponse);
        });
    }

    private void onUpdateFilterFailedMissingParam(ErrorResponse errorResponse) {
        BackgroundUtil.UI().execute(() -> {
            this.fragment.removeLoader();
        });
    }

    private void onRemoveFilterSucceed(Void v) {
        BackgroundUtil.UI().execute(() -> {
            this.fragment.removeLoader();
            this.filterRecyclerAdapter.reLoadList(showRemove);
            this.emptyStateViewModel.setVisibility(this.filterRecyclerAdapter.showEmptyState(showRemove));
            if (this.emptyStateViewModel.isVisibility()) {
                this.showFilterRemove();
            }
        });
    }

    private void onRemoveFilterFailed(ErrorResponse errorResponse) {
        BackgroundUtil.UI().execute(() -> {
            this.fragment.removeLoader();
            this.fragment.showAlertFilter(errorResponse);
        });
    }

    //
    // State
    //


    @Override
    public ViewModel.State saveInstanceState() {
        return new State(this);
    }

    @Override
    public void restoreInstance(ViewModel.State state) {
        super.restoreInstance(state);

        if (state instanceof State) {
            State stateFilter = (State)state;
            setShowRemove(stateFilter.showRemove);

            getToolbar().getMenu().clear();
            if (!stateFilter.showRemove) {
                getToolbar().inflateMenu(R.menu.menu_filter_remove);
            } else {
                getToolbar().inflateMenu(R.menu.menu_filter_save);
            }
        }
    }

    public static class State extends ViewModel.State {
        private boolean showRemove = false;

        public static final Creator<State> CREATOR = new Creator<State>() {
            @Override
            public State createFromParcel(Parcel in) {
                return new State(in);
            }

            @Override
            public State[] newArray(int size) {
                return new State[size];
            }
        };

        public State(FilterViewModel filterViewModel) {
            super(filterViewModel);

            this.showRemove = filterViewModel.showRemove;
        }

        public State(android.os.Parcel parcel) {
            super(parcel);

            this.showRemove = parcel.readInt() == 1 ? true : false;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);

            dest.writeInt(this.showRemove ? 1 : 0);
        }
    }

}
