package fr.com.panda.ui.base.util;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.StringRes;

import fr.com.panda.BR;

/**
 * @author Edouard Roussillon
 */

public class DialogEditTextModel extends BaseObservable {

    private CharSequence text;
    private int inputType;
    private @StringRes int hint;

    @Bindable
    public CharSequence getText() {
        return text;
    }

    public void setText(CharSequence text) {
        this.text = text;
        notifyPropertyChanged(BR.text);
    }

    @Bindable
    public int getInputType() {
        return inputType;
    }

    public void setInputType(int inputType) {
        this.inputType = inputType;
        notifyPropertyChanged(BR.inputType);
    }

    @Bindable
    public int getHint() {
        return hint;
    }

    public void setHint(int hint) {
        this.hint = hint;
        notifyPropertyChanged(BR.hint);
    }
}
