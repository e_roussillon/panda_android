package fr.com.panda.ui.profile.adapter;

import android.support.annotation.StringRes;

/**
 * @author Edouard Roussillon
 */

public class FlyingProfileItemViewModel {

    private @StringRes int title;
    private String subTitle;

    public FlyingProfileItemViewModel(@StringRes int title, String subTitle) {
        this.title = title;
        this.subTitle = subTitle;
    }

    public int getTitle() {
        return title;
    }

    public String getSubTitle() {
        return subTitle;
    }
}
