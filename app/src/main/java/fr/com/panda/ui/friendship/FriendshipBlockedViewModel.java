package fr.com.panda.ui.friendship;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.com.panda.R;
import fr.com.panda.databinding.FriendshipBlockedViewGroupBinding;
import fr.com.panda.databinding.FriendshipItemAdapterBinding;
import fr.com.panda.enums.FriendshipTabType;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.model.UserModel;
import fr.com.panda.ui.base.util.EmptyStateViewModel;

/**
 * @author Edouard Roussillon
 */

public class FriendshipBlockedViewModel extends FriendshipViewModel implements FriendshipViewModel.FriendshipItemClicked {

    private TabFriendshipFragment fragment;
    private FriendshipBlockedViewGroupBinding binding;
    private FriendshipRecyclerAdapter friendshipRecyclerAdapter;

    public FriendshipBlockedViewModel(TabFriendshipFragment fragment, ViewGroup viewGroup) {
        super(FriendshipTabType.BLOCKED, R.drawable.ic_dislike_big, R.string.friend_empty_state_blocked);

        LayoutInflater inflater = (LayoutInflater) fragment.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.fragment = fragment;
        this.binding = FriendshipBlockedViewGroupBinding.inflate(inflater, viewGroup, true);
    }

    @Override
    public void bind() {
        super.bind();

        this.friendshipRecyclerAdapter = new FriendshipRecyclerAdapter(this, this.friendshipTabMode);

        this.binding.friendshipBlockedRecycler.setLayoutManager(new LinearLayoutManager(this.binding.getRoot().getContext()));
        this.binding.friendshipBlockedRecycler.setAdapter(this.friendshipRecyclerAdapter);
        this.binding.setViewModel(this);
    }

    @Override
    public void start() {
        super.start();

        this.reloadList();
    }

    @Override
    public void reloadList() {
        this.friendshipRecyclerAdapter.reloadList();
        this.emptyStateViewModel.setVisibility(this.friendshipRecyclerAdapter.showEmptyState());
    }

    @Override
    public void unbind() {
        super.unbind();

        this.binding = null;
        this.friendshipRecyclerAdapter = null;
    }

    @Override
    public View getRoot() {
        return binding.getRoot();
    }

    //
    // FriendshipViewModel.FriendshipItemClicked
    //

    @Override
    public void launchChat(UserModel userModel) {
        if (this.fragment != null) {
            this.fragment.launchChat(userModel);
        }
    }

    @Override
    public void launchProfile(UserModel userModel) {
        if (this.fragment != null) {
            this.fragment.launchProfile(userModel);
        }
    }

    @Override
    public void updateFriendship(int userId, @FriendshipType.FriendshipMode int friendshipType) {
        //Should do nothing
    }
}
