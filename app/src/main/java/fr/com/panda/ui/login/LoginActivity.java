package fr.com.panda.ui.login;

import android.content.Intent;
import android.support.annotation.Nullable;

import fr.com.panda.R;
import fr.com.panda.databinding.LoginActivityBinding;
import fr.com.panda.ui.base.ViewModel;
import fr.com.panda.ui.base.ViewModelActivity;
import fr.com.panda.ui.register.RegisterActivity;

public class LoginActivity extends ViewModelActivity<LoginViewModel, LoginActivityBinding> {

    @Override
    protected LoginViewModel onCreateViewModel(LoginActivityBinding binding) {
        return new LoginViewModel(this, this.binding);
    }

    @Override
    protected int getLayout() {
        return R.layout.login_activity;
    }


    @Override
    protected void onBindViewModel() {
        super.onBindViewModel();

        this.binding.setViewModel(this.viewModel);
    }

    //
    // Animation
    //

    public void animLogoError() {

    }

    //
    // Launcher
    //

    public void launchRegister() {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        this.startActivity(intent);
    }
}
