package fr.com.panda.ui.profile;

import fr.com.panda.R;
import fr.com.panda.databinding.ProfileActivityBinding;
import fr.com.panda.ui.base.ViewModelActivity;

public class ProfileActivity extends ViewModelActivity<ProfileViewModel, ProfileActivityBinding> {


    @Override
    protected ProfileViewModel onCreateViewModel(ProfileActivityBinding binding) {
        return new ProfileViewModel(this, binding);
    }

    @Override
    protected int getLayout() {
        return R.layout.profile_activity;
    }
}
