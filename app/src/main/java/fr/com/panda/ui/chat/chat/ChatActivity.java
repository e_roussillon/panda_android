package fr.com.panda.ui.chat.chat;

import fr.com.panda.R;
import fr.com.panda.databinding.ChatActivityBinding;
import fr.com.panda.ui.base.ViewModelActivity;

public class ChatActivity extends ViewModelActivity<ChatViewModel, ChatActivityBinding> {

    @Override
    protected ChatViewModel onCreateViewModel(ChatActivityBinding binding) {
        return new ChatViewModel(this, binding);
    }

    @Override
    protected int getLayout() {
        return R.layout.chat_activity;
    }

}
