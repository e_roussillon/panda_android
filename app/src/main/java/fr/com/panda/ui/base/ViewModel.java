package fr.com.panda.ui.base;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.raizlabs.android.dbflow.annotation.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fr.com.panda.event.DisposableBag;
import fr.com.panda.http.ApiEventGlobal;
import fr.com.panda.manager.AnalyticsManager;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.model.LogoutReported;
import fr.com.panda.utils.ParcelUtil;

/**
 * @author Edouard Roussillon
 */

public abstract class ViewModel extends BaseObservable {

    //
    // Global
    //

    @NotNull
    private final DisposableBag bag = new DisposableBag();
    @NotNull
    private final AnalyticsManager analyticsManager = AnalyticsManager.sharedInstance;
    @NotNull
    private final SessionManager sessionManager = SessionManager.sharedInstance;

    @NotNull
    public final DisposableBag getBag() {
        return this.bag;
    }

    @NotNull
    public AnalyticsManager getAnalyticsManager() {
        return this.analyticsManager;
    }

    @NotNull
    public SessionManager getSessionManager() {
        return this.sessionManager;
    }

    public abstract View getRoot();

    public List<ViewModel> listViewModelChild = new ArrayList();

    private Map<String, ViewModel.State> childState = new HashMap();

    //
    // ViewModel Lifecycle
    //

    @CallSuper
    public void bind() {
        for (ViewModel item: listViewModelChild) {
            item.bind();
        }
    }

    @CallSuper
    public void start() {
        for (ViewModel item: listViewModelChild) {
            item.start();
        }
    }

    @CallSuper
    public void resume() {
        for (ViewModel item: listViewModelChild) {
            item.resume();
        }
    }


    @CallSuper
    public void stop() {
        this.bag.dispose();
        for (ViewModel item: listViewModelChild) {
            item.stop();
        }
    }

    @CallSuper
    public void unbind() {
        //We clean again the bag because in somecase android do not call stop
        this.bag.dispose();

        for (ViewModel item: listViewModelChild) {
            item.unbind();
        }
    }

    @CallSuper
    public State saveInstanceState() {
        for (ViewModel item: listViewModelChild) {
            childState.put(item.getClass().getName(), item.saveInstanceState());
        }
        return new State(this);
    }

    @CallSuper
    public void restoreInstance(State state) {
        this.childState = state.childState;

        Iterator it = this.childState.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();

            for (ViewModel item: listViewModelChild) {
                if (item.getClass().getName().equals(pair.getValue())) {
                    item.restoreInstance((State) pair.getValue());
                    break;
                }
            }
            it.remove(); // avoids a ConcurrentModificationException
        }
    }

    //
    // State
    //

    public static class State implements Parcelable {

        @Nullable
        private Map<String, ViewModel.State> childState;

        public State(ViewModel viewModel) {
            this.childState = viewModel.childState;
        }

        public State(Parcel parcel) {
            this.childState = ParcelUtil.readMap(parcel);
        }

        public static final Creator<State> CREATOR = new Creator<State>() {
            @Override
            public State createFromParcel(Parcel in) {
                return new State(in);
            }

            @Override
            public State[] newArray(int size) {
                return new State[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (dest != null) {
                ParcelUtil.writeMap(dest, flags, this.childState);
            }
        }
    }
}
