package fr.com.panda.ui.profile.adapter;

import fr.com.panda.databinding.FlyingProfileItemActionBinding;
import fr.com.panda.ui.base.adapter.BaseItemViewHolder;

/**
 * @author Edouard Roussillon
 */

public class FlyingActionItemHolder extends BaseItemViewHolder {

    private FlyingProfileItemActionBinding flyingProfileItemActionBinding;

    public FlyingActionItemHolder(FlyingProfileItemActionBinding flyingProfileItemActionBinding) {
        super(flyingProfileItemActionBinding.getRoot());

        this.flyingProfileItemActionBinding = flyingProfileItemActionBinding;
    }

    @Override
    public void bind(FlyingItemViewModel flyingItemViewModel) {
        super.bind(flyingItemViewModel);

        this.flyingProfileItemActionBinding.setViewModel(flyingItemViewModel.getFlyingActionItemViewModel());
    }
}
