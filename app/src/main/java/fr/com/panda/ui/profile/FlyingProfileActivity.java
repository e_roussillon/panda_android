package fr.com.panda.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;

import fr.com.panda.R;
import fr.com.panda.databinding.FlyingProfileActivityBinding;
import fr.com.panda.enums.FriendshipType;
import fr.com.panda.model.UserModel;
import fr.com.panda.ui.base.ViewModelActivity;
import fr.com.panda.ui.profile.adapter.FlyingActionItemViewModel;

public class FlyingProfileActivity extends ViewModelActivity<FlyingProfileViewModel, FlyingProfileActivityBinding> {

    private UserModel userModel;

    private static String TITLE_USER_MODEL = "TITLE_USER_MODEL";

    public static Intent initIntent(Context context, UserModel userModel) {
        final Intent intent = new Intent(context, FlyingProfileActivity.class);
        intent.putExtra(TITLE_USER_MODEL, userModel);
        return intent;
    }

    private static void initActivity(FlyingProfileActivity flyingProfileActivity) {
        if (flyingProfileActivity != null
            && flyingProfileActivity.getIntent() != null
            && flyingProfileActivity.getIntent().getParcelableExtra(TITLE_USER_MODEL) != null) {
            flyingProfileActivity.userModel = flyingProfileActivity.getIntent().getParcelableExtra(TITLE_USER_MODEL);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlyingProfileActivity.initActivity(this);

        if (this.userModel == null) {
            //TODO - SHOW ERROR
            finish();
        }

        this.setSupportActionBar(this.binding.chatToolbar);
//        this.binding.chatToolbar.setLogo(R.drawable.logo_small_panda);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setIcon(R.drawable.logo_small_panda);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        this.viewModel.updateUser(this.userModel);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected FlyingProfileViewModel onCreateViewModel(FlyingProfileActivityBinding binding) {
        return new FlyingProfileViewModel(this, binding);
    }

    @Override
    protected int getLayout() {
        return R.layout.flying_profile_activity;
    }
}
