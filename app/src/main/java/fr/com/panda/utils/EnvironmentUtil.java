package fr.com.panda.utils;

import fr.com.panda.BuildConfig;
import fr.com.panda.enums.ReleaseType;

public class EnvironmentUtil {

    public static boolean isTargetPandaBeta() {
        return BuildConfig.RELEASE_TYPE.equals(ReleaseType.BETA);
    }

    public static boolean isTargetPanda() {
        return BuildConfig.RELEASE_TYPE.equals(ReleaseType.RELEASE);
    }

    public static boolean isTargetPandaQA() {
        return BuildConfig.RELEASE_TYPE.equals(ReleaseType.QA);
    }

    public static boolean isTargetPandaDev() {
        return BuildConfig.RELEASE_TYPE.equals(ReleaseType.DEBUG);
    }


}
