package fr.com.panda.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orhanobut.hawk.Hawk;

import fr.com.panda.model.FindRoomsModel;
import fr.com.panda.model.Location;
import fr.com.panda.model.PublicRoomModel;

public class KeychainUtil {

    public static final KeychainUtil sharedInstance = new KeychainUtil();

    public void reset() {
        Hawk.delete(Constants.KEYS.kTOKEN);
        Hawk.delete(Constants.KEYS.kTOKEN_REFRESH);
        Hawk.delete(Constants.KEYS.kUSER_ID);
        Hawk.delete(Constants.KEYS.kJUST_LOGIN);
        Hawk.delete(Constants.KEYS.kIMAGE_DOWNLOADED);
        Hawk.delete(Constants.KEYS.kCURRENT_LOCALISATION);

//        if KeychainWrapper.defaultKeychainWrapper().hasValueForKey(Constants.KEYS.kPUSH_TOKEN) {
//            KeychainWrapper.defaultKeychainWrapper().removeObjectForKey(Constants.KEYS.kPUSH_TOKEN)
//        }

        this.resetLocalisation();
        this.resetPublicChat();

//        SQLManager.resetTables()
        this.leaveAllRooms();
    }

    public void resetDB() {
        Hawk.delete(Constants.KEYS.kCURRENT_DB);
    }

    public void resetWelcomeScreen() {
        Hawk.delete(Constants.KEYS.kWelcomeScreen);
    }

    public void resetPublicChat() {
//        SQLManager.resetPublicChatTables()
    }

    public void resetLocalisation() {
//        SocketManager.sharedInstance.leaveRoomPublicRooms()
//        GPSManager.sharedInstance.resetLocalisation()
//
        Hawk.delete(Constants.KEYS.kFIND_ROOM);
        Hawk.delete(Constants.KEYS.kCURRENT_ROOM);
        Hawk.delete(Constants.KEYS.kCURRENT_ROOM_ID);
    }

    public void leaveAllRooms() {
//        SocketManager.sharedInstance.stopSocket {
//            SocketManager.sharedInstance.leaveRooms()
//        }
    }

    public String getToken() {
        return Hawk.get(Constants.KEYS.kTOKEN, "");
    }

    public void setToken(String token) {
        Hawk.put(Constants.KEYS.kTOKEN, token);
    }

    public String getPushToken() {
        return Hawk.get(Constants.KEYS.kPUSH_TOKEN, "");
    }

    public void setPushToken(String pushToken) {
        Hawk.put(Constants.KEYS.kPUSH_TOKEN, pushToken);
    }

    public String getTokenRefresh() {
        return Hawk.get(Constants.KEYS.kTOKEN_REFRESH, "");
    }

    public void setTokenRefresh(String tokenRefresh) {
        Hawk.put(Constants.KEYS.kTOKEN_REFRESH, tokenRefresh);
    }

    public int getUserId() {
        return Hawk.get(Constants.KEYS.kUSER_ID, -1);
    }

    public void setUserId(int userId) {
        Hawk.put(Constants.KEYS.kUSER_ID, userId);
    }

    public boolean getJustLogin() {
        return Hawk.get(Constants.KEYS.kJUST_LOGIN, false);
    }

    public void setJustLogin(boolean justLogin) {
        Hawk.put(Constants.KEYS.kJUST_LOGIN, justLogin);
    }

    public boolean isLogin() {
        return (this.getToken().length() > 0);
    }

    @Nullable
    public FindRoomsModel getFindRooms() {
        return Hawk.get(Constants.KEYS.kFIND_ROOM, new FindRoomsModel());
    }

    public void setFindRooms(@NonNull FindRoomsModel findRoomsModel) {
        Hawk.put(Constants.KEYS.kFIND_ROOM, findRoomsModel);
    }

    public int getCurrentRoomId() {
        return Hawk.get(Constants.KEYS.kCURRENT_ROOM_ID, -1);
    }

    public void setCurrentRoomId(int currentRoomId) {
        Hawk.put(Constants.KEYS.kCURRENT_ROOM_ID, currentRoomId);
    }

    @Nullable
    public PublicRoomModel getCurrentRoom() {
        return Hawk.get(Constants.KEYS.kCURRENT_ROOM, null);
    }

    public void setCurrentRoom(@Nullable PublicRoomModel publicRoomModel) {
        Hawk.put(Constants.KEYS.kCURRENT_ROOM, publicRoomModel);
    }

    public Location getCurrentLocalisation() {
        return Hawk.get(Constants.KEYS.kCURRENT_LOCALISATION);
    }

    public void setCurrentLocalisation(Location location) {
        Hawk.put(Constants.KEYS.kCURRENT_LOCALISATION, location);
    }

    public boolean isWelcomeScreen() {
        return Hawk.get(Constants.KEYS.kWelcomeScreen, false);
    }

    public void setWelcomeScreen(boolean isWelcomeScreen) {
        Hawk.put(Constants.KEYS.kWelcomeScreen, isWelcomeScreen);
    }

// TODO - NEED TO SEE HOW SAVE IMAGE ON ANDROID
//    var saveImages: SaveImage? {
//        get {
//            if KeychainWrapper.defaultKeychainWrapper().hasValueForKey(Constants.KEYS.kIMAGE_DOWNLOADED) {
//                return KeychainWrapper.defaultKeychainWrapper().objectForKey(Constants.KEYS.kIMAGE_DOWNLOADED) as? SaveImage
//            } else {
//                return nil
//            }
//        }
//
//        set(images) {
//            if let ima = images {
//                KeychainWrapper.defaultKeychainWrapper().setObject(ima, forKey: Constants.KEYS.kIMAGE_DOWNLOADED)
//            }
//        }
//    }
}
