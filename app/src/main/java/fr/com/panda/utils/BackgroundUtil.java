package fr.com.panda.utils;

import java.util.concurrent.Executor;

import needle.BackgroundThreadExecutor;
import needle.Needle;

public class BackgroundUtil {

    public static int THREAD_API = 5;

    public static Executor API() {
//        return  Needle.onMainThread();
        return Needle.onBackgroundThread().withThreadPoolSize(BackgroundUtil.THREAD_API).withTaskType("API");
    }

    public static Executor UI() {
        return Needle.onMainThread();
    }
}
