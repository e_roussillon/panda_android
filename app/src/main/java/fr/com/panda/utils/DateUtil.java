package fr.com.panda.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by edouardroussillon on 12/30/16.
 */

public class DateUtil {

    public final static String FULL_DATE = "yyyy-MM-dd HH:mm:ss";
    public final static String ISO8601 = "yyyy-MM-dd'T'HH:mm:ss";
    public final static String ISO8601Z = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public final static String ISO8601Z_SMALL = "yyyy-MM-dd'T'HH:mm:ssZ";
    public final static String SMALL_DATE = "yyyy-MM-dd";
    public final static String MEDUIM_DATE = "dd MMM";
    public final static String DATE = "MM/dd/yyyy";
    public final static String TIME = "HH:mm";
    public final static String TIME_SEC = "HH:mm:ss";


    public static String getShortStyleDate(Date date) {
        return initDateToString(DateFormat.SHORT, date);
    }

    public static String getMediumStyleDate(Date date) {
        return initDateToString(DateFormat.MEDIUM, date);
    }

    public static String getLongStyleDate(Date date) {
        return initDateToString(DateFormat.LONG, date);
    }

    public static String getFullStyleDate(Date date) {
        return initDateToString(DateFormat.FULL, date);
    }

    public static String getDateToFullString(Date date) {
        return initDateToString(FULL_DATE, date);
    }

    public static Date getFullStringToDate(String date) {
        return initStringToDate(FULL_DATE, date);
    }

    public static String getSmallDateToString(Date date) {
        return initDateToString(SMALL_DATE, date);
    }

    public static Date getStringToSmallDate(String date) {
        return initStringToDate(SMALL_DATE, date);
    }

    public static String getDateToString(Date date) {
        return initDateToString(DATE, date);
    }


    public static Date getStringToDate(String date) {
        return initStringToDate(SMALL_DATE, date);
    }

    public static String getDateToISO8601ZString(Date date) {
        return initDateToString(ISO8601Z, date);
    }

    public static long timestamp(Date date) {
        return date.getTime() / 1_000;
    }

    public static long timestamp() {
        return timestamp(new Date());
    }

    public static Date timestampToDate(long time) {
        return new Date(time * 1_000);
    }

    public static String convertSecondToHHMMString(int secondtTime)
    {
        return initDateToString(TIME_SEC, new Date(secondtTime*1000L));
//        TimeZone tz = TimeZone.getTimeZone("UTC");
//        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
//        df.setTimeZone(tz);
//        String time = df.format(new Date(secondtTime*1000L));
//
//        return time;
    }

//    public static Date timestampToDate(long time) {
//        return new Date(time * 1_000);
//    }

    public static Date getISO8601ZStringToDate(String date) {
        return initStringToDate(ISO8601Z, date);
    }


    private static String initDateToString(int format, Date date) {
        return DateFormat.getDateInstance(format).format(date);
    }

    private static Date initDateToString(int format, String string) {
        try {
            return DateFormat.getDateInstance(format).parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String initDateToString(String format, Date date) {
        return new SimpleDateFormat(format).format(date);
    }

    private static Date initStringToDate(String format, String string) {
        try {
            return new SimpleDateFormat(format).parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
