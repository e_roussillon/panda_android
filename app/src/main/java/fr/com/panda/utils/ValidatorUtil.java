package fr.com.panda.utils;

import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;

import fr.com.panda.R;
import fr.com.panda.enums.ValidatorType;

/**
 * @author Edouard Roussillon
 */

public class ValidatorUtil {

    public static boolean isFieldValid(TextInputLayout textInputLayout, @ValidatorType.ValidatorMode int validatorMode) {

        switch (validatorMode) {
        case ValidatorType.IS_NOT_EMPTY:
            return isNotEmpty(textInputLayout, R.string.generic_error_empty_field);
        case ValidatorType.IS_EMAIL:
            return false;
        }

        return false;
    }

    private static boolean isNotEmpty(TextInputLayout textInputLayout, @StringRes int errorMessage) {
        if (TextUtils.isEmpty(textInputLayout.getEditText().getText())) {
            showError(textInputLayout, errorMessage);
            return false;
        }

        removeError(textInputLayout);
        return true;
    }

//    private static boolean isEmail(TextInputLayout textInputLayout, @StringRes int errorMessage) {
//        if (TextUtils.isEmpty(textInputLayout.getEditText().getText())) {
//            showError(textInputLayout, errorMessage);
//            return true;
//        }
//
//        removeError(textInputLayout);
//        return false;
//    }


    //
    // Generic
    //

    private static void showError(TextInputLayout textInputLayout, @StringRes int errorMessage) {
        textInputLayout.setError(textInputLayout.getContext().getString(errorMessage));
    }

    private static void removeError(TextInputLayout textInputLayout) {
        textInputLayout.setError(null);
    }

}
