package fr.com.panda.utils;

import fr.com.panda.BuildConfig;

public final class Constants {

    public static String PLATFORM = "android";

    public static class HEADER {
        public static String APP_ID = "123456qwerty";
//        public static String DEVICE_ID = UIDevice.currentDevice().identifierForVendor?.UUIDString ?? "";
        public static String LANGUAGE = "en";
        public static String JSON = "application/json";
        public static String DATA = "application/x-www-form-urlencoded";
        public static String ENCODING = "gzip";
    }

    public static class SOCKET {
        public static String kURL = BuildConfig.BASE_WS;
        public static String kPath = "socket";
        public static String kTransport = "websocket";
        public static String kProt = BuildConfig.BASE_WS_PROTOCOL;
        public static long RECONNECT_IN_SEC = 5;
        public static long HEARTBEAT_IN_SEC = 30;
        public static long FLUSH_EVERY_MESSAGE_IN_MSEC = 100;
    }

    public static class API {
        public static long kTimeoutSec = 15L;
        public static String kAPI = BuildConfig.BASE_URL;
    }

//    public static class COLOR {
//        //App Tint
//        public static kAppTint:UIColor = UIColor.UIColorFromRGB(0x3d3d3d)
//        public static kTextFieldTint:UIColor = UIColor.UIColorFromRGB(0x3d3d3d)
//        public static kTextViewTint:UIColor = UIColor.UIColorFromRGB(0x3d3d3d)
//        public static kToolTint:UIColor = UIColor.UIColorFromRGB(0x474747)
//        public static kToolBarTint:UIColor = UIColor.UIColorFromRGB(0xFFFFFF, alpha:0.8)
//        public static kTabbarTint:UIColor = UIColor.UIColorFromRGB(0xF5F5F5)
//        public static kTabbarShadow:UIColor = UIColor.UIColorFromRGB(0x474747)
//
//        //Loader
//        public static kBackgroundLoader:UIColor = UIColor.UIColorFromRGB(0xFFFFFF, alpha:0.0)
//        public static kSpinnerLoader:UIColor = UIColor.UIColorFromRGB(0x474747)
//
//        //Navigation Bar
//        public static kNavigationBar:UIColor = UIColor.UIColorFromRGB(0xFFFFFF, alpha:0.8)
//        public static kNavigationButton:UIColor = UIColor.UIColorFromRGB(0x474747)
//        public static kNavigationBarDarck:UIColor = UIColor.UIColorFromRGB(0x474747)
//        public static kNavigationButtonDarck:UIColor = UIColor.UIColorFromRGB(0xFFFFFF, alpha:0.8)
//
//        //View COntroller
//        public static kBackgroundViewController:UIColor = UIColor.UIColorFromRGB(0xF5F5F5)
//
//        //Text Filed
//        public static kTitlePlaceholder:UIColor = UIColor.UIColorFromRGB(0x858585)
//        public static kTitleError:UIColor = UIColor.UIColorFromRGB(0xF40606)
//        public static kViewLine:UIColor = UIColor.UIColorFromRGB(0x474747)
//
//        //Text Button
//        public static kTitleButtonLight:UIColor = UIColor.UIColorFromRGB(0x474747)
//        public static kTitleButtonDarck:UIColor = UIColor.whiteColor()
//
//        //Gender Button
//        public static kBorderSelected:UIColor = UIColor.UIColorFromRGB(0x1F1F1F)
//        public static kBorderError:UIColor = UIColor.UIColorFromRGB(0xF40606)
//
//        //Chat General
//        public static kBorderColorSending:UIColor = UIColor.UIColorFromRGB(0x999999, alpha: 0.15)
//        public static kBorderColorError:UIColor = UIColor.UIColorFromRGB(0xF40606)
//
//        //Chat Connection View
//        public static kBorderColorConnection:UIColor = UIColor.UIColorFromRGB(0x999999, alpha: 0.25)
//
//
//        public static kTextColorDate:UIColor = UIColor.UIColorFromRGB(0x6e6e6e)
//        public static kTextColorHighlight:UIColor = UIColor.UIColorFromRGB(0x474747)
//        public static kTextColorNormal:UIColor = UIColor.UIColorFromRGB(0x474747)
//        public static kTextColorNotInterested:UIColor = UIColor.UIColorFromRGB(0x474747, alpha: 0.7)
//    }

    public static class KEYS {
        public static String kTOKEN = "token";
        public static String kTOKEN_REFRESH = "tokenRefresh";
        public static String kPUSH_TOKEN = "pushToken";
        public static String kUSER_ID = "userId";
        public static String kJUST_LOGIN = "justLogin";
        public static String kIMAGE_DOWNLOADED = "imagesDownloaded";
        public static String kFIND_ROOM = "findRoom";
        public static String kCURRENT_ROOM_ID = "currentRoomId";
        public static String kCURRENT_ROOM = "currentRoom";
        public static String kCURRENT_LOCALISATION = "currentLocaliation";
        public static String kCURRENT_DB = "currentDB";
        public static String kWelcomeScreen = "welcomeScreen";
    }

    public static class FIELD {
        public static int kPseudoMin = 4;
        public static int kPseudoMax = 30;
    }

    public static class SIZE {
        public static double IMAGE = 1024.0; //Kilobyte
    }

}
