package fr.com.panda.utils;

import android.os.Build;

/**
 * @author Edouard Roussillon
 */

public class SimulatorUtil {
    public static boolean isRunningOnEmulator()
    {
        boolean result=//
                Build.FINGERPRINT.startsWith("generic")//
                        ||Build.FINGERPRINT.startsWith("unknown")//
                        ||Build.MODEL.contains("google_sdk")//
                        ||Build.MODEL.contains("Emulator")//
                        ||Build.MODEL.contains("Android SDK built for x86");
        if(result)
            return true;

        result |= Build.BRAND.startsWith("generic")&&Build.DEVICE.startsWith("generic");

        if(result)
            return true;

        result |= "google_sdk".equals(Build.PRODUCT);
        return result;
    }
}
