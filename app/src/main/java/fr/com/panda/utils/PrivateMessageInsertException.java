package fr.com.panda.utils;

/**
 * Created by edouardroussillon on 12/31/16.
 */

public class PrivateMessageInsertException extends Exception {

    public PrivateMessageInsertException(String message) {
        super(message);
    }
}
