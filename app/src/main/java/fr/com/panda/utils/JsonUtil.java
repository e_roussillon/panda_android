package fr.com.panda.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;

import fr.com.panda.http.util.DateDeserializer;

/**
 * @author Edouard Roussillon
 */

public class JsonUtil {
    public static final Gson GSON = new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();

    public static String toJson(Object src) {
        return GSON.toJson(src);
    }

    public static <T> T fromJson(String json, Class<T> classOfT) throws JsonSyntaxException {
        return GSON.fromJson(json, classOfT);
    }

    public static <T> T fromJson(String json, Type typeOfT) throws JsonSyntaxException {
        return GSON.fromJson(json, typeOfT);
    }

    public static  <T> T fromJson(HashMap<String, Object> hashMap, Class<T> classOfT) throws JsonSyntaxException {
        return fromJson(new JSONObject(hashMap).toString(), classOfT);
    }

    public static  <T> T fromJson(Object object, Class<T> classOfT) throws JsonSyntaxException {
        return fromJson(object, classOfT);
    }
}
