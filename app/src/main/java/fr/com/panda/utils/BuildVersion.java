package fr.com.panda.utils;

import android.os.Build;

/**
 * @author Edouard Roussillon
 */

public class BuildVersion {

    public static int getSDK() {
        return Build.VERSION.SDK_INT;
    }

}
