package fr.com.panda.utils;

import needle.Needle;

/**
 * Created by edouardroussillon on 12/31/16.
 */

public class NeedleUtil {

    public static void threadDB(Runnable run) {
        Needle.onBackgroundThread().withTaskType("panda-db").execute(run);
    }

    public static void threadAPI(Runnable run) {
        Needle.onBackgroundThread().withTaskType("panda-api").execute(run);
    }

}
