package fr.com.panda.utils;

import android.os.Parcel;

import java.util.HashMap;
import java.util.Map;

import fr.com.panda.ui.base.ViewModel;

/**
 * @author Edouard Roussillon
 */

public class ParcelUtil {

    static public void writeMap(Parcel parcel, int flags, Map<String, ViewModel.State> map) {
        parcel.writeInt(map.size());
        for(Map.Entry<String, ViewModel.State> e : map.entrySet()){
            parcel.writeString(e.getKey());
            parcel.writeParcelable(e.getValue(), flags);
        }
    }

    static public Map<String, ViewModel.State> readMap(Parcel parcel) {
        int size = parcel.readInt();
        Map<String, ViewModel.State> map = new HashMap(size);
        for(int i = 0; i < size; i++){
            map.put(parcel.readString(), parcel.readParcelable(ViewModel.State.class.getClassLoader()));
        }
        return map;
    }

}
