package fr.com.panda.utils;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by edouardroussillon on 12/29/16.
 */

@Database(name = PandaDB.NAME, version = PandaDB.VERSION)
public class PandaDB {

    public static final String NAME = "PandaDB"; // we will add the .db extension
    public static final String NAME_TEST = "PandaTestDB";

    public static final int VERSION = 1;
}
