package fr.com.panda.utils;

import android.text.TextUtils;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Request;

public class PathUtil {

    public static Request endpointWithProtocol(String protocol, String domainAndPort, String path, String transport, HashMap<String, String> params) {
        String theProto = "";

        if (protocol.equals("ws")) {
            theProto = "http";
        } else if (protocol.equals("wss")) {
            theProto = "https";
        } else {
            theProto = protocol;
        }

        final String theDomAndPort = PathUtil.removeLeadingAndTrailingSlashes(domainAndPort);
        final String thePath = PathUtil.removeLeadingAndTrailingSlashes(path);
        final String theTransport = PathUtil.removeLeadingAndTrailingSlashes(transport);
        final String url = theProto + "://" + theDomAndPort +"/" + thePath +"/" + theTransport;

        return PathUtil.resolveUrl(url, params);
    }

    private static Request resolveUrl(String url, HashMap<String, String> params) {
        String paramater = "";

        if (null != params) {
            params.entrySet().iterator();
            boolean isFirst = true;

            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (isFirst) {
                    isFirst = false;
                    paramater = entry.getKey() + "=" + entry.getValue();
                } else {
                    paramater = "&" + entry.getKey() + "=" + entry.getValue();
                }
            }
        }

        final String httpUrl = TextUtils.isEmpty(paramater) ? url : url + "?" + paramater;
        return new Request.Builder().url(httpUrl).build();
    }

    private static String removeTrailingSlash(String path) {
        if (TextUtils.isEmpty(path)) { return path; }
        if (path.lastIndexOf('/') > 0) {
            return path.substring(0, path.lastIndexOf('/'));
        }
        return path;
    }

    private static String removeLeadingSlash(String path) {
        if (TextUtils.isEmpty(path)) { return path; }
        if (path.startsWith("/")) {
            return path.substring(1, path.length() - 1);
        }
        return path;
    }

    private static String removeLeadingAndTrailingSlashes(String path) {
        return PathUtil.removeTrailingSlash(PathUtil.removeLeadingSlash(path));
    }
}
