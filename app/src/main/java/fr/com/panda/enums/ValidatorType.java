package fr.com.panda.enums;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Edouard Roussillon
 */

public class ValidatorType {
    public static final int IS_NOT_EMPTY = -1;
    public static final int IS_EMAIL = 0;

    @IntDef({ValidatorType.IS_NOT_EMPTY, ValidatorType.IS_EMAIL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ValidatorMode {}

    @ValidatorMode
    public static int fromId(int id) {
        switch (id) {
            case ValidatorType.IS_NOT_EMPTY:
                return ValidatorType.IS_NOT_EMPTY;
            default:
                return ValidatorType.IS_EMAIL;
        }
    }
}
