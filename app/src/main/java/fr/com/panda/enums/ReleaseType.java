package fr.com.panda.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ReleaseType {
    public static final String RELEASE = "release";
    public static final String BETA = "beta";
    public static final String DEBUG = "debug";
    public static final String QA = "qa";

    @StringDef({ReleaseType.RELEASE, ReleaseType.BETA, ReleaseType.DEBUG, ReleaseType.QA})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ReleaseMode {}

    @ReleaseMode
    public static String fromId(String id) {
        switch (id) {
            case ReleaseType.BETA:
                return ReleaseType.BETA;
            case ReleaseType.DEBUG:
                return ReleaseType.DEBUG;
            case ReleaseType.QA:
                return ReleaseType.QA;
            default:
                return ReleaseType.RELEASE;
        }
    }
}
