package fr.com.panda.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ScreeNameType {

    public static final String Splash = "screen_splash";
    public static final String BackupSplash = "screen_backup_splash";
    public static final String SignUp = "screen_signUp";
    public static final String SignIn = "screen_signIn";
    public static final String GeneralChat = "screen_general_chat";
    public static final String TradeChat = "screen_trade_chat";
    public static final String PrivateListChat = "screen_private_list_chat";
    public static final String PrivateChat = "screen_private_chat";
    public static final String Profile = "screen_profile";
    public static final String FlyingProfile = "screen_flying_profile";
    public static final String FilterLike = "screen_filter_like";
    public static final String FilterDislike = "screen_filter_dislike";
    public static final String FriendshipAccepted = "screen_friendship_accepted";
    public static final String FriendshipPending = "screen_friendship_pending";
    public static final String FriendshipBlocked = "screen_friendship_blocked";
    public static final String Search = "screen_search";
    public static final String Welcome = "screen_welcome";

    @StringDef({ScreeNameType.Splash,
            ScreeNameType.BackupSplash,
            ScreeNameType.SignUp,
            ScreeNameType.SignIn,
            ScreeNameType.GeneralChat,
            ScreeNameType.TradeChat,
            ScreeNameType.PrivateListChat,
            ScreeNameType.PrivateChat,
            ScreeNameType.Profile,
            ScreeNameType.FlyingProfile,
            ScreeNameType.FilterLike,
            ScreeNameType.FilterDislike,
            ScreeNameType.FriendshipAccepted,
            ScreeNameType.FriendshipPending,
            ScreeNameType.FriendshipBlocked,
            ScreeNameType.Search,
            ScreeNameType.Welcome})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ScreeNameMode {}

    @ScreeNameType.ScreeNameMode
    public static String fromId(String id) {
        switch (id) {
            case ScreeNameType.Splash:
                return ScreeNameType.Splash;
            case ScreeNameType.BackupSplash:
                return ScreeNameType.BackupSplash;
            case ScreeNameType.SignUp:
                return ScreeNameType.SignUp;
            case ScreeNameType.SignIn:
                return ScreeNameType.SignIn;
            case ScreeNameType.GeneralChat:
                return ScreeNameType.GeneralChat;
            case ScreeNameType.TradeChat:
                return ScreeNameType.TradeChat;
            case ScreeNameType.PrivateListChat:
                return ScreeNameType.PrivateListChat;
            case ScreeNameType.PrivateChat:
                return ScreeNameType.PrivateChat;
            case ScreeNameType.Profile:
                return ScreeNameType.Profile;
            case ScreeNameType.FlyingProfile:
                return ScreeNameType.FlyingProfile;
            case ScreeNameType.FilterLike:
                return ScreeNameType.FilterLike;
            case ScreeNameType.FilterDislike:
                return ScreeNameType.FilterDislike;
            case ScreeNameType.FriendshipAccepted:
                return ScreeNameType.FriendshipAccepted;
            case ScreeNameType.FriendshipPending:
                return ScreeNameType.FriendshipPending;
            case ScreeNameType.FriendshipBlocked:
                return ScreeNameType.FriendshipBlocked;
            case ScreeNameType.Search:
                return ScreeNameType.Search;
            default:
                return ScreeNameType.Welcome;
        }
    }
}
