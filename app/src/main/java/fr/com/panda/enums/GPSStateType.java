package fr.com.panda.enums;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Edouard Roussillon
 */

public class GPSStateType {
    public static final int DENIED = 0;
    public static final int NOT_DETERMINED = 1;
    public static final int FAILED = 2;
    public static final int LOOKING_FOR = 3;
    public static final int GOT_IT = 4;

    @IntDef({GPSStateType.DENIED, GPSStateType.NOT_DETERMINED, GPSStateType.FAILED, GPSStateType.LOOKING_FOR, GPSStateType.GOT_IT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface GPSStateMode {}

    @GPSStateMode
    public static int fromId(int id) {
        switch (id) {
            case GPSStateType.DENIED:
                return GPSStateType.DENIED;
            case GPSStateType.NOT_DETERMINED:
                return GPSStateType.NOT_DETERMINED;
            case GPSStateType.FAILED:
                return GPSStateType.FAILED;
            case GPSStateType.LOOKING_FOR:
                return GPSStateType.LOOKING_FOR;
            default:
                return GPSStateType.GOT_IT;
        }
    }
}