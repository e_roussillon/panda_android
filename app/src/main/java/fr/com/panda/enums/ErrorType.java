package fr.com.panda.enums;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class ErrorType {
    public static final int DB = 0;
    public static final int CHANGESET = 1;
    public static final int LOCALIZATION = 2;
    public static final int PUBLIC_ROOM = 3;
    public static final int AUTH = 4;
    public static final int USER_FRIENDSHIP = 5;
    public static final int PRIVATE_MESSAGE = 6;
    public static final int MESSAGE_SENDER = 7;
    public static final int USER = 8;

    @IntDef({ErrorType.DB, ErrorType.CHANGESET, ErrorType.LOCALIZATION, ErrorType.PUBLIC_ROOM, ErrorType.AUTH, ErrorType.USER_FRIENDSHIP, ErrorType.PRIVATE_MESSAGE, ErrorType.MESSAGE_SENDER, ErrorType.USER})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ErrorMode {}

    @ErrorMode
    public static int fromId(int id) {
        switch (id) {
            case ErrorType.DB:
                return ErrorType.DB;
            case ErrorType.LOCALIZATION:
                return ErrorType.LOCALIZATION;
            case ErrorType.PUBLIC_ROOM:
                return ErrorType.PUBLIC_ROOM;
            case ErrorType.AUTH:
                return ErrorType.AUTH;
            case ErrorType.USER_FRIENDSHIP:
                return ErrorType.USER_FRIENDSHIP;
            case ErrorType.PRIVATE_MESSAGE:
                return ErrorType.PRIVATE_MESSAGE;
            case ErrorType.MESSAGE_SENDER:
                return ErrorType.MESSAGE_SENDER;
            case ErrorType.USER:
                return ErrorType.USER;
            default:
                return ErrorType.CHANGESET;
        }
    }
}
