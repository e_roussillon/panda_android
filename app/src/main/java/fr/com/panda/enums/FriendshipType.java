package fr.com.panda.enums;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class FriendshipType {
    public static final int REPORTED = -1;
    public static final int NOT_FRIEND = 0;
    public static final int PENDING = 1;
    public static final int CANCEL_REQUEST = 2;
    public static final int REFUSED_BEFORE_BE_FRIEND = 3;
    public static final int BLOCKED_BEFORE_BE_FRIEND = 4;
    public static final int FRIEND = 5;
    public static final int REFUSED_AFTER_BE_FRIEND = 6;
    public static final int BLOCKED_AFTER_BE_FRIEND = 7;
    public static final int UNBLOCKED = 8;

    @IntDef({FriendshipType.NOT_FRIEND, FriendshipType.PENDING, FriendshipType.CANCEL_REQUEST, FriendshipType.REFUSED_BEFORE_BE_FRIEND, FriendshipType.BLOCKED_BEFORE_BE_FRIEND, FriendshipType.FRIEND, FriendshipType.REFUSED_AFTER_BE_FRIEND, FriendshipType.BLOCKED_AFTER_BE_FRIEND, FriendshipType.UNBLOCKED, FriendshipType.REPORTED})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FriendshipMode {}

    @FriendshipMode
    public static int fromId(int id) {
        switch (id) {
            case FriendshipType.PENDING:
                return FriendshipType.PENDING;
            case FriendshipType.CANCEL_REQUEST:
                return FriendshipType.CANCEL_REQUEST;
            case FriendshipType.REFUSED_BEFORE_BE_FRIEND:
                return FriendshipType.REFUSED_BEFORE_BE_FRIEND;
            case FriendshipType.BLOCKED_BEFORE_BE_FRIEND:
                return FriendshipType.BLOCKED_BEFORE_BE_FRIEND;
            case FriendshipType.FRIEND:
                return FriendshipType.FRIEND;
            case FriendshipType.REFUSED_AFTER_BE_FRIEND:
                return FriendshipType.REFUSED_AFTER_BE_FRIEND;
            case FriendshipType.BLOCKED_AFTER_BE_FRIEND:
                return FriendshipType.BLOCKED_AFTER_BE_FRIEND;
            case FriendshipType.UNBLOCKED:
                return FriendshipType.UNBLOCKED;
            case FriendshipType.REPORTED:
                return FriendshipType.REPORTED;
            default:
                return FriendshipType.NOT_FRIEND;
        }
    }
}
