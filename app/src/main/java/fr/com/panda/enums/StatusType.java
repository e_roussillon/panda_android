package fr.com.panda.enums;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by edouardroussillon on 12/29/16.
 */

public class StatusType {
    public static final int ERROR = -1;
    public static final int SENDING = 0;
    public static final int SENT = 1;
    public static final int RECEIVED = 2;
    public static final int READ = 3;
    public static final int CONFIRMED = 4;

    @IntDef({StatusType.ERROR, StatusType.SENDING, StatusType.SENT, StatusType.RECEIVED, StatusType.READ, StatusType.CONFIRMED})
    @Retention(RetentionPolicy.SOURCE)
    public @interface StatusMode {}

    @StatusMode
    public static int fromId(int id) {
        switch (id) {
            case StatusType.SENDING:
                return StatusType.SENDING;
            case StatusType.SENT:
                return StatusType.SENT;
            case StatusType.RECEIVED:
                return StatusType.RECEIVED;
            case StatusType.READ:
                return StatusType.READ;
            case StatusType.CONFIRMED:
                return StatusType.CONFIRMED;
            default:
                return StatusType.ERROR;
        }
    }
}
