package fr.com.panda.enums.api;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class LoginError {
    public static final int ERROR_EMAIL_OR_PASSWORD_EMPTY = 0;
    public static final int ERROR_RATE_LIMIT = 7;
    public static final int ERROR_NOT_ACTIVE = 8;
    public static final int ERROR_NONE = -1;

    @IntDef({LoginError.ERROR_EMAIL_OR_PASSWORD_EMPTY, LoginError.ERROR_RATE_LIMIT, LoginError.ERROR_NOT_ACTIVE, LoginError.ERROR_NONE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LoginErrorMode {}

    @LoginErrorMode
    public static int fromId(int id) {
        switch (id) {
            case LoginError.ERROR_EMAIL_OR_PASSWORD_EMPTY:
                return LoginError.ERROR_EMAIL_OR_PASSWORD_EMPTY;
            case LoginError.ERROR_RATE_LIMIT:
                return LoginError.ERROR_RATE_LIMIT;
            case LoginError.ERROR_NOT_ACTIVE:
                return LoginError.ERROR_NOT_ACTIVE;
            default:
                return LoginError.ERROR_NONE;
        }
    }
}
