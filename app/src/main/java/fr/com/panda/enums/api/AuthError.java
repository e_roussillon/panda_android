package fr.com.panda.enums.api;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class AuthError {
    public static final int PASSWORD_OR_LOGIN = 0;
    public static final int TOKEN_NOT_FOUND = 1;
    public static final int TOKEN_INVALID = 2;
    public static final int TOKEN_NOT_RENEW = 3;
    public static final int USER_REPORTED = 4;
    public static final int USER_DUPLICATED = 5;

    @IntDef({AuthError.PASSWORD_OR_LOGIN, AuthError.TOKEN_NOT_FOUND, AuthError.TOKEN_INVALID, AuthError.TOKEN_NOT_RENEW, AuthError.USER_REPORTED, AuthError.USER_DUPLICATED})
    @Retention(RetentionPolicy.SOURCE)
    public @interface AuthErrorMode {}

    @AuthErrorMode
    public static int fromId(int id) {
        switch (id) {
            case AuthError.PASSWORD_OR_LOGIN:
                return AuthError.PASSWORD_OR_LOGIN;
            case AuthError.TOKEN_NOT_FOUND:
                return AuthError.TOKEN_NOT_FOUND;
            case AuthError.TOKEN_INVALID:
                return AuthError.TOKEN_INVALID;
            case AuthError.USER_REPORTED:
                return AuthError.USER_REPORTED;
            case AuthError.USER_DUPLICATED:
                return AuthError.USER_DUPLICATED;
            default:
                return AuthError.TOKEN_NOT_RENEW;
        }
    }
}
