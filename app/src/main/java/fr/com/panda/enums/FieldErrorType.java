package fr.com.panda.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Edouard Roussillon
 */

public class FieldErrorType {
    public static final String ERROR_BLANK = "can't be blank";
    public static final String ERROR_INVALID = "is invalid";
    public static final String ERROR_FORMAT = "has invalid format";
    public static final String ERROR_USED = "has already been taken";
    public static final String ERROR_MIN_6_CHARACTER = "should be at least 6 character(s)";
    public static final String ERROR_MIN_4_CHARACTER = "should be at least 4 character(s)";
    public static final String ERROR_MAX_30_CHARACTER = "should be at most 30 character(s)";
    public static final String ERROR_BIRTDAY_UNDER_16 = "birthday under 16";

    @StringDef({FieldErrorType.ERROR_BLANK, FieldErrorType.ERROR_INVALID, FieldErrorType.ERROR_FORMAT, FieldErrorType.ERROR_USED,
            FieldErrorType.ERROR_MIN_6_CHARACTER, FieldErrorType.ERROR_MIN_4_CHARACTER, FieldErrorType.ERROR_MAX_30_CHARACTER,
            FieldErrorType.ERROR_BIRTDAY_UNDER_16})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FieldErrorMode {}

    @FieldErrorMode
    public static String fromId(String id) {
        switch (id) {
            case FieldErrorType.ERROR_BLANK:
                return FieldErrorType.ERROR_BLANK;
            case FieldErrorType.ERROR_INVALID:
                return FieldErrorType.ERROR_INVALID;
            case FieldErrorType.ERROR_FORMAT:
                return FieldErrorType.ERROR_FORMAT;
            case FieldErrorType.ERROR_USED:
                return FieldErrorType.ERROR_USED;
            case FieldErrorType.ERROR_MIN_6_CHARACTER:
                return FieldErrorType.ERROR_MIN_6_CHARACTER;
            case FieldErrorType.ERROR_MIN_4_CHARACTER:
                return FieldErrorType.ERROR_MIN_4_CHARACTER;
            case FieldErrorType.ERROR_MAX_30_CHARACTER:
                return FieldErrorType.ERROR_MAX_30_CHARACTER;
            default:
                return FieldErrorType.ERROR_BIRTDAY_UNDER_16;
        }
    }
}
