package fr.com.panda.enums.api;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class APIHandler {
    public static final int CONSUMED = 0;
    public static final int NOT_CONSUMED = 1;
    public static final int RENEW_TOKEN = -1;

    @IntDef({APIHandler.CONSUMED, APIHandler.NOT_CONSUMED, APIHandler.RENEW_TOKEN})
    @Retention(RetentionPolicy.SOURCE)
    public @interface APIHandlerMode {}

    @APIHandlerMode
    public static int fromId(int id) {
        switch (id) {
            case APIHandler.CONSUMED:
                return APIHandler.CONSUMED;
            case APIHandler.RENEW_TOKEN:
                return APIHandler.RENEW_TOKEN;
            default:
                return APIHandler.NOT_CONSUMED;
        }
    }
}
