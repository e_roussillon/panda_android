package fr.com.panda.enums;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class GenderType {
    public static final int NONE = 0;
    public static final int MALE = 1;
    public static final int FEMALE = 2;

    @IntDef({GenderType.NONE, GenderType.MALE, GenderType.FEMALE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface GenderMode {}

    @GenderMode
    public static int fromId(int id) {
        switch (id) {
            case GenderType.MALE:
                return GenderType.MALE;
            case GenderType.FEMALE:
                return GenderType.FEMALE;
            default:
                return GenderType.NONE;
        }
    }
}
