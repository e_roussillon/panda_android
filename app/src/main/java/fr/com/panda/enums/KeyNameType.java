package fr.com.panda.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class KeyNameType {

    public static final String UserId = "user_id";
    public static final String UserPseudo = "user_pseudo";
    public static final String ToUserId = "to_user_id";
    public static final String ToUserPseudo = "to_user_pseudo";
    public static final String Word = "word";
    public static final String Message = "message";
    public static final String FriendshipStatus = "friendship_status";
    public static final String FriendshipLastAction = "friendship_last_action";
    public static final String RequestAPI = "request_api";
    public static final String RequestParams = "request_params";
    public static final String RequestStatus = "request_status";
    public static final String RequestError = "request_error";
    public static final String EmailError = "email_error";
    public static final String PasswordError = "password_error";
    public static final String GenderError = "gender_error";
    public static final String BirthdayError = "birthday_error";
    public static final String RoomId = "room_id";
    public static final String RoomName = "room_name";

    @StringDef({KeyNameType.UserId,
            KeyNameType.UserPseudo,
            KeyNameType.ToUserId,
            KeyNameType.ToUserPseudo,
            KeyNameType.Word,
            KeyNameType.Message,
            KeyNameType.FriendshipStatus,
            KeyNameType.FriendshipLastAction,
            KeyNameType.RequestAPI,
            KeyNameType.RequestParams,
            KeyNameType.RequestStatus,
            KeyNameType.RequestError,
            KeyNameType.EmailError,
            KeyNameType.PasswordError,
            KeyNameType.GenderError,
            KeyNameType.BirthdayError,
            KeyNameType.RoomId,
            KeyNameType.RoomName})
    @Retention(RetentionPolicy.SOURCE)
    public @interface KeyNameMode {}

    @KeyNameType.KeyNameMode
    public static String fromId(String id) {
        switch (id) {
            case KeyNameType.UserId:
                return KeyNameType.UserId;
            case KeyNameType.UserPseudo:
                return KeyNameType.UserPseudo;
            case KeyNameType.ToUserId:
                return KeyNameType.ToUserId;
            case KeyNameType.ToUserPseudo:
                return KeyNameType.ToUserPseudo;
            case KeyNameType.Word:
                return KeyNameType.Word;
            case KeyNameType.Message:
                return KeyNameType.Message;
            case KeyNameType.FriendshipStatus:
                return KeyNameType.FriendshipStatus;
            case KeyNameType.FriendshipLastAction:
                return KeyNameType.FriendshipLastAction;
            case KeyNameType.RequestAPI:
                return KeyNameType.RequestAPI;
            case KeyNameType.RequestParams:
                return KeyNameType.RequestParams;
            case KeyNameType.RequestStatus:
                return KeyNameType.RequestStatus;
            case KeyNameType.RequestError:
                return KeyNameType.RequestError;
            case KeyNameType.EmailError:
                return KeyNameType.EmailError;
            case KeyNameType.PasswordError:
                return KeyNameType.PasswordError;
            case KeyNameType.GenderError:
                return KeyNameType.GenderError;
            case KeyNameType.BirthdayError:
                return KeyNameType.BirthdayError;
            case KeyNameType.RoomId:
                return KeyNameType.RoomId;
            default:
                return KeyNameType.RoomName;
        }
    }

}
