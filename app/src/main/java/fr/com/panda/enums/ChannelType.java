package fr.com.panda.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ChannelType {
    public static final String GENERIC = "general_channel";
    public static final String SALE = "sale_channel";
    public static final String BROADCAST = "broadcast_channel";
    public static final String PRIVATE = "private_channel";

    @StringDef({ChannelType.GENERIC, ChannelType.SALE, ChannelType.BROADCAST, ChannelType.PRIVATE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ChannelMode {}

    @ChannelType.ChannelMode
    public static String fromId(String id) {
        switch (id) {
            case ChannelType.SALE:
                return ChannelType.SALE;
            case ChannelType.PRIVATE:
                return ChannelType.PRIVATE;
            case ChannelType.BROADCAST:
                return ChannelType.BROADCAST;
            default:
                return ChannelType.GENERIC;
        }
    }
}
