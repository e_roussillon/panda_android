package fr.com.panda.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ActionNameType {

    public static final String ClickSignUp = "click_sign_up";
    public static final String SendSignUp = "send_sign_up";
    public static final String SendSignIn = "send_sign_in";
    public static final String MissingInfoSignIn = "missing_info_sign_in";
    public static final String MissingInfoSignUp = "missing_info_sign_up";
    public static final String ClickForgotPassword = "click_forgot_password";
    public static final String RequestSucceed = "request_succeed";
    public static final String RequestFailed = "request_failed";
    public static final String SendGeneralMessage = "send_general_message";
    public static final String HighlightGeneralMessage = "highlight_general_message";
    public static final String BlockedGeneralMessage = "blocked_general_message";
    public static final String BlockedUserGeneralMessage = "blocked_user_general_message";
    public static final String ReceivedGeneralMessage = "received_general_message";
    public static final String ErrorGeneralMessage = "error_general_message";
    public static final String SendTradeMessage = "send_trading_message";
    public static final String HighlightTradeMessage = "highlight_trade_message";
    public static final String BlockedTradeMessage = "blocked_trading_message";
    public static final String BlockedUserTradeMessage = "blocked_user_trading_message";
    public static final String ReceivedTradeMessage = "received_trading_message";
    public static final String ReceivedBroadcastMessage = "received_broadcast_message";
    public static final String ErrorTradeMessage = "error_trading_message";
    public static final String SendPrivateMessage = "send_private_message";
    public static final String ConfirmPrivateMessage = "confirm_private_message";
    public static final String JoinGeneralChat = "join_general_chat";
    public static final String JoinTradeChat = "join_trade_chat";
    public static final String JoinBroadcastChat = "join_broadcast_chat";
    public static final String JoinPrivateChat = "join_private_chat";
    public static final String LostConnectionChat = "lost_connection_chat";
    public static final String DisconnectionChat = "disconnection_chat";
    public static final String ConnectionChat = "connection_chat";
    public static final String UserDuplicate = "user_duplicate";
    public static final String UserReported = "user_reported";
    public static final String UserBackup = "user_backup";
    public static final String UserFriendship = "user_friendship";
    public static final String UserDisconnect = "user_disconnect";
    public static final String UserConnect = "user_connect";
    public static final String UsersConnect = "users_connect";
    public static final String ClickUserFromGeneralChat = "click_user_from_general_chat";
    public static final String ClickUserFromTradeChat = "click_user_from_trade_chat";
    public static final String ClickBlockUser = "click_block_user";
    public static final String ClickUnblockUser = "click_unblock_user";
    public static final String ClickReportUser = "click_report_user";
    public static final String ClickProfileUser = "click_profile_user";
    public static final String ClickLogout = "click_logout";
    public static final String ClickDismissProfile = "click_dismiss_profile";
    public static final String ClickEditProfile = "click_edit_profile";
    public static final String ClickCancelEditProfile = "click_cancel_edit_profile";
    public static final String ClickSaveEditProfile = "click_save_edit_profile";
    public static final String ClickEditPictureProfile = "click_edit_picture_profile";
    public static final String ClickCancelSelectPictureProfile = "click_cancel_select_picture_profile";
    public static final String ClickCancelSelectPicturePrivateMessage = "click_cancel_select_picture_private_message";
    public static final String ClickCameraProfile = "click_camera_profile";
    public static final String ClickCameraPrivateMessage = "click_camera_private_message";
    public static final String ClickGalleryProfile = "click_gallery_profile";
    public static final String ClickGalleryPrivateMessage = "click_gallery_private_message";
    public static final String PictureSelectedProfile = "picture_selected_profile";
    public static final String PictureSelectedPrivateMessage = "picture_selected_private_message";
    public static final String ClickDeleteFilterLike = "click_delete_filter_like";
    public static final String ClickNewFilterLike = "click_new_filter_like";
    public static final String ClickEditFilterLike = "click_edit_filter_like";
    public static final String ClickDeleteFilterDislike = "click_delete_filter_dislike";
    public static final String ClickNewFilterDislike = "click_new_filter_dislike";
    public static final String ClickEditFilterDislike = "click_edit_filter_dislike";

    @StringDef({ActionNameType.ClickSignUp,
            ActionNameType.SendSignUp,
            ActionNameType.SendSignIn,
            ActionNameType.MissingInfoSignIn,
            ActionNameType.MissingInfoSignUp,
            ActionNameType.ClickForgotPassword,
            ActionNameType.RequestSucceed,
            ActionNameType.RequestFailed,
            ActionNameType.SendGeneralMessage,
            ActionNameType.HighlightGeneralMessage,
            ActionNameType.BlockedGeneralMessage,
            ActionNameType.BlockedUserGeneralMessage,
            ActionNameType.ReceivedGeneralMessage,
            ActionNameType.ErrorGeneralMessage,
            ActionNameType.SendTradeMessage,
            ActionNameType.HighlightTradeMessage,
            ActionNameType.BlockedTradeMessage,
            ActionNameType.BlockedUserTradeMessage,
            ActionNameType.ReceivedTradeMessage,
            ActionNameType.ReceivedBroadcastMessage,
            ActionNameType.ErrorTradeMessage,
            ActionNameType.SendPrivateMessage,
            ActionNameType.ConfirmPrivateMessage,
            ActionNameType.JoinGeneralChat,
            ActionNameType.JoinTradeChat,
            ActionNameType.JoinBroadcastChat,
            ActionNameType.JoinPrivateChat,
            ActionNameType.LostConnectionChat,
            ActionNameType.DisconnectionChat,
            ActionNameType.ConnectionChat,
            ActionNameType.UserDuplicate,
            ActionNameType.UserReported,
            ActionNameType.UserBackup,
            ActionNameType.UserFriendship,
            ActionNameType.UserDisconnect,
            ActionNameType.UserConnect,
            ActionNameType.UsersConnect,
            ActionNameType.ClickUserFromGeneralChat,
            ActionNameType.ClickUserFromTradeChat,
            ActionNameType.ClickBlockUser,
            ActionNameType.ClickUnblockUser,
            ActionNameType.ClickReportUser,
            ActionNameType.ClickProfileUser,
            ActionNameType.ClickLogout,
            ActionNameType.ClickDismissProfile,
            ActionNameType.ClickEditProfile,
            ActionNameType.ClickCancelEditProfile,
            ActionNameType.ClickSaveEditProfile,
            ActionNameType.ClickEditPictureProfile,
            ActionNameType.ClickCancelSelectPictureProfile,
            ActionNameType.ClickCancelSelectPicturePrivateMessage,
            ActionNameType.ClickCameraProfile,
            ActionNameType.ClickCameraPrivateMessage,
            ActionNameType.ClickGalleryProfile,
            ActionNameType.ClickGalleryPrivateMessage,
            ActionNameType.PictureSelectedProfile,
            ActionNameType.PictureSelectedPrivateMessage,
            ActionNameType.ClickDeleteFilterLike,
            ActionNameType.ClickNewFilterLike,
            ActionNameType.ClickEditFilterLike,
            ActionNameType.ClickDeleteFilterDislike,
            ActionNameType.ClickNewFilterDislike,
            ActionNameType.ClickEditFilterDislike})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ActionNameMode {}

    @ActionNameMode
    public static String fromId(String id) {
        switch (id) {
            case ActionNameType.ClickSignUp:
                return ActionNameType.ClickSignUp;
            case ActionNameType.SendSignUp:
                return ActionNameType.SendSignUp;
            case ActionNameType.SendSignIn:
                return ActionNameType.SendSignIn;
            case ActionNameType.MissingInfoSignIn:
                return ActionNameType.MissingInfoSignIn;
            case ActionNameType.MissingInfoSignUp:
                return ActionNameType.MissingInfoSignUp;
            case ActionNameType.ClickForgotPassword:
                return ActionNameType.ClickForgotPassword;
            case ActionNameType.RequestSucceed:
                return ActionNameType.RequestSucceed;
            case ActionNameType.RequestFailed:
                return ActionNameType.RequestFailed;
            case ActionNameType.SendGeneralMessage:
                return ActionNameType.SendGeneralMessage;
            case ActionNameType.HighlightGeneralMessage:
                return ActionNameType.HighlightGeneralMessage;
            case ActionNameType.BlockedGeneralMessage:
                return ActionNameType.BlockedGeneralMessage;
            case ActionNameType.BlockedUserGeneralMessage:
                return ActionNameType.BlockedUserGeneralMessage;
            case ActionNameType.ReceivedGeneralMessage:
                return ActionNameType.ReceivedGeneralMessage;
            case ActionNameType.ErrorGeneralMessage:
                return ActionNameType.ErrorGeneralMessage;
            case ActionNameType.SendTradeMessage:
                return ActionNameType.SendTradeMessage;
            case ActionNameType.HighlightTradeMessage:
                return ActionNameType.HighlightTradeMessage;
            case ActionNameType.BlockedTradeMessage:
                return ActionNameType.BlockedTradeMessage;
            case ActionNameType.BlockedUserTradeMessage:
                return ActionNameType.BlockedUserTradeMessage;
            case ActionNameType.ReceivedTradeMessage:
                return ActionNameType.ReceivedTradeMessage;
            case ActionNameType.ReceivedBroadcastMessage:
                return ActionNameType.ReceivedBroadcastMessage;
            case ActionNameType.ErrorTradeMessage:
                return ActionNameType.ErrorTradeMessage;
            case ActionNameType.SendPrivateMessage:
                return ActionNameType.SendPrivateMessage;
            case ActionNameType.ConfirmPrivateMessage:
                return ActionNameType.ConfirmPrivateMessage;
            case ActionNameType.JoinGeneralChat:
                return ActionNameType.JoinGeneralChat;
            case ActionNameType.JoinTradeChat:
                return ActionNameType.JoinTradeChat;
            case ActionNameType.JoinBroadcastChat:
                return ActionNameType.JoinBroadcastChat;
            case ActionNameType.JoinPrivateChat:
                return ActionNameType.JoinPrivateChat;
            case ActionNameType.LostConnectionChat:
                return ActionNameType.LostConnectionChat;
            case ActionNameType.DisconnectionChat:
                return ActionNameType.DisconnectionChat;
            case ActionNameType.ConnectionChat:
                return ActionNameType.ConnectionChat;
            case ActionNameType.UserDuplicate:
                return ActionNameType.UserDuplicate;
            case ActionNameType.UserReported:
                return ActionNameType.UserReported;
            case ActionNameType.UserBackup:
                return ActionNameType.UserBackup;
            case ActionNameType.UserFriendship:
                return ActionNameType.UserFriendship;
            case ActionNameType.UserDisconnect:
                return ActionNameType.UserDisconnect;
            case ActionNameType.UserConnect:
                return ActionNameType.UserConnect;
            case ActionNameType.UsersConnect:
                return ActionNameType.UsersConnect;
            case ActionNameType.ClickUserFromGeneralChat:
                return ActionNameType.ClickUserFromGeneralChat;
            case ActionNameType.ClickUserFromTradeChat:
                return ActionNameType.ClickUserFromTradeChat;
            case ActionNameType.ClickBlockUser:
                return ActionNameType.ClickBlockUser;
            case ActionNameType.ClickUnblockUser:
                return ActionNameType.ClickUnblockUser;
            case ActionNameType.ClickReportUser:
                return ActionNameType.ClickReportUser;
            case ActionNameType.ClickProfileUser:
                return ActionNameType.ClickProfileUser;
            case ActionNameType.ClickLogout:
                return ActionNameType.ClickLogout;
            case ActionNameType.ClickDismissProfile:
                return ActionNameType.ClickDismissProfile;
            case ActionNameType.ClickEditProfile:
                return ActionNameType.ClickEditProfile;
            case ActionNameType.ClickCancelEditProfile:
                return ActionNameType.ClickCancelEditProfile;
            case ActionNameType.ClickSaveEditProfile:
                return ActionNameType.ClickSaveEditProfile;
            case ActionNameType.ClickEditPictureProfile:
                return ActionNameType.ClickEditPictureProfile;
            case ActionNameType.ClickCancelSelectPictureProfile:
                return ActionNameType.ClickCancelSelectPictureProfile;
            case ActionNameType.ClickCancelSelectPicturePrivateMessage:
                return ActionNameType.ClickCancelSelectPicturePrivateMessage;
            case ActionNameType.ClickCameraProfile:
                return ActionNameType.ClickCameraProfile;
            case ActionNameType.ClickCameraPrivateMessage:
                return ActionNameType.ClickCameraPrivateMessage;
            case ActionNameType.ClickGalleryProfile:
                return ActionNameType.ClickGalleryProfile;
            case ActionNameType.ClickGalleryPrivateMessage:
                return ActionNameType.ClickGalleryPrivateMessage;
            case ActionNameType.PictureSelectedProfile:
                return ActionNameType.PictureSelectedProfile;
            case ActionNameType.PictureSelectedPrivateMessage:
                return ActionNameType.PictureSelectedPrivateMessage;
            case ActionNameType.ClickDeleteFilterLike:
                return ActionNameType.ClickDeleteFilterLike;
            case ActionNameType.ClickNewFilterLike:
                return ActionNameType.ClickNewFilterLike;
            case ActionNameType.ClickEditFilterLike:
                return ActionNameType.ClickEditFilterLike;
            case ActionNameType.ClickDeleteFilterDislike:
                return ActionNameType.ClickDeleteFilterDislike;
            case ActionNameType.ClickNewFilterDislike:
                return ActionNameType.ClickNewFilterDislike;
            default:
                return ActionNameType.ClickEditFilterDislike;
        }
    }
}
