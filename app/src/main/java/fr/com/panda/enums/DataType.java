package fr.com.panda.enums;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by edouardroussillon on 12/30/16.
 */

public class DataType {
    public static final int NONE = -1;
    public static final int IMAGE = 0;
    public static final int URL = 1;

    @IntDef({DataType.NONE, DataType.IMAGE, DataType.URL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface DataMode {}

    @DataMode
    public static int fromId(int id) {
        switch (id) {
            case DataType.IMAGE:
                return DataType.IMAGE;
            case DataType.URL:
                return DataType.URL;
            default:
                return DataType.NONE;
        }
    }
}
