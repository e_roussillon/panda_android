package fr.com.panda.enums;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by edouardroussillon on 12/28/16.
 */

public class FriendshipTabType {
    public static final int ACCEPTED = 0;
    public static final int PENDING = 1;
    public static final int BLOCKED = 2;

    @IntDef({FriendshipTabType.ACCEPTED, FriendshipTabType.PENDING, FriendshipTabType.BLOCKED})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FriendshipTabMode {}

    @FriendshipTabMode
    public static int fromId(int id) {
        switch (id) {
            case FriendshipTabType.ACCEPTED:
                return FriendshipTabType.ACCEPTED;
            case FriendshipTabType.PENDING:
                return FriendshipTabType.PENDING;
            default:
                return FriendshipTabType.BLOCKED;
        }
    }
}
