package fr.com.panda.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by edouardroussillon on 12/30/16.
 */

public class SocketType {
    public static final String CLOSE = "phx_close";
    public static final String ERROR = "phx_error";
    public static final String JOIN = "phx_join";
    public static final String REPLY = "phx_reply";
    public static final String LEAVE = "phx_leave";
    public static final String HEARTBEAT = "heartbeat";
    public static final String NEWS_MSG = "news:msg";
    public static final String NEWS_MSG_CONFIRMATION = "news:msg:confirmation";
    public static final String NEWS_USERS_CONNECT = "news:users:connect";
    public static final String NEWS_USER_CONNECT = "news:user:connect";
    public static final String NEWS_USER_DISCONNECT = "news:user:disconnect";
    public static final String NEWS_USER_FRIENDSHIP = "news:user:friendship";
    public static final String NEWS_USER_DUPLICATE = "news:user:duplicate";
    public static final String NEWS_USER_REPORTED = "news:user:reported";
    public static final String NEWS_USER_PENDING = "news:msg:pending";
    public static final String NEWS_MSG_ERROR = "news:msg:error";



    @StringDef({SocketType.CLOSE,
            SocketType.ERROR,
            SocketType.JOIN,
            SocketType.REPLY,
            SocketType.LEAVE,
            SocketType.HEARTBEAT,
            SocketType.NEWS_MSG,
            SocketType.NEWS_MSG_CONFIRMATION,
            SocketType.NEWS_USERS_CONNECT,
            SocketType.NEWS_USER_CONNECT,
            SocketType.NEWS_USER_DISCONNECT,
            SocketType.NEWS_USER_FRIENDSHIP,
            SocketType.NEWS_USER_DUPLICATE,
            SocketType.NEWS_USER_REPORTED,
            SocketType.NEWS_USER_PENDING,
            SocketType.NEWS_MSG_ERROR})
    @Retention(RetentionPolicy.SOURCE)
    public @interface SocketMode {}

    @SocketMode
    public static String fromId(String id) {
        switch (id) {
            case SocketType.CLOSE:
                return SocketType.CLOSE;
            case SocketType.JOIN:
                return SocketType.JOIN;
            case SocketType.REPLY:
                return SocketType.REPLY;
            case SocketType.LEAVE:
                return SocketType.LEAVE;
            case SocketType.HEARTBEAT:
                return SocketType.HEARTBEAT;
            case SocketType.NEWS_MSG:
                return SocketType.NEWS_MSG;
            case SocketType.NEWS_MSG_CONFIRMATION:
                return SocketType.NEWS_MSG_CONFIRMATION;
            case SocketType.NEWS_USERS_CONNECT:
                return SocketType.NEWS_USERS_CONNECT;
            case SocketType.NEWS_USER_CONNECT:
                return SocketType.NEWS_USER_CONNECT;
            case SocketType.NEWS_USER_DISCONNECT:
                return SocketType.NEWS_USER_DISCONNECT;
            case SocketType.NEWS_USER_FRIENDSHIP:
                return SocketType.NEWS_USER_FRIENDSHIP;
            case SocketType.NEWS_USER_DUPLICATE:
                return SocketType.NEWS_USER_DUPLICATE;
            case SocketType.NEWS_USER_REPORTED:
                return SocketType.NEWS_USER_REPORTED;
            case SocketType.NEWS_USER_PENDING:
                return SocketType.NEWS_USER_PENDING;
            case SocketType.NEWS_MSG_ERROR:
                return SocketType.NEWS_MSG_ERROR;
            default:
                return SocketType.ERROR;
        }
    }


}
