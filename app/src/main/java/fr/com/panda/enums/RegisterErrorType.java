package fr.com.panda.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Edouard Roussillon
 */

public class RegisterErrorType {
    public static final String ERROR_PSEUDO = "pseudo";
    public static final String ERROR_EMAIL = "email";
    public static final String ERROR_BIRTHDAY = "birthday";
    public static final String ERROR_PASSWORD = "password";

    @StringDef({RegisterErrorType.ERROR_PSEUDO, RegisterErrorType.ERROR_EMAIL, RegisterErrorType.ERROR_BIRTHDAY, RegisterErrorType.ERROR_PASSWORD})
    @Retention(RetentionPolicy.SOURCE)
    public @interface RegisterErrorMode {}

    @RegisterErrorMode
    public static String fromId(String id) {
        switch (id) {
            case RegisterErrorType.ERROR_PSEUDO:
                return RegisterErrorType.ERROR_PSEUDO;
            case RegisterErrorType.ERROR_EMAIL:
                return RegisterErrorType.ERROR_EMAIL;
            case RegisterErrorType.ERROR_BIRTHDAY:
                return RegisterErrorType.ERROR_BIRTHDAY;
            default:
                return RegisterErrorType.ERROR_PASSWORD;
        }
    }
}
