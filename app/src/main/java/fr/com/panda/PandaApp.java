package fr.com.panda;

import android.app.Application;
import android.content.Context;

import com.orhanobut.hawk.Hawk;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.sql.Time;
import java.util.HashMap;

import fr.com.panda.enums.SocketType;
import fr.com.panda.manager.BackupManager;
import fr.com.panda.manager.ChatManager;
import fr.com.panda.manager.GPSManager;
import fr.com.panda.manager.SessionManager;
import fr.com.panda.manager.SocketManager;
import fr.com.panda.model.ErrorResponse;
import fr.com.panda.model.FindRoomsModel;
import fr.com.panda.socket.phoenix.PhoenixMessage;
import fr.com.panda.socket.phoenix.PhoenixSocket;
import fr.com.panda.utils.Constants;
import fr.com.panda.utils.PrngFixes;
import timber.log.Timber;

/**
 * Created by edouardroussillon on 12/29/16.
 */

public class PandaApp extends Application {

    private static Context mContext;
    private SocketManager socketManager;
    private SessionManager sessionManager;
    private BackupManager backupManager;
    private ChatManager chatManager;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        PrngFixes.apply();

        if (BuildConfig.DEBUG) Timber.plant(new Timber.DebugTree());

        Hawk.init(this).build();
        FlowManager.init(new FlowConfig.Builder(this)
                .openDatabasesOnInit(true)
                .build());

//        socketManager = SocketManager.sharedInstance;
//        backupManager = BackupManager.sharedInstance;
//        chatManager = ChatManager.sharedInstance;
//
//        backupManager.onBackupSucceed.add(this::backupSuccess);
//        backupManager.onBackupFailed.add(this::errorResponse);
//
//        chatManager.onFindPublicRoomSucceed.add(this::findPublicRoomSucceed);
//
//        sessionManager = SessionManager.sharedInstance;
//        sessionManager.onGetSessionSucceed.add(this::isLogin);
//        sessionManager.onGetSessionFailed.add(this::errorResponse);
//        sessionManager.login("dovi@gmail.com", "123456");


//        socketManager.startSocket();
    }

    public static Context getContext() {
        return mContext;
    }

    public void isLogin(Void v) {
        backupManager.backup();
    }

    public void backupSuccess(Void v) {
        chatManager.findPublicRoom();

    }

    public void errorResponse(ErrorResponse v) {
        Timber.e("login error");
    }

    public void findPublicRoomSucceed(FindRoomsModel findRoomsModel) {
//        socketManager.joinGeneralChannel(this.chatManager.getCurrentRoomId(), this.chatManager.getFindRooms().token);
//        socketManager.joinSaleChannel(this.chatManager.getCurrentRoomId(), this.chatManager.getFindRooms().token);
    }

}
