//import android.os.SystemClock
//import android.support.annotation.NonNull
//import android.support.test.InstrumentationRegistry
//import android.support.test.espresso.Espresso
//import android.support.test.espresso.action.ViewActions.click
//import android.support.test.espresso.assertion.ViewAssertions
//import android.support.test.espresso.assertion.ViewAssertions.matches
//import android.support.test.espresso.intent.Intents
//import android.support.test.espresso.matcher.ViewMatchers
//import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
//import android.text.TextUtils
//import android.view.View
//import br.com.carrefour.sdk.ApiHandler
//import br.com.carrefour.test.ControllerTestRule
//import br.com.concretesolutions.requestmatcher.RequestMatcherRule
//import com.google.gson.reflect.TypeToken
//import org.hamcrest.Matcher
//import org.hamcrest.Matchers.not
//
//fun fillField(id: Int, text: String, scroll: Boolean = true, pressActionButton: Boolean = false, keepKeyboardOpen: Boolean = false) {
//    displayed { id(id, scroll) }
//    withId(id).fill(text, pressActionButton, keepKeyboardOpen)
//}
//
//fun launch(rule: ControllerTestRule) {
//    Espresso.registerIdlingResources(rule)
//    rule.launch()
//    Intents.init()
//    doWait()
//    InstrumentationRegistry.getInstrumentation().waitForIdleSync()
//}
//
//fun doWait(time: Long = 200) {
//    SystemClock.sleep(time)
//}
//
//fun readAsset(@NonNull server: RequestMatcherRule, @NonNull asset: String): String = server.readFixture(asset)
//
//inline fun <reified T> fromJson(@NonNull server: RequestMatcherRule, @NonNull asset: String) = ApiHandler.BUILDER.create().fromJson<T>(readAsset(server, asset), object : TypeToken<T>() {}.type)
//
//fun clickView(func: ClickRobot.() -> Unit) = ClickRobot().apply { func() }
//fun displayed(func: DisplayedRobot.() -> Unit) = DisplayedRobot().apply { func() }
//fun notDisplayed(func: NotDisplayedRobot.() -> Unit) = NotDisplayedRobot().apply { func() }
//fun notExist(func: NotExistRobot.() -> Unit) = NotExistRobot().apply { func() }
//
//class ClickRobot {
//    fun id(viewId: Int, scroll: Boolean = true): ClickRobot {
//        withId(viewId).scroll(scroll).perform(click())
//        return this
//    }
//
//    fun text(textId: Int, text: String = "", scroll: Boolean = true): ClickRobot {
//        if (TextUtils.isEmpty(text)) withText(textId).scroll(scroll).perform(click())
//        else withText(text).scroll(scroll).perform(click())
//        return this
//    }
//
//    fun contentDescription(contentDescriptionId: Int, contentDescriptionText: String = "", scroll: Boolean = true): ClickRobot {
//        if (TextUtils.isEmpty(contentDescriptionText)) withContentDescription(contentDescriptionId).scroll(scroll).perform(click())
//        else withContentDescription(contentDescriptionText).scroll(scroll).perform(click())
//        return this
//    }
//
//    fun item(recyclerId: Int, position: Int): ClickRobot {
//        withId(recyclerId).clickItem(position)
//        return this
//    }
//}
//
//class DisplayedRobot : VisibilityRobot() {
//    fun id(id: Int, scroll: Boolean = true): DisplayedRobot =
//            super.id(id, true, scroll) as DisplayedRobot
//
//    fun text(textId: Int, text: String = "", scroll: Boolean = true): DisplayedRobot {
//        if (TextUtils.isEmpty(text)) return super.text(textId, true, scroll) as DisplayedRobot
//        return super.text(text, true, scroll) as DisplayedRobot
//    }
//
//    fun contentDescription(contentDescriptionId: Int, contentDescriptionText: String = "", scroll: Boolean = true): DisplayedRobot {
//        if (TextUtils.isEmpty(contentDescriptionText)) return super.contentDescription(contentDescriptionId, true, scroll) as DisplayedRobot
//        return super.contentDescription(contentDescriptionText, true, scroll) as DisplayedRobot
//    }
//
//}
//
//class NotDisplayedRobot : VisibilityRobot() {
//    fun id(viewId: Int, scroll: Boolean = true): NotDisplayedRobot =
//            super.id(viewId, false, scroll) as NotDisplayedRobot
//
//    fun text(textId: Int, text: String = "", scroll: Boolean = true): NotDisplayedRobot {
//        if (TextUtils.isEmpty(text)) return super.text(textId, false, scroll) as NotDisplayedRobot
//        return super.text(text, false, scroll) as NotDisplayedRobot
//    }
//
//    fun contentDescription(contentDescriptionId: Int, contentDescriptionText: String = "", scroll: Boolean = true): NotDisplayedRobot {
//        if (TextUtils.isEmpty(contentDescriptionText)) return super.contentDescription(contentDescriptionId, false, scroll) as NotDisplayedRobot
//        return super.contentDescription(contentDescriptionText, false, scroll) as NotDisplayedRobot
//    }
//}
//
//class NotExistRobot {
//    fun id(viewId: Int): NotExistRobot {
//        withId(viewId).check(ViewAssertions.doesNotExist())
//        return this
//    }
//
//    fun text(textId: Int, text: String = ""): NotExistRobot {
//        if (TextUtils.isEmpty(text)) withText(textId).check(ViewAssertions.doesNotExist())
//        else withText(text).check(ViewAssertions.doesNotExist())
//        return this
//    }
//
//    fun contentDescription(contentDescriptionId: Int, contentDescriptionText: String = ""): NotExistRobot {
//        if (TextUtils.isEmpty(contentDescriptionText)) withText(contentDescriptionId).check(ViewAssertions.doesNotExist())
//        else withText(contentDescriptionText).check(ViewAssertions.doesNotExist())
//        return this
//    }
//}
//
//open class VisibilityRobot {
//
//    fun id(viewId: Int, checkVisible: Boolean, scroll: Boolean): VisibilityRobot {
//        withId(viewId).scroll(scroll).check(matches(visibility(checkVisible)))
//        return this
//    }
//
//    fun text(textId: Int, checkVisible: Boolean, scroll: Boolean): VisibilityRobot {
//        withText(textId).scroll(scroll).check(matches(visibility(checkVisible)))
//        return this
//    }
//
//    fun text(text: String, checkVisible: Boolean, scroll: Boolean): VisibilityRobot {
//        withText(text).scroll(scroll).check(matches(visibility(checkVisible)))
//        return this
//    }
//
//    fun contentDescription(contentDescriptionId: Int, checkVisible: Boolean, scroll: Boolean): VisibilityRobot {
//        withContentDescription(contentDescriptionId)
//                .scroll(scroll)
//                .check(matches(visibility(checkVisible)))
//        return this
//    }
//
//    fun contentDescription(contentDescriptionText: String, checkVisible: Boolean, scroll: Boolean): VisibilityRobot {
//        withContentDescription(contentDescriptionText)
//                .scroll(scroll)
//                .check(matches(visibility(checkVisible)))
//        return this
//    }
//
//    private fun visibility(checkVisible: Boolean): Matcher<View> {
//        if (checkVisible) return isDisplayed()
//        return not(isDisplayed())
//    }
//}
