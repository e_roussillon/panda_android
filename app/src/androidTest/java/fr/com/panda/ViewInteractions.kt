import android.support.test.espresso.Espresso
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.v7.widget.RecyclerView

fun ViewInteraction.scroll(scroll: Boolean): ViewInteraction {
    if (scroll) return this.perform(ViewActions.scrollTo())
    return this
}

fun ViewInteraction.fill(text: String, pressActionButton: Boolean = false, keepKeyboardOpen: Boolean = false): ViewInteraction {
    if (pressActionButton) return perform(ViewActions.typeText(text), ViewActions.pressImeActionButton())
    if (keepKeyboardOpen) return perform(ViewActions.typeText(text))
    return perform(ViewActions.typeText(text), ViewActions.closeSoftKeyboard())
}

fun ViewInteraction.clickItem(position: Int): ViewInteraction {
    return this.perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(position, ViewActions.click()))
}

fun withId(viewId: Int): ViewInteraction {
    return Espresso.onView(ViewMatchers.withId(viewId))
}

fun withText(textId: Int): ViewInteraction {
    return Espresso.onView(ViewMatchers.withText(textId))
}

fun withText(text: String): ViewInteraction {
    return Espresso.onView(ViewMatchers.withText(text))
}

fun withContentDescription(contentDescriptionId: Int): ViewInteraction {
    return Espresso.onView(ViewMatchers.withContentDescription(contentDescriptionId))
}

fun withContentDescription(contentDescriptionText: String): ViewInteraction {
    return Espresso.onView(ViewMatchers.withContentDescription(contentDescriptionText))
}